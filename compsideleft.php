<div id="sideleft" class="compsideleftdiv">
	<!--
	<h1 id="trackingtitle">Tracking partners</h1>
	-->
	<ul id="comp_trackinglist">	
		
		<!--	
		<li><a onclick="comp_links('.games')">.Games</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://dotgames.info/spots/PdhKJ7LZoWNoAB2e?partner=adcolony&product_id=[PRODUCT_ID]&google_ad_id=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('42trck')">42trck</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://42trck.com/click?action=54306&pubid=ad_colony&click_id=[SERVE_TIME]&mac_address_sha1=[MAC_SHA1]&apple_ifa=[IDFA]&device_os_version=[OS_VERSION]&device_os=[PLATFORM]&device_model=[DEVICE_MODEL]&wifi_status=[NETWORK_TYPE]&device_countrycode=[COUNTRY_CODE]&sub_adgroup=[ZONE_TYPE]&source_app_id=[PRODUCT_ID]&google_advertising_id=[GOOGLE_AD_ID]&sub_site=[APP_ID]&source_site_id=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('aarki')">Aarki</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://ar.aarki.net/rd?src=26E44EEB0368E09AAA&ofr=828B74DC0CF4671BOU&advertising_id=[GOOGLE_AD_ID]&site_id=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('accesstrade')">Accesstrade</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://h.accesstrade.net/sp/cc?rk=0100jizf00f31d&option=949352161&add=[IDFA]</div></i></li>
		<li><a onclick="comp_links('adaction')">AdAction</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://tracking.adactioninteractive.com/aff_c?offer_id=4114&aff_id=2118&google_aid=[GOOGLE_AD_ID]&aff_sub5=[AD_CREATIVE_NAME]&aff_sub=[PRODUCT_ID]&source=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('adbrix')">Adbrix</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://ref.ad-brix.com/v1/referrallink?ak=710530923&ck=5697997&agreement_key=[IDFA]&cb_param1=[PRODUCT_ID]&cb_param2=[IDFA]</div></i></li>
		<li><a onclick="comp_links('adcrops')">Adcrops</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://t.adcrops.net/ad/p/r?_site=418&_article=23307&_image=9023&ssub1=[PRODUCT_ID]&ssub2=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('addict')">Addict Mobile</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://track.addict-mobile.net/2/track/1108414210?device_ip=[IP_ADDRESS]&idfa=[IDFA]&sub_publisher=[PUBLISHER_ID]&sub_site=[APP_ID]&creative=[RAW_AD_CREATIVE_ID]&country=[COUNTRY_CODE]</div></i></li>
		<li><a onclick="comp_links('adinnovation')">Adinnovation</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://api.adstapi.com/app/click?k=yqxove3T&gaid=[GOOGLE_AD_ID]&pubid=[APP_ID]&prod_id=[PRODUCT_ID]</div></i></li>
		<li><a onclick="comp_links('adsage')">Adsage</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://track.adsage.com/trc/ptrac/x.gif?ver=T01&slpg=9CRX71&trctype=1&orderid=191&ordertype=outside&channel=206&appid=1059976078&idfa=[IDFA]&clientip=[IP_ADDRESS]&from=client </div></i></li>
		<li><a onclick="comp_links('adstore')">Adstore</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://a1.adstore.jp/app/click?k=feqTwH&idfa=[IDFA]&pubid=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('adstrack')">Adstrack</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://mt.adstrck.com/aff_c?offer_id=566&aff_id=1076&aff_sub=[IDFA]</div></i></li>
		<li><a onclick="comp_links('adways')">Adways</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://tc.pgss.jp/aff_c?offer_id=1197&aff_id=1047&aff_sub=[PRODUCT_ID]&aff_sub2=[APP_ID]&source=[PUBLISHER_ID]&ios_ifa=[IDFA]</div></i></li>
		<li><a onclick="comp_links('adwaysnonin')">Adways Non-incent</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://tc.pgss.jp/aff_c?offer_id=1995&aff_id=1047&aff_sub=[PRODUCT_ID]&aff_sub2=[APP_ID]&source=[PUBLISHER_ID]&android_id=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('adzcore')">Adzcore</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://click.adzcore.com/1.0.6d41602d2d33138b978cf8373fcc5f58c?pt_gid=904260&idfa=[IDFA]&sha1_mac=[MAC_SHA1]&prod_id=[PRODUCT_ID]&direct=1</div></i></li>
		<li><a onclick="comp_links('affle')">Affle</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://click.affle.co/global/click.php?af_cid=1462868247&af_pid=23343&af_pageid=22950&product_id=[PRODUCT_ID]&af_advertising_id=[IDFA]&app_id=[APP_ID]&raw_advertising_id=[IDFA]&api_key=kWY5NexbS7U4aKWMFq7PbPxpwy26ykSj</div></i></li>
		<li><a onclick="comp_links('alibaba')">Alibaba</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://click.union.ucweb.com/index.php?service=RedirectService&pub=limj3@adcolony&offer_id=aliexpress.ios&uc_trans_1=436672029&uc_trans_2=[IDFA]&uc_trans_3=[IDFA]&subpub=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('altrooz')">Altrooz</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://tracking.altrooz.com/mobile/click?c=3736&s=69&cp=video&p=[APP_ID]&idfa=[IDFA]</div></i></li>
		<li><a onclick="comp_links('appcoach')">Appcoach</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://fc.appcoachs.com/tl?a=25&o=1566&s5=[PRODUCT_ID]&s3=[GOOGLE_AD_ID]&s4=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('appcounter')">Appcounter</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://ad.appcount.net/cl/?b_id=GxF26Lea&t_id=1&afad_param_1=[GOOGLE_AD_ID]&afad_param_2=[PRODUCT_ID]&afad_param_3=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('applift')">Applift</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://tracking.applift.com/aff_c?offer_id=13893&aff_id=6382&aff_sub=xxxtransidxxx&aff_sub2=xxxpubidxxx</div></i></li>
		<li><a onclick="comp_links('apptizer')">Apptizer</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://tr.apptizer.jp/ad/p/r?_site=1145&_article=3913&_link=1411649&_image=1411649&gaid=[GOOGLE_AD_ID] </div></i></li>
		<li><a onclick="comp_links('apsalar')">Apsalar</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://ad.apsalar.com/api/v1/ad?re=0&a=naturalmotion&i=com.naturalmotion.dawnoftitans&ca=AdColony_DOT_IOS&an=AdColony&p=iOS&pl=[AD_GROUP_NAME]&udid=[RAW_UDID]&idfa=[IDFA]&odin=[ODIN1]&udi1=[UDID]&api_key=d4636d565f59e217801b5721af716eac&product_id=[PRODUCT_ID]&s=[APP_ID]&h=7fbaad31c18baa3dd25d27b85fd1c84acd11f083</div></i></li>
		<li><a onclick="comp_links('aquto')">Aquto</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://app.kickbit.com/api/campaign/datarewards/identify?r=https%3A%2F%2Fapp.appsflyer.com%2Fcom.zeptolab.thieves.google%3Faf_prt%3DAquto%26pid%3Dadcolony_int%26c%3DATT_MoVE_Zepto_Android_Control%26advertising_id%3D[GOOGLE_AD_ID]%26app_id%3Dcom.zeptolab.thieves.google%26clickid%3D[MAC_SHA1]&debugId=ATT_MoVE_Zepto_Android_Control</div></i></li>
		<li><a onclick="comp_links('art')">ART</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://art.d2c.ne.jp/ad/p/r?_site=433&_app=406&_article=3214&_link=29130&_image=29301&sad=653542050&_deviceid=[IDFA] </div></i></li>
		<li><a onclick="comp_links('babeltime')">Babeltime</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://mapieoh.gamepanda.us/adstat/appclick/appclickrurl?s2s&transid=com.babeltime.fknsg.us.gp&source=Adcolony&os=android&idfa=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('blind')">Blind Ferret</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://tracking.blindferretmedia.com/aff_c?offer_id=16146&aff_id=3844&google_aid=[GOOGLE_AD_ID]&aff_sub5=com.bethsoft.falloutshelter&aff_sub4=com.bethsoft.falloutshelter&aff_sub2=[APP_ID]&source=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('bluetrack')">Bluetrack</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://clicks.bluetrackmedia.com/cclick.php?creative=248545&campaign=31369&affiliate=7605&sid=[IDFA]&sid3=[PRODUCT_ID]</div></i></li>
		<li><a onclick="comp_links('chance')">Chance</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://www.adsensor.org/track/537062/3B959E123A81326E2E4DF20DBC52BE20?campaignId=znhMLWJIX63g&os=iOS&s2s=1&product_id=997636530&idfa=[IDFA]</div></i></li>
		<li><a onclick="comp_links('clickky')">Clickky</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://cpactions.com/api/v1.0/clk/track/proxy?ad_id=2193255&site_id=12931</div></i></li>
		<li><a onclick="comp_links('dauup')">DauUp</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://tr.sensorclick.com/aff_c?offer_id=1848&aff_id=1070&aff_sub=[GOOGLE_AD_ID]&aff_sub5=[APP_ID]&unid=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('deepforest')">Deepforest Media</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://ad.dpclk.com/dfad/click?promotion_unit_id=aV2tNlPN&network_id=adcolony&mac_sha1=[MAC_SHA1]&open_udid=[OPEN_UDID]&odin=[ODIN1]&android_id_sha1=[SHA1_ANDROID_ID]&device_id_sha1=[SHA1_IMEI]&platform=[PLATFORM]&adc_version=[ADC_VERSION]&os_version=[OS_VERSION]&device_model=[DEVICE_MODEL]&network_type=[NETWORK_TYPE]&language=[LANGUAGE]&timestamp=[SERVE_TIME]&dma_code=[DMA_CODE]&country_code=[COUNTRY_CODE]&zone_type=[ZONE_TYPE]&pub_id=[PUBLISHER_ID]&buy_id=XE3cxI12&google_aid=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('domob')">Domob</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://c.dsp.domob.cn/chn/clk?chnid=10003&cid=12622&idfa=[IDFA]&extinfo=997636530&u=https%3A%2F%2Fitunes.apple.com%2Fcn%2Fapp%2Fheng-sao-qian-jun-tang-guo%2Fid997636530%3Fl%3Dzh</div></i></li>
		<li><a onclick="comp_links('fiksu')">Fiksu</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://handler.fiksu.com/click?adid=779dd9d0-745a-0134-4508-22000b27a828&site_id=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('firebase')">Firebase</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://app-measurement.com/redirect?gmpaid=1:1051489540849:ios:40a019052404dd58&anid=adcolony&idfa=%5BIDFA%5D&cm=video&cn=%5BAD_CAMPAIGN_NAME%5D&cs=AdColony&url=https%3A%2F%2Fitunes.apple.com%2Fus%2Fapp%2Fid1105834924</div></i></li>
		<li><a onclick="comp_links('flurry')">Flurry</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://ad.apps.fm/p40PAWVODYNiKrDDfYB4p_E7og6fuV2oOMeOQdRqrE1ojLKTQI29FN4Vbd2LmB3OfUIfpf9ZrDKSrawWTWLvcNr40zlxO_eaNuvNMdsBXhIcJ00KE2Pk4h0K0aJ1EXWX?raw_advertising_id=[IDFA]&product_id=932493382</div></i></li>
		<li><a onclick="comp_links('fox')">CyberZ/FOX</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://app-adforce.jp/ad/p/r?_site=40734&_article=250523&_link=6313842&_image=5688561&suid=[PRODUCT_ID]&_spl=[APP_ID]&adid=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('fun')">Fun games for free</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://a.topaz-analytics.com/api/v2/ads/click/battletanksbeta?source=adcolony&campaign=[RAW_AD_CAMPAIGN_ID]&medium=video&adid=[GOOGLE_AD_ID]&source_app=[APP_ID]&country=[COUNTRY_CODE]&group_id=[RAW_AD_GROUP_ID]&creative_id=[RAW_AD_CREATIVE_ID]&platform=play</div></i></li>
		<li><a onclick="comp_links('gaea')">Gaea</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://ad.gaeamobile.net/?r=adcolony&auth=34kAiHWz&idfa=[IDFA]&prod_id=[PRODUCT_ID]&gc=[APP_ID]&click_id=[CLICK_ID]&device_ip=[IP_ADDRESS]</div></i></li>
		<li><a onclick="comp_links('gameloft')">Gameloft</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://extads.gameloft.com/adcolony/click.php?gcampaign_id=2786&gproduct_id=1694&gos=ios&idfa=[IDFA]&country_code=[COUNTRY_CODE]&ip_address=[IP_ADDRESS]&app_id=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('glispa')">Glispa</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://ads.glispa.com/sw/229620/CD43268/[SERVE_TIME]&placement=[APP_ID]&subid5=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('glispa2')">Glispa2</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://trk.glispa.com/pc/dHkK9xgXH9Blx2y_REuUwLaf2pbEMqvbMp-7BtITVBc/CF?m.idfa=[IDFA]&subid1=[CLICK_ID]&placement=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('growmobile')">Growmobile</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://click.growmobile.com/b/?a=414&c=4799&ts=40&as=7256&GF=0&tsd_idfa=[IDFA]&tsd_cr=[AD_CREATIVE_NAME]&tsd_pubid=[APP_ID]&tsd_tsm=[SERVE_TIME] </div></i></li>
		<li><a onclick="comp_links('happy')">Happy Elements</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://adcall.happyelements.com/call?ukey=langyabang_ioscn_prod&adver=adcolony&srcid=359&product_id=939493542&idfa=[IDFA]</div></i></li>
		<li><a onclick="comp_links('hasoffers')">Hasoffers</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://launch1.co/serve?action=click&publisher_id=184123&site_id=108642&agency_id=184&my_ad=test&ios_ifa=[IDFA]</div></i></li>
		<li><a onclick="comp_links('headway')">Headway Digital</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://go4.mobrain.xyz/8e9b063?p=[APP_ID]&sid=[CLICK_ID]&android_a_id=[GOOGLE_AD_ID]&idfa=[IDFA]&app_id=[PRODUCT_ID]&param1=[IDFA]&param2=[GOOGLE_AD_ID]&param3=[PRODUCT_ID]</div></i></li>
		<li><a onclick="comp_links('hunt')">Hunt Mobile</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://interactions.huntmad.com/interactions/?hm_campaign=34688&hm_creative=46628&hm_advertiser_id=2179&hm_token=669151455&hm_cid=xxxtransidxxx&hm_custom5=xxxgoogleadidxxx&hm_custom4=xxxidfaxxx&hm_sid=xxxpubidxxx&hm_datetime=xxxtimestampxxx&hm_format=REDIRECT&hm_type=click&hm_fid=81&hm_sourcename=Offer+Manager+OR</div></i></li>
		<li><a onclick="comp_links('hypergrowth')">Hypergrowth</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://app.appsflyer.com/id1108305861?pid=hypergrowthhq_int&af_click_lookback=7d&c=hg-ios-au-adc&af_siteid=57a2f6c75b3776.36847671&clickid=[MAC_SHA1]&idfa=[IDFA]&app_id=[PRODUCT_ID]</div></i></li>
		<li><a onclick="comp_links('icon')">Icon peak</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://ads.iconpeak.com/aff_c?offer_id=10780&aff_id=3101&aff_sub=xxxtransidxxx&aff_sub2=xxxpubidxxx</div></i></li>
		<li><a onclick="comp_links('idreamsky')">idreamsky</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://spm.mobgi.com/track/common?acid=4953&idfa=[IDFA]&af_siteid=[APP_ID]&af_sub1=[AD_CREATIVE_NAME]</div></i></li>
		<li><a onclick="comp_links('inmobi')">Inmobi</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://wadogo.go2cloud.org/aff_c?offer_id=8399&aff_id=3333&aff_sub3=[PRODUCT_ID]&google_aid=[GOOGLE_AD_ID]&ios_ifa=[IDFA]</div></i></li>
		<li><a onclick="comp_links('ironsource')">Ironsource</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://ironsource.go2cloud.org/aff_c?offer_id=10116&aff_id=1461&aff_sub=xxxtransidxxx&aff_sub4=xxxpubidxxx_xxxxsubidxxx&device_id=xxxgoogleadidxxx</div></i></li>
		<li><a onclick="comp_links('jampp')">Jampp</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://tracking.jampp.com/click?pub=357&campaign=4743&action=33253&pubid=ad_colony&click_id=[SERVE_TIME]&apple_ifa=[IDFA]&device_os_version=[OS_VERSION]&device_os=[PLATFORM]&device_model=[DEVICE_MODEL]&wifi_status=[NETWORK_TYPE]&device_countrycode=[COUNTRY_CODE]&sub_adgroup=[ZONE_TYPE]&source_app_id=com.contextlogic.wish&google_advertising_id=[GOOGLE_AD_ID]&sub_site=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('lenzmx')">Lenzmx/MobVista</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://tracking.lenzmx.com/async_click?mb_pl=ios&mb_nt=cb460&mb_campid=wy_yys_cn_ios&product_id=[PRODUCT_ID]&mb_idfa=[IDFA]&mb_auth=QJ8jRShfltJW5nU0&mb_ip=[IP_ADDRESS]&mb_subid=[PUBLISHER_ID]&mb_os=[OS_VERSION]&mb_device=[DEVICE_MODEL]&mb_language=[LANGUAGE]</div></i></li>
		<li><a onclick="comp_links('lionmobi')">Lionmobi</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://play.google.com/store/apps/details?id=com.lionmobi.powerclean&referrer=channel%3Dadcolony%26post_back_id%3D[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('localytics')">Localytics</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://a.localytics.com/redirect/fsy80yvatb05dsd0q1to?partner=ad_colony&idfa=[IDFA]&bundle=376510438</div></i></li>
		<li><a onclick="comp_links('longtu')">Longtu</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://p.longtugame.com/apimob/colonyAd?idfa=[IDFA]&product_id=[PRODUCT_ID]&domain=sks&click_id=[CLICK_ID]&ad_id=[APP_ID]&union_id=11034</div></i></li>
		<li><a onclick="comp_links('mail.ru')">Mail.ru</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://1l-s2s.mail.ru/s2s/adid/2197207_1/sipn/source/pid/101320/pof/1/f/3/?_1lpb_id=1642&idfa=[IDFA]&_1larg_sub=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('massive')">Massiveimpact</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://tracking.affilimob.com/aff_c?ms=205&offer_id=6856&aff_id=1&ad=460096&prod_id=1004862413&idfa=[IDFA]&pubid=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('master')">Master of Times</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://trace.mediamob.cn/tracking/t?sourceCode=45&rep=63&stId=42&idfa=[IDFA]&canalId=[APP_ID]&appId=[PRODUCT_ID]&location=[COUNTRY_CODE]&ip=[IP_ADDRESS]&clickid=[CLICK_ID]</div></i></li>
		<li><a onclick="comp_links('mdotm')">MdotM</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://ads.mdotm.com/ads/c.php?uc=63348&c=21b9dc42&utm_source=adcolony&odin=[ODIN1]&aid=[IDFA]&aid_type=raw</div></i></li>
		<li><a onclick="comp_links('mediamob')">Mediamob</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://trace.mediamob.cn/tracking/t?sourceCode=45&rep=62&stId=41&idfa=[IDFA]&canalId=[APP_ID]&appId=[PRODUCT_ID]&location=[COUNTRY_CODE]&ip=[IP_ADDRESS]&clickid=[CLICK_ID]</div></i></li>
		<li><a onclick="comp_links('metaps')">Metaps</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://t.metaps.biz/v1/cpi/click?campaign_id=kojp-co-gu3-alchemist-f6-android559a81435013cbf3fc7f0d0b8a&network_id=223&adid=[GOOGLE_AD_ID]&appstore_id=[STORE_ID]&creative_name=[AD_CREATIVE_NAME]&creative_size=[RAW_AD_CREATIVE_ID]&device_hash_method=sha1&device_id=[SHA1_ANDROID_ID]&device_id_is_hashed=true&device_id_type=android_id&imei_sha1=[SHA1_IMEI]&mac_sha1=[MAC_SHA1]&odin=[ODIN1]&pbr=1&site_id=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('microfan')">Microfan</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://da.microfunplus.cn/mfp_bl/adColony/click?idfa=[IDFA]&platform=iOS&prodid=897447840&siteid=[APP_ID]&ip=[IP_ADDRESS]</div></i></li>
		<li><a onclick="comp_links('minimob')">Minimob</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://clicks.minimob.com/tracking/click?clickid=1&trafficsource=1373690092&offerid=24442908107855287&product_id=[PRODUCT_ID]&idfa=[IDFA]&gaid=[GOOGLE_AD_ID]&pub_subid=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('motive')">Motive</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://traktum.com/?a=65066&c=689307&s3=[PRODUCT_ID]&s4=[SHA1_ANDROID_ID]&s5=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('nswitch')">Nswitch</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://inter-nswitch.nasmob.com/click?nsw_camp_id=503&nsw_media_id=50&product_id=[PRODUCT_ID]&google_ad_id=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('oneway')">Oneway</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://tracking.oneway.mobi/tl?a=9&o=721&s1=[IDFA]&s4=1166856103&s3=[CLICK_ID]</div></i></li>
		<li><a onclick="comp_links('opera')">Opera</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://www.mediamob.in/?a=21k82&b=ktiv6tm</div></i></li>
		<li><a onclick="comp_links('oupeng')">Oupeng</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://t.adbxb.cn/clk/adcolony?product_id=[PRODUCT_ID]&idfa=[IDFA]&gaid=[GOOGLE_AD_ID]&clk_id=[CLICK_ID]&app_id=[APP_ID]&publisher_id=[PUBLISHER_ID]&campaign_id=[RAW_AD_CAMPAIGN_ID]&campaign_name=[AD_CAMPAIGN_NAME]&ad_group_id=[RAW_AD_GROUP_ID]&ad_group_name=[AD_GROUP_NAME]&creative_id=[RAW_AD_CREATIVE_ID]&creative_name=[AD_CREATIVE_NAME]&os=[PLATFORM]&os_version=[OS_VERSION]&model=[DEVICE_MODEL]&country=[COUNTRY_CODE]&ip=[IP_ADDRESS]&language=[LANGUAGE]</div></i></li>
		<li><a onclick="comp_links('plarium')">Plarium</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://mt.x-plarium.com/?type=click&campaignId=55858&deviceId=[IDFA]&appId=blXCA&publisher=[AD_CREATIVE_NAME]&placement=[PUBLISHER_ID]&attributionId=[CLICK_ID]&partner=adcolony&ip=[IP_ADDRESS]&useragent=[USER_AGENT_MOZILLA]&noredirect=1</div></i></li>
		<li><a onclick="comp_links('plunware')">Plunware</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://tapit.go2cloud.org/aff_c?offer_id=8705&aff_id=1105&aff_sub=[APP_ID]&aff_sub4=[GOOGLE_AD_ID]&aff_sub2=com.ubercab</div></i></li>
		<li><a onclick="comp_links('pmad')">Pmad</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://pmad-mbad.com/api/click?creative_id=419&partner_clickid=[PRODUCT_ID]&publisher_id=[PUBLISHER_ID]&idfa=[IDFA]</div></i></li>
		<li><a onclick="comp_links('seads')">Seads</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://tc.pgss.jp/aff_c?offer_id=1995&aff_id=1047&aff_sub=[PRODUCT_ID]&aff_sub2=[APP_ID]&source=[PUBLISHER_ID]&android_id=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('simpysam')">Simpysam</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://app.simpysam.com/?id=1065845844&pid=adcolony&c=WA_Global&clickid=[MAC_SHA1]&idfa=[IDFA]&app_id=1065845844</div></i></li>
		<li><a onclick="comp_links('smaad')">Smaad</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://app.adjust.com/13yv03?s2s=1&android_id_lower_sha1=[SHA1_ANDROID_ID]&gps_adid=[GOOGLE_AD_ID]&install_callback=https%3A%2F%2Fcpa.adcolony.com%2Fon_user_action%3Fapi_key%3D7d442b42abea4549de732321531487fe%26product_id%3D[PRODUCT_ID]%26raw_android_id%3D%7Bandroid_id%7D%26google_ad_id%3D%7Bgps_adid%7D&adgroup=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('smartc')">Smart-C</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://smart-c.jp/c?i=4Dk0N50npej4016ry&guid=ON&u=&gaid=[GOOGLE_AD_ID]&product_id=[PRODUCT_ID]</div></i></li>
		<li><a onclick="comp_links('spoint')">Social Point</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://socialpoint15.go2cloud.org/aff_c?offer_id=109&aff_id=1009&aff_sub=[PRODUCT_ID]&aff_sub2=6dBaBBwSSzJNRDZ8EN5CCPrmnWKTHYFu&aff_sub3=[APP_ID]&aff_sub4=[IDFA]</div></i></li>
		<li><a onclick="comp_links('talking')">Talking data</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://lnk0.com/ckIJNh?idfa=[IDFA]&product_id=[PRODUCT_ID]</div></i></li>
		<li><a onclick="comp_links('tapsense')">TapSense</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://c.tapsense.com/c/track/adcolony/521c0e41e4b0661979161a7c?cid=ae677a394a&creative_name=TestAds_320x480_1&idfa=[IDFA]&odin=[ODIN1]&sha1_imei=[SHA1_IMEI]&sha1_android_id=[SHA1_ANDROID_ID]&ext_click_id={ext_click_id}&app_id=[PUBLISHER_ID]</div></i></li>
		<li><a onclick="comp_links('tenjin')">Tenjin</a><i class="em em-grey_question tooltip"><div class="tooltiptext">https://track.tenjin.io/v0/click/hnhX9r2XlQhbATvAJGzZwx?advertising_id=[IDFA]&site_id=[APP_ID]</div></i></li>
		<li><a onclick="comp_links('thrive')">Thrive</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://loadinghere.com/path/lp.php?trvid=10591&trvx=8fc872b4&idfa=[IDFA]&prodid=[PRODUCT_ID]</div></i></li>
		<li><a onclick="comp_links('urad')">Urad.com</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://adtracker.urad.com.tw/appsflyer?campaign=720&redirect=aHR0cDovL2FwcC5hcHBzZmx5ZXIuY29tL2NvbS5haWNvbWJvLm9sOVlpbj9waWQ9dXJhZF9pbnQmYz1waWMwMSZjbGlja2lkPXtjbGlja2lkfSZhZl9zaXRlaWQ9e2FuaWR9JmFmX3N1YjE9e2FjaWR9&ad_network=5a623a15d7aad95453fad9854da503d5&adcolony_appid=[PRODUCT_ID]&advertising_id=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('webmedia')">Webmedia</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://wmadv.go2cloud.org/aff_c?offer_id=2494&aff_id=1716&aff_sub={google_ad_id}&source=[PUBLISHER_ID]&android_id=[GOOGLE_AD_ID]&ios_ifa=[IDFA]</div></i></li>
		<li><a onclick="comp_links('wink')">The Wink Mobile</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://track.twkmobile.com/aff_c?offer_id=880&aff_id=1480&aff_sub=sg.com.theedgeproperty.app&aff_sub2=[GOOGLE_AD_ID]</div></i></li>
		<li><a onclick="comp_links('wisetracker')">Wisetracker</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://cdn.wisetracker.co.kr/wa/wiseAdt.do?_wtno=704&_wtpkg=_N_&_wtufn=ALL&_wtus=N&_wthst=trk.hnsmall.wisetracker.co.kr&_wts=P1472096140379&_wtc=C1478521418236&_wtm=AT0003&_wtw=DA&_wtdl=https%3A%2F%2Fitunes.apple.com%2Fkr%2Fapp%2Fid687619460&_wtp=1&_wtckp=1440&_wtland=https%3A%2F%2Fitunes.apple.com%2Fkr%2Fapp%2Fid687619460&_wtcntr=&_wtadpr=&_wtlngt=&_wtal=hnsmallapp%3A%2F%2Fm.hnsmall.com%2Fevent%2Fview%2F201501%2Fa1%3Fchannel_code%3D20552%26utm_source%3Dadcolony%26utm_medium%3Dbanner%26utm_content%3DTVC%26utm_campaign%3Dvideo_1610&_wtcid=[CLICK_ID]&_wtgpid=[GOOGLE_AD_ID]&_wtidfa=[IDFA]&_wtpst=adcolony</div></i></li>
		<li><a onclick="comp_links('wooga')">Wooga</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://woogatrack.com/track?network=adcolony&campaign=[AD_GROUP_NAME]&adgroup=_[APP_ID]_[COUNTRY_CODE]&idfa=[IDFA]&app_id=pli&ad=_[DEVICE_MODEL]_[AD_CREATIVE_NAME]</div></i></li>
		<li><a onclick="comp_links('youappi')">Youappi</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://track.56txs4.com/app/directlink?accesstoken=9d8c2ede-aae6-4d8d-94bf-58d75fd86d17&appid=39691&campaignpartnerid=97220&subid=[PRODUCT_ID]&publishertoken=[APP_ID]&publishername=&usertoken=&deviceAndroidId=[GOOGLE_AD_ID]&deviceIfa=[IDFA]</div></i></li>
		<li><a onclick="comp_links('youmi2')">Youmi2</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://af.api.youmi.net/ios/v1/recv?s=571997dc5b4wwwUm222mA9LdZjyytJqxqxi&ifa=[IDFA]&ip=[IP_ADDRESS]&clickid=[CLICK_ID]</div></i></li>
		<li><a onclick="comp_links('zadsmobile')">Zadsmobile</a><i class="em em-grey_question tooltip"><div class="tooltiptext">http://zadsmobilellc.go2cloud.org/aff_c?offer_id=13&aff_id=5&aff_sub=993999961&aff_sub2=[IDFA]</div></i></li>
		-->
	
		<li><a onclick="comp_links('.games')">.Games</a></li>
		<li><a onclick="comp_links('aarki')">Aarki</a></li>
		<li><a onclick="comp_links('accesstrade')">Accesstrade</a></li>
		<li><a onclick="comp_links('adaction')">AdAction</a></li>
		<li><a onclick="comp_links('adbrix')">Adbrix</a></li>
		<li><a onclick="comp_links('adcrops')">Adcrops</a></li>
		<li><a onclick="comp_links('addict')">Addict Mobile</a></li>
		<li><a onclick="comp_links('adinnovation')">Adinnovation</a></li>
		<li><a onclick="comp_links('adsage')">Adsage</a></li>
		<li><a onclick="comp_links('adstore')">Adstore</a></li>
		<li><a onclick="comp_links('adstrack')">Adstrack</a></li>
		<li><a onclick="comp_links('adways')">Adways</a></li>
		<li><a onclick="comp_links('adwaysnonin')">Adways Non-incent</a></li>
		<li><a onclick="comp_links('adzcore')">Adzcore</a></li>
		<li><a onclick="comp_links('affle')">Affle</a></li>
		<li><a onclick="comp_links('alibaba')">Alibaba</a></li>
		<li><a onclick="comp_links('altrooz')">Altrooz</a></li>
		<li><a onclick="comp_links('appcoach')">Appcoach</a></li>
		<li><a onclick="comp_links('appcounter')">Appcounter</a></li>
		<li><a onclick="comp_links('applift')">Applift</a></li>
		<li><a onclick="comp_links('apptizer')">Apptizer</a></li>
		<li><a onclick="comp_links('apsalar')">Apsalar</a></li>
		<li><a onclick="comp_links('aquto')">Aquto</a></li>
		<li><a onclick="comp_links('art')">ART</a></li>
		<li><a onclick="comp_links('babeltime')">Babeltime</a></li>
		<li><a onclick="comp_links('blind')">Blind Ferret</a></li>
		<li><a onclick="comp_links('bluetrack')">Bluetrack</a></li>
		<li><a onclick="comp_links('chance')">Chance</a></li>
		<li><a onclick="comp_links('clickky')">Clickky</a></li>
		<li><a onclick="comp_links('dauup')">DauUp</a></li>
		<li><a onclick="comp_links('deepforest')">Deepforest Media</a></li>
		<li><a onclick="comp_links('domob')">Domob</a></li>
		<li><a onclick="comp_links('fiksu')">Fiksu</a></li>
		<li><a onclick="comp_links('firebase')">Firebase</a></li>
		<li><a onclick="comp_links('flurry')">Flurry</a></li>
		<li><a onclick="comp_links('fox')">CyberZ/FOX</a></li>
		<li><a onclick="comp_links('fun')">Fun games for free</a></li>
		<li><a onclick="comp_links('gaea')">Gaea</a></li>
		<li><a onclick="comp_links('gameloft')">Gameloft</a></li>
		<li><a onclick="comp_links('glispa')">Glispa</a></li>
		<li><a onclick="comp_links('glispa2')">Glispa2</a></li>
		<li><a onclick="comp_links('growmobile')">Growmobile</a></li>
		<li><a onclick="comp_links('happy')">Happy Elements</a></li>
		<li><a onclick="comp_links('hasoffers')">Hasoffers</a></li>
		<li><a onclick="comp_links('headway')">Headway Digital</a></li>
		<li><a onclick="comp_links('hunt')">Hunt Mobile</a></li>
		<li><a onclick="comp_links('hypergrowth')">Hypergrowth</a></li>
		<li><a onclick="comp_links('icon')">Icon peak</a></li>
		<li><a onclick="comp_links('idreamsky')">idreamsky</a></li>
		<li><a onclick="comp_links('inmobi')">Inmobi</a></li>
		<li><a onclick="comp_links('ironsource')">Ironsource</a></li>
		<li><a onclick="comp_links('jampp')">Jampp</a></li>
		<li><a onclick="comp_links('lenzmx')">Lenzmx/MobVista</a></li>
		<li><a onclick="comp_links('lionmobi')">Lionmobi</a></li>
		<li><a onclick="comp_links('localytics')">Localytics</a></li>
		<li><a onclick="comp_links('longtu')">Longtu</a></li>
		<li><a onclick="comp_links('mail.ru')">Mail.ru</a></li>
		<li><a onclick="comp_links('massive')">Massiveimpact</a></li>
		<li><a onclick="comp_links('master')">Master of Times</a></li>
		<li><a onclick="comp_links('mdotm')">MdotM</a></li>
		<li><a onclick="comp_links('mediamob')">Mediamob</a></li>
		<li><a onclick="comp_links('metaps')">Metaps</a></li>
		<li><a onclick="comp_links('microfan')">Microfan</a></li>
		<li><a onclick="comp_links('minimob')">Minimob</a></li>
		<li><a onclick="comp_links('motive')">Motive</a></li>
		<li><a onclick="comp_links('nswitch')">Nswitch</a></li>
		<li><a onclick="comp_links('oneway')">Oneway</a></li>
		<li><a onclick="comp_links('opera')">Opera</a></li>
		<li><a onclick="comp_links('oupeng')">Oupeng</a></li>
		<li><a onclick="comp_links('plarium')">Plarium</a></li>
		<li><a onclick="comp_links('plunware')">Plunware</a></li>
		<li><a onclick="comp_links('pmad')">Pmad</a></li>
		<li><a onclick="comp_links('seads')">Seads</a></li>
		<li><a onclick="comp_links('simpysam')">Simpysam</a></li>
		<li><a onclick="comp_links('singular')">Singular</a></li>
		<li><a onclick="comp_links('smaad')">Smaad</a></li>
		<li><a onclick="comp_links('smartc')">Smart-C</a></li>
		<li><a onclick="comp_links('spoint')">Social Point</a></li>
		<li><a onclick="comp_links('talking')">Talking data</a></li>
		<li><a onclick="comp_links('tapsense')">TapSense</a></li>
		<li><a onclick="comp_links('tenjin')">Tenjin</a></li>
		<li><a onclick="comp_links('thrive')">Thrive</a></li>
		<li><a onclick="comp_links('urad')">Urad.com</a></li>
		<li><a onclick="comp_links('webmedia')">Webmedia</a></li>
		<li><a onclick="comp_links('wink')">The Wink Mobile</a></li>
		<li><a onclick="comp_links('wisetracker')">Wisetracker</a></li>
		<li><a onclick="comp_links('wooga')">Wooga</a></li>
		<li><a onclick="comp_links('youappi')">Youappi</a></li>
		<li><a onclick="comp_links('youmi2')">Youmi2</a></li>
		<li><a onclick="comp_links('zadsmobile')">Zadsmobile</a></li>
	
	
	</ul>
</div>