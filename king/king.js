Validateking = function() {
	king_url = document.getElementById("king_url").value;
	output_tags(king_url);
	redirectURL(king_url, '#redurldiv');
	$("#redurldiv").show();
};

output_tags = function(string) {

	var is_tappid = (string.indexOf("targetAppId") > -1);

	if (is_tappid) {
		
		// get the base URL
		
		var base1 = string.split("?");
		var base2 = base1[0];
		console.log(base2);
		
		
		// get the targetAppId

		var substr1 = string.split("targetAppId=");
		var substr2 = substr1[1].split("&");
		var tapid = substr2[0];
		var output = "The targetAppId is: " + tapid + "<br /><br />";

		// click common part

		var click_common = base2 + "?type=video&os=[PLATFORM]&countryCode=[COUNTRY_CODE]&publisher=[PUBLISHER_ID]";
		click_common += "&site=[APP_ID]&campaignName=[RAW_AD_CAMPAIGN_ID]&creativeName=[AD_CREATIVE_NAME]&creativeSize=&noRedirect=TRUE";
		click_common += "&bypassFingerprint=true&product_id=[PRODUCT_ID]&appId=[STORE_ID]&targetAppId=";
		click_common += tapid;

		// click and and ios

		var click_and = "&androidId_sha1=[SHA1_ANDROID_ID]&googleAdId_raw=[GOOGLE_AD_ID]&network=adcolony_android&google_ad_id=[GOOGLE_AD_ID]&sha1_android_id=[SHA1_ANDROID_ID]";
		var click_ios = "&idfa_raw=[IDFA]&network=adcolony&raw_advertising_id=[IDFA]";

		var out_click = "";
		var out_imp = "";

		// platorm check

		var idfa = "[IDFA]";
		var gaid = "[GOOGLE_AD_ID]";

		var is_idfa = (string.indexOf(idfa) > -1);
		var is_gaid = (string.indexOf(gaid) > -1);

		if (is_idfa) {
			out_click = click_common + click_ios;
			$("#vnotes_alert").html("");
		}
		if (is_gaid) {
			out_click = click_common + click_and;
			$("#vnotes_alert").html("");
		}

		out_imp = out_click.replace("click?type", "impression?type");

		if (!is_idfa && !is_gaid) {
			$("#vnotes_alert").html("Missing [IDFA] and/or [GOOGLE_AD_ID].<br /><br />");
		}

		$("#vnotes_comment").html(output);

		// king_imp0 for the output string

		var king_imp0 = "Let's use these URLs:\n\n";
		king_imp0 += "HTML5:\n" + out_click + "\n\n";
		king_imp0 += "Video starts:\n" + out_imp;
		$("#king_imp").css("height", "250px");
		document.getElementById("king_imp").value = king_imp0;

	} else {
		$("#vnotes_alert").html("Missing targetAppId in the URL!<br /><br />");
		$("#vnotes_comment").html("");
		document.getElementById("king_imp").value = "";
	}

}

$(document).ready(function() {
	divToggle(false, notestitle, notesdiv, notesdown, notesup);
	divToggle(true, valtitle, valdiv, valdown, valup);
	divToggle(true, imptitle, impdiv, impdown, impup);
	divToggle(true, pbtitle, pbdiv, pbdown, pbup);
	divToggle(false, vttitle, vtdiv, vtdown, vtup);
	divToggle(false, pietitle, piediv, piedown, pieup);
});
