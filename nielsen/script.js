// global variable changed depending on whether the url needs to be hard coded for ios or android
var hardCoded = "regular";

// hard coded drd button pressed
function hardCodedDrd () {
	window.hardCoded = "android";
	fixUrl();
}

// hard coded ios button pressed
function hardCodedIos () {
	window.hardCoded = "ios";
	fixUrl();
}

// find the protocol of the url and return the type
function findProtocol (url) {
	var protocol = "";
	if (url.includes("safari")) {
		protocol = "ios";
	} else if (url.includes("browser")) {
		protocol = "android";
	} else {
		protocol = "regular";
	}
	return protocol;
}

//main function
function fixUrl() {

	$('#button').hide();

	$('#parameters').empty();
	$('#complete-url').hide();
	$('#parameters-text').hide();
	$('#complete-url-text').hide();
	$('#error-message').empty();
	$('#protocol-message').empty();

	// grab url from textbox
	var url = document.getElementById("textbox").value;

	// before we start manipulating the url, we save it in a variable to compare with the new url later
	var oldUrl = getUrl(url);

	// this variable will be contain the url after manipulation
	var completeUrl = "";

	if (url.split("/")[2] !== "secure-gl.imrworldwide.com") {
		document.getElementById("error-message").innerHTML = "<p class='text-danger'>This is not a secure-gl.imrworldwide.com URL so is not built to work with this tool. Please <a href='mailto:max.elston@adcolony.com?Subject=URL%20converter%20error:%20Nielsen%20tool%20issue' target='_top'>contact support</a> for more info.</p>";
		window.hardCoded = "regular";
		return;
	}

	if (window.hardCoded == "ios") {
		url = url.split("image")[0] + "image" + "&c9=devid,{TARGET_IDFA}&c13=asid,P44504205-3604-4D0D-ABAB-9189D3795790&uoo={TARGET_AD_TRACKING_ENABLED}&r={timestamp}";
	} else if (window.hardCoded = "android") {
		url = url.split("image")[0] + "image" + "&c9=devid,{TARGET_ANDROID_ADVERTISING_ID}&c13=asid,P44504205-3604-4D0D-ABAB-9189D3795790&uoo={TARGET_ANDROID_ADVERTISING_ID_OPT_OUT}&r={timestamp}";
	}

	$('#complete-url').show();

	document.getElementById("complete-url").innerHTML = url;

	$('#complete-url-text').show();

	window.hardCoded = "regular";	
}

function getUrl(url) {
	// this splits the URL into the base URL and an array of the paramters names and values
	var parameters = url.split('?');
	var baseUrl = parameters[0];
	parameters = parameters[1].split('&');
	return loopThroughParameters(parameters, baseUrl);
}

function loopThroughParameters(parameters, baseUrl) {
	var parametersLength = parameters.length;

	// create a new array to populate with the split parameter values
	var splitParameters = [];

	// loop through the array splitting apart the variables and the values
	for (var i = 0; i < parametersLength; i++) {
	    var variableSplit = parameters[i].split('=');
	    splitParameters.push({name: variableSplit[0],value: variableSplit[1]});
	}

	// create an object to return the base url and paramters of a link
	var newUrl = {
		baseUrl: baseUrl,
		parameters: splitParameters
	}

	return newUrl;
}