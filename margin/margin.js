gross = "";

function round(value, exp) {
	if ( typeof exp === 'undefined' || +exp === 0)
		return Math.round(value);

	value = +value;
	exp = +exp;

	if (isNaN(value) || !( typeof exp === 'number' && exp % 1 === 0))
		return NaN;

	// Shift
	value = value.toString().split('e');
	value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

	// Shift back
	value = value.toString().split('e');
	return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}

function discr_calc() {

	unit_odc = $("#unit_odc").val();
	discr_odc = $("#discr_odc").val();
	discr_odc_per = discr_odc / 100;

	// overdelivery

	overd_odc = discr_odc_per / (1 - discr_odc_per) * 100;
	overd_odc1 = round(overd_odc, 4);
	$("#overd_odc").val(overd_odc1);

	// total amount

	total_odc = unit_odc / (1 - discr_odc_per);
	$("#total_odc").val(total_odc);

}

function calculate() {

	gross = $("#gross").val();

	ag_disc = $("#ag_disc").val();
	ag_disc_per = ag_disc / 100;

	ag_trad = $("#ag_trad").val();
	ag_trad_per = ag_trad / 100;
	
	//ac_margin = $("#ac_margin").val();
	//ac_margin_per = ac_margin / 100;

	unit_am = $("#unit_am").val();
	
	cpm = $("#cpm_dol").val();
	console.log(cpm);
	
	

	discr = $("#discr").val();
	discr_per = discr / 100;

	// CALCULATIONS
	
	// CPCV DOL
	
	cpcv_dol = cpm / 1000;
	cpcv_dol1 = round(cpcv_dol, 4);
	$("#cpcv_dol").val(cpcv_dol1);

	// Agency discount value

	ag_disc_val = gross * ag_disc_per;
	ag_disc_val1 = round(ag_disc_val,4);
	$("#ag_disc_val").val(ag_disc_val1);

	// Net of agency

	net_of_ag = gross * (1 - ag_disc_per);
	net_of_ag1 = round(net_of_ag, 4);
	$("#net_of_ag").val(net_of_ag1);

	// Agency trading

	ag_trad_val = net_of_ag * ag_trad_per;
	ag_trad_val1 = round(ag_trad_val, 4);
	$("#ag_trad_val").val(ag_trad_val1);
	
	
	
	
	

	// Total amount

	total_am = unit_am / (1 - discr_per);
	$("#total_am").val(total_am);

	// Overdelivery needed

	over_del = discr_per / (1 - discr_per) * 100;
	over_del1 = round(over_del, 4);
	$("#over_del").val(over_del1);

	

	getRate("GBP", "USD");

}

function barter() {

	is_non_barter = $("#non-barter").is(':checked');
	$("#ag_trad").val(30);

	if (is_non_barter) {
		console.log("non-barter");
		$("#ag_disc").val(15);
	} else {
		console.log("barter");
		$("#ag_disc").val(27.75);
	}
	
	if (gross !== "") {
		calculate();
	}
}

function getRate(from, to) {
	var script = document.createElement('script');
	script.setAttribute('src', "https://query.yahooapis.com/v1/public/yql?q=select%20rate%2Cname%20from%20csv%20where%20url%3D'http%3A%2F%2Fdownload.finance.yahoo.com%2Fd%2Fquotes%3Fs%3D" + from + to + "%253DX%26f%3Dl1n'%20and%20columns%3D'rate%2Cname'&format=json&callback=parseExchangeRate");
	document.body.appendChild(script);
}

function parseExchangeRate(data) {
	var name0 = data;
	console.log(name0);
	var name1 = data.query;
	console.log(name1);
	var name2 = data.query.results;
	console.log(name2);
	var name = data.query.results.row.name;
	var rate = parseFloat(data.query.results.row.rate, 10);
	var text1 = "Exchange rate " + name + " is " + rate;
	//console.log(text1);
	rate1 = rate;

	$("#exch_disp").html(rate);

	try {

		if (!isNaN(cpm)) {
			
			// CPCV (£)

			cpcv_pound = cpcv_dol / rate;
			cpcv_pound1 = round(cpcv_pound, 6);
			$("#cpcv_pound").val(cpcv_pound1);
			
			// CPM (£)

			cpm_pound = cpcv_pound * 1000;
			cpm_pound1 = round(cpm_pound, 6);
			$("#cpm_pound").val(cpm_pound1);

			
			
			// COGS

	cogs = unit_am * cpcv_pound;
	cogs1 = round(cogs);
	$("#cogs").val(cogs1);

	// Margin

	margin_val = net_of_ag - cogs - ag_trad_val;
	margin_val1 = round(margin_val,2);
	$("#margin_val").val(margin_val1);
	
	// Margin (%)
	
	margin_per = margin_val / net_of_ag * 100;
	margin_per1 = round(margin_per, 4);
	$("#ac_margin").val(margin_per1);

		}
	} catch(err) {
	}
}

//getRate("GBP", "USD");

$(document).ready(function() {

	divToggle(false, notestitle, notesdiv, notesdown, notesup);
	divToggle(true, cpcvtitle, cpcvdiv, cpcvdown, cpcvup);
	//divToggle(false, formulastitle, formulasdiv, formulasdown, formulasup);
	//divToggle(true, overdtitle, overddiv, overddown, overdup);

	getRate("GBP", "USD");

});
