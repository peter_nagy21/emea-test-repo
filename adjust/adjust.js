function PostbackGen() {
	var adjust_url = document.getElementById("adjust_url").value;
	var api_key = "7d442b42abea4549de732321531487fe";
	var adj_campaign_token = document.getElementById("adj_campaign_token").value;
	var adj_ad_group_token = document.getElementById("adj_ad_group_token").value;
	var adj_creative_token = document.getElementById("adj_creative_token").value;
	var adj_other_token = document.getElementById("adj_other_token").value;
	var pie_open_token = document.getElementById("pie_open_token").value;
	var pie_transaction_token = document.getElementById("pie_transaction_token").value;
	var pie_credits_spent_token = document.getElementById("pie_credits_spent_token").value;
	var pie_payment_info_added_token = document.getElementById("pie_payment_info_added_token").value;
	var pie_achievement_unlocked_token = document.getElementById("pie_achievement_unlocked_token").value;
	var pie_level_achieved_token = document.getElementById("pie_level_achieved_token").value;
	var pie_app_rated_token = document.getElementById("pie_app_rated_token").value;
	var pie_activated_token = document.getElementById("pie_activated_token").value;
	var pie_tutorial_completed_token = document.getElementById("pie_tutorial_completed_token").value;
	var pie_social_sharing_event_token = document.getElementById("pie_social_sharing_event_token").value;
	var pie_registration_completed_token = document.getElementById("pie_registration_completed_token").value;
	var pie_session_token = document.getElementById("pie_session_token").value;
	var pie_add_to_cart_token = document.getElementById("pie_add_to_cart_token").value;
	var pie_add_to_wishlist_token = document.getElementById("pie_add_to_wishlist_token").value;
	var pie_checkout_initiated_token = document.getElementById("pie_checkout_initiated_token").value;
	var pie_content_view_token = document.getElementById("pie_content_view_token").value;
	var pie_install_token = document.getElementById("pie_install_token").value
	var pie_invite_token = document.getElementById("pie_invite_token").value;
	var pie_login_token = document.getElementById("pie_login_token").value;
	var pie_reservation_token = document.getElementById("pie_reservation_token").value;
	var pie_search_token = document.getElementById("pie_search_token").value;
	var pie_update_token = document.getElementById("pie_update_token").value;
	var pie_custom1_token = document.getElementById("pie_custom1_token").value;
	var pie_custom2_token = document.getElementById("pie_custom2_token").value;
	var pie_custom3_token = document.getElementById("pie_custom3_token").value;
	var pie_custom4_token = document.getElementById("pie_custom4_token").value;
	var pie_custom5_token = document.getElementById("pie_custom5_token").value;

	var adjust_base_url = adjust_url + "?s2s=1&idfa=[IDFA]&install_callback=https%3A%2F%2Fcpa.adcolony.com%2Fon_user_action%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]" + "%26raw_advertising_id%3D%7Bidfa%7D";
	if (document.getElementById("adjust_url").value == "") {
		adjust_base_url = adjust_url;
	} else {
	}
	if (document.getElementById("adj_campaign").checked) {
		adjust_base_url = adjust_base_url + "&campaign=" + adj_campaign_token;
	} else {
	}

	if (document.getElementById("adj_ad_group").checked) {
		adjust_base_url = adjust_base_url + "&adgroup=" + adj_ad_group_token;
	} else {
	}

	if (document.getElementById("adj_creative").checked) {
		adjust_base_url = adjust_base_url + "&creative=" + adj_creative_token;
	} else {
	}

	if (document.getElementById("adj_other").checked) {
		adjust_base_url = adjust_base_url + adj_other_token;
	} else {
	}

	if (document.getElementById("pie_open").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_open_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fopen%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26timezone%3D%7Btimezone%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_transaction").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_transaction_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Ftransaction%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D%26price%3D%7Brevenue%7D%26currency_code%3D%7Bcurrency%7D";
	} else {
	}

	if (document.getElementById("pie_credits_spent").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_credits_spent_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcredits_spent%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D%26currency_code%3D%7Bcurrency%7D";
	} else {
	}

	if (document.getElementById("pie_payment_info_added").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_payment_info_added_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fpayment_info_added%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_achievement_unlocked").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_achievement_unlocked_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fachievement_unlocked%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_level_achieved").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_level_achieved_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Flevel_achieved%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_app_rated").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_app_rated_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fapp_rated%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_activated").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_activated_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Factivated%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_tutorial_completed").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_tutorial_completed_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Ftutorial_completed%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_social_sharing_event").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_social_sharing_event_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fsocial_sharing_event%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_registration_completed").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_registration_completed_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fregistration_completed%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_session").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_session_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fsession%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D%26session_length%3D%7Btime_spent%7D";
	} else {
	}

	if (document.getElementById("pie_add_to_cart").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_add_to_cart_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fadd_to_cart%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_add_to_wishlist").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_add_to_wishlist_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fadd_to_wishlist%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_checkout_initiated").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_checkout_initiated_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcheckout_initiated%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_content_view").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_content_view_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcontent_view%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_install").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_install_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Finstall%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_invite").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_invite_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Finvite%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_login").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_login_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Flogin%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_reservation").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_reservation_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Freservation%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_search").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_search_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fsearch%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_update").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_update_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fupdate%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_custom1").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_custom1_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcustom_event%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26event%3DADCT_CUSTOM_EVENT_1%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_custom2").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_custom2_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcustom_event%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26event%3DADCT_CUSTOM_EVENT_2%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}
	if (document.getElementById("pie_custom3").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_custom3_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcustom_event%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26event%3DADCT_CUSTOM_EVENT_3%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}
	if (document.getElementById("pie_custom4").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_custom4_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcustom_event%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26event%3DADCT_CUSTOM_EVENT_4%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}
	if (document.getElementById("pie_custom5").checked) {
		adjust_base_url = adjust_base_url + "&event_callback_" + pie_custom5_token + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcustom_event%3Fapi_key%3D" + api_key + "%26product_id%3D[PRODUCT_ID]%26event%3DADCT_CUSTOM_EVENT_5%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}
	
	// add fingerprint parameters
	
	var fp = "&ip_address=[IP_ADDRESS]&adcolony_click_id=[CLICK_ID]&cost_id=[CLICK_ID]";
	
	adjust_base_url += fp;

	// remove spaces warning

	var space_rem = false;

	adjust_base_url_nospace = adjust_base_url.replace(/\s+/g, '');

	if (adjust_base_url !== adjust_base_url_nospace) {
		adjust_base_url = adjust_base_url_nospace;
		space_rem = true;
		console.log("Spaces have been removed!");
	}

	var adjust_base_url_imp = adjust_base_url.replace("app.adjust.com/", "view.adjust.com/impression/");
	adjust_base_url_imp = adjust_base_url_imp.replace("io.adjust.com/", "view.adjust.com/impression/");
	adjust_base_url_imp = adjust_base_url_imp.replace("s2s=1&", "");
	

	var sevday = document.getElementById("iosvt7").checked;
	var oneday = document.getElementById("iosvt1").checked;
	var noday = document.getElementById("iosvt0").checked;

	ios_validateVT = function() {

		if (!oneday && !sevday && !noday) {
			return false;
		} else {
			return true;
		}
	}
	var vt_ok = ios_validateVT();
	console.log(vt_ok);

	// remove spaces warning

	var space_rem = false;

	adjust_base_url_nospace = adjust_base_url.replace(/\s+/g, '');

	if (adjust_base_url !== adjust_base_url_nospace) {
		adjust_base_url = adjust_base_url_nospace;
		space_rem = true;
		console.log("Spaces have been removed!");
	}

	var final_alert = "";
	var vt_not_ok = "Select view-through attribution window!<br /><br />";
	var spaces = "<span style='color: #777;'>Spaces have been removed!</span><br /><br />";

	console.log(vt_ok);

	if (vt_ok) {
		if (space_rem) {
			final_alert = spaces;
		}
		redirectURL(adjust_base_url, '#redurldiv2');
	} else {
		final_alert = vt_not_ok;
	}

	$('#adj_alert_ios').html(final_alert);

	// output

	if (noday) {
		var ki1 = "HTML5:<br />" + adjust_base_url + "<br /><br />Thank you<br /><br />";
		$('#adj_out_ios').show();
		$("#adj_html5_ios").hide();
		$("#adj_complete_ios").html(ki1);
	}

	if (sevday) {
		var ki1 = "Video starts: <br />" + adjust_base_url + "<br /><br />Thank you<br /><br />";
		$('#adj_out_ios').show();
		$("#adj_html5_ios").hide();
		$("#adj_complete_ios").html(ki1);
	}

	if (oneday) {
		var ki2 = "HTML5: <br/>" + adjust_base_url + "<br /><br />";
		var ki3 = "Video starts: <br />" + adjust_base_url_imp + "<br /><br />Thank you<br /><br />";
		$('#adj_out_ios').show();
		$("#adj_html5_ios").show();
		$("#adj_html5_ios").html(ki2);
		$("#adj_complete_ios").html(ki3);
	}

	// End of output
}

function PostbackGenAnd() {
	var adjust_url_a = document.getElementById("adjust_url_a").value;
	var api_key_a = "7d442b42abea4549de732321531487fe";
	var adj_campaign_token_a = document.getElementById("adj_campaign_token_a").value;
	var adj_ad_group_token_a = document.getElementById("adj_ad_group_token_a").value;
	var adj_creative_token_a = document.getElementById("adj_creative_token_a").value;
	var adj_other_token_a = document.getElementById("adj_other_token_a").value;
	var pie_open_token_a = document.getElementById("pie_open_token_a").value;
	var pie_transaction_token_a = document.getElementById("pie_transaction_token_a").value;
	var pie_credits_spent_token_a = document.getElementById("pie_credits_spent_token_a").value;
	var pie_payment_info_added_token_a = document.getElementById("pie_payment_info_added_token_a").value;
	var pie_achievement_unlocked_token_a = document.getElementById("pie_achievement_unlocked_token_a").value;
	var pie_level_achieved_token_a = document.getElementById("pie_level_achieved_token_a").value;
	var pie_app_rated_token_a = document.getElementById("pie_app_rated_token_a").value;
	var pie_activated_token_a = document.getElementById("pie_activated_token_a").value;
	var pie_tutorial_completed_token_a = document.getElementById("pie_tutorial_completed_token_a").value;
	var pie_social_sharing_event_token_a = document.getElementById("pie_social_sharing_event_token_a").value;
	var pie_registration_completed_token_a = document.getElementById("pie_registration_completed_token_a").value;
	var pie_session_token_a = document.getElementById("pie_session_token_a").value;
	var pie_add_to_cart_token_a = document.getElementById("pie_add_to_cart_token_a").value;
	var pie_add_to_wishlist_token_a = document.getElementById("pie_add_to_wishlist_token_a").value;
	var pie_checkout_initiated_token_a = document.getElementById("pie_checkout_initiated_token_a").value;
	var pie_content_view_token_a = document.getElementById("pie_content_view_token_a").value;
	var pie_install_token_a = document.getElementById("pie_install_token_a").value
	var pie_invite_token_a = document.getElementById("pie_invite_token_a").value;
	var pie_login_token_a = document.getElementById("pie_login_token_a").value;
	var pie_reservation_token_a = document.getElementById("pie_reservation_token_a").value;
	var pie_search_token_a = document.getElementById("pie_search_token_a").value;
	var pie_update_token_a = document.getElementById("pie_update_token_a").value;
	var pie_custom1_token_a = document.getElementById("pie_custom1_token_a").value;
	var pie_custom2_token_a = document.getElementById("pie_custom2_token_a").value;
	var pie_custom3_token_a = document.getElementById("pie_custom3_token_a").value;
	var pie_custom4_token_a = document.getElementById("pie_custom4_token_a").value;
	var pie_custom5_token_a = document.getElementById("pie_custom5_token_a").value;

	var adjust_base_url_a = adjust_url_a + "?s2s=1&android_id_lower_sha1=[SHA1_ANDROID_ID]&gps_adid=[GOOGLE_AD_ID]&install_callback=https%3A%2F%2Fcpa.adcolony.com%2Fon_user_action%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26raw_android_id%3D%7Bandroid_id%7D%26google_ad_id%3D%7Bgps_adid%7D";

	if (document.getElementById("adjust_url_a").value == "") {
		adjust_base_url_a = adjust_url_a;
	} else {
	}

	if (document.getElementById("adj_campaign_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&campaign=" + adj_campaign_token_a;
	} else {
	}

	if (document.getElementById("adj_ad_group_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&adgroup=" + adj_ad_group_token_a;
	} else {
	}

	if (document.getElementById("adj_creative_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&creative=" + adj_creative_token_a;
	} else {
	}
	if (document.getElementById("adj_other_a").checked) {
		adjust_base_url_a = adjust_base_url_a + adj_other_token_a;
	} else {
	}

	if (document.getElementById("pie_open_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_open_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fopen%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_transaction_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_transaction_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Ftransaction%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D%26price%3D%7Brevenue%7D%26currency_code%3D%7Bcurrency%7D";
	} else {
	}

	if (document.getElementById("pie_credits_spent_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_credits_spent_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcredits_spent%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D%26currency_code%3D%7Bcurrency%7D";
	} else {
	}

	if (document.getElementById("pie_payment_info_added_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_payment_info_added_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fpayment_info_added%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_achievement_unlocked_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_achievement_unlocked_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fachievement_unlocked%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_level_achieved_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_level_achieved_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Flevel_achieved%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_app_rated_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_app_rated_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fapp_rated%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_activated_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_activated_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Factivated%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_tutorial_completed_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_tutorial_completed_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Ftutorial_completed%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_social_sharing_event_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_social_sharing_event_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fsocial_sharing_event%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_registration_completed_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_registration_completed_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fregistration_completed%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_session_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_session_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fsession%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D%26session_length%3D%7Btime_spent%7D";
	} else {
	}

	if (document.getElementById("pie_add_to_cart_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_add_to_cart_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fadd_to_cart%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_add_to_wishlist_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_add_to_wishlist_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fadd_to_wishlist%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_checkout_initiated_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_checkout_initiated_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcheckout_initiated%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_content_view_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_content_view_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcontent_view%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_install_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_install_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Finstall%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_invite_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_invite_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Finvite%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_login_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_login_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Flogin%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_reservation_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_reservation_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Freservation%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_search_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_search_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fsearch%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_update_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_update_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fupdate%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}

	if (document.getElementById("pie_custom1_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_custom1_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcustom_event%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26event%3DADCT_CUSTOM_EVENT_1%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}
	if (document.getElementById("pie_custom2_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_custom2_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcustom_event%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26event%3DADCT_CUSTOM_EVENT_2%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}
	if (document.getElementById("pie_custom3_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_custom3_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcustom_event%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26event%3DADCT_CUSTOM_EVENT_3%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}
	if (document.getElementById("pie_custom4_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_custom4_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcustom_event%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26event%3DADCT_CUSTOM_EVENT_4%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}
	if (document.getElementById("pie_custom5_a").checked) {
		adjust_base_url_a = adjust_base_url_a + "&event_callback_" + pie_custom5_token_a + "=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fcustom_event%3Fapi_key%3D" + api_key_a + "%26product_id%3D[PRODUCT_ID]%26event%3DADCT_CUSTOM_EVENT_5%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D";
	} else {
	}
	
	// add fingerprint parameters
	
	var fp = "&ip_address=[IP_ADDRESS]&adcolony_click_id=[CLICK_ID]&cost_id=[CLICK_ID]";
	
	adjust_base_url_a += fp;

	// remove spaces warning

	var space_rem = false;

	adjust_base_url_a_nospace = adjust_base_url_a.replace(/\s+/g, '');

	if (adjust_base_url_a !== adjust_base_url_a_nospace) {
		adjust_base_url_a = adjust_base_url_a_nospace;
		space_rem = true;
		console.log("Spaces have been removed!");
	}

	var adjust_base_url_a_imp = adjust_base_url_a.replace("app.adjust.com/", "view.adjust.com/impression/");
	adjust_base_url_a_imp = adjust_base_url_a_imp.replace("io.adjust.com/", "view.adjust.com/impression/");
	adjust_base_url_a_imp = adjust_base_url_a_imp.replace("s2s=1&", "");

	var sevday = document.getElementById("andvt7").checked;
	var oneday = document.getElementById("andvt1").checked;
	var noday = document.getElementById("andvt0").checked;

	and_validateVT = function() {

		if (!oneday && !sevday && !noday) {
			return false;
		} else {
			return true;
		}
	}
	var vt_ok = and_validateVT();

	var final_alert = "";
	var vt_not_ok = "Select view-through attribution window!<br /><br />";
	var spaces = "<span style='color: #777;'>Spaces have been removed!</span><br /><br />";

	if (vt_ok) {
		if (space_rem) {
			final_alert = spaces;
		}
		redirectURL(adjust_base_url_a, '#redurldiv1');
	} else {
		final_alert = vt_not_ok;
	}

	$('#adj_alert').html(final_alert);

	// output

	if (noday) {
		var ki1 = "HTML5: <br />" + adjust_base_url_a + "<br /><br />Thank you<br /><br />";
		$('#adj_out').show();
		$("#adj_html5").hide();
		$("#adj_complete").html(ki1);
	}

	if (sevday) {
		var ki1 = "Video starts: <br />" + adjust_base_url_a + "<br /><br />Thank you<br /><br />";
		$('#adj_out').show();
		$("#adj_html5").hide();
		$("#adj_complete").html(ki1);
	}

	if (oneday) {
		var ki2 = "HTML5: <br/>" + adjust_base_url_a + "<br /><br />";
		var ki3 = "Video starts: <br />" + adjust_base_url_a_imp + "<br /><br />Thank you<br /><br />";
		$('#adj_out').show();
		$("#adj_html5").show();
		$("#adj_html5").html(ki2);
		$("#adj_complete").html(ki3);
	}

	// End of output

}

chooseios = function() {
	$("#adjust_ios_gen").css("display", "block");
	$("#adjust_os_switch").css("display", "none");
	$("#adjparams").css("display", "block");
}
chooseandroid = function() {
	$("#adjust_android_gen").css("display", "block");
	$("#adjust_os_switch").css("display", "none");
	$("#adjparams").css("display", "block");
}
iosback = function() {
	$("#adjust_ios_gen").css("display", "none");
	$("#adjust_os_switch").css("display", "block");
	$("#adjparams").css("display", "none");
}
andback = function() {
	$("#adjust_android_gen").css("display", "none");
	$("#adjust_os_switch").css("display", "block");
	$("#adjparams").css("display", "none");
}

$(document).ready(function() {
	$("#ios_pie_toggle").click(function() {
		$("#adjust_ios_pie").toggleClass("adjust_visible");
	});
	$("#and_pie_toggle").click(function() {
		$("#adjust_android_pie").toggleClass("adjust_visible");
	});
});

iosappid = function() {
	var boxchecked = $("#adj_ad_group").attr("checked");
	if (boxchecked) {
		$("#adj_ad_group_token").val("");
		$("#adj_ad_group").attr("checked", false);
	} else {
		$("#adj_ad_group_token").val("[APP_ID]");
		$("#adj_ad_group").attr("checked", true);
	}
}
ioscampname = function() {
	var boxchecked = $("#adj_campaign").attr("checked");
	if (boxchecked) {
		$("#adj_campaign_token").val("");
		$("#adj_campaign").attr("checked", false);
	} else {
		$("#adj_campaign_token").val("[AD_GROUP_NAME]");
		$("#adj_campaign").attr("checked", true);
	}
}

iosadcreativename = function() {
	var boxchecked = $("#adj_creative").attr("checked");
	if (boxchecked) {
		$("#adj_creative_token").val("");
		$("#adj_creative").attr("checked", false);
	} else {
		$("#adj_creative_token").val("[AD_CREATIVE_NAME]");
		$("#adj_creative").attr("checked", true);
	}
}
andappid = function() {
	var boxchecked = $("#adj_ad_group_a").attr("checked");
	if (boxchecked) {
		$("#adj_ad_group_token_a").val("");
		$("#adj_ad_group_a").attr("checked", false);
	} else {
		$("#adj_ad_group_token_a").val("[APP_ID]");
		$("#adj_ad_group_a").attr("checked", true);
	}
}
andcampname = function() {
	var boxchecked = $("#adj_campaign_a").attr("checked");
	if (boxchecked) {
		$("#adj_campaign_token_a").val("");
		$("#adj_campaign_a").attr("checked", false);
	} else {
		$("#adj_campaign_token_a").val("[AD_GROUP_NAME]");
		$("#adj_campaign_a").attr("checked", true);
	}
}
andadcreativename = function() {
	var boxchecked = $("#adj_creative_a").attr("checked");
	if (boxchecked) {
		$("#adj_creative_token_a").val("");
		$("#adj_creative_a").attr("checked", false);
	} else {
		$("#adj_creative_token_a").val("[AD_CREATIVE_NAME]");
		$("#adj_creative_a").attr("checked", true);
	}
}

AdjVT = function() {
	var vt1 = document.getElementById("adjvtr1").checked;
	var vt7 = document.getElementById("adjvtr7").checked;
	
	if (vt1 || vt7) {
			$('#adjvt_2').show();
			$("#adjvt_mess").hide();
	var link = $("#adjvt_1").val();
	var vtlink = "";
	var is_adj_com = (link.indexOf("app.adjust.com") > -1);
	var is_adj_io = (link.indexOf("app.adjust.io") > -1);
	
	if (!is_adj_com && !is_adj_io) {
		$("#adjvt_mess").show();
		$("#adjvt_mess").html("The URL is a bit strange...Could you double check it?<br /><br />");
	}
	
	if (is_adj_com) {
		vtlink = link.replace("app.adjust.com","view.adjust.com/impression");
	}
	if (is_adj_io) {
		vtlink = link.replace("app.adjust.io","view.adjust.io/impression");
	}
	var vtlink1 = vtlink.replace("s2s=1&","");
	vtlink = vtlink1;
	
	var output = "";
	
	if (vt1) {
		output = "Let's use these URLs: <br /><br />HTML5:<br />" + link + "<br /><br />Video starts:<br />" + vtlink + "<br /><br />Thank you<br /><br />";
	}
	if (vt7) {
		output = "Let's use these URLs: <br /><br />Video starts:<br />" + link + "<br /><br />Thank you<br /><br />";
	}
	
	$("#adjvt_2").html(output);
		
	} else {
		$("#adjvt_mess").show();
		$("#adjvt_mess").html("Please select VT.<br /><br />");
		console.log("Please select VT.");
	}
}

$(document).ready(function() {

	$('#adj_out, #adjvt_mess').hide();
	
	$('#adj_out_ios').hide();
	$('#adjvt_2').hide();
	
	divToggle(true, vtltitle, vtldiv, vtldown, vtlup);
	divToggle(true, pie_session_title, pie_session_div, pie_session_down, pie_session_up);
	divToggle(false, notestitle, notesdiv, notesdown, notesup);
	divToggle(true, valtitle, valdiv, valdown, valup);
	divToggle(true, pbtitle, pbdiv, pbdown, pbup);
	divToggle(false, vttitle, vtdiv, vtdown, vtup);
	divToggle(false, pietitle, piediv, piedown, pieup);
});
