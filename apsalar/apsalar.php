<?php
include ("http://localhost:8888/password_protect.php");
?>
<?php
include '../header.php';
?>
<?php
include '../sideleft.php';
?>

<?php include_once("analyticstracking.php") ?>

<script type="text/javascript" src="apsalar.js"></script>

<div id="maincontent">

	<h1>Apsalar</h1>

	<h3 class="black plink" id="notestitle">1. Notes <i class="fa fa-arrow-down" id="notesdown"></i><i class="fa fa-arrow-up" id="notesup"></i></h3>
	<div id="notesdiv" class="ndiv">
		<ul>
			<li>
				Make sure <span class="black">an=Adcolony</span> (if an=Adcolony+%28Opera%29 then ask for a tag under the Adcolony integration)
			</li>
			<li>
				<span class="black">api_key=</span>d4636d565f59e217801b5721af716eac
			</li>
			<li>
				<span class="black">pl</span> is the campaign parameter, if requested to pass campaign info
			</li>
			<li>
				Set <span class="black">&product_id=</span> to the mobile app id / bundle id (goal ID), for iOS you may use the [PRODUCT_ID] macro if you’d like
			</li>
			<li>
				Click on the tag and make sure that it redirects to the correct app
			</li>
		</ul>
		<br />
		<h3 class="black">Example URLs:</h3>
		<span class="black">iOS Click:</span>
		<br />
		http://ad.apsalar.com/api/v1/ad?re=0&a=naturalmotion&i=com.naturalmotion.dawnoftitans&ca=AdColony_DOT_IOS&an=AdColony&p=iOS&pl=[AD_GROUP_NAME]&udid=[RAW_UDID]&idfa=[IDFA]&odin=[ODIN1]&udi1=[UDID]&api_key=d4636d565f59e217801b5721af716eac&product_id=[PRODUCT_ID]&s=[APP_ID]&h=7fbaad31c18baa3dd25d27b85fd1c84acd11f083
		<br />
		<br />
		<span class="black">Android Click (needs to be hardcoded):</span>
		<br />
		http://ad.apsalar.com/api/v1/ad?re=0&a=naturalmotion&i=com.naturalmotion.dawnoftitans&ca=AdColony_DOT_GP&an=AdColony&p=Android&pl=[AD_GROUP_NAME]&andi=[ANDROID_ID]&and1=[SHA1_ANDROID_ID]&aifa=[GOOGLE_AD_ID]&api_key=d4636d565f59e217801b5721af716eac&product_id=[PRODUCT_ID]&s=[APP_ID]&h=7fbaad31c18baa3dd25d27b85fd1c84acd11f083
		<br />
		<br /> 
		<span class="black">Impression:</span>
		<br />
		http://ad.apsalar.com/api/v1/ad?re=0&a=naturalmotion&i=com.naturalmotion.dawnoftitans&ca=AdColony_DOT_IOS&an=AdColony&p=iOS&pl=[AD_GROUP_NAME]&udid=[RAW_UDID]&idfa=[IDFA]&odin=[ODIN1]&udi1=[UDID]&api_key=d4636d565f59e217801b5721af716eac&product_id=[PRODUCT_ID]&s=[APP_ID]&h=7fbaad31c18baa3dd25d27b85fd1c84acd11f083&op=impression
		<br />
		<br />
	</div>

	<h3 id="valtitle" class="black plink">2. Tracking URL validation <i class="fa fa-arrow-down" id="valdown"></i><i class="fa fa-arrow-up" id="valup"></i></h3>
	<div id="valdiv">

		Paste the click URL below:
		<br />
		<form id="valform">
			<textarea name="apsalar_url" id="apsalar_url"></textarea>
			<br />
			<br />
			<input type="radio" name="vt" id="vt0" value="not enabled">
			No view-through
			<br>
			<input type="radio" name="vt" id="vt1" value="enabled">
			View-through is enabled
			<br />
			<br />
			<input type="button" class="btn-class" value="Validate" onclick="ValidateApsalar()">
			<br />
			<br />
		</form>
		<h3 class="black plink" id="urlparamstitle">Check URL parameters <i class="fa fa-arrow-down" id="urlpdown"></i><i class="fa fa-arrow-up" id="urlpup"></i></h3>
		<div id="urlsplit_div"></div>
		<div id="apsalarparams">
			<table id="apsalar_paramtable">
				<tr>
					<th class="firstcol">Parameter</th>
					<th class="secondcol">Macro/Value</th>
					<th class="thirdcol">Notes</th>
				</tr>
				<tr class="tep" id="aps1">
					<td>a</td>
					<td>specified by Apsalar</td>
					<td></td>
				</tr>
				<tr class="tep" id="aps2">
					<td>i</td>
					<td>specified by Apsalar</td>
					<td></td>
				</tr>
				<tr class="tep" id="aps3">
					<td>an</td>
					<td>AdColony</td>
					<td></td>
				</tr>
				<tr class="tep" id="aps4">
					<td>ca</td>
					<td>specified by Apsalar</td>
					<td>campaign that will appear in Apsalar</td>
				</tr>
				<tr class="tep" id="aps5">
					<td>p</td>
					<td>iOS / Android</td>
					<td></td>
				</tr>
				<tr class="tep" id="aps6">
					<td>idfa</td>
					<td>[IDFA]</td>
					<td>for iOS</td>
				</tr>
				<tr class="tep" id="aps7">
					<td>aifa</td>
					<td>[GOOGLE_AD_ID]</td>
					<td>for Android</td>
				</tr>
				<tr class="tep" id="aps8">
					<td>product_id</td>
					<td>[PRODUCT_ID]</td>
					<td>**value can also be the mobile app id. Must be hardcoded as the bundle id on Android.</td>
				</tr>
				<tr class="tep" id="aps9">
					<td>api_key</td>
					<td style="word-break: break-all;">d4636d565f59e217801b5721af716eac</td>
					<td>(Adcolony API key for Apsalar)</td>
				</tr>
				<tr class="tep" id="aps10">
					<td>h</td>
					<td>specified by Apsalar</td>
					<td></td>
				</tr>

			</table>
			<h3 id="apsalar_otherp_title" class="black plink">Commonly used values <i class="fa fa-arrow-down" id="apsalar_otherp_down"></i><i class="fa fa-arrow-up" id="apsalar_otherp_up"></i></h3>
			<div id="apsalar_otherparams">
				<table id="apsalar_otherparamtable">
					<tr>
						<th class="fourthcol">Add</th>
						<th class="firstcol">Parameter</th>
						<th class="secondcol">Macro/Value</th>
						<th class="thirdcol">Notes</th>
					</tr>
					<tr id="tr_aps11">
						<td></td>
						<td>re</td>
						<td>specified by Apsalar</td>
						<td></td>
					</tr>
					<tr id="tr_aps12">
						<td><input type="checkbox" name="param" id="ch_aps12" onclick="add_remove_params(this)"></td>
						<td>andi</td>
						<td>[ANDROID_ID]</td>
						<td></td>
					</tr>
					<tr id="tr_aps13">
						<td><input type="checkbox" name="param" id="ch_aps13" onclick="add_remove_params(this)"></td>
						<td>and1</td>
						<td>[SHA1_ANDROID_ID]</td>
						<td></td>
					</tr>
					<tr id="tr_aps14">
						<td><input type="checkbox" name="param" id="ch_aps14" onclick="add_remove_params(this)"></td>
						<td>udid</td>
						<td>[RAW_UDID]</td>
						<td></td>
					</tr>
					<tr id="tr_aps15">
						<td><input type="checkbox" name="param" id="ch_aps15" onclick="add_remove_params(this)"></td>
						<td>odin</td>
						<td>[ODIN1]</td>
						<td></td>
					</tr>
					<tr id="tr_aps16">
						<td><input type="checkbox" name="param" id="ch_aps16" onclick="add_remove_params(this)"></td>
						<td>udi1</td>
						<td>[UDID]</td>
						<td></td>
					</tr>
					<tr id="tr_aps17">
						<td><input type="checkbox" name="param" id="ch_aps17" onclick="add_remove_params(this)"></td>
						<td>s</td>
						<td>[APP_ID]</td>
						<td></td>
					</tr>
					<tr id="tr_aps18">
						<td></td>
						<td>pl</td>
						<td>specified by Apsalar</td>
						<td>ad group that will appear in Apsalar</td>
					</tr>
				</table>

				<br />
			</div>
		</div>
		<div id="vnotes">
			<div id="redurldiv"></div>
			<div id="vnotes_comment"></div>
			<div id="vnotes_alert"></div>
		</div>
		<div class="clear"></div>
	</div>

	<h3 class="black plink" id="pbtitle">3. Postback - check the 3rd party Dashboard <i class="fa fa-arrow-down" id="pbdown"></i><i class="fa fa-arrow-up" id="pbup"></i></h3>
	<div id="pbdiv" class="ndiv">

		<a class="blue" href="https://partners.apsalar.com" target="_blank">Apsalar Dashboard</a>
		<br />
		Login details can be found <a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ad-ops/3rd-party-dashboards" target="_blank">here</a>. (AdColony internal document.)
		<br />
		<br />
	</div>

	<h3 class="black plink" id="imptitle">4. Click / impression URLs <i class="fa fa-arrow-down" id="impdown"></i><i class="fa fa-arrow-up" id="impup"></i></h3>
	<div id="impdiv">
		<form>
			<fieldset id="apsalar_output">
				<textarea name="apsalar_imp" id="apsalar_imp"></textarea>
				<br />
			</fieldset>
		</form>
		<br />
	</div>

	<h3 class="black plink" id="vttitle">5. View Through Attribution Window <i class="fa fa-arrow-down" id="vtdown"></i><i class="fa fa-arrow-up" id="vtup"></i></h3>
	<div id="vtdiv">
		<table id="vt_table">
			<tr>
				<th colspan="2">VIEW ATTRIBUTION</th>
				<th colspan="3">LOOKBACK WINDOWS</th>
				<th style="min-width: 140px;">CLIENT-SIDE ACTION</th>
			</tr>
			<tr class="tep" id="tpid">
				<th>Dedicated View Tags</th>
				<th>Recommended Implementation</th>
				<th style="min-width: 120px;">Flexible Lookback Window</th>
				<th style="min-width: 90px;">Default Click Lookback</th>
				<th style="min-width: 90px;">Default View Lookback</th>
				<th></th>
			</tr>
			<tr>
				<td>No</td>
				<td class="lefta">Click tag (with &op=impression) goes on complete, regular click tag goes on HTML5 (hardcoded on Android)</td>
				<td>Yes</td>
				<td>7 days</td>
				<td>7 days</td>
				<td class="lefta">No action necessary. Can be enabled at App level.</td>
			</tr>
		</table>
		<br />
	</div>
	<h3 class="black plink" id="pietitle">6. PIE <i class="fa fa-arrow-down" id="piedown"></i><i class="fa fa-arrow-up" id="pieup"></i></h3>
	<div id="piediv">
		PIE events should be implemented by the client in apsalar dashboard.
		<br />
		More info about the client setup can be found here:
		<a class="blue" href="http://support.adcolony.com/customer/portal/articles/1914521-postbacks-for-top-tracking-partners#Apsalar" target="_blank">Apsalar PIE setup</a>
	</div>

</div>

<div class="clear"></div>

<?php
include '../footer.php';
?>

