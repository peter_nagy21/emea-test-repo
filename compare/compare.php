<?php
include ("http://localhost:8888/password_protect.php");
?>

<?php
include '../header.php';
?>

<?php
include '../compsideleft.php';
?>

<?php include_once("analyticstracking.php") ?>

<script type="text/javascript" src="comp_val.js"></script>

<div id="maincontent" class="comp_content">

	<h1>Compare URLs "validation" page</h1>

	<h3 id="trchtitle2" class="black plink">Tracking URL checker <i class="fa fa-arrow-down" id="trchdown2"></i><i class="fa fa-arrow-up" id="trchup2"></i></h3>

	<div id="trchdiv2">
		Paste the URL below:
		<br/ >
			<textarea name="tracking_checker" id="tracking_checker"></textarea>
			<br />
			<input type="button" class="btn-class" value="Check tracking provider" onclick="check_track('tracking_checker',tr_id_div)"/>
			<br />
			<br />
			<div id="urlsplitdiv3"></div>
			<div id="tr_id_div">
				&nbsp;
			</div>

			The full list of integrated tracking partners can be found <a class="blue" href="https://docs.google.com/spreadsheets/d/1ZUL3K6kqQYzqwDglnRirUPHJer1fxraDWSkgdha-dyg/edit?ts=569e97f2#gid=0" target="_blank">here</a> (TAM version).
			<br />
			<br />
	</div>

	<h3 id="trchtitle" class="black plink">URL comparison validation tool <i class="fa fa-arrow-down" id="trchdown"></i><i class="fa fa-arrow-up" id="trchup"></i></h3>

	<div id="trchdiv">
		Select the working URL example from the options below:
		<br/ >
			<br/ >
				<h3 id="comp_track_title" class="blue bigger">Gameloft</h3>

				<div id="comp_ios_cl_cont">
					<span class="ios_color">iOS click URL: </span><span id="hc_ios_cl" class="hc_warning" style="display: none;">We need to HARDCODE the URL!</span>
					<br/ >
						<input type="radio" name="comp_r" id="comp_r1" class="comp_r_class"/>
						<textarea name="comp_texta" id="comp_texta1" class="comp_text_class"></textarea>
				</div>

				<div id="comp_ios_imp_cont">
					<span class="ios_color">iOS impression URL: </span><span id="hc_ios_imp" class="hc_warning" style="display: none;">We need to HARDCODE the URL!</span>
					<br />
					<input type="radio" name="comp_r" id="comp_r2" class="comp_r_class"/>
					<textarea name="comp_texta" id="comp_texta2" class="comp_text_class"></textarea>
				</div>

				<div id="comp_and_cl_cont">
					<br />
					<span class="and_color">Android click URL: </span><span id="hc_and_cl" class="hc_warning" style="display: none;">We need to HARDCODE the URL!</span>
					<br />
					<input type="radio" name="comp_r" id="comp_r3" class="comp_r_class"/>
					<textarea name="comp_texta" id="comp_texta3" class="comp_text_class"></textarea>
				</div>

				<div id="comp_and_imp_cont">
					<span class="and_color">Android impression URL: </span><span id="hc_and_imp" class="hc_warning" style="display: none;">We need to HARDCODE the URL!</span>
					<br />
					<input type="radio" name="comp_r" id="comp_r4" class="comp_r_class"/>
					<textarea name="comp_texta" id="comp_texta4" class="comp_text_class"></textarea>
				</div>

				<div id="comp_notes_cont" style="display: none;">
					<br />
					<span class="ios_color">Notes:</span>
					<br />
					<div id="comp_notes_div">
						test
					</div>
				</div>
	</div>
	<br />

	<h3 id="comp_title" class="black bigger">Compare!</h3>

	<div id="comp_div" class="comp_div_class">
		<div id="geturl1" class="geturl1_visible">

			Paste the URL below:
			<br />
			<form id="valform">
				<textarea name="input_url" id="input_url"></textarea>
				<!--
				<input type="button" class="btn-class" value="Check URL" onclick="checkURLgen('input_url')">
				-->
				<input type="button" class="btn-class" value="Compare!" id="compare_button" onclick="compareURLs1_val()">
				<input type="button" class="btn-class" value="Compare and sort A-Z!" id="compare_button" onclick="compareURLs2_val()">
				<br />
				<br />
			</form>
			
			<div id="redirectdiv"></div>

			<div id="urlsplitdiv"></div>
			<div id="redurldiv"></div>
			<div id="vnotes_comment"></div>
			<div id="vnotes_alert"></div>
		</div>

		<div id="geturl2">

			The working URL from above:
			<br />
			<form id="valform2">
				<textarea name="input_url2" id="input_url2"></textarea>
				<br />
				<br />
				<br />
			</form>
			<div id="redirectdiv2"></div>
			<div id="urlsplitdiv2"></div>
		</div>
		<div style="clear: both;"></div>

		<div id="samediv">
			<span id="samealert"></span>
		</div>

		<div id="keydiv" style="display: none;">
			<span style="font-weight: bold; color: black; font-size: 16px;">Key:</span>
			<br />
			<table id="keytable" style="margin-top: 5px;">
				<tr>
					<td style='color: green; background-color: #DFD;'>Green:</td>
					<td>All good!</td>
				</tr>
				<tr>
					<td style='color: #C90; background-color: #FFE9AD;'>Orange:</td>
					<td>Same parameter, different value</td>
				</tr>
				<tr>
					<td style='color: #D00; background-color: #FFADB1;'>Red:</td>
					<td>Different parameter / base URL</td>
				</tr>

			</table>
		</div>
		<br />

	</div>
</div>

<?php
include '../footer.php';
?>

