<?php
include ("http://localhost:8888/password_protect.php");
?>

<?php
include '../header.php';
?>

<?php include_once("analyticstracking.php") ?>

<script type="text/javascript" src="ampage.js"></script>

<div id="maincontent">

	<h1>AM page</h1>

	<h3 class="black bigger plink" id="reqtitle">Requirements for tracking partners <i class="fa fa-arrow-down" id="req_otherp_down"></i><i class="fa fa-arrow-up" id="req_otherp_up"></i></h3>
	<div id="am_req_div">

		<select id="tr_select">
			<option value="" selected disabled>Choose tracking provider</option>
			<option value="tune">Tune</option>
			<option value="adjust">Adjust</option>
			<option value="appsflyer">AppsFlyer</option>
			<option value="kochava">Kochava</option>
			<option value="wooga">Wooga</option>
			<option value="gameloft">Gameloft</option>
			<option value="apsalar">Apsalar</option>
			<option value="king">King.com</option>
			<option value="fiksu">FIKSU</option>
			<option value="growmobile">GrowMobile</option>
			<option value="notfound">It's not on this list...</option>
		</select>
		&nbsp;&nbsp;
		<input class="btn-class" type="button" value="Let me see the requirements!" onclick="show_am_req()" />
		<div id="track_req">
			<h3 id="trh1" class="blue bigger"></h3>
			<span id="pb_icon"></span><h3 id="pb_h3">Postback</h3>
			<div id="tr_req_pb">
				The postback comes here.
			</div>
			<br />
			<span id="ai_icon"></span><h3 id="ai_h3">Can they send us all installs?</h3>
			<div id="tr_req_ai">
				"All installs" section comes here.
			</div>
			<br />
			<span id="vt_icon"></span><h3 id="vt_h3">View-through</h3>
			<div id="tr_req_vt">
				The View-through comes here.
			</div>
			<br />
			<span id="pm_icon"></span><h3 id="pm_h3">Parameters / macros</h3>
			<div id="tr_req_pm">
				The parameters / macros section comes here.
			</div>

			<div id="am_tune_param" class="am_paramdiv">
				<h3 id="am_tune_otherp_title" class="plink greytitle">Tune - Commonly used parameters / values <i class="fa fa-arrow-down" id="tune_otherp_down"></i><i class="fa fa-arrow-up" id="tune_otherp_up"></i></h3>
				<div id="tune_otherparams">
					<table id="am_tune_otherparamtable">
						<tr>
							<th class="firstcol">Parameter</th>
							<th class="secondcol">Macro/Value</th>
							<th class="thirdcol">Notes</th>
						</tr>
						<tr class="tep" id="tpid">
							<td>publisher_id</td>
							<td>specified by MAT</td>
							<td>numerical</td>
						</tr>
						<tr class="tep" id="tsid">
							<td>site_id</td>
							<td>specified by MAT</td>
							<td>numerical</td>
						</tr>
						<tr class="tep" id="tidfa">
							<td>ios_ifa</td>
							<td>[IDFA]</td>
							<td>for iOS</td>
						</tr>
						<tr class="tep" id="tgaid">
							<td>google_aid</td>
							<td>[GOOGLE_AD_ID]</td>
							<td>for Android</td>
						</tr>
						<tr class="tep" id="tjson">
							<td>response_format</td>
							<td>json</td>
							<td></td>
						</tr>
						<tr>
							<td>offer_id</td>
							<td>specified by MAT</td>
							<td>numerical</td>
						</tr>
						<tr id="tr_mac_address">
							<td>mac_address_sha1</td>
							<td>[MAC_SHA1]</td>
							<td></td>
						</tr>
						<tr id="tr_odin">
							<td>odin</td>
							<td>[ODIN1]</td>
							<td></td>
						</tr>
						<tr id="tr_sub_pub">
							<td>sub_publisher</td>
							<td>[PUBLISHER_ID]</td>
							<td></td>
						</tr>
						<tr id="tr_sub_site">
							<td>sub_site</td>
							<td>[APP_ID]</td>
							<td></td>
						</tr>
						<tr id="tr_sub_camp">
							<td>sub_campaign</td>
							<td>[AD_CAMPAIGN_NAME]</td>
							<td></td>
						</tr>
						<tr id="tr_sub_adgr">
							<td>sub_adgroup</td>
							<td>[AD_GROUP_NAME]</td>
							<td></td>
						</tr>
						<tr id="tr_sub_ad">
							<td>sub_ad</td>
							<td>[AD_CREATIVE_NAME]</td>
							<td></td>
						</tr>
						<tr id="tr_dev_mod">
							<td>device_model</td>
							<td>[DEVICE_MODEL]</td>
							<td></td>
						</tr>
						<tr>
							<td>sub_keyword</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>my_partner</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>advertiser_sub_site</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="tr_adv_sub_camp">
							<td>advertiser_sub_campaign</td>
							<td>[AD_CAMPAIGN_NAME]</td>
							<td></td>
						</tr>
						<tr id="tr_adv_sub_adgr">
							<td>advertiser_sub_adgroup</td>
							<td>[AD_GROUP_NAME]</td>
							<td></td>
						</tr>
						<tr id="tr_adv_sub_ad">
							<td>advertiser_sub_ad</td>
							<td>[AD_CREATIVE_NAME]</td>
							<td></td>
						</tr>
						<tr>
							<td>advertiser_sub_keyword</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>my_site</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="tr_my_camp">
							<td>my_campaign</td>
							<td>[AD_CAMPAIGN_NAME]</td>
							<td></td>
						</tr>
						<tr id="tr_my_adgr">

							<td>my_adgroup</td>
							<td>[AD_GROUP_NAME]</td>
							<td></td>
						</tr>
						<tr id="tr_my_ad">

							<td>my_ad</td>
							<td>[AD_CREATIVE_NAME]</td>
							<td></td>
						</tr>
						<tr>
							<td>my_keyword</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="tr_country">
							<td>country_code</td>
							<td>[COUNTRY_CODE]</td>
							<td></td>
						</tr>
						<tr id="tr_ip">
							<td>device_ip</td>
							<td>[IP_ADDRESS]</td>
							<td></td>
						</tr>
						<tr>
							<td>cost_model</td>
							<td></td>
							<td>value must be hardcoded as cpi or cpcv</td>
						</tr>
						<tr>
							<td>cost</td>
							<td></td>
							<td>value must be hardcoded (we don't support a macro for passing cost)</td>
						</tr>
						<tr>
							<td>sub1</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>sub2</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>sub3</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>sub4</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>sub5</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="tr_sub_place">
							<td>sub_placement</td>
							<td>[STORE_ID]</td>
							<td>(used 10/1 onwards)</td>
						</tr>
					</table>
				</div>
			</div>
			<div id="am_adjust_param" class="am_paramdiv">
				<h3 id="am_adj_otherp_title" class="plink greytitle">Adjust - Commonly used parameters / values <i class="fa fa-arrow-down" id="adj_otherp_down"></i><i class="fa fa-arrow-up" id="adj_otherp_up"></i></h3>
				<div id="am_adjparams">
					<table id="adj_paramtable">
						<tr>
							<th class="firstcol">Parameters</th>
							<th class="secondcol">Macro/Value</th>
							<th class="thirdcol">Notes</th>
						</tr>
						<tr>
							<td>idfa</td>
							<td>[IDFA]</td>
							<td>for iOS</td>
						</tr>
						<tr>
							<td>gps_adid</td>
							<td>[GOOGLE_AD_ID]</td>
							<td>for Android</td>
						</tr>
						<tr>
							<td>android_id_lower_sha1</td>
							<td>[SHA1_ANDROID_ID]</td>
							<td>for Android</td>
						</tr>
						<tr>
							<td>s2s</td>
							<td>1</td>
							<td></td>
						</tr>
						<tr>
							<td>campaign</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>adgroup</td>
							<td>[APP_ID]</td>
							<td></td>
						</tr>
						<tr>
							<td>creative</td>
							<td>[AD_CREATIVE_NAME]</td>
							<td></td>
						</tr>
						<tr>
							<td>label</td>
							<td></td>
							<td>rarely used - requires config in Adjust on client's end</td>
						</tr>
						<tr>
							<td>tracker limit</td>
							<td>#</td>
							<td>only allows the amount of values we pass to be up to # specified</td>
						</tr>
					</table>
				</div>
			</div>
			<div id="am_apps_param" class="am_paramdiv">
				<h3 id="am_apps_otherp_title" class="plink greytitle">AppsFlyer - Commonly used parameters / values <i class="fa fa-arrow-down" id="apps_otherp_down"></i><i class="fa fa-arrow-up" id="apps_otherp_up"></i></h3>
				<div id="am_appsparams">
					<table id="apps_paramtable">
						<tr>
							<th class="firstcol">Parameters</th>
							<th class="secondcol">Macro/Value</th>
							<th class="thirdcol">Notes</th>
						</tr>
						<tr class="tep" id="pid">
							<td>pid</td>
							<td>adcolony_int</td>
							<td></td>
						</tr>
						<tr>
							<td>c</td>
							<td>specified by CLIENT</td>
							<td>campaign that will appear in Appsflyer</td>
						</tr>
						<tr class="tep" id="clickid">
							<td>clickid</td>
							<td>[MAC_SHA1]</td>
							<td></td>
						</tr>
						<tr class="tep" id="idfa">
							<td>idfa</td>
							<td>[IDFA]</td>
							<td>for iOS</td>
						</tr>
						<tr id="sha1">
							<td>sha1_android_id</td>
							<td>[SHA1_ANDROID_ID]</td>
							<td></td>
						</tr>
						<tr class="tep" id="advid">
							<td>advertising_id</td>
							<td>[GOOGLE_AD_ID]</td>
							<td>for Android</td>
						</tr>
						<tr class="tep" id="red">
							<td>redirect</td>
							<td>false</td>
							<td>for Android</td>
						</tr>
						<tr class="tep" id="appid">
							<td>app_id</td>
							<td>[PRODUCT_ID]</td>
							<td>**value can also be the mobile app id or bundle id</td>
						</tr>
						<tr id="af_siteid">
							<td>af_siteid</td>
							<td>[APP_ID]</td>
							<td></td>
						</tr>
						<tr id="af_sub1">
							<td>af_sub1</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="af_sub2">
							<td>af_sub2</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="af_sub3">
							<td>af_sub3</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="af_sub4">
							<td>af_sub4</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="af_sub5">
							<td>af_sub5</td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="am_koch_param" class="am_paramdiv">
				<h3 id="am_koch_otherp_title" class="plink greytitle">Kochava - Commonly used parameters / values <i class="fa fa-arrow-down" id="koch_otherp_down"></i><i class="fa fa-arrow-up" id="koch_otherp_up"></i></h3>
				<div id="am_kochparams">
					<table id="am_koch_paramtable">
						<tr>
							<th class="firstcol">Parameter</th>
							<th class="secondcol">Macro/Value</th>
							<th class="thirdcol">Notes</th>
						</tr>
						<tr class="tep" id="kcid">
							<td>campaign_id</td>
							<td>specified by Kochava</td>
							<td></td>
						</tr>
						<tr class="tep" id="knid">
							<td>network_id</td>
							<td>specified by Kochava</td>
							<td></td>
						</tr>
						<tr class="tep" id="kdevid">
							<td>device_id</td>
							<td>[IDFA]</td>
							<td>for iOS</td>
						</tr>
						<tr class="tep" id="kadid">
							<td>adid</td>
							<td>[GOOGLE_AD_ID]</td>
							<td>for Android</td>
						</tr>
						<tr class="tep" id="kappend">
							<td>append_app_conv_trk_params</td>
							<td>1</td>
							<td>for Android</td>
						</tr>
						<tr class="tep" id="kpbr">
							<td>pbr</td>
							<td>1</td>
							<td>for Android click tags</td>
						</tr>
						<tr>
							<td>device_id_type</td>
							<td>idfa</td>
							<td>or iOS</td>
						</tr>
						<tr>
							<td>device_hash_method</td>
							<td>sha1</td>
							<td></td>
						</tr>
						<tr id="tr_sha1">
							<td>device_id</td>
							<td>[SHA1_ANDROID_ID]</td>
							<td></td>
						</tr>
						<tr>
							<td>device_id_is_hashed</td>
							<td>true</td>
							<td></td>
						</tr>
						<tr>
							<td>device_id_type</td>
							<td>android_id</td>
							<td></td>
						</tr>
						<tr id="tr_imei">
							<td>imei_sha1</td>
							<td>[SHA1_IMEI]</td>
							<td></td>
						</tr>
						<tr id="tr_mac">
							<td>mac_sha1</td>
							<td>[MAC_SHA1]</td>
							<td></td>
						</tr>
						<tr id="tr_odin">
							<td>odin</td>
							<td>[ODIN1]</td>
							<td></td>
						</tr>
						<tr id="tr_openud">
							<td>openudid</td>
							<td>[OPEN_UDID]</td>
							<td></td>
						</tr>
						<tr id="tr_udid">
							<td>udid</td>
							<td>[UDID]</td>
							<td></td>
						</tr>
						<tr id="tr_siteid">
							<td>siteid</td>
							<td>[APP_ID]</td>
							<td></td>
						</tr>
						<tr id="tr_creativeid">
							<td>creative_id</td>
							<td>[AD_CREATIVE_NAME]</td>
							<td></td>
						</tr>
						<tr>
							<td>click_id</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="tr_devmod">
							<td>device_model</td>
							<td>[DEVICE_MODEL]</td>
							<td></td>
						</tr>
						<tr id="tr_devos">
							<td>device_os</td>
							<td>[PLATFORM]</td>
							<td></td>
						</tr>
						<tr id="tr_devver">
							<td>device_ver</td>
							<td>[OS_VERSION]</td>
							<td></td>
						</tr>
						<tr id="tr_devcon">
							<td>device_connection</td>
							<td>[NETWORK_TYPE]</td>
							<td></td>
						</tr>
						<tr id="tr_ip">
							<td>ip_address</td>
							<td>[IP_ADDRESS]</td>
							<td></td>
						</tr>
						<tr id="tr_country">
							<td>country</td>
							<td>[COUNTRY_CODE]</td>
							<td></td>
						</tr>
						<tr id="tr_dma">
							<td>user_dma</td>
							<td>[DMA_CODE]</td>
							<td></td>
						</tr>
						<tr>
							<td>cp_name1</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>cp_name2</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>cp_name3</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>cp_value1</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>cp_value2</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>cp_value3</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="tr_appstoreid">
							<td>appstore_id</td>
							<td>[STORE_ID]</td>
							<td>(used 10/1 onwards)</td>
						</tr>
						<tr id="tr_impid">
							<td>impression_id</td>
							<td>[SERVE_TIME]</td>
							<td>(view based parameter)</td>
						</tr>
						<tr id="tr_event">
							<td>event</td>
							<td>completed_view</td>
							<td>(view based parameter)</td>
						</tr>
					</table>
				</div>
			</div>
			<div id="am_woo_param" class="am_paramdiv">
				<h3 id="am_woo_otherp_title" class="plink greytitle">Wooga - Commonly used parameters / values <i class="fa fa-arrow-down" id="woo_otherp_down"></i><i class="fa fa-arrow-up" id="woo_otherp_up"></i></h3>
				<div id="am_wooparams">
					<table id="am_woo_paramtable" class="am_paramtable">
						<tr>
							<th class="firstcol" style="width: 100px;">Parameter</th>
							<th class="secondcol">Macro/Value</th>
							<th class="thirdcol">Notes</th>
						</tr>
						<tr class="tep" id="kcid">
							<td>network</td>
							<td>adcolony</td>
							<td></td>
						</tr>
						<tr id="knid">
							<td>campaign</td>
							<td>Specified by Wooga</td>
							<td></td>
						</tr>
						<tr id="kdevid">
							<td>adgroup</td>
							<td>[APP_ID]</td>
							<td></td>
						</tr>
						<tr class="tep" id="kadid">
							<td>idfa</td>
							<td>[IDFA]</td>
							<td>for iOS</td>
						</tr>
						<tr class="tep" id="tr_imei">
							<td>partner_data</td>
							<td>[GOOGLE_AD_ID]</td>
							<td>for Android</td>
						</tr>
						<tr>
							<td>app_id</td>
							<td>Specified by Wooga</td>
							<td>** this is not the same as our [APP_ID]</td>
						</tr>
					</table>
				</div>
			</div>
			<div id="am_gloft_param" class="am_paramdiv">
				<h3 id="am_gloft_otherp_title" class="plink greytitle">Gameloft - Commonly used parameters / values <i class="fa fa-arrow-down" id="gloft_otherp_down"></i><i class="fa fa-arrow-up" id="gloft_otherp_up"></i></h3>
				<div id="am_gloftparams">
					<table id="am_gloft_paramtable" class="am_paramtable">
						<tr>
							<th class="firstcol" style="width: 100px;">Parameter</th>
							<th class="">Macro/Value</th>
							<th class="thirdcol">Notes</th>
						</tr>
						<tr class="tep" id="kcid">
							<td>gcampaign_id</td>
							<td>Specified by Gameloft</td>
							<td></td>
						</tr>
						<tr class="tep" id="knid">
							<td>gproduct_id</td>
							<td>Specified by Gameloft</td>
							<td></td>
						</tr>
						<tr class="tep" id="kdevid">
							<td>gos</td>
							<td>ios</td>
							<td>for iOS</td>
						</tr>
						<tr class="tep" id="kadid">
							<td>idfa</td>
							<td>[IDFA]</td>
							<td>for iOS</td>
						</tr>
						<tr class="tep" id="tr_imei">
							<td>google_ad_id</td>
							<td>[GOOGLE_AD_ID]</td>
							<td>for Android</td>
						</tr>
						<tr>
							<td>country_code</td>
							<td>[COUNTRY_CODE]</td>
							<td></td>
						</tr>
						<tr>
							<td>ip_address</td>
							<td>[IP_ADDRESS]</td>
							<td></td>
						</tr>
						<tr>
							<td>app_id</td>
							<td>[APP_ID]</td>
							<td></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="am_aps_param" class="am_paramdiv">
				<h3 id="am_aps_otherp_title" class="plink greytitle">Apsalar - Commonly used parameters / values <i class="fa fa-arrow-down" id="aps_otherp_down"></i><i class="fa fa-arrow-up" id="aps_otherp_up"></i></h3>
				<div id="am_apsparams">
					<table id="am_aps_paramtable" class="am_paramtable">
						<tr>
							<th class="firstcol" style="width: 100px;">Parameter</th>
							<th class="secondcol">Macro/Value</th>
							<th class="thirdcol">Notes</th>
						</tr>
						<tr id="kcid">
							<td>re</td>
							<td>Specified by Apsalar</td>
							<td></td>
						</tr>
						<tr class="tep" id="knid">
							<td>a</td>
							<td>Specified by Apsalar</td>
							<td></td>
						</tr>
						<tr class="tep" id="kdevid">
							<td>i</td>
							<td>Specified by Apsalar</td>
							<td></td>
						</tr>
						<tr class="tep" id="kadid">
							<td>an</td>
							<td>AdColony</td>
							<td></td>
						</tr>
						<tr class="tep" id="tr_imei">
							<td>ca</td>
							<td>Specified by Apsalar</td>
							<td>campaign that will appear in Apsalar</td>
						</tr>
						<tr class="tep">
							<td>p</td>
							<td>iOS</td>
							<td>for iOS</td>
						</tr>
						<tr class="tep">
							<td>p</td>
							<td>Android</td>
							<td>for Android</td>
						</tr>
						<tr>
							<td>pl</td>
							<td></td>
							<td>ad group that will appear in Apsalar</td>
						</tr>
						<tr class="tep">
							<td>idfa</td>
							<td>[IDFA]</td>
							<td>for iOS</td>
						</tr>
						<tr class="tep">
							<td>aifa</td>
							<td>[GOOGLE_AD_ID]</td>
							<td>for Android</td>
						</tr>
						<tr class="tep">
							<td>product_id</td>
							<td>[PRODUCT_ID]</td>
							<td>**value can also be the mobile app id. Must be hardcoded as the bundle id on Android.</td>
						</tr>
						<tr class="tep">
							<td>api_key</td>
							<td>d4636d565f59e217801b5721af716eac</td>
							<td>(AdColony API key for Apsalar)</td>
						</tr>
						<tr class="tep">
							<td>h</td>
							<td>specified by Apsalar</td>
							<td></td>
						</tr>
						<tr>
							<td>andi</td>
							<td>[SHA1_ANDROID_ID]</td>
							<td></td>
						</tr>
						<tr>
							<td>and1</td>
							<td>[SHA1_ANDROID_ID]</td>
							<td></td>
						</tr>
						<tr>
							<td>udid</td>
							<td>[RAW_UDID]</td>
							<td></td>
						</tr>
						<tr>
							<td>odin</td>
							<td>[ODIN1]</td>
							<td></td>
						</tr>
						<tr>
							<td>udi1</td>
							<td>[UDID]</td>
							<td></td>
						</tr>
						<tr>
							<td>s</td>
							<td>[APP_ID]</td>
							<td></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="am_fik_param" class="am_paramdiv">
				<h3 id="am_fik_otherp_title" class="plink greytitle">FIKSU - Commonly used parameters / values <i class="fa fa-arrow-down" id="fik_otherp_down"></i><i class="fa fa-arrow-up" id="fik_otherp_up"></i></h3>
				<div id="am_fikparams">
					<table id="am_fik_paramtable" class="am_paramtable">
						<tr>
							<th class="firstcol" style="width: 100px;">Parameter</th>
							<th class="secondcol">Macro/Value</th>
							<th class="thirdcol">Notes</th>
						</tr>
						<tr class="tep" id="kcid">
							<td>adid</td>
							<td>Specified by FIKSU</td>
							<td></td>
						</tr>
						<tr class="tep" id="knid">
							<td>advertising_id</td>
							<td>[IDFA]</td>
							<td>for iOS</td>
						</tr>
						<tr class="tep" id="kdevid">
							<td>gaid</td>
							<td>[GOOGLE_AD_ID]</td>
							<td>for Android</td>
						</tr>
						<tr class="tep" id="kadid">
							<td>site_id</td>
							<td>[APP_ID]</td>
							<td></td>
						</tr>
						<tr id="tr_imei">
							<td>app_id</td>
							<td>[APP_ID]</td>
							<td></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="am_grow_param" class="am_paramdiv">
				<h3 id="am_grow_otherp_title" class="plink greytitle">Growmobile - Commonly used parameters / values <i class="fa fa-arrow-down" id="grow_otherp_down"></i><i class="fa fa-arrow-up" id="grow_otherp_up"></i></h3>
				<div id="am_growparams">
					<table id="am_grow_paramtable" class="am_paramtable">
						<tr>
							<th class="firstcol" style="width: 100px;">Parameter</th>
							<th class="secondcol">Macro/Value</th>
							<th class="thirdcol">Notes</th>
						</tr>
						<tr class="tep" id="kcid">
							<td>partner</td>
							<td>ad_colony</td>
							<td></td>
						</tr>
						<tr class="tep" id="kcid">
							<td>app_key</td>
							<td>Specified by Grow</td>
							<td></td>
						</tr>
						<tr class="tep" id="kcid">
							<td>campaign_id</td>
							<td>Specified by Grow</td>
							<td></td>
						</tr>
						<tr class="tep" id="knid">
							<td>aid</td>
							<td>[IDFA]</td>
							<td>for iOS</td>
						</tr>
						<tr class="tep" id="kdevid">
							<td>gaid</td>
							<td>[GOOGLE_AD_ID]</td>
							<td>for Android</td>
						</tr>
						<tr id="kadid">
							<td>ts</td>
							<td>[SERVE_TIME]</td>
							<td></td>
						</tr>
						<tr id="tr_imei">
							<td>pub_id</td>
							<td>[APP_ID]</td>
							<td></td>
						</tr>
						<tr id="tr_imei">
							<td>creative_id</td>
							<td>[AD_CREATIVE_NAME]</td>
							<td></td>
						</tr>
						<tr class="tep" id="tr_imei">
							<td>&redirect</td>
							<td></td>
							<td>**must be included for s2s tracking on iOS and Android</td>
						</tr>
					</table>
				</div>
			</div>
			<div id="am_king_param" class="am_paramdiv">
				<h3 id="am_king_otherp_title" class="plink greytitle">King.com - Commonly used parameters / values <i class="fa fa-arrow-down" id="king_otherp_down"></i><i class="fa fa-arrow-up" id="king_otherp_up"></i></h3>
				<div id="am_kingparams">
					<table id="am_king_paramtable" class="am_paramtable">
						<tr>
							<th class="firstcol" style="width: 100px;">Parameter</th>
							<th class="secondcol">Macro/Value</th>
							<th class="thirdcol">Notes</th>
						</tr>
						<tr class="tep" id="kcid">
							<td>type</td>
							<td>video or ad</td>
							<td>video for completes, ad for HTML5</td>
						</tr>
						<tr class="tep" id="kcid">
							<td>idfa_raw</td>
							<td>[IDFA]</td>
							<td></td>
						</tr>
						<tr class="tep" id="kcid">
							<td>googleAdId</td>
							<td>[GOOGLE_AD_ID]</td>
							<td></td>
						</tr>
						<tr id="knid">
							<td>androidId_sha1</td>
							<td>[SHA1_ANDROID_ID]</td>
							<td></td>
						</tr>
						<tr class="tep" id="kdevid">
							<td>network</td>
							<td>adcolony or adcolony_android</td>
							<td></td>
						</tr>
						<tr class="tep" id="kadid">
							<td>targetAppId</td>
							<td>Specified by KING</td>
							<td></td>
						</tr>
						<tr id="tr_imei">
							<td>publisher</td>
							<td>[PUBLISHER_ID]</td>
							<td></td>
						</tr>
						<tr id="tr_imei">
							<td>site</td>
							<td>[APP_ID]</td>
							<td></td>
						</tr>
						<tr>
							<td>campaingName</td>
							<td>[RAW_AD_CAMPAIGN_ID]</td>
							<td></td>
						</tr>
						<tr>
							<td>creativeName</td>
							<td>[AD_CREATIVE_NAME]</td>
							<td></td>
						</tr>
						<tr>
							<td>creativeSize</td>
							<td></td>
							<td></td>
						</tr>
						<tr class="tep" id="tr_imei">
							<td>product_id</td>
							<td>[PRODUCT_ID]</td>
							<td></td>
						</tr>
						<tr class="tep" id="tr_imei">
							<td>raw_advertising_id</td>
							<td>[IDFA]</td>
							<td>for iOS</td>
						</tr>
						<tr class="tep" id="tr_imei">
							<td>google_ad_id</td>
							<td>[GOOGLE_AD_ID]</td>
							<td>for Android</td>
						</tr>
						<tr id="tr_imei">
							<td>sha1_android_id</td>
							<td>[SHA1_ANDROID_ID]</td>
							<td>for Android</td>
						</tr>
						<tr class="tep" id="tr_imei">
							<td>noRedirect</td>
							<td>true</td>
							<td></td>
						</tr>
						<tr class="tep" id="tr_imei">
							<td>byPassFingerprint</td>
							<td>true</td>
							<td></td>
						</tr>
						<tr id="tr_imei">
							<td>appId</td>
							<td>[APP_ID]</td>
							<td></td>
						</tr>
						<tr id="tr_imei">
							<td>countryCode</td>
							<td>[COUNTRY_CODE]</td>
							<td></td>
						</tr>
						<tr id="tr_imei">
							<td>os</td>
							<td>[PLATFORM]</td>
							<td></td>
						</tr>
					</table>
				</div>
			</div>
			<br />
			<span id="hc_icon"></span><h3 id="hc_h3">Hardcoded URL</h3>
			<div id="tr_req_hc">
				Hardcoded URL comes here.
			</div>
			<br />
			<span id="pie_icon"></span><h3 id="pie_h3">PIE - Post install events</h3>
			<div id="tr_req_pie">
				PIE section comes here.
			</div>
		</div>
		<div id="track_req_alert"></div>
		<br />
	</div>

	<h3 id="trchtitle" class="black plink hide">Tracking URL checker <i class="fa fa-arrow-down" id="trchdown"></i><i class="fa fa-arrow-up" id="trchup"></i></h3>

	<div id="trchdiv">
		Paste the URL below:
		<br/ >
			<textarea name="tracking_checker" id="tracking_checker"></textarea>
			<br />
			<input type="button" class="btn-class" value="Check tracking provider" onclick="check_track('tracking_checker',tr_id_div)"/>
			<input type="button" class="btn-class" value="Check the parameters" onclick="getURLparams('tracking_checker',urlsplitdiv3)">
			<br />
			<br />
			<div id="urlsplitdiv3"></div>
			<div id="tr_id_div">
				&nbsp;
			</div>
			The full list of integrated tracking partners can be found <a class="blue" href="https://docs.google.com/spreadsheets/d/14SrvnTaq7jd1_GQsg8z6zXb5G-fwDebCNEjPCCPDgd8/edit#gid=0" target="_blank">here</a>.
			<br />
			<br />
			<span style="font-weight: bold;">MACROs</span>: For an external version that you can send to clients, click <a class="blue" href="macros.pdf" target="_blank">here</a>. (updated: october 2016)
			<br />
			<br />
	</div>

	<!-- as discussed with Ligita, they probably don't need this table

	<h3 id="reqtabletitle" class="black plink">Requirements table <i class="fa fa-arrow-down" id="reqdown"></i><i class="fa fa-arrow-up" id="requp"></i></h3>
	<div id="reqtablediv">

	<table id="amtable">
	<tr>
	<th>Tracking provider</th>
	<th>Postback</th>
	<th colspan="2">View-through</th>
	<th>Parameters / Macros</th>
	</tr>
	<tr>
	<td>Tune</td>
	<td class="yellow">Client should set up the postback in Tune.
	<br />
	TAM can check it.</td>
	<td colspan="2" class="yellow">Default: 7 days
	<br />
	Client should set up the View-through in Tune.
	<br />
	TAM can check it.</td>
	<td>Commonly used values</td>
	</tr>
	<tr>
	<td>Adjust</td>
	<td class="green">The postback is hardcoded at the end of the tracking URL.</td>
	<td class="red">1 day
	<br />
	Ask the client to enable view thru tracking in their integration with Adjust</td>
	<td class="green">7 days
	<br />
	No action necessary - we implement it on our end.</td>
	<td>Commonly used values</td>
	</tr>
	<tr>
	<td>Appsflyer</td>
	<td class="green">The postback should be automatically set up in AppsFlyer.</td>
	<td class="red">1 day
	<br />
	Ask the client to enable view thru tracking in their integration with AppsFlyer.</td>
	<td class="green">7 days
	<br />
	No action necessary - we implement it on our end.</td>
	<td>Commonly used values</td>
	</tr>
	<tr>
	<td>Kochava</td>
	<td>?</td>
	<td colspan="2">Default: 1 day
	<br />
	Client should enable and/or adjust View-through tracking in their integration with Kochava.
	<br />
	We need the impression URL as well as the click URL if the View-through is enabled. </td>
	<td>Commonly used values</td>
	</tr>
	<tr>
	<td>Wooga</td>
	<td>?</td>
	<td colspan="2">7 days (matched to click window)</td>
	<td>Commonly used values</td>
	</tr>
	<tr>
	<td>Gameloft</td>
	<td>?</td>
	<td colspan="2">3 days</td>
	<td>Commonly used values</td>
	</tr>
	<tr>
	<td>ApSalar</td>
	<td>?</td>
	<td colspan="2">7 days (matched to click window)</td>
	<td>Commonly used values</td>
	</tr>
	<tr>
	<td>King.com</td>
	<td>?</td>
	<td colspan="2">12 hours</td>
	<td>Commonly used values</td>
	</tr>
	<tr>
	<td>FIKSU</td>
	<td>?</td>
	<td colspan="2">7 days</td>
	<td>Commonly used values</td>
	</tr>
	<tr>
	<td>GrowMobile</td>
	<td>?</td>
	<td colspan="2">7 days</td>
	<td>Commonly used values</td>
	</tr>
	</table>

	</div>
	-->

	<h3 class="black plink hide" id="vtchecktitle">View-through checker tool <i class="fa fa-arrow-down" id="vtcheck_down"></i><i class="fa fa-arrow-up" id="vtcheck_up"></i></h3>
	<div id="vtcheck_div">
		Copy <span class="bold">all</span> URLs from the dash and paste them below:
		<br />
		<br />
		<table class="macrotable">
			<tr>
				<td>HTML5:</td>
				<td>
				<input type="text" name="hmtl5" id="html5_input" class="vt_input"/>
				</td>
			</tr>
			<tr>
				<td>Video Starts:</td>
				<td>
				<input type="text" name="vstart" id="vstart_input" class="vt_input"/>
				</td>
			</tr>
			<tr>
				<td>Video Completes:</td>
				<td>
				<input type="text" name="vcomplete" id="vcomplete_input" class="vt_input"/>
				</td>
			</tr>
		</table>
		<br />
		<input type="button" id="vtcheck_button" onclick="vt_check()" value="Check view-through" class="btn-class"/>
		<br />
		<br />
		<div id="vt_prov_div"></div>
		<br />
		<div id="vt_result"></div>
		<br />
	</div>

	<h3 class="black plink hide" id="geturl_title">URL comparison Tool <i class="fa fa-arrow-down" id="comp_down"></i><i class="fa fa-arrow-up" id="comp_up"></i></h3>
	<div id="comp_div">
		<div id="geturl1" class="geturl1_visible">

			Paste the first URL below:
			<br />
			<form id="valform">
				<textarea name="input_url" id="input_url"></textarea>
				<!--
				<input type="button" class="btn-class" value="Check URL" onclick="checkURLgen('input_url')">
				-->
				<input type="button" class="btn-class" value="Compare!" id="compare_button" onclick="compareURLs1()">
				<input type="button" class="btn-class" value="Compare and sort A-Z!" id="compare_button" onclick="compareURLs2()">
				<br />
				<br />
			</form>

			<div id="urlsplitdiv"></div>
			<div id="redurldiv"></div>
			<div id="vnotes_comment"></div>
			<div id="vnotes_alert"></div>
		</div>

		<div id="geturl2" class="geturl2_visible">
			
			Paste the second URL below:
			<br />
			<form id="valform2">
				<textarea name="input_url2" id="input_url2"></textarea>
				<br />
				<br />
				<br />
			</form>
			<div id="urlsplitdiv2"></div>
		</div>
		<div style="clear: both;"></div>

		<div id="samediv">
			<span id="samealert"></span>
		</div>

		<div id="keydiv" style="display: none;">
			<span style="font-weight: bold; color: black; font-size: 16px;">Key:</span>
			<br />
			<table id="keytable" style="margin-top: 5px;">
				<tr>
					<td style='color: green; background-color: #DFD;'>Green:</td>
					<td>All good!</td>
				</tr>
				<tr>
					<td style='color: #C90; background-color: #FFE9AD;'>Orange:</td>
					<td>Same parameter, different value</td>
				</tr>
				<tr>
					<td style='color: #D00; background-color: #FFADB1;'>Red:</td>
					<td>Different parameter / base URL</td>
				</tr>

			</table>
		</div>
		<br />

	</div>
	
	<h3 class="black" id="other_links">Useful links</h3>
	<div>
		<!--
		<a href="http://support.adcolony.com/customer/en/portal/articles/2918564-attribution-optimization---how-to-guides" target="_blank" class="blue">Attribution optimization</a><br />
		-->
		<a href="http://support.adcolony.com/customer/en/portal/articles/2918521-set-up-a-postback" target="_blank" class="blue">Set up a postback</a><br />
		<a href="http://support.adcolony.com/customer/en/portal/articles/2918550-enable-full-install-feed" target="_blank" class="blue">Enable full install feed</a><br />
		<a href="http://support.adcolony.com/customer/en/portal/articles/2918546-enable-view-thru-attribution" target="_blank" class="blue">Enable VT attribution</a><br />
		<a href="http://support.adcolony.com/customer/en/portal/articles/1914521-post-install-event-postbacks-for-top-tracking-partners" target="_blank" class="blue">Set up PIE events</a>
		
		
	</div>
	
	
	
</div>

<?php
include '../footer.php';
?>

