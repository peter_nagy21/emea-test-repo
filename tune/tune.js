ValidateTune = function() {

	$('input[type=checkbox]').attr('checked', false);
	$('#tune_otherparamtable tbody tr').css("color", "#777");

	final_comment = "";
	final_alert = "";

	tune_url = document.getElementById("tune_url").value;

	// remove spaces
	tune_url_nospace = tune_url.replace(/\s+/g, '');

	if (tune_url !== tune_url_nospace) {
		tune_url = tune_url_nospace;
		final_comment += "Spaces have been removed!<br /><br />";
	}
	
	// Olga-proofing links :)
	
	var imp_part = "?action=impression";
	var is_imp_link = (tune_url.indexOf(imp_part) > -1);
	
	var ua_dup1 = "[USER_AGENT],[USER_AGENT]";
	var is_ua_dup1 = (tune_url.indexOf(ua_dup1) > -1);
	
	var ua_dup2 = "[USER_AGENT_MOZILLA],[USER_AGENT_MOZILLA]";
	var is_ua_dup2 = (tune_url.indexOf(ua_dup2) > -1);
	
	if (is_imp_link) {
		tune_url = tune_url.replace("?action=impression", "?action=click");
		final_comment += "'?action=impression' has changed to '?action=click'<br /><br />";
	}
	
	if (is_ua_dup1) {
		tune_url = tune_url.replace("[USER_AGENT],[USER_AGENT]", "[USER_AGENT]");
		final_comment += "'[USER_AGENT],[USER_AGENT]' has changed to '[USER_AGENT]'<br /><br />";
	}
	
	if (is_ua_dup2) {
		tune_url = tune_url.replace("[USER_AGENT_MOZILLA],[USER_AGENT_MOZILLA]", "[USER_AGENT_MOZILLA]");
		final_comment += "'[USER_AGENT_MOZILLA],[USER_AGENT_MOZILLA]' has changed to '[USER_AGENT_MOZILLA]'<br /><br />";
	}

	// validations

	var url_good = validate_url(tune_url);
	if (!url_good) {
		final_alert += "It looks like the URL is not correct.<br /><br />";
	}
	var qs_good = validate_questionmarks(tune_url);
	if (!qs_good) {
		final_alert += "The URL contains more than one questionmarks!<br /><br />"
	}

	var macro_check_text = validate_macros(tune_url);
	var macros_good = (macro_check_text === "");
	final_alert += macro_check_text;

	url_all_good = (url_good && qs_good);
	console.log("all good: " + url_all_good);

	// END of validation

	// check / add response_format=json

	var json = "response_format=json";
	var is_json = (tune_url.indexOf(json) > -1);

	if (!is_json) {
		tune_url += "&response_format=json";
		final_comment += "'&response_format=json' has been added to the URL!<br /><br />";
	}

	// check / add device_ip

	var devip = "device_ip";
	var is_devip = (tune_url.indexOf(devip) > -1);

	if (!is_devip) {
		tune_url += "&device_ip=[IP_ADDRESS]";
		final_comment += "'&device_ip=[IP_ADDRESS]' has been added to the URL!<br /><br />";
	}

	// check / add ref_id

	var refid = "ref_id";
	var is_refid = (tune_url.indexOf(refid) > -1);

	if (!is_refid) {
		tune_url += "&ref_id=[CLICK_ID]";
		final_comment += "'&ref_id=[CLICK_ID]' has been added to the URL!<br /><br />";
	}

	// check / add user_agent

	var ua = "user_agent";
	var is_ua = (tune_url.indexOf(ua) > -1);

	var ifa = "ios_ifa";
	var is_ifa = (tune_url.indexOf(ifa) > -1);

	if (!is_ua) {

		if (is_ifa) {
			tune_url += "&user_agent=[USER_AGENT_MOZILLA]";
			final_comment += "'&user_agent=[USER_AGENT_MOZILLA]' has been added to the URL!<br /><br />";
		} else {
			tune_url += "&user_agent=[USER_AGENT]";
			final_comment += "'&user_agent=[USER_AGENT]' has been added to the URL!<br /><br />";
		}
	}

	// view-through

	output_tags(tune_url);

	if (vt_ok && url_all_good) {
		validate_url(tune_url);
		checkTuneParams(tune_url);
		redirectURL(tune_url, '#redurldiv');
		splitURL(tune_url, urlsplit_div);
		$("#redurldiv").show();
		$("#urlparamstitle").show();
	} else {
		$("#vnotes_comment").html("");
		$("#vnotes_alert").html(final_alert);
		$("#redurldiv").hide();
	}
};

output_tags = function(string) {

	var vt0 = document.getElementById("vt0").checked;
	var vt1 = document.getElementById("vt1").checked;

	vt_ok = false;

	if (vt0 || vt1) {
		vt_ok = true;
	} else {
		final_alert += "<br />Select view-through attribution window!<br /><br />";
		$("#vnotes_alert").html(final_alert);
	}

	// get the impresion URLs from click

	var tune_imp = string.replace("action=click", "action=impression");
	tune_imp = tune_imp.replace("&response_format=json", "");
	tune_imp += "&tracking_id=AC[RAW_AD_CAMPAIGN_ID]-[ADCOLONY_TIMESTAMP_MILLIS]-[DEVICE_ID]&view_percent=";

	// tune_imp25 for quartiles output

	var tune_imp25 = "1st quartile:\n\n" + tune_imp + "25\n\n";
	tune_imp25 += "Midpoint:\n\n" + tune_imp + "50\n\n";
	tune_imp25 += "3rd quartile:\n\n" + tune_imp + "75\n\n";
	tune_imp25 += "Complete:\n\n" + tune_imp + "100";

	// tune_imp0 for the output string

	if (vt0) {
		var tune_imp0 = "Let's use this URL:\n\n";
		tune_imp0 += "HTML5:\n" + string + "\n\n";
		$("#tune_imp").css("height", "210px");
	}

	if (vt1) {
		var tune_imp0 = "Let's use these URLs:\n\n";
		tune_imp0 += "HTML5:\n" + string + "\n\n";
		tune_imp0 += "Video Starts:\n" + tune_imp + "0\n\n";

		//tune_imp0 += "Complete:\n" + tune_imp + "100";
		$("#tune_imp").css("height", "330px");
	}

	tune_imp0 += "Thank you";

	if (vt_ok) {
		document.getElementById("tune_imp").value = tune_imp0;
		document.getElementById("tune_imp25").value = tune_imp25;
	}
}

FpTune = function() {

	fp_message = "";

	var fp0 = document.getElementById("fp0").checked;
	var fp1 = document.getElementById("fp1").checked;

	fp_ok = false;

	if (fp0 || fp1) {
		fp_ok = true;
	} else {
		fp_message += "Select fingerprinting status!<br />";
	}

	if (fp0) {
		fp_message += "No need to change the default setup in the dash.<br />";
	}

	if (fp1) {
		
		fp_message += "AdOps will need to disable 'Only target traffic with IDFA/GAID' in the dash.<br />";

			tune_imp0 = document.getElementById("tune_imp").value;
			tune_imp1 = tune_imp0 + "\n\n@adops: please don't forget to disable 'Only target traffic with IDFA/GAID' in the dash. Thanks ";
			document.getElementById("tune_imp").value = tune_imp1;

	}

	$("#fp_message").html(fp_message);
	//$("#fp_alert").removeClass("alert");

}
checkTuneParams = function(string) {
	var pubid = "publisher_id";
	var sid = "site_id";
	var idfa = "ios_ifa=[IDFA]";
	var gaid = "google_aid=[GOOGLE_AD_ID]";
	var json = "response_format=json";
	var devip = "device_ip=[IP_ADDRESS]";
	var refid = "ref_id=[CLICK_ID]";
	var ua = "user_agent";

	var is_pubid = (string.indexOf(pubid) > -1);
	var is_sid = (string.indexOf(sid) > -1);
	var is_idfa = (string.indexOf(idfa) > -1);
	var is_gaid = (string.indexOf(gaid) > -1);
	var is_json = (string.indexOf(json) > -1);
	var is_devip = (string.indexOf(devip) > -1);
	var is_refid = (string.indexOf(refid) > -1);
	var is_ua = (string.indexOf(ua) > -1);

	// Check / re-color parameter list
	var tpid_alert = "";
	var tsid_alert = "";
	var tidfa_alert = "";
	var tgaid_alert = "";
	var tjson_alert = "";
	var tdip_alert = "";
	var tclickid_alert = "";
	var tua_alert = "";
	var all_good = false;

	if (is_pubid) {
		$("#tpid td").css("color", "green");
	} else {
		tpid_alert = "Missing / incorrect 'publisher_id' parameter!<br />";
		$("#tpid td").css("color", "#D00");
	}

	if (is_sid) {
		$("#tsid td").css("color", "green");
	} else {
		tsid_alert = "Missing / incorrect 'site_id' parameter!<br />";
		$("#tsid td").css("color", "#D00");
	}

	if (is_idfa) {
		$("#tidfa td").css("color", "green");
	} else {
		//tidfa_alert = "Missing / incorrect 'ios_ifa' parameter / [IDFA]!<br />"
		//$("#tidfa td").css("color","#D00");
	}

	if (is_gaid) {
		$("#tgaid td").css("color", "green");
	} else {
		//tgaid_alert = "Missing / incorrect 'google_aid' parameter / [GOOGLE_AD_ID]!<br />";
		//$("#tgaid td").css("color","#D00");
	}

	if (is_json) {
		$("#tjson td").css("color", "green");
	} else {
		tjson_alert = "Missing / incorrect 'response_format' parameter!<br />";
		$("#tjson td").css("color", "#D00");
	}

	if (is_devip) {
		$("#tdip td").css("color", "green");
	} else {
		tdip_alert = "Missing / incorrect 'device_ip=[IP_ADDRESS]' parameter!<br />";
		$("#tdip td").css("color", "#D00");
	}

	if (is_refid) {
		$("#tclickid td").css("color", "green");
	} else {
		tclickid_alert = "Missing / incorrect 'ref_id=[CLICK_ID]' parameter!<br />";
		$("#tclickid td").css("color", "#D00");
	}

	if (is_ua) {
		$("#tua td").css("color", "green");
	} else {
		tua_alert = "Missing / incorrect 'user_agent' parameter!<br />";
		$("#tua td").css("color", "#D00");
	}

	// iOS or Android

	if (is_idfa && is_gaid) {
		final_alert += "Both [IDFA] and [GOOGLE_AD_ID] are in the URL!<br />";
	}

	if (is_idfa && !is_gaid) {
		final_comment += "It looks like we have an iOS camapign!<br />";
		$("#tgaid td").css("color", "#000");
	}

	if (!is_idfa && is_gaid) {
		final_comment += "It looks like we have an Android camapign!<br />";
		$("#tidfa td").css("color", "#000");
	}

	if (!is_idfa && !is_gaid) {
		final_alert += "Missing / incorrect [IDFA] or [GOOGLE_AD_ID]!<br />";
		$("#tidfa td").css("color", "#D00");
		$("#tgaid td").css("color", "#D00");
	}

	// End of iOS or Android

	// All good

	all_good = is_pubid && is_sid && (is_idfa || is_gaid) && is_json;

	if (all_good) {
		final_comment += "All the parameters should be fine!<br /><br />";
		$("#vnotes_comment").css("color", "green");
	}

	final_alert += tpid_alert + tsid_alert + tidfa_alert + tgaid_alert + tjson_alert + "<br />";

	$("#vnotes_comment").html(final_comment);
	$("#vnotes_alert").html(final_alert);

	// check the other parameters, naming is important!

	check_otherparams = function(param, sub_string) {
		$("#ch_" + param).attr("disabled", false);
		var str = sub_string;
		var is_str = (string.indexOf(str) > -1);
		if (is_str) {
			$("#tr_" + param).css("color", "green");
			$("#ch_" + param).attr("checked", true);
		}
	}
	check_otherparams("mac_address", "&mac_address_sha1=[MAC_SHA1]");
	check_otherparams("odin", "&odin=[ODIN1]");
	check_otherparams("sub_pub", "&sub_publisher=[PUBLISHER_ID]");
	check_otherparams("sub_site", "&sub_site=[APP_ID]");
	check_otherparams("sub_camp", "&sub_campaign=[AD_CAMPAIGN_NAME]");
	check_otherparams("sub_adgr", "&sub_adgroup=[AD_GROUP_NAME]");
	check_otherparams("sub_ad", "&sub_ad=[AD_CREATIVE_NAME]");
	check_otherparams("dev_mod", "&device_model=[DEVICE_MODEL]");
	check_otherparams("adv_sub_camp", "&advertiser_sub_campaign=[AD_CAMPAIGN_NAME]");
	check_otherparams("adv_sub_adgr", "&advertiser_sub_adgroup=[AD_GROUP_NAME]");
	check_otherparams("adv_sub_ad", "&advertiser_sub_ad=[AD_CREATIVE_NAME]");
	check_otherparams("my_camp", "&my_campaign=[AD_CAMPAIGN_NAME]");
	check_otherparams("my_adgr", "&my_adgroup=[AD_GROUP_NAME]");
	check_otherparams("my_ad", "&my_ad=[AD_CREATIVE_NAME]");
	check_otherparams("country", "&country_code=[COUNTRY_CODE]");
	check_otherparams("ip", "&device_ip=[IP_ADDRESS]");
	check_otherparams("sub_place", "&sub_placement=[STORE_ID]");

}
add_remove_params = function(elem) {

	var id = $(elem).attr("id");
	var id2 = id.replace("ch_", "");
	var sub_str = "";

	switch(id2) {
	case "mac_address":
		sub_str = "&mac_address_sha1=[MAC_SHA1]";
		break;
	case "odin":
		sub_str = "&odin=[ODIN1]";
		break;
	case "sub_pub":
		sub_str = "&sub_publisher=[PUBLISHER_ID]";
		break;
	case "sub_site":
		sub_str = "&sub_site=[APP_ID]";
		break;
	case "sub_camp":
		sub_str = "&sub_campaign=[AD_CAMPAIGN_NAME]";
		break;
	case "sub_adgr":
		sub_str = "&sub_adgroup=[AD_GROUP_NAME]";
		break;
	case "sub_ad":
		sub_str = "&sub_ad=[AD_CREATIVE_NAME]";
		break;
	case "dev_mod":
		sub_str = "&device_model=[DEVICE_MODEL]";
		break;
	case "adv_sub_camp":
		sub_str = "&advertiser_sub_campaign=[AD_CAMPAIGN_NAME]";
		break;
	case "adv_sub_adgr":
		sub_str = "&advertiser_sub_adgroup=[AD_GROUP_NAME]";
		break;
	case "adv_sub_ad":
		sub_str = "&advertiser_sub_ad=[AD_CREATIVE_NAME]";
		break;
	case "my_camp":
		sub_str = "&my_campaign=[AD_CAMPAIGN_NAME]";
		break;
	case "my_adgr":
		sub_str = "&my_adgroup=[AD_GROUP_NAME]";
		break;
	case "my_ad":
		sub_str = "&my_ad=[AD_CREATIVE_NAME]";
		break;
	case "country":
		sub_str = "&country_code=[COUNTRY_CODE]";
		break;
	case "ip":
		sub_str = "&device_ip=[IP_ADDRESS]";
		break;
	case "sub_place":
		sub_str = "&sub_placement=[STORE_ID]";
		break;
	}

	var tr_id = id.replace("ch", "tr");
	tr_id = "#" + tr_id;

	var boxchecked = $(elem).attr("checked");
	if (boxchecked) {
		$(tr_id).css("color", "green");
		tune_url += sub_str;
		output_tags(tune_url);
	} else {
		$(tr_id).css("color", "#777");
		tune_url = tune_url.replace(sub_str, "");
		output_tags(tune_url);
	}
}

$(document).ready(function() {
	$('input[type=checkbox]').attr('disabled', 'true');
	divToggle(false, notestitle, notesdiv, notesdown, notesup);
	divToggle(true, valtitle, valdiv, valdown, valup);
	divToggle(true, urlparamstitle, urlsplit_div, urlpdown, urlpup);
	divToggle(false, tune_otherp_title, tune_otherparams, tune_otherp_down, tune_otherp_up);
	divToggle(true, imptitle, impdiv, impdown, impup);
	divToggle(false, quartiletitle, quartilediv, quardown, quarup);
	divToggle(true, pbtitle, pbdiv, pbdown, pbup);
	divToggle(true, fptitle, fpdiv, fpdown, fpup);
	divToggle(false, pbmtitle, pbmdiv, pbmdown, pbmup);
	divToggle(false, vttitle, vtdiv, vtdown, vtup);
	divToggle(false, pietitle, piediv, piedown, pieup);
});
