<?php 
header('Access-Control-Allow-Origin: *'); 
header('Access-Control-Allow-Origin: https://clients-api.adcolony.com');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="keywords" content="AdColcony, tracking" />
		<meta name="description" content="AdColony Tracking Database and Tools" />

		<title>AdColony &raquo; Tracking Knowledgebase and Tools</title>
		
		<link rel="stylesheet" type="text/css" media="all" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
		<link href="http://tamtrackingtool.com/css/style.css" rel="stylesheet" type="text/css" />
		<link rel="icon" type="image/x-icon" href="http://tamtrackingtool.com/css/rocket.ico" />
		
		<!-- Old css links below -->

		
		
		<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow&v1' rel='stylesheet' type='text/css' />
		<link href='http://fonts.googleapis.com/css?family=Wire+One&v1' rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="http://tamtrackingtool.com/vfab_files/style.css" />
		<link rel="stylesheet" type="text/css" href="http://tamtrackingtool.com/vfab_files/cloud-zoom.css" />
		<link href="http://fonts.googleapis.com/css?family=Cabin+Sketch:bold" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="http://tamtrackingtool.com/vfab_files/default.css" />
		<link rel='stylesheet' id='taylorjames_custom_style-css'  href='http://tamtrackingtool.com/vfab_files/admincss.css' type='text/css' media='all' />
		<link rel='stylesheet' id='lightboxStyle-css'  href='http://tamtrackingtool.com/vfab_files/colorbox.css' type='text/css' media='screen' />
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">

		<!--[if lt IE 7]>
		<link href="css/ie6fix.css" rel="stylesheet" type="text/css"  />
		<script src="js/ie7.js"></script>
		<script src="js/DD_belatedPNG.js"></script>
		<script>
		DD_belatedPNG.fix('img, div, span, h1, h2, h3,  h4,h5, h6, input, ul, li, form, p, a');
		</script>
		<![endif]-->
		<!--[if IE 7]>
		<link href="css/ie7fix.css" rel="stylesheet" type="text/css"  />
		<![endif]-->
		<!--[if IE 8]>
		<link href="css/ie8fix.css" rel="stylesheet" type="text/css"  />
		<![endif]-->
		
		
		<script type='text/javascript' src='http://tamtrackingtool.com/vfab_files/jquery.tools.min.js?ver=3.0.4'></script>
		<script src="http://tamtrackingtool.com/vfab_files/jquery.cycle.all.min.js"></script>
		
		<!--
		<script src="http://tamtrackingtool.com/vfab_files/include.js"></script>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  		-->
		
		<script type='text/javascript' src='test-tracking.js'></script>
		<script type='text/javascript' src='test-compare_urls.js'></script>
		
		<script>
			(function(i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] ||
				function() {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

			ga('create', 'UA-91837658-1', 'auto');
			ga('send', 'pageview');

</script>
        
	</head>
	<body>
		<div id="wrapper">
			<a name="top"></a>
			<!-- header -->
			<div id="header" class="container">
				<a href="http://tamtrackingtool.com/index.php"> <img id="logo" src="http://tamtrackingtool.com/css/ac_logo_black.png" alt="logo" /></a>
				<span id="logo_title">TEST PAGE</span>
				<img id="undcon" src="http://tamtrackingtool.com/css/mobile.png" />
				<!-- <div class="motto">_&nbsp;&nbsp;&nbsp; Creative Production Studio</div> -->
			</div>

			<!-- /header -->

			<div class="container" id="main-menu-container">
				<ul id="menu">
					<li >
						<a href="http://tamtrackingtool.com/index.php">
						<?php
						$uri = $_SERVER['REQUEST_URI'];
						if (eregi('index', $uri)) {echo "<b><span style='color: black;'>Home</span></b>";
						} else {echo "Home";
						}
						?></a>
					</li>
					<li>
						<a href="https://sites.google.com/a/adcolony.com/adcolony-internal/?pli=1" target="_blank">AdColony Internal Wiki</a>
					</li>
					<li >
						<a href="http://tamtrackingtool.com/ampage/ampage.php">
						<?php
						$uri = $_SERVER['REQUEST_URI'];
						if (eregi('ampage', $uri)) {echo "<b><span style='color: black;'>Performance AM Page</span></b>";
						} else {echo "<b><span style='color: #1a5ca6;'>Performance AM Page</span></b>";
						}
						?></a>
					</li>
					<li >
						<a href="http://tamtrackingtool.com/repapi/repapi.php">
						<?php
						$uri = $_SERVER['REQUEST_URI'];
						if (eregi('repapi', $uri)) {echo "<b><span style='color: black;'>Reporting API</span></b>";
						} else {echo "<b><span style='color: #1a5ca6'>Reporting API</span></b>";
						}
						?></a>
					</li>
					<li >
						<a href="http://tamtrackingtool.com/brand/brand.php">
						<?php
						$uri = $_SERVER['REQUEST_URI'];
						if (eregi('brand', $uri) || eregi('nielsen', $uri)) {echo "<b><span style='color: black;'>Brand Tools</span></b>";
						} else {echo "<b><span style='color: rgb(147, 27, 28)'>Brand Tools</span></b>";
						}
						?></a>
					</li>
					
					<!--
					<li >
						<a href="">
						<?php
						$uri = $_SERVER['REQUEST_URI'];
						if (eregi('trackingpartners', $uri)) {echo "<b><span style='color: black;'>Tracking partners</span></b>";
						} else {echo "Tracking partners";
						}
						?></a>
					</li>
				-->
				</ul>
			</div>

