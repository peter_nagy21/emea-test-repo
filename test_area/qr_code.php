<?php
include ("http://localhost:8888/password_protect.php");
?>

<?php
include '../header.php';
?>

<?php
include '../sideleft.php';
?>

<?php include_once("analyticstracking.php")
?>

<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="http://code.jquery.com/mobile/1.1.0/jquery.mobile-1.1.0.min.js"></script>
<script type="text/javascript">
	function setChartImage() {
		$("#chart").show();
		var query = {
			cht : "qr",
			choe : "UTF-8",
			chs : $("#size").val(),
			chld : $(":radio[name='ec']:checked").val(),
			chl : $("#text").val()
		};
		var url = "http://chart.apis.google.com/chart?" + $.param(query);

		$("#chart").attr('src', url);
		$("#url").val(url);
		$("#link").attr('href', url);
	}
</script>
<style type="text/css">
	img#chart {
		margin: 0;
		background: #fff;
		border: 1px solid black;
	}
</style>
<title>QR code generator</title>

<div id="maincontent">

	<div data-role="header">
		<h1>QR code Generator (UTF-8)</h1>
	</div>
	<div data-role="content">
		<ul data-role="listview">
			<li>
				<label for="size">Size</label>
				<select id="size" onchange="setChartImage()" style="display: none;">
					<option value="50x50">50x50</option>
					<option value="75x75">75x75</option>
					<option value="100x100">100x100</option>
					<option value="150x150">150x150</option>
					<option value="300x300" selected="">300x300</option>
				</select>
			</li>
			<li>
				<fieldset data-role="controlgroup" data-type="horizontal" style="display: none;">
					<legend>
						Error correction
					</legend>
					<input type="radio" name="ec" id="ec-L" value="L" onclick="setChartImage()" />
					<label for="ec-L">7%</label>
					<input type="radio" name="ec" id="ec-M" value="M" onclick="setChartImage()" />
					<label for="ec-M">15%</label>
					<input type="radio" name="ec" id="ec-Q" value="Q" onclick="setChartImage()" />
					<label for="ec-Q">25%</label>
					<input type="radio" name="ec" id="ec-H" value="H" onclick="setChartImage()" checked="checked" />
					<label for="ec-H">30%</label>
				</fieldset>
			</li>
			<li>
				Paste the trackign URL here:
				<br />
				<br />
				<textarea id="text" cols="20" rows="5"></textarea>
				<br />
				<br />
			</li>
			<li>
				<button class="btn-class" data-theme="b" onclick="setChartImage()">
					Generate QR code
				</button>
				<br />
			</li>
			<li>
				<div>
					<a id="link" href="#"><img id="chart" src="" alt="QR code" style="display: none;" /></a>
				</div>
			</li>
		</ul>
	</div>

</div>

<?php
include '../footer.php';
?>