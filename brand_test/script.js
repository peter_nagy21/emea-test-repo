function fixUrl() {
	// grab url from textbox
	var url = document.getElementById('textbox').value;
	// get the url parameters
	var parameters = getParameters(url);
    // create table of parameters
	createTable(parameters);

	document.getElementById('demo').innerHTML = url.split('?')[0] + '?';

    for (var i = 0; i < parameters.length; i++) {
    	var newSpan = document.createElement('span');
    	newSpan.innerHTML = parameters[i].value;
    	newSpan.id = 'parameter-value'+[i];
    	document.getElementById('demo').innerHTML += parameters[i].name + '=';
        document.getElementById('demo').appendChild(newSpan);
        if (i != parameters.length - 1){
        	document.getElementById('demo').innerHTML += '&';
        }
    }
}

function createTable(parameters) {

	var html = '<table class="table table-bordered">';
    for (var i = 0; i < parameters.length; i++) {
        html+='<tr>';
        html+='<td>'+parameters[i].name+'</td>';
        html+='<td><input type="text" class="form-control" oninput="updateParameter('+i+', this)" value='+parameters[i].value+' list="datalist1" ></td>';
        html+='</tr>';
    }

    html+='</table>';

    $('#table').html(html);
}

function getParameters(url) {
	var parameters = url.split('?')[1].split('&');
	return parameters.map(function(index){
		return {name: index.split('=')[0], value: index.split('=')[1]};
	});
}

function updateParameter(i, parameter) {
	document.getElementById('parameter-value'+i).innerHTML = parameter.value;
}