<?php
include ("http://localhost:8888/password_protect.php");
?>

<?php
include 'header.php';
?>

<?php
include 'sideleft.php';
?>

<?php include_once("analyticstracking.php")
?>

<div id="maincontent">
	<div id="geturl1">
		<h3 class="black" id="geturl_title">URL comparison Tool &nbsp;&nbsp;
		<input type="button" class="btn-class" value="Compare URLs" id="comp_url_toggle">
		</h3>

		Paste the URL below:
		<br />
		<form id="valform">
			<textarea name="input_url" id="input_url"></textarea>			
			
 <input type="button" class="btn-class" value="Check URL" onclick="checkURLgen('input_url')">

			<input type="button" class="btn-class" value="Get parameters" onclick="getURLparams('input_url',urlsplitdiv)">
			<input type="button" class="btn-class" value="Sort parameters" onclick="getURLparamsSort('input_url',urlsplitdiv)">
			<br />
			<br />
		</form>
		<div id="urlsplitdiv"></div>

	</div>

	<div id="geturl2">
		<h3 class="black"></h3>
		<br />

		Paste the URL below:
		<br />
		<form id="valform2">
			<textarea name="input_url2" id="input_url2"></textarea>
			<input type="button" class="btn-class" value="Compare!" id="compare_button" onclick="compareURLs1()">
			<input type="button" class="btn-class" value="Compare and sort A-Z!" id="compare_button" onclick="compareURLs2()">
			<!--
			<input type="button" class="btn-class" value="Get parameters" onclick="getURLparams('input_url2',urlsplitdiv2)">
			<input type="button" class="btn-class" value="Sort parameters" onclick="getURLparamsSort('input_url2',urlsplitdiv2)">
			-->
			<br />
			<br />
		</form>
		<div id="urlsplitdiv2"></div>
	</div>
	<div style="clear: both;"></div>

	<div id="samediv">
		<span id="samealert"></span>
	</div>

	<div id="keydiv" style="display: none;">
		<span style="font-weight: bold; color: black; font-size: 16px;">Key:</span>
		<br />
		<table id="keytable" style="margin-top: 5px;">
			<tr>
				<td style='color: green; background-color: #DFD;'>Green:</td>
				<td>All good!</td>
			</tr>
			<tr>
				<td style='color: #C90; background-color: #FFE9AD;'>Orange:</td>
				<td>Same parameter, different value</td>
			</tr>
			<tr>
				<td style='color: #D00; background-color: #FFADB1;'>Red:</td>
				<td>Different parameter / base URL</td>
			</tr>

		</table>
		<br />
	</div>

	<div id="redurldiv"></div>
	<div id="vnotes_comment"></div>
	<div id="vnotes_alert"></div>
	
	<h3 class="bold" style='color: rgb(147, 27, 28); font-weight: bold !important;'>Validation instructions</h3>
	<div id="val_instr">
		<table class="val_table">
			<tr>
				<td><a href="https://docs.google.com/document/d/1fz_SkDZUctisb8XwL24cqerVSo1CWly7koSA9gAQyGs/" target="_blank"><img src="../css/logo_tune1.png" class="logo_track"/></a></td>
				<td><a href="https://docs.google.com/document/d/1qhkU3fhmvUJO8GqJ5CZq5Yc_AGxklvwo8udxSePR8ps/" target="_blank"><img src="../css/logo_adj1.png" class="logo_track"/></a></td>
				<td><a href="https://docs.google.com/document/d/1PrGtNU9SxnXiNDkLUz2Fic2nCZij2UqVCp7UmT-tOiM/" target="_blank"><img src="../css/logo_appsfl1.png" class="logo_track"/></a></td>
				<td><a href="https://docs.google.com/document/d/1lHUd_zl4wgjzZlM3zCaUFUwD_6Ubv7bTLvxd7hu2eeQ/" target="_blank"><img src="../css/logo_koch1.png" class="logo_track"/></a></td>
			</tr>
		</table>
		
		<!--

		Validating a TUNE link
		<br />
		Validating an Adjust link
		<br />
		Validating an AppsFlyer link
		<br />
		Validating a Kochava link
		<br />
		
		-->
		
		<br />
	</div>

	<h3 class="black" id="uninh3">Uninstall process on Android</h3>
	<ol style="list-style: inherit;">
		<li>
			Uninstall the app
		</li>
		<li>
			Accounts -> remove the google account
		</li>
		<li>
			reset the google ad id (GAID)
		</li>
		<li>
			kill google play store (clear data)
		</li>
		<li>
			kill the test app (clear data)
		</li>
	</ol>
	<br />
	<!--
	<h3 class="black">URL decoder / encoder tool</h3>
	<textarea name="dec_url_in" id="dec_url_in"></textarea>
	<input type="button" class="btn-class" value="Decode" onclick="decodeURL('dec_url_in',dec_url_out)"/>
	<input type="button" class="btn-class" value="Encode" onclick="encodeURL('dec_url_in',dec_url_out)"/><br /><br />
	<textarea name="dec_url_out" id="dec_url_out"></textarea>
	<br />
	-->
	<h3 class="black">Useful links</h3>

	<a class="blue" href="https://docs.google.com/document/d/1x3ABDVaR-Xy5lnCxdr9ej6DHT_ol-Il62pnXEd_6dak/edit" target="_blank">Jessi's - Partner notes</a>
	<br />
	<a class="blue" href="https://docs.google.com/spreadsheets/d/1TDx5lXO57IbAigODRN0miueYbI3-cswGBKDesQ14blY/edit?ts=568cf7a4#gid=0" target="_blank">Login details of different accounts</a>
	<br />
	<a class="blue" href="https://docs.google.com/spreadsheets/d/1iD3ezrKDVpiITO7VGhLsymI2y1NxdE54eAJJ-smPFVQ/edit#gid=0" target="_blank">Jessi's - List of tracking parameters</a>
	<br />
	<a class="blue" href="https://docs.google.com/spreadsheets/d/1T5uFxml-w-74V8Yro6C8RKEbaVkk9VfLJspKt6uny2w/edit#gid=0" target="_blank">Jessi's - Tracking Partners + View Through</a>
	<br />
	<a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ua/macros-external-document" target="_blank">MACROs</a>
	<br />
	<a class="blue" href="https://docs.google.com/spreadsheets/d/1uExJLD_Ql7EjZz2IIJnv02-Y3nMoKZWJVnKAqy7yqd4/edit?ts=569446e4#gid=0" target="_blank">API keys</a>
	<br />
	<a class="blue" href="http://cpa.adtilt.com/most_recent_actions?api_key=bb2cf0647ba654d7228dd3f9405bbc6a&product_id=" target="_blank">Install feed URL</a>
	<br />
	<a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ad-ops/3rd-party-dashboards" target="_blank">3rd party dashboards</a>
	<br />
	<a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ad-ops/vpn-to-another-country" target="_blank">VPN to Another Country</a>
	<br />
	<a class="blue" href="https://docs.google.com/spreadsheets/d/1ZUL3K6kqQYzqwDglnRirUPHJer1fxraDWSkgdha-dyg/edit?ts=569e97f2#gid=0" target="_blank">Partner Speadsheet</a>
	<br />
	<a class="blue" href="http://support.adcolony.com/customer/portal/articles/1914521-postbacks-for-top-tracking-partners" target="_blank">Post Install Events for Top Tracking Partners</a>
	<br />
	<a class="blue" href="http://tamtrackingtool.com/css/RestCheatSheet2.png" target="_blank">HTTP status codes</a>
	<br />

	<!--

	<h3 class="black">Compare URLs</h3>
	<textarea name="comp_url1" id="comp_url1"></textarea>
	<br />
	<textarea name="comp_url2" id="comp_url2"></textarea>
	<br />
	<input type="button" class="btn-class" value="Compare!" id="compare_URLs_button" onclick="compareURLs()">
	<br />
	<br />
	<div id="output_tables">
	<div id="output_tables1"></div>
	<div id="output_tables2"></div>
	</div>

	-->

</div>

<?php
include 'footer.php';
?>

