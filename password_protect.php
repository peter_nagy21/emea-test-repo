<?php

###############################################################
# Page Password Protect 2.13
###############################################################
# Visit http://www.zubrag.com/scripts/ for updates
############################################################### 
#
# Usage:
# Set usernames / passwords below between SETTINGS START and SETTINGS END.
# Open it in browser with "help" parameter to get the code
# to add to all files being protected. 
#    Example: password_protect.php?help
# Include protection string which it gave you into every file that needs to be protected
#
# Add following HTML code to your page where you want to have logout link
# <a href="http://www.example.com/path/to/protected/page.php?logout=1">Logout</a>
#
###############################################################

/*
-------------------------------------------------------------------
SAMPLE if you only want to request login and password on login form.
Each row represents different user.

$LOGIN_INFORMATION = array(
  'zubrag' => 'root',
  'test' => 'testpass',
  'admin' => 'passwd'
);

--------------------------------------------------------------------
SAMPLE if you only want to request only password on login form.
Note: only passwords are listed

$LOGIN_INFORMATION = array(
  'root',
  'testpass',
  'passwd'
);

--------------------------------------------------------------------
*/

##################################################################
#  SETTINGS START
##################################################################

// Add login/password pairs below, like described above
// NOTE: all rows except last must have comma "," at the end of line
$LOGIN_INFORMATION = array(
  'zubrag' => 'root',
  'admin' => 'trackpass21'
);

// request login? true - show login and password boxes, false - password box only
define('USE_USERNAME', false);

// User will be redirected to this page after logout
define('LOGOUT_URL', 'http://www.example.com/');

// time out after NN minutes of inactivity. Set to 0 to not timeout
define('TIMEOUT_MINUTES', 0);

// This parameter is only useful when TIMEOUT_MINUTES is not zero
// true - timeout time from last activity, false - timeout time from login
define('TIMEOUT_CHECK_ACTIVITY', true);

##################################################################
#  SETTINGS END
##################################################################


///////////////////////////////////////////////////////
// do not change code below
///////////////////////////////////////////////////////

// show usage example
if(isset($_GET['help'])) {
  die('Include following code into every page you would like to protect, at the very beginning (first line):<br>&lt;?php include("' . str_replace('\\','\\\\',__FILE__) . '"); ?&gt;');
}

// timeout in seconds
$timeout = (TIMEOUT_MINUTES == 0 ? 0 : time() + TIMEOUT_MINUTES * 60);

// logout?
if(isset($_GET['logout'])) {
  setcookie("verify", '', $timeout, '/'); // clear password;
  header('Location: ' . LOGOUT_URL);
  exit();
}

if(!function_exists('showLoginPasswordProtect')) {

// show login form
function showLoginPasswordProtect($error_msg) {
?>
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="keywords" content="AdColcony, tracking" />
		<meta name="description" content="AdColony Tracking Database and Tools" />

		<title>AdColony &raquo; Tracking Knowledgebase and Tools</title>
		
		<link rel="stylesheet" type="text/css" media="all" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
		<link href="http://tamtrackingtool.com/css/style.css" rel="stylesheet" type="text/css" />
		<link rel="icon" type="image/x-icon" href="http://tamtrackingtool.com/css/rocket.ico" />
		
		<!-- Old css links below -->

		
		
		<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow&v1' rel='stylesheet' type='text/css' />
		<link href='http://fonts.googleapis.com/css?family=Wire+One&v1' rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="http://tamtrackingtool.com/vfab_files/style.css" />
		<link rel="stylesheet" type="text/css" href="http://tamtrackingtool.com/vfab_files/cloud-zoom.css" />
		<link href="http://fonts.googleapis.com/css?family=Cabin+Sketch:bold" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="http://tamtrackingtool.com/vfab_files/default.css" />
		<link rel='stylesheet' id='taylorjames_custom_style-css'  href='http://tamtrackingtool.com/vfab_files/admincss.css' type='text/css' media='all' />
		<link rel='stylesheet' id='lightboxStyle-css'  href='http://tamtrackingtool.com/vfab_files/colorbox.css' type='text/css' media='screen' />
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">

		<!--[if lt IE 7]>
		<link href="css/ie6fix.css" rel="stylesheet" type="text/css"  />
		<script src="js/ie7.js"></script>
		<script src="js/DD_belatedPNG.js"></script>
		<script>
		DD_belatedPNG.fix('img, div, span, h1, h2, h3,  h4,h5, h6, input, ul, li, form, p, a');
		</script>
		<![endif]-->
		<!--[if IE 7]>
		<link href="css/ie7fix.css" rel="stylesheet" type="text/css"  />
		<![endif]-->
		<!--[if IE 8]>
		<link href="css/ie8fix.css" rel="stylesheet" type="text/css"  />
		<![endif]-->
		
		
		<script type='text/javascript' src='http://tamtrackingtool.com/vfab_files/jquery.tools.min.js?ver=3.0.4'></script>
		<script src="http://tamtrackingtool.com/vfab_files/jquery.cycle.all.min.js"></script>
		
		<!--
		<script src="http://tamtrackingtool.com/vfab_files/include.js"></script>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  		-->
		
		<script type='text/javascript' src='http://tamtrackingtool.com/js/tracking.js'></script>
		<script type='text/javascript' src='http://tamtrackingtool.com/vfab_files/compare_urls.js'></script>
        
	</head>
<body>
	<div id="wrapper">
	<div id="header" class="container">
				<a href="http://tamtrackingtool.com/index.php"> <img id="logo" src="http://tamtrackingtool.com/css/ac_logo_black.png" alt="logo" /></a>
				<span id="logo_title">Tracking Knowledgebase and Tools</span>
				<img id="undcon" src="http://tamtrackingtool.com/css/mobile.png" />
				<!-- <div class="motto">_&nbsp;&nbsp;&nbsp; Creative Production Studio</div> -->
			</div>
			</div>
  <style>
	input {
		border: 1px solid black;
	}
  </style>
  <div style="width:500px; margin-left:auto; margin-right:auto; text-align:center; margin-top: 60px;">
  <form method="post">
    <h3>Please enter password to access this page</h3>
    <font color="red"><?php echo $error_msg; ?></font><br />
<?php
if (USE_USERNAME)
	echo 'Login:<br /><input type="input" name="access_login" /><br />Password:<br />';
 ?>
    <input type="password" name="access_password" /><p></p><input type="submit" name="Submit" value="Submit" />
  </form>
  <br />
  <span style="font-size:10px; color: #B0B0B0; font-family: Verdana, Arial;">To request a password / If you have forgotten the password - please contact Peter (<a href="mailto:peter.nagy@adcolony.com" target="_top">peter.nagy@adcolony.com</a>)</span>
  </div>
</body>
</html>

<?php
// stop at this point
die();
}
}

// user provided password
if (isset($_POST['access_password'])) {

$login = isset($_POST['access_login']) ? $_POST['access_login'] : '';
$pass = $_POST['access_password'];
if (!USE_USERNAME && !in_array($pass, $LOGIN_INFORMATION)
|| (USE_USERNAME && ( !array_key_exists($login, $LOGIN_INFORMATION) || $LOGIN_INFORMATION[$login] != $pass ) )
) {
showLoginPasswordProtect("Incorrect password.");
}
else {
// set cookie if password was validated
setcookie("verify", md5($login.'%'.$pass), $timeout, '/');

// Some programs (like Form1 Bilder) check $_POST array to see if parameters passed
// So need to clear password protector variables
unset($_POST['access_login']);
unset($_POST['access_password']);
unset($_POST['Submit']);
}

}

else {

// check if password cookie is set
if (!isset($_COOKIE['verify'])) {
showLoginPasswordProtect("");
}

// check if cookie is good
$found = false;
foreach($LOGIN_INFORMATION as $key=>$val) {
$lp = (USE_USERNAME ? $key : '') .'%'.$val;
if ($_COOKIE['verify'] == md5($lp)) {
$found = true;
// prolong timeout
if (TIMEOUT_CHECK_ACTIVITY) {
setcookie("verify", md5($lp), $timeout, '/');
}
break;
}
}
if (!$found) {
showLoginPasswordProtect("");
}

}
?>
