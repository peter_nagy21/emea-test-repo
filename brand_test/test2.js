function getCORS(url, success) {
    var xhr = new XMLHttpRequest();
    if (!('withCredentials' in xhr)) xhr = new XDomainRequest(); // fix IE8/9
    xhr.open('GET', url);
    xhr.onload = success;
    xhr.send();
    return xhr;
}

// example request
getCORS('https://clients-api.adcolony.com/api/v2/advertiser_summary?user_credentials=W0KYONhCiK0usqvGFy1B&date=03102017&format=json&group_by=campaign,ad_group', function(request){
    var response = request.currentTarget.response || request.target.responseText;
    console.log(response);
});



// working without CORS

var arr = [];
$(document).ready(function () {
    $.ajax({ 
        type: 'GET', 
       // url: 'https://clients-api.adcolony.com/api/v2/advertiser_summary?user_credentials=W0KYONhCiK0usqvGFy1B&date=03132017&format=json', 
        url: 'https://clients-api.adcolony.com/api/v2/advertiser_summary?user_credentials=W0KYONhCiK0usqvGFy1B&date=03102017&format=json&group_by=campaign,ad_group',
        data: { get_param: 'value' }, 
        success: function (data) { 
            var names = data;
            arr = Object.values(data);
           arr = arr[1];
            console.log('length: ' + arr.length);     
            
        buildHtmlTable('#excelDataTable');
        }
    });
});

// Builds the HTML Table out of arr.
function buildHtmlTable(selector) {
  var columns = addAllColumnHeaders(arr, selector);

  for (var i = 0; i < arr.length; i++) {
    var row$ = $('<tr/>');
    for (var colIndex = 0; colIndex < columns.length; colIndex++) {
      var cellValue = arr[i][columns[colIndex]];
      if (cellValue == null) cellValue = "";
      row$.append($('<td/>').html(cellValue));
    }
    $(selector).append(row$);
  }
}

// Adds a header row to the table and returns the set of columns.
// Need to do union of keys from all records as some records may not contain
// all records.
function addAllColumnHeaders(arr, selector) {
  var columnSet = [];
  var headerTr$ = $('<tr/>');

  for (var i = 0; i < arr.length; i++) {
    var rowHash = arr[i];
    for (var key in rowHash) {
      if ($.inArray(key, columnSet) == -1) {
        columnSet.push(key);
        headerTr$.append($('<th/>').html(key));
      }
    }
  }
  $(selector).append(headerTr$);

  return columnSet;
}
