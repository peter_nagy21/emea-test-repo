output = "";
base_url = "";

function fixUrl() {
	// grab url from textbox
	var url = document.getElementById('textbox').value;
	// get the url parameters
	parameters = getParameters(url);
	// create table of parameters
	createTable(parameters);
	
	base_url = url.split('?')[0] + '?';
	
	display_text();
}


function createTable(parameters) {

	var html = '<table class="table table-bordered">';
	for (var i = 0; i < parameters.length; i++) {
		html += '<tr>';
		html += '<td>' + parameters[i].name + '</td>';
		html += '<td><input type"text" class="form-control" oninput="updateParameter(' + i + ', this)" value=' + parameters[i].value + ' list="datalist1" ></td>';
		html += '</tr>';

	}

	html += '</table>';

	$('#table1').html(html);
}

function display_text(){
	//var raw_html = document.getElementById("demo").innerText;
	//document.getElementById("text_output").innerHTML = raw_html;
	
	output = base_url;
	
	for (var i = 0; i < parameters.length; i++) {
		output += parameters[i].name + "=" + parameters[i].value + "&";
	}
	
	output = output.substring(0,output.length-1);
	
	output += "<br /><br />";
	
	document.getElementById("text_output").innerHTML = output;
	
}

function getParameters(url) {
	var parameters = url.split('?')[1].split('&');
	return parameters.map(function(index) {
		return {
			name : index.split('=')[0],
			value : index.split('=')[1]
		};
	});
}

function updateParameter(i, parameter) {
	//document.getElementById('parameter-value' + i).innerHTML = parameter.value;
	parameters[i].value = parameter.value;
	display_text();
}

$(document).ready(function() {
	divToggle(false, mtitle, mdiv, mdown, mup);
});
