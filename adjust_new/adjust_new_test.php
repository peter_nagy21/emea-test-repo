<?php
include ("http://localhost:8888/password_protect.php");
?>

<?php
include '../header.php';
?>

<?php
include '../sideleft.php';
?>

<?php include_once("analyticstracking.php") ?>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css" href="css/main.css" />

<div id="maincontent">
	
	<h1>Adjust new page Test</h1>
	
	<h3 class="black plink" id="notestitle">Example URLs <i class="fa fa-arrow-down" id="notesdown"></i><i class="fa fa-arrow-up" id="notesup"></i></h3>
	<div id="notesdiv" class="ndiv">
		<ul>	
			<br />
			<li>
				EXAMPLE 1:<br /> 
				https://app.adjust.com/eez5w4
			</li>
			<br />
			<li>
				EXAMPLE 2:<br /> 
				https://app.adjust.com/1tfsk7?s2s=1&idfa=[IDFA]&install_callback=https%3A%2F%2Fcpa.adcolony.com%2Fon_user_action%3Fapi_key%3D7d442b42abea4549de732321531487fe%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D&campaign=[AD_GROUP_NAME]&adgroup=[APP_ID]&creative=[AD_CREATIVE_NAME]&label=sub_publisher%3D[PUBLISHER_ID]%26Sub_site%3D[APP_ID]%26MobileApp%3D[STORE_ID]%26CreativeID%3D[RAW_AD_CREATIVE_ID]%26Partner_id%3D8&tracker_limit=100000
			</li>
		</ul>
		<br />
	</div>
	

	<div class="container">
		<!-- Example row of columns -->
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<input class="form-control" id="url" placeholder="Insert URL here">
					</input>
				</div>
				<button type="button" onclick="validate(this)" class="btn btn-default">
					iOS
				</button>
				<button type="button" onclick="validate(this)" class="btn btn-success">
					Android
				</button>
			</div>
			<div class="col-md-12">
				<div id="base-url"></div>
				<div id="table"></div>
			</div>
			<div class="col-md-6">
				<div id="add-pie"></div>
			</div>
			<div class="col-md-12">
				<div id="pie"></div>
			</div>
			<div class="col-md-12">
				<div id="os"></div>
			</div>
			<div class="col-md-12">
				<div id="complete-url"></div>
			</div>
		</div>
		<datalist id="datalist1">
			<option value="[DEVICE_ID]"><option value="[IDFA]"><option value="[MD5_IDFA]"><option value="[SHA1_UC_ADVERTISER_ID]"><option value="[SHA1_ADVERTISER_ID]"><option value="[ODIN1]"><option value="[MAC_SHA1]"><option value="[GOOGLE_AD_ID]"><option value="[SHA1_ANDROID_ID]"><option value="[SHA1_IMEI]"><option value="[PRODUCT_ID]"><option value="[APP_ID]"><option value="[RAW_APP_ID]"><option value="[APP_NAME]"><option value="[RAW_AD_CAMPAIGN_ID]"><option value="[AD_CAMPAIGN_NAME]"><option value="[RAW_AD_GROUP_ID]"><option value="[AD_GROUP_NAME]"><option value="[RAW_AD_CREATIVE_ID]"><option value="[AD_CREATIVE_NAME]"><option value="[ADC_VERSION]"><option value="[OS_VERSION]"><option value="[PLATFORM]"><option value="[DEVICE_MODEL]"><option value="[DEVICE_GROUP]"><option value="[NETWORK_TYPE]"><option value="[LANGUAGE]"><option value="[SERVE_TIME]"><option value="[DMA_CODE]"><option value="[COUNTRY_CODE]"><option value="[COUNTRY_CODE_UK]"><option value="[ZONE_TYPE]"><option value="[ZONE_UUID]"><option value="[PUBLISHER_ID]"><option value="[PUBLISHER_NAME]"><option value="[ADCOLONY_TIMESTAMP]"><option value="[ADCOLONY_TIMESTAMP_MILLIS]"><option value="[IP_ADDRESS]"><option value="[STORE_ID]"><option value="[BID]"><option value="[BID_TYPE]"><option value="[TRANS_ID]"><option value="[CLICK_ID]"><option value="[USER_AGENT_MOZILLA]"><option value="[USER_AGENT]">
		</datalist>
		<hr>
	</div>
	<!-- /container -->

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/pie.js"></script>
<script src="js/main.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
	( function(b, o, i, l, e, r) {
			b.GoogleAnalyticsObject = l;
			b[l] || (b[l] = function() {
				(b[l].q = b[l].q || []).push(arguments)
			});
			b[l].l = +new Date;
			e = o.createElement(i);
			r = o.getElementsByTagName(i)[0];
			e.src = '//www.google-analytics.com/analytics.js';
			r.parentNode.insertBefore(e, r)
		}(window, document, 'script', 'ga'));
	ga('create', 'UA-XXXXX-X', 'auto');
	ga('send', 'pageview'); 
</script>

<?php
include '../footer.php';
?>

