<?php
include ("localhost:8888/password_protect.php");
include 'header.php';
include 'sideleft.php';
include_once("analyticstracking.php")
?>

<div id="maincontent">
	
	<!-- Max's URL tool -->
	
	<link href="maxtools/style_max.css" rel="stylesheet">
	<link href="maxtools/style_max_nobootstrap.css" rel="stylesheet">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="maxtools/url_editor/script.js"></script>
	<!-- Bootstrap core CSS
	<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
	 -->
	
	
	<!-- END OF Max's URL tool -->
	
	<div id="geturl1">
		<h3 class="black" id="geturl_title">URL comparison Tool &nbsp;&nbsp;
		<input type="button" class="btn-class" value="Compare URLs" id="comp_url_toggle">
		</h3>

		Paste the URL below:
		<br />
		<form id="valform">
			<textarea name="input_url" id="input_url"></textarea>			
			
 <input type="button" class="btn-class" value="Check URL" onclick="checkURLgen('input_url')">

			<input type="button" class="btn-class" value="Get parameters" onclick="getURLparams('input_url',urlsplitdiv)">
			<input type="button" class="btn-class" value="Sort parameters" onclick="getURLparamsSort('input_url',urlsplitdiv)">
			<br />
			<br />
		</form>
		<div id="urlsplitdiv"></div>

	</div>

	<div id="geturl2">
		<h3 class="black"></h3>
		<br />

		Paste the URL below:
		<br />
		<form id="valform2">
			<textarea name="input_url2" id="input_url2"></textarea>
			<input type="button" class="btn-class" value="Compare!" id="compare_button" onclick="compareURLs1()">
			<input type="button" class="btn-class" value="Compare and sort A-Z!" id="compare_button" onclick="compareURLs2()">
			<!--
			<input type="button" class="btn-class" value="Get parameters" onclick="getURLparams('input_url2',urlsplitdiv2)">
			<input type="button" class="btn-class" value="Sort parameters" onclick="getURLparamsSort('input_url2',urlsplitdiv2)">
			-->
			<br />
			<br />
		</form>
		<div id="urlsplitdiv2"></div>
	</div>
	<div style="clear: both;"></div>

	<div id="samediv">
		<span id="samealert"></span>
	</div>

	<div id="keydiv" style="display: none;">
		<span style="font-weight: bold; color: black; font-size: 16px;">Key:</span>
		<br />
		<table id="keytable" style="margin-top: 5px;">
			<tr>
				<td style='color: green; background-color: #DFD;'>Green:</td>
				<td>All good!</td>
			</tr>
			<tr>
				<td style='color: #C90; background-color: #FFE9AD;'>Orange:</td>
				<td>Same parameter, different value</td>
			</tr>
			<tr>
				<td style='color: #D00; background-color: #FFADB1;'>Red:</td>
				<td>Different parameter / base URL</td>
			</tr>

		</table>
		<br />
	</div>

	<div id="redurldiv"></div>
	<div id="vnotes_comment"></div>
	<div id="vnotes_alert"></div>
	
	<br />
	
	<h3 class="black plink" id="mtitle"><span style="text-transform: none;">Max's</span> URL editor <i class="fa fa-arrow-down" id="mdown"></i><i class="fa fa-arrow-up" id="mup"></i></h3>
	<div id="mdiv">
		<div class="form-group">
			
			<p>
				Paste your URL to edit below:
			</p>
			<textarea rows="4" type="text" class="form-control" name="textbox" id="textbox"></textarea>
		</div>
		<br />
		<div class="row">
			<div class="col-sm-12">
				<input class="btn-class" onchange="myFunction()" type="submit" name="button" id="edit-url-button" onclick="fixUrl()" value="EDIT URL"/>
			</div>
		</div>
		<p id="demo"></p>
		<div id="text_output"></div>
		<div id="table1">
		</div>
		
		<datalist id="datalist1">
			<option value="[DEVICE_ID]"><option value="[IDFA]"><option value="[MD5_IDFA]"><option value="[SHA1_UC_ADVERTISER_ID]"><option value="[SHA1_ADVERTISER_ID]"><option value="[ODIN1]"><option value="[MAC_SHA1]"><option value="[GOOGLE_AD_ID]"><option value="[SHA1_ANDROID_ID]"><option value="[SHA1_IMEI]"><option value="[PRODUCT_ID]"><option value="[APP_ID]"><option value="[RAW_APP_ID]"><option value="[APP_NAME]"><option value="[RAW_AD_CAMPAIGN_ID]"><option value="[AD_CAMPAIGN_NAME]"><option value="[RAW_AD_GROUP_ID]"><option value="[AD_GROUP_NAME]"><option value="[RAW_AD_CREATIVE_ID]"><option value="[AD_CREATIVE_NAME]"><option value="[ADC_VERSION]"><option value="[OS_VERSION]"><option value="[PLATFORM]"><option value="[DEVICE_MODEL]"><option value="[DEVICE_GROUP]"><option value="[NETWORK_TYPE]"><option value="[LANGUAGE]"><option value="[SERVE_TIME]"><option value="[DMA_CODE]"><option value="[COUNTRY_CODE]"><option value="[COUNTRY_CODE_UK]"><option value="[ZONE_TYPE]"><option value="[ZONE_UUID]"><option value="[PUBLISHER_ID]"><option value="[PUBLISHER_NAME]"><option value="[ADCOLONY_TIMESTAMP]"><option value="[ADCOLONY_TIMESTAMP_MILLIS]"><option value="[IP_ADDRESS]"><option value="[STORE_ID]"><option value="[BID]"><option value="[BID_TYPE]"><option value="[TRANS_ID]"><option value="[CLICK_ID]"><option value="[USER_AGENT_MOZILLA]"><option value="[USER_AGENT]">
		</datalist>

		<div class="row">
			<div class="col-sm-12">	
			</div>
		</div>
		<br />
			
	</div>
	
			
	
	<!--
	<h3 class="bold" style='color: rgb(147, 27, 28); font-weight: bold !important;'>Validation instructions</h3>
	<div id="val_instr">
		<table class="val_table">
			<tr>
				<td><a href="https://docs.google.com/document/d/1fz_SkDZUctisb8XwL24cqerVSo1CWly7koSA9gAQyGs/" target="_blank"><img src="../css/logo_tune1.png" class="logo_track"/></a></td>
				<td><a href="https://docs.google.com/document/d/1qhkU3fhmvUJO8GqJ5CZq5Yc_AGxklvwo8udxSePR8ps/" target="_blank"><img src="../css/logo_adj1.png" class="logo_track"/></a></td>
				<td><a href="https://docs.google.com/document/d/1PrGtNU9SxnXiNDkLUz2Fic2nCZij2UqVCp7UmT-tOiM/" target="_blank"><img src="../css/logo_appsfl1.png" class="logo_track"/></a></td>
				<td><a href="https://docs.google.com/document/d/1lHUd_zl4wgjzZlM3zCaUFUwD_6Ubv7bTLvxd7hu2eeQ/" target="_blank"><img src="../css/logo_koch1.png" class="logo_track"/></a></td>
			</tr>
		</table>
		
		
		
		<br />
	</div>
	-->
	
	<h3 class="black">Useful links</h3>
	
	<a class="blue" href="https://confluence.adcolony.net/display/TP/Reset+Device+ID" target="_blank">Reset Device ID</a>
	<br />
	<a class="blue" href="https://docs.google.com/document/d/1x3ABDVaR-Xy5lnCxdr9ej6DHT_ol-Il62pnXEd_6dak/edit" target="_blank">Jessi's - Partner notes</a>
	<br />
	<a class="blue" href="https://docs.google.com/spreadsheets/d/1TDx5lXO57IbAigODRN0miueYbI3-cswGBKDesQ14blY/edit?ts=568cf7a4#gid=0" target="_blank">Login details of different accounts</a>
	<br />
	<a class="blue" href="https://docs.google.com/spreadsheets/d/1iD3ezrKDVpiITO7VGhLsymI2y1NxdE54eAJJ-smPFVQ/edit#gid=0" target="_blank">Jessi's - List of tracking parameters</a>
	<br />
	<a class="blue" href="https://docs.google.com/spreadsheets/d/1T5uFxml-w-74V8Yro6C8RKEbaVkk9VfLJspKt6uny2w/edit#gid=0" target="_blank">Jessi's - Tracking Partners + View Through</a>
	<br />
	<a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ua/macros-external-document" target="_blank">MACROs</a>
	<br />
	<a class="blue" href="https://docs.google.com/spreadsheets/d/1uExJLD_Ql7EjZz2IIJnv02-Y3nMoKZWJVnKAqy7yqd4/edit?ts=569446e4#gid=0" target="_blank">API keys</a>
	<br />
	<a class="blue" href="http://cpa.adtilt.com/most_recent_actions?api_key=bb2cf0647ba654d7228dd3f9405bbc6a&product_id=" target="_blank">Install feed URL</a>
	<br />
	<a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ad-ops/3rd-party-dashboards" target="_blank">3rd party dashboards</a>
	<br />
	<a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ad-ops/vpn-to-another-country" target="_blank">VPN to Another Country</a>
	<br />
	<a class="blue" href="https://docs.google.com/spreadsheets/d/1ZUL3K6kqQYzqwDglnRirUPHJer1fxraDWSkgdha-dyg/edit?ts=569e97f2#gid=0" target="_blank">Partner Speadsheet</a>
	<br />
	<a class="blue" href="http://support.adcolony.com/customer/portal/articles/1914521-postbacks-for-top-tracking-partners" target="_blank">Post Install Events for Top Tracking Partners</a>
	<br />
	<a class="blue" href="http://tamtrackingtool.com/css/RestCheatSheet2.png" target="_blank">HTTP status codes</a>
	<br />
	<a class='blue' href='http://tamtrackingtool.com/compare/tenjin_test.pdf' target='_blank'>Tenjin testing instructions</a>  (from Tenjin)
	<br />
	<br />

	<!--

	<h3 class="black">Compare URLs</h3>
	<textarea name="comp_url1" id="comp_url1"></textarea>
	<br />
	<textarea name="comp_url2" id="comp_url2"></textarea>
	<br />
	<input type="button" class="btn-class" value="Compare!" id="compare_URLs_button" onclick="compareURLs()">
	<br />
	<br />
	<div id="output_tables">
	<div id="output_tables1"></div>
	<div id="output_tables2"></div>
	</div>

	-->

</div>

<?php
include 'footer.php';
?>

