<?php
include ("http://localhost:8888/password_protect.php");
?>

<?php
include '../header.php';
?>

<?php
include '../sideleft.php';
?>

<?php include_once("analyticstracking.php") ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
	$(function() {
		$("#start_date").datepicker();
	}); 
</script>
<script>
	$(function() {
		$("#end_date").datepicker();
	}); 
</script>

<script type="text/javascript" src="repapi.js"></script>

<div id="maincontent" class="comp_content">

	<h1>Advertiser Reporting API info page</h1>

	<h3 class="black plink" id="notestitle">1. Notes <i class="fa fa-arrow-down" id="notesdown"></i><i class="fa fa-arrow-up" id="notesup"></i></h3>
	<div id="notesdiv" class="ndiv">
		The use of the reporting API is intended for Advertisers running their campaigns on AdColony Video Ad Network to retrieve ad <br />performance reporting data via an API. The API uses HTTP GET to make reporting requests and retrieve results from the server.
		<br />
		<br />
		The full reporting API documentation can be downloaded from <a href="http://support.adcolony.com/customer/portal/articles/1578773-adcolony-advertiser-api-update" target="_blank" class="blue">this</a> AdColony support page.
		<br />
		<br />
	</div>
	
	<h3 class="black plink" id="faqtitle">2. Frequently Asked Questions <i class="fa fa-arrow-down" id="faqdown"></i><i class="fa fa-arrow-up" id="faqup"></i></h3>
	<div id="faqdiv" class="ndiv">
		
		<p class="boldp">Where can we find the <span class="icon-red">API key</span> for the client?</p>
		<p class="answer">Go into the advertiser’s user account (i.e. <a href="https://ops.adcolony.com/users/16299/edit" target="_blank">https://ops.adcolony.com/users/16299/edit</a>). 
		<br />Please look for "<span>Read-Only API Key</span>".<br />
		<a href="https://ops.adcolony.com/admin/master_account_list" target="_blank" class="blue">Search for the API key in AdColony dashboard.</a></p>
		
		<p class="boldp">What are the different  <span class="icon-red">group_by values</span>?</p>
		<p class="answer">The report can be broken down by <span class="bold">campaign, ad_group, creative, country, app</span> and <span class="bold">platform</span>.</p>
		
		<p class="boldp">Do we support all of the <span class="icon-red">group_by combinations</span>?</p>
		<p class="nomb">1) NO - not all of the combinations are supported. <i class="em em-tired_face"></i></p>
		<p class="nomb">2) For example the following: <span class="bold">group_by=creative,country</span> combination is <span class="bold">not supported</span>.</p>
		<p class="nomb">3) However the most detailed report is supported: <span class="bold">group_by=campaign,ad_group,creative,country,app,platform</span>. <i class="em em---1"></i></p>
		<p class="answer">4) The list of supported group_by combinations can be found <a class="blue" href="http://tamtrackingtool.com/repapi/ADC_rep_API_supported_groupbys.pdf" target="_blank">here</a>.</p>
		
		<p class="boldp">Do we support <span class="icon-red">group_by=device_type</span>? (iPhone, iPad, tablet, phone, etc...)</p>
		<p class="answer">NO - this is not supported at the moment. <i class="em em-tired_face"></i></p>
		
		<p class="boldp">Do we support <span class="icon-red">date_group=month</span>?</p>
		<p class="answer">NO - this is not supported at the moment. <i class="em em-tired_face"></i></p>
		
		<p class="boldp">Do we support <span class="icon-red">hourly breakdown</span> in the reports?</p>
		<p class="answer">YES - we support the following date_group values: <span class="bold">aggregate, day, hour</span>. <i class="em em---1"></i></p>
		
		<!--
		<p class="boldp">Automated S3 buckets for clients?</p>
		<p class="answer">Was King successful?</p>
		-->
		
		<p class="boldp">How can the client see the <span class="icon-red">app names</span> in the reports?</p>
		<p class="nomb">1) First they need to sign an NDA agreement with us.</p>
		<p class="nomb">2) We will need to enable "app transparency" in the AdColony dashboard. (See next point below.)</p>
		<p class="nomb">3) The client will need to include "group_by=app" in the query URL.</p>
		<p>4) The "group_by=app" can significantly increase the size of the report so the client might need to reduce the date range.</p>
		
		<p class="boldp">You can view whether the <span class="icon-red">app transparency</span> is enabled by:</p>
		<p class="nomb">1) Going into the advertiser’s user account (i.e. <a href="https://ops.adcolony.com/users/16299/edit" target="_blank">https://ops.adcolony.com/users/16299/edit</a>).</p>
		<p class="nomb">2) Click “Manage Permissions” at the bottom.</p>
		<p class="nomb">3) Look under Access App Transparency API — it will either be set to “Yes” or “No”.</p>
		<p>4) If you have permission, you can modify it.</p>
		
		<p class="boldp">Error message: The report requested exceeds the allowed <span class="icon-red">capacity limit</span>. What should we do?</p>
		<p class="nomb">The report is limited by <span class="bold">the number of rows:  250K</span>.</p>
		<p class="nomb">1) Try to reduce the report size by <span class="bold">decreasing the date range</span>, or</p>
		<p class="nomb">2)  <span class="bold">specifying campaign ids</span>, or</p>
		<p>3) <span class="bold">reducing your groupings</span>.</p>
		
		<p class="boldp">API  <span class="icon-red">Queue Limit</span>:</p>
		<p class="nomb">As designed, <span class="bold">up to five reports</span> can be actively processing at the same time (per API key). Additional reporting request made while five reports are actively running are added to a queue to become active once an existing report completes.
				<br /><span class="bold">The support queue length is 25 reports.</span> When five reports are actively running, and the queue count has hit 25 reports, any additional request will return a 429 error.</p>
		<p>The recommended method of pulling concurrent reports is to run in batches between 15-20 reports up front, and then make additional requests as reports from the initial batch complete.</p>
		
		<p class="boldp">Error messages: <span class="icon-red">HTTP ERROR 504</span> or <span class="icon-red">page isn't working</span> or <span class="icon-red">page took too long to respond</span>.</p>
		<p class="nomb">1) If you use <span class="bold">"https://clients.adcolony.com"</span> in the URL, please change it to the more recent <span class="bold">"https://clients-api.adcolony.com"</span>.</p>
		<p class="nomb">2) If you already use <span class="bold">"https://clients-api.adcolony.com"</span> in the URL, please try to reduce the date range / grouping.</p>
		<p class="answer">3) If you still have the issue please contact the <a href="mailto:tracking-support@adcolony.com" target="_top">TAM team</a>.</p>
		
		
		<br />
	</div>
	
	<h3 class="black plink" id="extitle">3. Examples <i class="fa fa-arrow-down" id="exdown"></i><i class="fa fa-arrow-up" id="exup"></i></h3>
	<div id="exdiv" class="ndiv">
		<a href="https://ops.adcolony.com/admin/master_account_list" target="_blank" class="blue">Search for the API key in AdColony dashboard.</a><br /><br />
		
		<div class="ex_cont_div">
		<div class="boldp example">Example 1: <br />Retrieve daily performance report on all campaigns breakdown by ad group for a selected date range from 1/23/2017 to 1/24/2017:</div>
		<div class="ex_api_input"><br />API key: <input type="text" id="ex1_api" class="api_imp" /> <input type="button" class="btn-class" value="Shoot!" onclick="example('ex1')" /></div>
		<div class="clear"></div>
		<div class="ex_api_link" id="ex1_link">https://clients-api.adcolony.com/api/v2/advertiser_summary?user_credentials=<span class='bold'>'API_KEY_HERE'</span>&date=01232017&end_date=01242017&group_by=ad_group&date_group=day</div>
		<div id="ex1_output" style="display: none;">Click here for the result: csv, json, xml</div>
		</div>
		
		<div class="ex_cont_div">
		<div class="boldp example">Example 2: <br />Retrieve aggregate performance report on all campaigns breakdown by ad group for a selected date range from 1/23/2017 to 1/24/2017:</div>
		<div class="ex_api_input"><br />API key: <input type="text" id="ex2_api" class="api_imp" /> <input type="button" class="btn-class" value="Shoot!" onclick="example('ex2')" /></div>
		<div class="clear"></div>
		<div class="ex_api_link" id="ex2_link">http://clients-api.adcolony.com/api/v2/advertiser_summary?user_credentials=<span class='bold'>'API_KEY_HERE'</span>&date=01232017&end_date=01242017&group_by=ad_group&date_group=aggregate</div>
		<div id="ex2_output" style="display: none;">Click here for the result: csv, json, xml</div>
		</div>
		
		<div class="ex_cont_div">
		<div class="boldp example">Example 3: <br />Retrieve aggregate performance report on all campaigns breakdown by campaign and country for a selected date range from 1/20/2017 to 1/24/2017:</div>
		<div class="ex_api_input"><br />API key: <input type="text" id="ex3_api" class="api_imp" /> <input type="button" class="btn-class" value="Shoot!" onclick="example('ex3')" /></div>
		<div class="clear"></div>
		<div class="ex_api_link" id="ex3_link">http://clients-api.adcolony.com/api/v2/advertiser_summary?user_credentials=<span class='bold'>'API_KEY_HERE'</span>&date=01202017&end_date=01242017&group_by=campaign,country&date_group=aggregate</div>
		<div id="ex3_output" style="display: none;">Click here for the result: csv, json, xml</div>
		</div>
		
		<!--
		<div class="ex_cont_div">
		<div class="boldp example">Example 4: <br />Retrieve daily performance report on a specified campaign breakdown by ad group on 5/1/2015:</div>
		<div class="ex_api_input"><br />API key: <input type="text" id="ex4_api" class="api_imp" /> <input type="button" class="btn-class" value="Shoot!" onclick="example('ex4')" /></div>
		<div class="clear"></div>
		<div class="ex_api_link" id="ex4_link">http://clients-api.adcolony.com/api/v2/advertiser_summary?user_credentials=<span class='bold'>'API_KEY_HERE'</span>&date=05012015&campaign_id=14388&group_by=ad_group&date_group=day</div>
		<div id="ex4_output" style="display: none;">Click here for the result: csv, json, xml</div>
		</div>
		
		<div class="ex_cont_div">
		<div class="boldp example">Example 5: <br />Retrieve aggregate performance report on a specified campaign breakdown by ad group for a selected date range from 5/1/2015 to 5/2/2015:</div>
		<div class="ex_api_input"><br />API key: <input type="text" id="ex5_api" class="api_imp" /> <input type="button" class="btn-class" value="Shoot!" onclick="example('ex5')" /></div>
		<div class="clear"></div>
		<div class="ex_api_link" id="ex5_link">http://clients-api.adcolony.com/api/v2/advertiser_summary?user_credentials=<span class='bold'>'API_KEY_HERE'</span>&date=05012015&end_date=05022015&campaign_id=14388&group_by=ad_group&date_ group=aggregate</div>
		<div id="ex5_output" style="display: none;">Click here for the result: csv, json, xml</div>
		</div>
		
		<div class="ex_cont_div">
		<div class="boldp example">Example 6: <br />Retrieve aggregate performance report on a specified campaign breakdown by ad group and by country for a selected date range from 5/1/2015 to 5/2/2015:</div>
		<div class="ex_api_input"><br />API key: <input type="text" id="ex6_api" class="api_imp" /> <input type="button" class="btn-class" value="Shoot!" onclick="example('ex6')" /></div>
		<div class="clear"></div>
		<div class="ex_api_link" id="ex6_link">http://clients-api.adcolony.com/api/v2/advertiser_summary?user_credentials=<span class='bold'>'API_KEY_HERE'</span>&date=05012015&end_date=05022015&campaign_id=14388&group_by=ad_group&group _by=country&date_group=aggregate</div>
		<div id="ex6_output" style="display: none;">Click here for the result: csv, json, xml</div>
		</div>
		
		<div class="ex_cont_div">
		<div class="boldp example">Example 7: <br />Retrieve daily performance report on a specified campaign breakdown by ad group and by creative for a selected date range from 5/1/2015 to 5/2/2015:</div>
		<div class="ex_api_input"><br />API key: <input type="text" id="ex7_api" class="api_imp" /> <input type="button" class="btn-class" value="Shoot!" onclick="example('ex7')" /></div>
		<div class="clear"></div>
		<div class="ex_api_link" id="ex7_link">http://clients-api.adcolony.com/api/v2/advertiser_summary?user_credentials=<span class='bold'>'API_KEY_HERE'</span>&date=05012015&end_date=05022015&campaign_id=14388&group_by=ad_gro up&group_by=creative&date_group=aggregate</div>
		<div id="ex7_output" style="display: none;">Click here for the result: csv, json, xml</div>
		</div>
		-->
		
		
	</div>
	<br />
	

	<h3 id="bapititle" class="black plink">4. Build the query URL <i class="fa fa-arrow-down" id="bapidown"></i><i class="fa fa-arrow-up" id="bapiup"></i></h3>
	<div id="bapidiv">
		<form id="bapiform">

			<table class="pietable">
				<tr>
					<td>Base URL:</td>
					<td colspan="3">https://clients-api.adcolony.com/api/v2/advertiser_summary</td>
				</tr>
				<tr>
					<td>API key (user_credentials):</td>
					<td>
					<input type="text" name="api_key" id="api_key">
					</td>
					<td class="red_td">*required</td>
					<td><a href="https://ops.adcolony.com/admin/master_account_list" target="_blank" class="blue">Search for the API key in AdColony dashboard.</a></td>
				</tr>
				<tr>
					<td>Start date / Date:</td>
					<td>
					<input type="text" name="start_date" id="start_date">
					</td>
					<td colspan="2" class="red_td">*required, unless interval is specified</td>
				</tr>
				<tr>
					<td>End date:</td>
					<td>
					<input type="text" name="end_date" id="end_date">
					</td>
					<td colspan="2">*optional</td>
				</tr>
				<tr>
					<td>Format:</td>
					<td>
					<input type="radio" name="format" id="json" value="json">
					json
					<input type="radio" name="format" id="csv" value="csv" checked="checked">
					csv
					<input type="radio" name="format" id="xml" value="xml">
					xml</td>
					<td colspan="2">*optional, default: json, however it's set to csv here...</td>
				</tr>
				<tr>
					<td>Date group:</td>
					<td>
					<input type="radio" name="date_group" id="aggregate" value="aggregate">
					aggregate
					<input type="radio" name="date_group" id="day" value="day">
					day
					<input type="radio" name="date_group" id="hour" value="hour">
					hour</td>
					<td colspan="2">*optional, default: aggregate</td>
				</tr>
				<tr>
					<td>Group by:</td>
					<td>
					<input type="checkbox" name="group_by" id="gr_campaign" value="campaign">
					campaign
					<br />
					<input type="checkbox" name="group_by" id="gr_adgroup" value="ad_group">
					ad_group
					<br />
					<input type="checkbox" name="group_by" id="gr_creative" value="creative">
					creative
					<br />
					<input type="checkbox" name="group_by" id="gr_country" value="country">
					country
					<br />
					<input type="checkbox" name="group_by" id="gr_app" value="app">
					app
					<br />
					<!--
					<input type="checkbox" name="group_by" id="gr_zone" value="zone">
					zone
					<br />
					-->
					<input type="checkbox" name="group_by" id="gr_platform" value="platform">
					platform
					<br />
					</td>
					<td colspan="2">*optional</td>
				</tr>
				<tr>
					<td>Campaign ID:</td>
					<td>
					<input type="text" name="campaign_id" id="campaign_id">
					</td>
					<td colspan="2">*optional</td>
				</tr>
				<tr>
					<td>Interval</td>
					<td>
					<select name="interval" id="interval">
						<option value=""></option>
						<option value="today">today</option>
						<option value="yesterday">yesterday</option>
						<option value="last24">last24</option>
						<option value="this_week">this_week</option>
						<option value="last_week">last_week</option>
						<option value="this_month">this_month</option>
						<option value="last_month">last_month</option>
						<option value="last_30_days">last_30_days</option>
						<option value="this_year">this_year</option>
						<option value="last_year">last_year</option>
						<option value="all_time">all_time</option>
					</select></td>
					<td colspan="2">*optional, it will override the "date"</td>
				</tr>
			</table>
			<br />
			<input type="button" class="btn-class" name="buildURL" id="buildURL" value="Build the query URL!" onclick="build_url()">
		</form>

		<br />
		<div id="alertdiv"></div>
		<div id="urldiv"></div>
		<br />

	</div>
	
	
	
	<h3 class="black plink" id="hiddentitle"> &nbsp;<i class="fa fa-arrow-down" id="hiddendown" style="display: none !important;"></i><i class="fa fa-arrow-up" id="hiddenup" style="display: none !important;"></i></h3>
	<div id="hiddendiv">
		Peter test campaign API key: W0KYONhCiK0usqvGFy1B
	</div>

</div>

<?php
include '../footer.php';
?>

