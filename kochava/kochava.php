<?php
include ("http://localhost:8888/password_protect.php");
?>

<?php
include '../header.php';
?>

<?php
include '../sideleft.php';
?>

<?php include_once("analyticstracking.php") ?>

<script type="text/javascript" src="kochava.js"></script>

<div id="maincontent">

	<h1>Kochava</h1>
	
	<h3 class="bold"><a href="https://docs.google.com/document/d/1lHUd_zl4wgjzZlM3zCaUFUwD_6Ubv7bTLvxd7hu2eeQ/" target="_blank" style='color: rgb(147, 27, 28) !important; font-weight: bold !important;'>Kochava Validation instructions</a></h3>

	<h3 class="black plink" id="notestitle">1. Notes <i class="fa fa-arrow-down" id="notesdown"></i><i class="fa fa-arrow-up" id="notesup"></i></h3>
	<div id="notesdiv" class="ndiv">

		<h3>Example Links:</h3>
		Click:
		<br />
		https://control.kochava.com/v1/cpi/click?campaign_id=koyousician56c093c85905f3e1c60309e2e5&network_id=18&appstore_id=[STORE_ID]&device_id=[IDFA]&device_id_type=idfa&mac_sha1=[MAC_SHA1]&odin=[ODIN1]&openudid=[OPEN_UDID]&site_id=[APP_ID]&udid=[UDID]
		<br />
		<br />
		Impression:
		<br />
		http://imp.control.kochava.com/track/impression?campaign_id=koyousician56c093c85905f3e1c60309e2e5&network_id=18&impression_id=[SERVE_TIME]&event=completed_view&user_dma=[DMA_CODE]&country=[COUNTRY_CODE]&ip_address=[IP_ADDRESS]&device_connection=[NETWORK_TYPE]&device_model=[DEVICE_MODEL]&device_os_version=[OS_VERSION]&device_os=[PLATFORM]&device_id=[IDFA]&creative_size=[AD_CREATIVE_NAME]&creative_id=[RAW_AD_CREATIVE_ID]&site_id=[APP_ID]&appstore_id=[STORE_ID]
		<br />
		<br />
		Amazon click:
		<br />
		https://control.kochava.com/v1/cpi/click?campaign_id=kopanda-pop-amazon558c723ebacac384dd5b7018fd&network_id=223&adid=[GOOGLE_AD_ID]&appstore_id=[STORE_ID]&device_hash_method=sha1&device_id=[SHA1_ANDROID_ID]&device_id_is_hashed=true&device_id_type=android_id&imei_sha1=[SHA1_IMEI]&mac_sha1=[MAC_SHA1]&odin=[ODIN1]&pbr=1&site_id=[APP_ID]&network_campaign_name=[AD_CAMPAIGN_NAME]&append_app_conv_trk_params=1
		<br />
		<br />
	</div>

	<h3 id="valtitle" class="black plink">2. Tracking URL validation <i class="fa fa-arrow-down" id="valdown"></i><i class="fa fa-arrow-up" id="valup"></i></h3>
	<div id="valdiv">

		Paste the click URL below:
		<br />
		<form id="valform">
			<textarea name="koch_url" id="koch_url"></textarea>
			<span id="koch_imp_title" style="display: none;"> 
				<br />
				Impression URL:
				<br />
			</span>
			<textarea name="koch_url_imp" id="koch_url_imp" style="display: none;"></textarea>				
			<div id="koch_imp_div" style="display: none;">
				<br />
				<span id="chURLs_span" class="redbold">Paste the impression URL above and press:</span>
				<input type="button" class="btn-class" value="Check URLs" onclick="compare_koch_urls()">
				<div id="url_comp_div"></div>
			</div>
			<br />
			<input type="radio" name="vt" id="vt0" value="not enabled">
			No view-through
			<br>
			<input type="radio" name="vt" id="vt1" value="enabled gen">
			View-through is enabled / I want to generate the impression URL
			<br />
			<input type="radio" name="vt" id="vt2" value="enabled">
			View-through is enabled / I have the impression URL
			<br />
			<br />
			<input type="button" class="btn-class" value="Validate" onclick="ValidateKochava()">
			<br />
			<br />
		</form>
		<h3 class="black plink" id="urlparamstitle">Check URL parameters <i class="fa fa-arrow-down" id="urlpdown"></i><i class="fa fa-arrow-up" id="urlpup"></i></h3>
		<div id="urlsplit_div"></div>
		<div id="kochparams">
			<table id="koch_paramtable">
				<tr>
					<th class="firstcol">Parameter</th>
					<th class="secondcol">Macro/Value</th>
					<th class="thirdcol">Notes</th>
				</tr>
				<tr class="tep" id="kcid">
					<td>campaign_id</td>
					<td>specified by Kochava</td>
					<td></td>
				</tr>
				<tr class="tep" id="knid">
					<td>network_id</td>
					<td>specified by Kochava</td>
					<td></td>
				</tr>
				<tr class="tep" id="kdevid">
					<td>device_id</td>
					<td>[IDFA]</td>
					<td>for iOS</td>
				</tr>
				<tr class="tep" id="kadid">
					<td>adid</td>
					<td>[GOOGLE_AD_ID]</td>
					<td>for Android</td>
				</tr>
				<tr class="tep" id="kappend">
					<td>append_app_conv_trk_params</td>
					<td>1</td>
					<td>for Android</td>
				</tr>
				<tr class="tep" id="kpbr">
					<td>pbr</td>
					<td>1</td>
					<td>for Android click tags</td>
				</tr>
			</table>
			<h3 id="koch_otherp_title" class="black plink">Commonly used values <i class="fa fa-arrow-down" id="koch_otherp_down"></i><i class="fa fa-arrow-up" id="koch_otherp_up"></i></h3>
			<div id="koch_otherparams">
				<table id="koch_otherparamtable">
					<tr>
						<th class="fourthcol">Add</th>
						<th class="firstcol">Parameter</th>
						<th class="secondcol">Macro/Value</th>
						<th class="thirdcol">Notes</th>
					</tr>
					<tr>
						<td></td>
						<td>device_id_type</td>
						<td>idfa</td>
						<td>or iOS</td>
					</tr>
					<tr>
						<td></td>
						<td>device_hash_method</td>
						<td>sha1</td>
						<td></td>
					</tr>
					<tr id="tr_sha1">
						<td>
						<input type="checkbox" name="param" id="ch_sha1" onclick="add_remove_params(this)">
						</td>
						<td>device_id</td>
						<td>[SHA1_ANDROID_ID]</td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td>device_id_is_hashed</td>
						<td>true</td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td>device_id_type</td>
						<td>android_id</td>
						<td></td>
					</tr>
					<tr id="tr_imei">
						<td>
						<input type="checkbox" name="param" id="ch_imei" onclick="add_remove_params(this)">
						</td>
						<td>imei_sha1</td>
						<td>[SHA1_IMEI]</td>
						<td></td>
					</tr>
					<tr id="tr_mac">
						<td>
						<input type="checkbox" name="param" id="ch_mac" onclick="add_remove_params(this)">
						</td>
						<td>mac_sha1</td>
						<td>[MAC_SHA1]</td>
						<td></td>
					</tr>
					<tr id="tr_odin">
						<td>
						<input type="checkbox" name="param" id="ch_odin" onclick="add_remove_params(this)">
						</td>
						<td>odin</td>
						<td>[ODIN1]</td>
						<td></td>
					</tr>
					<tr id="tr_openud">
						<td>
						<input type="checkbox" name="param" id="ch_openud" onclick="add_remove_params(this)">
						</td>
						<td>openudid</td>
						<td>[OPEN_UDID]</td>
						<td></td>
					</tr>
					<tr id="tr_udid">
						<td>
						<input type="checkbox" name="param" id="ch_udid" onclick="add_remove_params(this)">
						</td>
						<td>udid</td>
						<td>[UDID]</td>
						<td></td>
					</tr>
					<tr id="tr_siteid">
						<td>
						<input type="checkbox" name="param" id="ch_siteid" onclick="add_remove_params(this)">
						</td>
						<td>siteid</td>
						<td>[APP_ID]</td>
						<td></td>
					</tr>
					<tr id="tr_creativeid">
						<td>
						<input type="checkbox" name="param" id="ch_creativeid" onclick="add_remove_params(this)">
						</td>
						<td>creative_id</td>
						<td>[AD_CREATIVE_NAME]</td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td>click_id</td>
						<td></td>
						<td></td>
					</tr>
					<tr id="tr_devmod">
						<td>
						<input type="checkbox" name="param" id="ch_devmod" onclick="add_remove_params(this)">
						</td>
						<td>device_model</td>
						<td>[DEVICE_MODEL]</td>
						<td></td>
					</tr>
					<tr id="tr_devos">
						<td>
						<input type="checkbox" name="param" id="ch_devos" onclick="add_remove_params(this)">
						</td>
						<td>device_os</td>
						<td>[PLATFORM]</td>
						<td></td>
					</tr>
					<tr id="tr_devver">
						<td>
						<input type="checkbox" name="param" id="ch_devver" onclick="add_remove_params(this)">
						</td>
						<td>device_ver</td>
						<td>[OS_VERSION]</td>
						<td></td>
					</tr>
					<tr id="tr_devcon">
						<td>
						<input type="checkbox" name="param" id="ch_devcon" onclick="add_remove_params(this)">
						</td>
						<td>device_connection</td>
						<td>[NETWORK_TYPE]</td>
						<td></td>
					</tr>
					<tr id="tr_ip">
						<td>
						<input type="checkbox" name="param" id="ch_ip" onclick="add_remove_params(this)">
						</td>
						<td>ip_address</td>
						<td>[IP_ADDRESS]</td>
						<td></td>
					</tr>
					<tr id="tr_country">
						<td>
						<input type="checkbox" name="param" id="ch_country" onclick="add_remove_params(this)">
						</td>
						<td>country</td>
						<td>[COUNTRY_CODE]</td>
						<td></td>
					</tr>
					<tr id="tr_dma">
						<td>
						<input type="checkbox" name="param" id="ch_dma" onclick="add_remove_params(this)">
						</td>
						<td>user_dma</td>
						<td>[DMA_CODE]</td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td>cp_name1</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td>cp_name2</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td>cp_name3</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td>cp_value1</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td>cp_value2</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td>cp_value3</td>
						<td></td>
						<td></td>
					</tr>
					<tr id="tr_appstoreid">
						<td>
						<input type="checkbox" name="param" id="ch_appstoreid" onclick="add_remove_params(this)">
						</td>
						<td>appstore_id</td>
						<td>[STORE_ID]</td>
						<td>(used 10/1 onwards)</td>
					</tr>
					<tr id="tr_impid">
						<td>
						<input type="checkbox" name="param" id="ch_impid" onclick="add_remove_params(this)">
						</td>
						<td>impression_id</td>
						<td>[SERVE_TIME]</td>
						<td>(view based parameter)</td>
					</tr>
					<tr id="tr_event">
						<td></td>
						<td>event</td>
						<td>completed_view</td>
						<td>(view based parameter)</td>
					</tr>
				</table>
				<br />
			</div>

		</div>
		<div id="vnotes">
			<div id="redurldiv"></div>
			<div id="vnotes_comment"></div>
			<div id="vnotes_alert"></div>
		</div>
		<div class="clear"></div>
	</div>

	<h3 class="black plink" id="pbtitle">3. Postback - check the 3rd party Dashboard <i class="fa fa-arrow-down" id="pbdown"></i><i class="fa fa-arrow-up" id="pbup"></i></h3>
	<div id="pbdiv" class="ndiv">

		<a class="blue" href="https://go.kochava.com/apps/" target="_blank">Kochava Dashboard</a>
		<br />
		Login details can be found <a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ad-ops/3rd-party-dashboards" target="_blank">here</a>. (AdColony internal document.)
		<br />
		<br />
		<h3 class="black plink" id="pbmtitle">More <i class="fa fa-arrow-down" id="pbmdown"></i><i class="fa fa-arrow-up" id="pbmup"></i></h3>
		<div id="pbmdiv">
			<ul>
				<li>
					Open up the Kochava portal page and validate the postback:
					<ul>
						<li>
							Is it set up? Find the mobile app under the advertiser and navigate to Postback Configuration tab.
						</li>
						<li>
							Is the product_id correct?
						</li>
						<li>
							Is the api_key correct? <span style="font-weight: bold; color: black;">25b8c23e2b6bfe08e75aed432f46149b</span>
						</li>
					</ul>
				</li>

			</ul>
		</div>
		<br />
	</div>
	
	<h3 class="black plink" id="fptitle">4. Device fingerprinting <i class="fa fa-arrow-down" id="fpdown"></i><i class="fa fa-arrow-up" id="fpup"></i></h3>
	<div id="fpdiv" class="ndiv">
		
		<span id="fp_alert" class="bold">
			PLEASE CHECK THE FINGERPRINTING SETUP
		</span>
		
		
	<form id="fpform">
			<input type="radio" name="fp" id="fp0" value="fp_not_ok">
			Fingerprinting doesn't look good enough.
			<br>
			<input type="radio" name="fp" id="fp1" value="fp_ok">
			Fingerprinting should be fine.
			<br />
			<br />
			<input type="button" class="btn-class" value="I have checked fingerprinting" onclick="FpKoch()">
			<br />
			<br />
		</form>	
		<div id="fp_message"></div>
		<br />
		
	</div>

	<h3 class="black plink" id="imptitle">5. Click / Impression URLs <i class="fa fa-arrow-down" id="impdown"></i><i class="fa fa-arrow-up" id="impup"></i></h3>
	<div id="impdiv">
		<form>
			<fieldset id="tune_output">
				<textarea name="koch_imp" id="koch_imp"></textarea>
			</fieldset>
		</form>
		<br />
	</div>

	<h3 class="black plink" id="vttitle">6. View Through Attribution Window <i class="fa fa-arrow-down" id="vtdown"></i><i class="fa fa-arrow-up" id="vtup"></i></h3>
	<div id="vtdiv">
		<table id="vt_table">
			<tr>
				<th colspan="2">VIEW ATTRIBUTION</th>
				<th colspan="3">LOOKBACK WINDOWS</th>
				<th>CLIENT-SIDE ACTION</th>
			</tr>
			<tr class="tep" id="tpid">
				<th>Dedicated View Tags</th>
				<th>Recommended Implementation</th>
				<th style="min-width: 120px;">Flexible Lookback Window</th>
				<th>Default Click Lookback</th>
				<th>Default View Lookback</th>
				<th></th>
			</tr>
			<tr>
				<td>Yes</td>
				<td class="lefta">Implement impression tag on complete field.</td>
				<td>Yes</td>
				<td>30 days</td>
				<td>1 day</td>
				<td class="lefta">Enable and/or adjust the impression window in Kochava.
				<br />
				Can be enabled at App level.</td>
			</tr>
		</table>
		<br />
	</div>
	<h3 class="black plink" id="pietitle">7. PIE <i class="fa fa-arrow-down" id="piedown"></i><i class="fa fa-arrow-up" id="pieup"></i></h3>
	<div id="piediv">
		PIE events should be implemented by the client in Kochava dashboard.
		<br />
		More info about the client setup can be found here:
		<a class="blue" href="http://support.adcolony.com/customer/portal/articles/1914521-postbacks-for-top-tracking-partners#Kochava" target="_blank">Kochava PIE setup</a>
	</div>

</div>

<div class="clear"></div>

<?php
include '../footer.php';
?>

