function validate() {
	var url = document.getElementById('url').value
	var parameters = [];
	console.log(url.split('/')[3]);

	if (!url.split('?')[1]) {
		console.log('no parameters');
		if (url.split('/')[3].indexOf('?') > -1) {
			url = url.split('?')[0].split('/')[3];
		} else {
			url = url.split('/')[3];
		}
		console.log(parameters);
		parameters.push({name:'campaign', value:''});
		parameters.push({name:'adgroup', value:''});
		parameters.push({name:'creative', value:''});
	} else {
		parameters = getParameters(url);
		parameters = parameters.filter(function (index) { return index !== undefined });
		url = url.split('?')[0].split('/')[3];
	}
	parameters.push({name:'ip_address', value:'[IP_ADDRESS]'})
	parameters.push({name:'adcolony_click_id', value:'[CLICK_ID]'})
	parameters.push({name:'s2s', value:'1'})
	createTable(parameters);
	var html = '<table class="table table-bordered">';
	html+='<tr>';
	html+='<td>https://app.adjust.com/</td>';
	html+='<td><input type"text" class="form-control" id="base-url-value" value='+ url +'></td>';
	html+='</tr>';
	html+='</table>';
	$('#base-url').html(html);
	html = '<button type="button" onclick="addPie()" class="btn btn-default btn-lg btn-block">Add PIE</button>';
	$('#add-pie').html(html);
	html = '<button type="button" onclick="generateUrl(this)" class="btn btn-default">iOS</button><button type="button" onclick="generateUrl(this)" class="btn btn-success">Android</button>';
	$('#os').html(html);
}

function createTable(parameters) {

	var html = '<table class="table table-bordered">';
    for (var i = 0; i < parameters.length; i++) {
        html+='<tr>';
        html+='<td class="parameter-name">'+parameters[i].name+'</td>';
        if (parameters[i].name == 'ip_address' || parameters[i].name == 'adcolony_click_id' || parameters[i].name == 's2s'){
        	html+='<td><input readonly type"text" class="form-control parameter-value" oninput="updateParameter('+i+', this)" value='+parameters[i].value+' list="datalist1" ></td>';
        } else {
        	html+='<td><input list="datalist1" type"text" class="form-control parameter-value" oninput="updateParameter('+i+', this)" value='+parameters[i].value+'></td>';
        }
        html+='</tr>';
    }

    html+='</table>';

    $('#table').html(html);
}

function getParameters(url) {
	var parameters = url.split('?')[1].split('&');
	return parameters.map(function (index){
		if (index.split('=')[0] == 'campaign' || index.split('=')[0] == 'adgroup' || index.split('=')[0] == 'creative') {
			return {name: index.split('=')[0], value: index.split('=')[1]};
		} else {
			return;
		}
	});
}

function updateParameter(i, parameter) {
	console.log(parameter);
}

function addPie(){

	if (document.getElementById("pie-table")) {

		console.log("table exists");
	    var x=document.getElementById('pie-table');
	    var len = x.rows.length;
	    var new_row = x.rows[len-1].cloneNode(true);
	    x.firstElementChild.appendChild(new_row)

	} else {
		var html = '<table id="pie-table" class="table table-bordered">';
		html+='<tr>';
		html+='<th>pie</th>';
		html+='<th>pie</th>';
		html+='<th>pie</th>';
		html+='<th>pie</th>';
		html+='</tr>';
		html+='<tr>';
		html+='<td><select id="pie-event-name" class="form-control pie-event-name"><option>Choose PIE event</option></select></td>';
		html+='<td><input type="text" class="form-control pie-value" placeholder="PIE value"></td>';
		html+='<td><button type="button" onclick="deleteRow(this)" class="btn btn-danger btn-block"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>';
		html+='</tr>';
		html+='</table>';

		$('#pie').html(html);
		

		var select = document.getElementById("pie-event-name"); 
		var options = [
			'Open',
	 		'Session',
	 		'Payment Info Added',
	 		'Transaction',
	 		'Add to Cart',
	 		'Checkout Initiated',
	 		'Credits Spent',
	 		'Add to Wishlist',
	 		'Activated',
	 		'Achievement Unlocked',
	 		'Content View',
	 		'Level Achieved',
	 		'Install',
	 		'App Rated',
	 		'Invite',
	 		'Login',
	 		'Tutorial Completed',
	 		'Reservation',
	 		'Social Sharing Event',
	 		'Search',
	 		'Custom Event 1',
	 		'Custom Event 2',
	 		'Custom Event 3',
	 		'Custom Event 4',
	 		'Custom Event 5',
	 		'Registration Completed',
	 		'Update'
	 	];

	 	options.sort();

		for(var i = 0; i < options.length; i++) {
			var opt = options[i];
		    var el = document.createElement("option");
		    el.textContent = opt;
		    el.value = opt;
		    select.appendChild(el);		
		}
	}
}

function deleteRow(row)
{
    var i=row.parentNode.parentNode.rowIndex;
    document.getElementById('pie-table').deleteRow(i);
}

function generateUrl(os)
{
	console.log(os.innerText);
	var x = document.getElementsByClassName('pie-event-name');
	var y = document.getElementsByClassName('pie-value');
	var pieToFetch = [];
	var pieValues = [];
	for (i = 0; i < x.length; i++) {
		pieToFetch.push(x[i].value.toLowerCase().split(' ').join('_'));
		pieValues.push(y[i].value);
	}
	var pieForUrl = [];
	if (os.innerText == 'Android') {
		var standardParameters = pie.androidParameters;
	} else {
		var standardParameters = pie.iosParameters;
	}
	pieToFetch.forEach(function (pieEvent){
		pie.eventType.forEach(function (index){
			if (pieEvent == index.name) {
				if (index.extraParameters) {
					pieForUrl.push(pie.baseUrl + pieEvent + standardParameters + index.extraParameters);
				} else {
					pieForUrl.push(pie.baseUrl + pieEvent + standardParameters);
				}
			}
		});
	});
	pieForUrl = pieForUrl.map(function (index){
		index = index.split('[PRODUCT_ID]');
		pieValues.forEach(function (pieValue){
			return index = {name: 'event_callback_' + pieValue, value: encodeURIComponent(index[0]) + '[PRODUCT_ID]' + encodeURIComponent(index[1])};
		})
		return index;
	});

	console.log(pieForUrl);
	completeUrl = 'https://app.adjust.com/' + document.getElementById('base-url-value').value + '?';
	parameterValues = document.getElementsByClassName('parameter-value');
	parameterNames = document.getElementsByClassName('parameter-name');
	parameters = [];
	for (i = 0; i < parameterValues.length; i++) {
		parameters.push({name: parameterNames[i].innerText, value: parameterValues[i].value});
	}
	pieForUrl.forEach(function (index){parameters.push(index)});
	if (os.innerText == 'Android') {
		parameters.push({name: 'android_id_lower_sha1', value: '[SHA1_ANDROID_ID]'})
		parameters.push({name: 'gps_adid', value: '[GOOGLE_AD_ID]'})
		parameters.push({name: 'install_callback', value: 'https%3A%2F%2Fcpa.adcolony.com%2Fon_user_action%3Fapi_key%3D7d442b42abea4549de732321531487fe%26product_id%3D[PRODUCT_ID]%26raw_android_id%3D%7Bandroid_id%7D%26google_ad_id%3D%7Bgps_adid%7D'})
	} else {
		parameters.push({name: 'idfa', value: '[IDFA]'})
		parameters.push({name: 'install_callback', value: 'https%3A%2F%2Fcpa.adcolony.com%2Fon_user_action%3Fapi_key%3D7d442b42abea4549de732321531487fe%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D'})
	}
	console.log(parameters);

	parameters.forEach(function (index){
		completeUrl = completeUrl + index.name + '=' + index.value + '&';
	});
	
	completeUrl = completeUrl.slice(0, -1);
	console.log(completeUrl);
	var html = '<p>' + completeUrl + '</p>';
	$('#complete-url').html(html);
}