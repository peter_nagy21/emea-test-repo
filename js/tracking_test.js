redirectURL = function(string, outputdiv) {
	var sub_str1 = "&response_format=json";
	var sub_str2 = "s2s=1&";
	var sub_str3 = "&redirect=false";
	var redURL = string.replace(sub_str1, "");
	redURL = redURL.replace(sub_str2, "");
	redURL = redURL.replace(sub_str3, "");
	console.log(redURL);
	$(outputdiv).html("<a class='blue' href='" + redURL + "'target='_blank'>Click here to be redirected to the app!</a><br /><br />");
}
divToggle = function(init, tlink, tdiv, ard, aru) {
	var isOpen = init;
	if (isOpen) {
		$(tdiv).show();
		$(aru).show();
		$(ard).hide();
	} else {
		$(tdiv).hide();
		$(aru).hide();
		$(ard).show();
	}
	$(tlink).click(function() {
		$(tdiv).slideToggle({
			duration : 400,
		});
		if (isOpen) {
			$(ard).show();
			$(aru).hide();
			isOpen = false;
		} else {
			$(ard).hide();
			$(aru).show();
			isOpen = true;
		}
	});
}
splitURL = function(string, div_id) {
	var res1 = string.split("?");
	console.log(params);
	var base_url = res1[0];
	var params_string = res1[1];
	var params = params_string.split("&");
	var params2 = [];
	var tablestr = "<table><tr><td>Base URL:</td><td>";
	tablestr += base_url;
	tablestr += "</td></tr>";

	for (var i = 0; i < params.length; i++) {
		var psub = params[i].split("=");
		params2[i, 0] = psub[0];
		params2[i, 1] = psub[1];
		tablestr += "<tr><td>";
		tablestr += params2[i, 0];
		tablestr += "</td><td>";
		tablestr += params2[i, 1];
		tablestr += "</td></tr>";
	}

	tablestr += "</table><br />";
	$(div_id).html(tablestr);
}
splitURLsort = function(string, div_id) {
	var res1 = string.split("?");
	var base_url = res1[0];
	var params_string = res1[1];
	var params = params_string.split("&");
	params.sort();
	var params2 = [];
	var tablestr = "<table><tr><td>Base URL:</td><td>";
	tablestr += base_url;
	tablestr += "</td></tr>";

	for (var i = 0; i < params.length; i++) {
		var psub = params[i].split("=");
		params2[i, 0] = psub[0];
		params2[i, 1] = psub[1];
		tablestr += "<tr><td>";
		tablestr += params2[i, 0];
		tablestr += "</td><td>";
		tablestr += params2[i, 1];
		tablestr += "</td></tr>";
	}

	tablestr += "</table><br />";
	$(div_id).html(tablestr);
}
getURLparams = function(input_id, output_id) {
	var url = document.getElementById(input_id).value;
	console.log(url);
	console.log(output_id);
	splitURL(url, output_id);
}
getURLparamsSort = function(input_id, output_id) {
	var url = document.getElementById(input_id).value;
	console.log(url);
	console.log(output_id);
	splitURLsort(url, output_id);
}
// testing - still not working
/*

var output_table1 = "";
var output_table2 = "";
var output_text_id = "";

url_to_array = function(input_id1, input_id2, output_table1, output_table2, output_text_id) {
	var url1 = document.getElementById(input_id1).value;
	var url2 = document.getElementById(input_id2).value;

	// params1 array
	var res1 = url1.split("?");
	var base_url1 = res1[0];
	var params_string1 = res1[1];
	var params1 = params_string1.split("&");
	params1.sort();
	var pl1 = params1.length;
	console.log(pl1);
	params_arr1 = [];
	for (var k = 1; k <= pl1; k++) {
		params_arr1[k - 1] = params1[k - 1].split("=");
		//console.log(params_arr1[k - 1][0]);
	}

	// params2 array
	var res2 = url2.split("?");
	var base_url2 = res2[0];
	var params_string2 = res2[1];
	var params2 = params_string2.split("&");
	params2.sort();
	var pl2 = params2.length;
	console.log(pl2);
	params_arr2 = [];
	for (var l = 1; l <= pl2; l++) {
		params_arr2[l - 1] = params2[l - 1].split("=");
		//console.log(params_arr2[l - 1][0]);
	}

	// compare params1 and params2
	for (var i = 0; i < pl1; i++) {
	}
	var foundit = false;
	for (var j = 0; j < pl2; j++) {
		var comp1 = params_arr1[i][0];
		console.log(comp1);
		var comp2 = params_arr2[j][0];
		console.log(comp2);

		if (comp1 === comp1) {
			foundit = true;
			console.log(foundit);
		}
	}
	console.log(foundit);
}
url_to_array("input_url", "input_url2", output_table1, output_table2, output_text_id);

*/
// End of testing

showURL2 = function() {
	$("#geturl2").show();
	console.log("something");
}

$(document).ready(function() {
	$("#geturl2").hide();
	$("#comp_url_toggle").click(function() {
		$("#geturl1").toggleClass("geturl1_visible");
		$("#geturl2").toggleClass("geturl2_visible");
	});
});
