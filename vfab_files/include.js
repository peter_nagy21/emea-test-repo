jQuery(document).ready(function(){
  
	jQuery('a[href*=#]').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
		&& location.hostname == this.hostname) {
			var $target = jQuery(this.hash);
			$target = $target.length && $target
			|| jQuery('[name=' + this.hash.slice(1) +']');
			if ($target.length) {
				var targetOffset = $target.offset().top;
				jQuery('html,body')
				.animate({scrollTop: targetOffset}, 1000);
				return false;
			}
		}
	});
});

/* post meta boxes functions */

var visual_content_items = Array('_title_','_thumbnail_link_','_large_image_link_','_type_','_category_');

function taylorjamesExtraFieldsRemove(elem) {
  var doIt = confirm('Are you sure you want to delete this item?');
  if (doIt) {
    var container = jQuery(elem).closest("div");
    jQuery(elem).closest("div").fadeOut(400,function(){
      if (jQuery("#tj_cd_extra_fields_container div:visible").length == 0) {
        jQuery(this).find(".tj_extra_fields_size").remove();
        jQuery(this).clearForm();
      } else {
        jQuery(elem).closest("div").remove();
      }
      taylorjamesResetExtraFields();
    });
  }
}

function taylorjamesVisualContentRemove(elem) {
  var doIt = confirm('Are you sure you want to delete this item?');
  if (doIt) {
    var container = jQuery(elem).closest("ul");
    jQuery(elem).closest("li").fadeOut(400,function(){
      var elem_class = (jQuery(this).find(".tj_visual_content_type").val() == 'still') ? '.taylorjames_visual_content_still' : '.taylorjames_visual_content_video';
      if (jQuery("#taylorjames_visual_content_sortable "+elem_class+":visible").length == 0) {
        jQuery(this).find(".tj_visual_content_type").remove();
        if (jQuery("#taylorjames_visual_content_sortable li:visible").length == 0)  jQuery("#taylorjames_visual_content div:first").hide();
        jQuery("#taylorjames_visual_content .taylorjames_drag_area").hide();
        jQuery(this).find(".taylorjames_visual_content_thumb").css("background-image","");
        jQuery(this).clearForm();
      } else {
        jQuery(elem).closest("li").remove();
        if (jQuery("#taylorjames_visual_content_sortable li:visible").length == 1) {
          jQuery("#taylorjames_visual_content .taylorjames_drag_area").hide();
        }
      }
      taylorjamesResetVisualContentFields(container);
    });
  }
}

function taylorjamesBTSRemove(elem) {
  var doIt = confirm('Are you sure you want to delete this item?');
  if (doIt) {
    var container = jQuery(elem).closest("ul");
    jQuery(elem).closest("li").fadeOut(400,function(){
      var elem_class = (jQuery(this).find(".tj_bts_type").val() == 'still') ? '.taylorjames_bts_still' : '.taylorjames_bts_video';
      if (jQuery("#taylorjames_bts_sortable "+elem_class+":visible").length == 0) {
        jQuery(this).find(".tj_bts_type").remove();
        jQuery("#taylorjames_bts_sortable .taylorjames_drag_area").hide();
        jQuery(this).find(".taylorjames_visual_content_thumb").css("background-image","");
        jQuery(this).clearForm();
      } else {
        jQuery(elem).closest("li").remove();
        if (jQuery("#taylorjames_bts_sortable li:visible").length == 1) {
          jQuery("#taylorjames_bts_sortable .taylorjames_drag_area").hide();
        }
      }
      taylorjamesResetVisualContentFields(container);
    });
  }
}

function taylorjamesSlideShowRemove(elem) {
  var doIt = confirm('Are you sure you want to delete this item?');
  if (doIt) {
    var container = jQuery(elem).closest("ul");
    jQuery(elem).closest("li").fadeOut(400,function(){
      var elem_class ='.taylorjames_slideshow_still';
      if (jQuery("#taylorjames_slideshow_sortable "+elem_class+":visible").length == 0) {
        jQuery(this).find(".tj_visual_content_type").remove();
        jQuery("#taylorjames_slideshow_sortable .taylorjames_drag_area").hide();
        jQuery(this).find(".taylorjames_visual_content_thumb").css("background-image","");
        jQuery(this).clearForm();
      } else {
        jQuery(elem).closest("li").remove();
        if (jQuery("#taylorjames_slideshow_sortable li:visible").length == 1) {
          jQuery("#taylorjames_slideshow_sortable .taylorjames_drag_area").hide();
        }
      }
      taylorjamesResetSlideShowFields(container);
    });
  }
}

function taylorjamesImageBrowse(elem) {
  if (typeof jQuery.getUrlVar('post') == 'undefined') {
    alert('No post id found, please save as draft first');
    jQuery(elem).blur();
  } else {
    var ajax_string = '/wp-content/themes/taylorjames/browse_images.php?id=' + jQuery.getUrlVar('post') + '&elem='+jQuery(elem).attr("id") + '&type=' + jQuery(elem).attr("data-type");
    jQuery('#ex2').jqm({ajax: ajax_string,modal:false}).jqmShow();
  }
}

function taylorjamesResetVisualContentFields(elem) {
  var i=1;
  if (jQuery(elem).attr("tagName") == "A") {
    var container = '#' + jQuery(elem).closest("div").prev().attr("id");
    var section = jQuery(elem).closest("div").prev().attr("id");
  } else if (jQuery(elem).attr("tagName") == "UL") {
    var container = '#' + jQuery(elem).closest("div").parent().parent().attr("id");
    var section = jQuery(elem).attr("id");
  }
  
  var field_names = (section == 'taylorjames_bts' || section == 'taylorjames_bts_sortable') ? '_tj_bts_' : '_tj_visual_content_';

  jQuery(container + " li:visible").each(function(){
    var loop=0;
    if (typeof jQuery(this).find(".tj_visual_content_type").val() == 'undefined') {
      var pos_string = (jQuery(this).find(".tj_bts_type").val() == 'still') ? 'Still' : 'Video';
    } else {
      var pos_string = (jQuery(this).find(".tj_visual_content_type").val() == 'still') ? 'Still' : 'Video';
    }
    jQuery(this).find("legend span").text(pos_string + ' at Position #' + i);
    var section = jQuery(this).parent().attr("id");
    jQuery(this).find("label").each(function(){
      jQuery(this).attr("for",section+visual_content_items[loop] + String(i));
      jQuery(this).next("input,select,textarea").attr("id",section+visual_content_items[loop] + String(i));
      if (jQuery(this).next("select").hasClass("tj_select_multiple") || jQuery(this).next("select").hasClass("tj_select_multiple_alt")) {
        jQuery(this).next("input,select,textarea").attr("name",field_names + "field_" + String(loop + 1) + '_' + String(i)+ '[]');
      } else {
        jQuery(this).next("input,select,textarea").attr("name",field_names + "field_" + String(loop + 1) + '_' + String(i));
      }
      jQuery(this).closest("li").find(".taylorjames_visual_content_thumb").attr("id","_taylorjames_visual_content_thumb_"+String(i));
      loop++;
    });     
    i++;
  });
  jQuery(container+"_sortable li:hidden").each(function(){
    jQuery(this).find("label").each(function(){
      jQuery(this).attr("for","");
      jQuery(this).next("input,select").attr("id","");
      jQuery(this).closest("li").find(".taylorjames_visual_content_thumb").attr("id","");
    });
  });
}

function taylorJamesFillSortingArray() {
  var ids = new Array();
  jQuery("#tj_sorting_list li").each(function(){
    ids.push(jQuery(this).attr("data-id"));
  });
  jQuery("#tj_sorting_values").val(ids.join(','));
}

function taylorjamesResetExtraFields() {
  var field_names = '_tj_cd_extra_';

  var i=1;
  jQuery("#tj_cd_extra_fields_container div:visible").each(function(){
    var loop=0;
    var section = jQuery(this).parent().attr("id");
    jQuery(this).find("label").each(function(){
      jQuery(this).attr("for",field_names + "field_" + String(loop + 1) + '_' + String(i));
      jQuery(this).next("input,select,textarea").attr("id",field_names + "field_" + String(loop + 1) + '_' + String(i));
      if (jQuery(this).next("select").hasClass("tj_select_multiple")) {
        jQuery(this).next("input,select,textarea").attr("name",field_names + "field_" + String(loop + 1) + '_' + String(i)+ '[]');
      } else {
        jQuery(this).next("input,select,textarea").attr("name",field_names + "field_" + String(loop + 1) + '_' + String(i));
      }
      loop++;
    });     
    i++;
  });
  jQuery("#tj_cd_extra_fields_container div:hidden").each(function(){
    jQuery(this).find("label").each(function(){
      jQuery(this).attr("for","");
      jQuery(this).next("input,select").attr("id","");
    });
  });
}

function taylorjamesResetSlideShowFields(elem) {
  var i=1;
  if (jQuery(elem).attr("tagName") == "A") {
    var container = '#taylorjames_slideshow_sortable';
    var section = jQuery(elem).closest("div").prev().attr("id");
  } else if (jQuery(elem).attr("tagName") == "UL") {
    var container = '#taylorjames_slideshow_sortable';
    var section = jQuery(elem).attr("id");
  }
  
  var field_names = '_tj_slideshow_';

  jQuery(container + " li:visible").each(function(){
    var loop=0;
    var pos_string = '';
    jQuery(this).find("legend span").text(pos_string + 'Position #' + i);
    var section = jQuery(this).parent().attr("id");
    jQuery(this).find("label").each(function(){
      jQuery(this).attr("for",section+visual_content_items[loop] + String(i));
      jQuery(this).next("input,select,textarea").attr("id",section+visual_content_items[loop] + String(i));
      if (jQuery(this).next("select").hasClass("tj_select_multiple")) {
        jQuery(this).next("input,select,textarea").attr("name",field_names + "field_" + String(loop + 1) + '_' + String(i)+ '[]');
      } else {
        jQuery(this).next("input,select,textarea").attr("name",field_names + "field_" + String(loop + 1) + '_' + String(i));
      }
      jQuery(this).closest("li").find(".taylorjames_visual_content_thumb").attr("id","_taylorjames_visual_content_thumb_"+String(i));
      loop++;
    });     
    i++;
  });
  jQuery(container+"_sortable li:hidden").each(function(){
    jQuery(this).find("label").each(function(){
      jQuery(this).attr("for","");
      jQuery(this).next("input,select").attr("id","");
      jQuery(this).closest("li").find(".taylorjames_visual_content_thumb").attr("id","");
    });
  });
}

jQuery(document).ready(function(){

  Date.format = 'dd/mm/yyyy';
  if(jQuery("#taylorjames_slideshow_sortable li:visible .date-pick").length > 0) jQuery('#taylorjames_slideshow_sortable li:visible .date-pick').datePicker();  

  jQuery(".toggle_show_hide").click(function(){
    var text = (jQuery(this).text() == 'show') ? 'hide' : 'show';
    var visible = (jQuery(this).parent().next().is(':visible')) ? true : false;
    jQuery(this).text(text);
    if (visible) {
      jQuery(this).parent().next().hide();
    } else {
      jQuery(this).parent().next().show();
    }
    return false;
  });

  if (jQuery("#taylorjames_visual_content_sortable li:visible").length <= 1) {
    jQuery("#taylorjames_visual_content_sortable li:visible").find(".taylorjames_drag_area").hide();
  }

  if (jQuery("#taylorjames_bts_sortable li:visible").length <= 1) {
    jQuery("#taylorjames_bts_sortable li:visible").find(".taylorjames_drag_area").hide();
  }
  
  if (jQuery("#taylorjames_slideshow_sortable li:visible").length <= 1) {
    jQuery("#taylorjames_slideshow_sortable li:visible").find(".taylorjames_drag_area").hide();
  }
  
	var i=1;
	jQuery('.tj_customEditor textarea').each(function(e)
	{
		var id = jQuery(this).attr('id');

		if (!id)
		{
			id = 'tj_customEditor-' + i++;
			jQuery(this).attr('id',id);
		}
		tinyMCE.execCommand('mceAddControl', false, id);
	});
  
  jQuery('.tj_select_multiple').live('mouseover',function(){
    jQuery(this).closest('fieldset').find(".taylorjames_visual_content_category_helper small").css("opacity",1);
  });

  jQuery('.tj_select_multiple').live('mouseout',function(){
    jQuery(this).closest('fieldset').find(".taylorjames_visual_content_category_helper small").css("opacity",0);
  });
  
  jQuery('#tj_images_select_choose, .tj_images_select_image_container div img').live('click',function(){
    jQuery.get('/wp-content/themes/taylorjames/browse_images.php?get_image_url='+jQuery('.tj_images_select option:selected').val(),function(data){
      if (data.length > 0) {
        var exists = false;
        var section = (jQuery('#'+jQuery('.tj_images_select').attr("data-elem")).closest("ul").attr("id"));
        jQuery('#'+section+' input').each(function(){
          if (jQuery(this).val() == data) {
            exists = true;
          }
        });
        var doIt = true;
        if (exists) {
          doIt = confirm('This image is already on another still, proceed anyway?');
        }
        if (doIt) {
          jQuery('#'+jQuery('.tj_images_select').attr("data-elem")).val(data);
          var percentage = Math.round((128*100) / parseInt(jQuery('.tj_images_select :selected').attr("data-width")));
          var height = Math.round((parseInt(jQuery('.tj_images_select :selected').attr("data-height")) * percentage) / 100);
          if (jQuery('#'+jQuery('.tj_images_select').attr("data-elem")).attr("data-type") == "thumbnail") {
            var thumb_preview = (jQuery("#"+jQuery('.tj_images_select').attr("data-elem")).closest("li").find(".tj_visual_content_type").val() == 'still') ? '.taylorjames_visual_content_thumb' : '.taylorjames_visual_content_thumb_video';
            jQuery("#"+jQuery('.tj_images_select').attr("data-elem")).parent().parent().find(thumb_preview).find("img").remove();
            var image = jQuery("<img />");
            jQuery(image).attr("width",128);
            jQuery(image).attr("height",height);
            jQuery(image).attr("src",jQuery('.tj_images_select_image_container div img').attr("src"));
            jQuery(image).appendTo( jQuery("#"+jQuery('.tj_images_select').attr("data-elem")).parent().parent().find(thumb_preview));
          }
          if (jQuery('#'+jQuery('.tj_images_select').attr("data-elem")).attr("data-type") == "blog_image" || jQuery('#'+jQuery('.tj_images_select').attr("data-elem")).attr("data-type") == "bts" || jQuery('#'+jQuery('.tj_images_select').attr("data-elem")).attr("data-type") == "slideshow" || jQuery('#'+jQuery('.tj_images_select').attr("data-elem")).attr("data-type") == "discipline" || jQuery('#'+jQuery('.tj_images_select').attr("data-elem")).attr("data-type") == "pickmix") {
            var thumb_preview = '.taylorjames_visual_content_thumb';
            jQuery("#"+jQuery('.tj_images_select').attr("data-elem")).parent().parent().find(thumb_preview).find("img").remove();
            var image = jQuery("<img />");
            jQuery(image).attr("width",128);
            jQuery(image).attr("height",height);
            jQuery(image).attr("src",jQuery('.tj_images_select_image_container div img').attr("src"));
            jQuery(image).appendTo( jQuery("#"+jQuery('.tj_images_select').attr("data-elem")).parent().parent().find(thumb_preview));
          }
          jQuery('#ex2').jqmHide();
        }
      }
    });
    return false;
  })

  jQuery('.tj_images_select').live('change',function(){
    if (jQuery(this).val() != '') {
      var selected_item = jQuery(this);
      jQuery.get("/wp-content/themes/taylorjames/browse_images.php?get_thumb_url="+jQuery(this).val(),function(data){
        if (data.length > 0) {
          var thumbImage = jQuery("<img>");
          jQuery(thumbImage).attr("width",parseInt(jQuery(selected_item).find(":selected").attr("data-width") / 3));
          jQuery(thumbImage).attr("height",parseInt(jQuery(selected_item).find(":selected").attr("data-height") / 3));
          if (jQuery('#'+jQuery('.tj_images_select').attr("data-elem")).attr("data-type") == 'thumbnail') {
            jQuery(thumbImage).attr("width",parseInt(jQuery(selected_item).find(":selected").attr("data-width")));
            jQuery(thumbImage).attr("height",parseInt(jQuery(selected_item).find(":selected").attr("data-height")));
          } else {
          }
          jQuery(thumbImage).attr("src",data);
          if (jQuery('#tj_images_select_container div:hidden').length > 0) jQuery('#tj_images_select_container div').show();
          if (jQuery('#tj_images_select_choose:hidden').length > 0) jQuery('#tj_images_select_choose').show();
          jQuery('#tj_images_select_container div div').html('');
          jQuery(thumbImage).appendTo('#tj_images_select_container div div');
        }
      })
    } else {
      jQuery('#tj_images_select_choose').hide();
      jQuery('#tj_images_select_container div').hide();
      jQuery('#tj_images_select_container div div').html('');
    }
  });
  
  jQuery('#tj_projects_sorting_select').live('change',function(){
    if (jQuery(this).val() != '') {
      document.location.href = './edit.php?post_type=project&page=projectsorder&category=' + jQuery(this).val();
    }
  });

  /* post meta boxes */
  jQuery("#tj_client_details_more").click(function(){
    jQuery('.taylorjames_client_details div:first').toggle();
    jQuery('#tj_client_details_add_more').toggle();
    jQuery(this).parent().toggle();
    
    if (jQuery("#tj_cd_extra_fields_container div").length > 0) {
      jQuery("#tj_cd_extra_fields_container div").each(function(){
        if (jQuery(this).find(".tj_extra_fields_size").length > 0) {
          jQuery(this).closest("div").show();
        }
      });
    }
    return false;
  });
  
  if (jQuery("#taylorjames_visual_content_sortable").length > 0) {
    jQuery("#taylorjames_visual_content_sortable").sortable({
      stop: function(event, ui) {
        taylorjamesResetVisualContentFields(this);
      },
      handle: '.taylorjames_drag_area'
    });
  }

  if (jQuery("#taylorjames_bts_sortable").length > 0) {
    jQuery("#taylorjames_bts_sortable").sortable({
      stop: function(event, ui) {
        taylorjamesResetVisualContentFields(this);
      },
      handle: '.taylorjames_drag_area'
    });
  }

  if (jQuery("#taylorjames_slideshow_sortable").length > 0) {
    jQuery("#taylorjames_slideshow_sortable").sortable({
      stop: function(event, ui) {
        taylorjamesResetSlideShowFields(this);
      },
      handle: '.taylorjames_drag_area'
    });
  }
  
  if (jQuery("#tj_sorting_list").length > 0) {
    taylorJamesFillSortingArray();
    jQuery("#tj_sorting_list").sortable({
      stop: function(event, ui) {
        taylorJamesFillSortingArray();
      }
    });
  }
  
  //extra fields for client details
  jQuery("#tj_client_details_add_more").click(function(){
    var element = jQuery("#tj_cd_extra_fields_container div:first").clone().appendTo(jQuery("#tj_cd_extra_fields_container"));
    if (jQuery("#tj_cd_extra_fields_container div:first:hidden").length > 0 || jQuery(".taylorjames_extra_fields:hidden").length > 0) {
      jQuery("#tj_cd_extra_fields_container div:first").remove();
      jQuery(element).show();
      jQuery("#tj_cd_extra_fields_container div:first:hidden").show();
    }
    //hidden field for type
    if (jQuery(element).find(".tj_extra_fields_size").length > 0) {
      jQuery(element).find(".tj_extra_fields_size").val("1");
    } else {
      var content_type = jQuery("<input />");
      jQuery(content_type).attr("name","_tj_extra_fields_size[]");
      jQuery(content_type).addClass("tj_extra_fields_size");
      jQuery(content_type).val("1");
      jQuery(content_type).attr("type","hidden");
      jQuery(content_type).appendTo(element);
    }
    
    taylorjamesResetExtraFields();
    
    //end hidden field for type
    jQuery(element).clearForm();
    jQuery.scrollTo(element);
    jQuery("#tj_cd_extra_fields_container div:last").find("input:first").focus();
    return false;
  });
  
  jQuery("#tj_visual_content_add_still,#tj_visual_content_add_video").click(function(){
    var elem = (jQuery(this).attr("id") == 'tj_visual_content_add_still') ? '.taylorjames_visual_content_still' : '.taylorjames_visual_content_video';
    var element = jQuery("#taylorjames_visual_content_sortable " + elem + ":first").clone().appendTo(jQuery("#taylorjames_visual_content_sortable"));
    if (jQuery("#taylorjames_visual_content div:first:hidden").length > 0 || jQuery(elem+":hidden").length > 0) {
      jQuery("#taylorjames_visual_content_sortable " + elem + ":first").remove();
      jQuery(element).show();
      jQuery("#taylorjames_visual_content div:first:hidden").show();
    }
    //hidden field for type
    if (jQuery(element).find(".tj_visual_content_type").length > 0) {
      if (jQuery(this).attr("id") == 'tj_visual_content_add_still') {
        jQuery(element).find(".tj_visual_content_type").val("still");
      } else {
        jQuery(element).find(".tj_visual_content_type").val("video");
      }      
    } else {
      var content_type = jQuery("<input />");
      jQuery(content_type).attr("name","_tj_visual_content_type[]");
      jQuery(content_type).addClass("tj_visual_content_type");
      if (jQuery(this).attr("id") == 'tj_visual_content_add_still') {
        jQuery(content_type).val("still");
      } else {
        jQuery(content_type).val("video");
      }
      jQuery(content_type).attr("type","hidden");
      jQuery(content_type).appendTo(element);
    }
    //end hidden field for type

    taylorjamesResetVisualContentFields(this);
    
    if (jQuery("#taylorjames_visual_content li:visible").length > 1) {
      jQuery("#taylorjames_visual_content .taylorjames_drag_area:hidden").show();
    }
    jQuery(element).find(".taylorjames_visual_content_thumb").find("img").remove();
    jQuery(element).find(".taylorjames_visual_content_thumb_video").find("img").remove();
    jQuery(element).clearForm();
    jQuery.scrollTo(element);
    jQuery(element).find("input:first").focus();
    return false;
  });
  
  jQuery("#tj_bts_add_still,#tj_bts_add_video").click(function(){
    var elem = (jQuery(this).attr("id") == 'tj_bts_add_still') ? '.taylorjames_bts_still' : '.taylorjames_bts_video';
    var element = jQuery("#taylorjames_bts_sortable " + elem + ":first").clone().appendTo(jQuery("#taylorjames_bts_sortable"));
    if (jQuery("#taylorjames_bts div:first:hidden").length > 0 || jQuery(elem+":hidden").length > 0) {
      jQuery("#taylorjames_bts_sortable " + elem + ":first").remove();
      jQuery(element).show();
      jQuery("#taylorjames_bts div:first:hidden").show();
    }
    //hidden field for type
    if (jQuery(element).find(".tj_bts_type").length > 0) {
      if (jQuery(this).attr("id") == 'tj_bts_add_still') {
        jQuery(element).find(".tj_bts_type").val("still");
      } else {
        jQuery(element).find(".tj_bts_type").val("video");
      }      
    } else {
      var content_type = jQuery("<input />");
      jQuery(content_type).attr("name","_tj_bts_type[]");
      jQuery(content_type).addClass("tj_bts_type");
      if (jQuery(this).attr("id") == 'tj_bts_add_still') {
        jQuery(content_type).val("still");
      } else {
        jQuery(content_type).val("video");
      }
      jQuery(content_type).attr("type","hidden");
      jQuery(content_type).appendTo(element);
    }
    //end hidden field for type
    taylorjamesResetVisualContentFields(this);
    if (jQuery("#taylorjames_bts li:visible").length > 1) {
      jQuery("#taylorjames_bts .taylorjames_drag_area:hidden").show();
    }
    jQuery(element).find(".taylorjames_visual_content_thumb").find("img").remove();
    jQuery(element).find(':input').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');   
    jQuery.scrollTo(element);
    jQuery(element).find("input:first").focus();
    return false;
  });
  
  //homepage slideshow
  jQuery("#tj_slideshow_add_still").click(function(){
    var elem = '.taylorjames_slideshow_still';
    var element = jQuery("#taylorjames_slideshow_sortable " + elem + ":first").clone().appendTo(jQuery("#taylorjames_slideshow_sortable"));

    jQuery(element).show();

    //hidden field for type
    if (jQuery(element).find(".tj_slideshow_type").length > 0) {
      jQuery(element).find(".tj_slideshow_type").val("still");
    } else {
      var content_type = jQuery("<input />");
      jQuery(content_type).attr("name","_tj_slideshow_type[]");
      jQuery(content_type).addClass("tj_slideshow_type");
      jQuery(content_type).val("still");
      jQuery(content_type).attr("type","hidden");
      jQuery(content_type).appendTo(element);
    }
    //end hidden field for type
    taylorjamesResetSlideShowFields(this);
    if (jQuery("#taylorjames_slideshow_sortable li:visible").length > 1) {
      jQuery("#taylorjames_slideshow_sortable .taylorjames_drag_area:hidden").show();
    }
    jQuery(element).find(".taylorjames_visual_content_thumb").find("img").remove();
    jQuery(element).find(':input').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');   
    jQuery.scrollTo(element);
    jQuery(element).find("input:first").focus();
    jQuery(element).find("a.dp-choose-date").remove();
    jQuery(element).find(".date-pick").removeClass('dp-applied');
    jQuery(element).find('.date-pick').datePicker();
    return false;
  });
  
  jQuery("#tj_bts_add_content").click(function(){
    jQuery('#taylorjames_bts div:first:hidden').show();
    jQuery(this).parent().hide()
    jQuery(this).parent().next().show();
    jQuery(this).parent().next().next().show();
  });

  //previous code

  jQuery('a.blog-more').click(function(){
  		var thisid = jQuery(this).attr("id");
  		thisid = thisid.replace(/read-more-/,"");
  		jQuery(this).hide();
        jQuery('#post-content-' + thisid).show('slow');
        jQuery('#read-less-' + thisid).fadeIn();
  });

  jQuery('a.blog-close').click(function(){
  		 var thisid = jQuery(this).attr("id");
    	 thisid = thisid.replace(/read-less-/,"");
    	 jQuery(this).hide();
        jQuery('#post-content-' + thisid).hide('slow');
        jQuery('#read-more-' + thisid).fadeIn();         
      })
    
        jQuery('a.about-more').click(function(){
    		var thisid = jQuery(this).attr("id");
    		thisid = thisid.replace(/read-more-/,"");
    		jQuery(this).hide();
          jQuery('#post-content-' + thisid).show('slow');
          jQuery('#read-less-' + thisid).fadeIn();
          //jQuery('#about-excerpt').hide();
    });

   jQuery('a.about-close').click(function(){
   		 var thisid = jQuery(this).attr("id");
      	 thisid = thisid.replace(/read-less-/,"");
      	 jQuery(this).hide();
          jQuery('#post-content-' + thisid).hide('slow');
          jQuery('#read-more-' + thisid).fadeIn(); 
         // jQuery('#about-excerpt').show();        
        });
        
         jQuery('ul.miniGallery li a').click(function(){
	   		 jQuery('#added-to-lightbox').hide();
	   		 jQuery('#add-to-lightbox').show(); 
        });
        
         jQuery('#add-to-lightbox').click(function(){
	   		 jQuery('#added-to-lightbox').show();
	   		 jQuery('#add-to-lightbox').hide(); 
	   		 var lbimg = jQuery('#main_project_image').attr('src'); 
	   		 var newvidth = jQuery("#project-thumbnails img.active").attr("src"); 
	   		 var newvid = jQuery("#main_project_video").html(); 
	   		 jQuery.post("/lightbox.php", { newimg: lbimg, newvidth: newvidth, newvid: newvid} );
        });
        

        
         jQuery('#cs-add-to-lightbox').click(function(){
	   		 jQuery('#added-to-lightbox').show();
	   		 jQuery('#cs-add-to-lightbox').hide(); 
	   		 var lbimg = jQuery("#case-study-slideshow img.active").attr("src"); 
	   		 jQuery.post("/lightbox.php", { newimg: lbimg} );
        });
        
         jQuery('#slideshow-next').click(function(){
	   		 jQuery('#added-to-lightbox').hide();
	   		 jQuery('#cs-add-to-lightbox').show(); 
        }); 
        jQuery('#slideshow-prev').click(function(){
	   		 jQuery('#added-to-lightbox').hide();
	   		 jQuery('#cs-add-to-lightbox').show(); 
        });        
   
        
          jQuery('#mailing-button').click(function(){
            jQuery('#mailing-content').show('slow');
      });

        
        // jQuery("#clients-button[rel]").overlay();
        // jQuery("#googlemap-button[rel]").overlay();
        // jQuery("#lightbox img[rel]").overlay({fixed:false});
        
        // jQuery('.lightbox-gallery > li').draggable();
    
});

jQuery(function() {
	// initialize scrollable
  // jQuery(".scrollable").scrollable({
  //  circular: true
  // });
});

jQuery.extend({
  getUrlVars: function(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name){
    return jQuery.getUrlVars()[name];
  }
});

jQuery.extend({
  clearForm:function() {
  return this.each(function() {
    var type = this.type, tag = this.tagName.toLowerCase();
    if (tag == 'form')
      return $(':input',this).clearForm();
    if (type == 'text' || type == 'password' || tag == 'textarea')
      this.value = '';
    else if (type == 'checkbox' || type == 'radio')
      this.checked = false;
    else if (tag == 'select')
      this.selectedIndex = -1;
  });
}
});  

;(function($, iphoneStyle) {

// Constructor
$[iphoneStyle] = function(elem, options) {
  this.$elem = $(elem);
  
  // Import options into instance variables
  var obj = this;
  $.each(options, function(key, value) {
    obj[key] = value;
  });
  
  // Initialize the control
  this.wrapCheckboxWithDivs();
  this.attachEvents();
  this.disableTextSelection();
  
  if (this.resizeHandle)    { this.optionallyResize('handle'); }
  if (this.resizeContainer) { this.optionallyResize('container'); }
  
  this.initialPosition();
};

$.extend($[iphoneStyle].prototype, {
  // Wrap the existing input[type=checkbox] with divs for styling and grab DOM references to the created nodes
  wrapCheckboxWithDivs: function() {
    this.$elem.wrap('<div class="' + this.containerClass + '" />');
    this.container = this.$elem.parent();
    
    this.offLabel  = $('<label class="'+ this.labelOffClass +'">' +
                         '<span>'+ this.uncheckedLabel +'</span>' +
                       '</label>').appendTo(this.container);
    this.offSpan   = this.offLabel.children('span');
    
    this.onLabel   = $('<label class="'+ this.labelOnClass +'">' +
                         '<span>'+ this.checkedLabel +'</span>' +
                       '</label>').appendTo(this.container);
    this.onSpan    = this.onLabel.children('span');
    
    this.handle    = $('<div class="' + this.handleClass + '">' +
                         '<div class="' + this.handleRightClass + '">' +
                           '<div class="' + this.handleCenterClass + '" />' +
                         '</div>' +
                       '</div>').appendTo(this.container);
  },
  
  // Disable IE text selection, other browsers are handled in CSS
  disableTextSelection: function() {
    if (!$.browser.msie) { return; }

    // Elements containing text should be unselectable
    $.each([this.handle, this.offLabel, this.onLabel, this.container], function() {
      $(this).attr("unselectable", "on");
    });
  },
  
  // Automatically resize the handle or container
  optionallyResize: function(mode) {
    var onLabelWidth  = this.onLabel.width(),
        offLabelWidth = this.offLabel.width();
        
    if (mode == 'container') {
      var newWidth = (onLabelWidth > offLabelWidth) ? onLabelWidth : offLabelWidth;
      newWidth += this.handle.width() + 15; 
    } else { 
      var newWidth = (onLabelWidth < offLabelWidth) ? onLabelWidth : offLabelWidth;
    }
    
    this[mode].css({ width: newWidth });
  },
  
  attachEvents: function() {
    var obj = this;
    
    // A mousedown anywhere in the control will start tracking for dragging
    this.container
      .bind('mousedown touchstart', function(event) {          
        event.preventDefault();
        
        if (obj.$elem.is(':disabled')) { return; }
          
        var x = event.pageX || event.originalEvent.changedTouches[0].pageX;
        $[iphoneStyle].currentlyClicking = obj.handle;
        $[iphoneStyle].dragStartPosition = x;
        $[iphoneStyle].handleLeftOffset  = parseInt(obj.handle.css('left'), 10) || 0;
        $[iphoneStyle].dragStartedOn     = obj.$elem;
      })
    
      // Utilize event bubbling to handle drag on any element beneath the container
      .bind('iPhoneDrag', function(event, x) {
        event.preventDefault();
        
        if (obj.$elem.is(':disabled')) { return; }
        if (obj.$elem != $[iphoneStyle].dragStartedOn) { return; }
        
        var p = (x + $[iphoneStyle].handleLeftOffset - $[iphoneStyle].dragStartPosition) / obj.rightSide;
        if (p < 0) { p = 0; }
        if (p > 1) { p = 1; }
        obj.handle.css({ left: p * obj.rightSide });
        obj.onLabel.css({ width: p * obj.rightSide + 4 });
        obj.offSpan.css({ marginRight: -p * obj.rightSide });
        obj.onSpan.css({ marginLeft: -(1 - p) * obj.rightSide });
      })
    
        // Utilize event bubbling to handle drag end on any element beneath the container
      .bind('iPhoneDragEnd', function(event, x) {
        if (obj.$elem.is(':disabled')) { return; }
        
        var checked;
        if ($[iphoneStyle].dragging) {
          var p = (x - $[iphoneStyle].dragStartPosition) / obj.rightSide;
          checked = (p < 0) ? Math.abs(p) < 0.5 : p >= 0.5;
        } else {
          checked = !obj.$elem.attr('checked');
        }
        
        obj.$elem.attr('checked', checked);

        $[iphoneStyle].currentlyClicking = null;
        $[iphoneStyle].dragging = null;
        obj.$elem.change();
      });
  
    // Animate when we get a change event
    this.$elem.change(function() {
      if (obj.$elem.is(':disabled')) {
        obj.container.addClass(obj.disabledClass);
        return false;
      } else {
        obj.container.removeClass(obj.disabledClass);
      }
      
      var new_left = obj.$elem.attr('checked') ? obj.rightSide : 0;

      obj.handle.animate({         left: new_left },                 obj.duration);
      obj.onLabel.animate({       width: new_left + 4 },             obj.duration);
      obj.offSpan.animate({ marginRight: -new_left },                obj.duration);
      obj.onSpan.animate({   marginLeft: new_left - obj.rightSide }, obj.duration);
    });
  },
  
  // Setup the control's inital position
  initialPosition: function() {
    this.offLabel.css({ width: this.container.width() - 5 });

    var offset = ($.browser.msie && $.browser.version < 7) ? 3 : 6;
    this.rightSide = this.container.width() - this.handle.width() - offset;

    if (this.$elem.is(':checked')) {
      this.handle.css({ left: this.rightSide });
      this.onLabel.css({ width: this.rightSide + 4 });
      this.offSpan.css({ marginRight: -this.rightSide });
    } else {
      this.onLabel.css({ width: 0 });
      this.onSpan.css({ marginLeft: -this.rightSide });
    }
    
    if (this.$elem.is(':disabled')) {
      this.container.addClass(this.disabledClass);
    }
  }
});

// jQuery-specific code
$.fn[iphoneStyle] = function(options) {
  var checkboxes = this.filter(':checkbox');
  
  // Fail early if we don't have any checkboxes passed in
  if (!checkboxes.length) { return this; }
  
  // Merge options passed in with global defaults
  var opt = $.extend({}, $[iphoneStyle].defaults, options);
  
  checkboxes.each(function() {
    $(this).data(iphoneStyle, new $[iphoneStyle](this, opt));
  });

  if (!$[iphoneStyle].initComplete) {
    // As the mouse moves on the page, animate if we are in a drag state
    $(document)
      .bind('mousemove touchmove', function(event) {
        if (!$[iphoneStyle].currentlyClicking) { return; }
        event.preventDefault();
        
        var x = event.pageX || event.originalEvent.changedTouches[0].pageX;
        if (!$[iphoneStyle].dragging &&
            (Math.abs($[iphoneStyle].dragStartPosition - x) > opt.dragThreshold)) { 
          $[iphoneStyle].dragging = true; 
        }
    
        $(event.target).trigger('iPhoneDrag', [x]);
      })

      // When the mouse comes up, leave drag state
      .bind('mouseup touchend', function(event) {        
        if (!$[iphoneStyle].currentlyClicking) { return; }
        event.preventDefault();
    
        var x = event.pageX || event.originalEvent.changedTouches[0].pageX;
        $($[iphoneStyle].currentlyClicking).trigger('iPhoneDragEnd', [x]);
      });
      
    $[iphoneStyle].initComplete = true;
  }
  
  return this;
}; // End of $.fn[iphoneStyle]

$[iphoneStyle].defaults = {
  duration:          200,                       // Time spent during slide animation
  checkedLabel:      'ON',                      // Text content of "on" state
  uncheckedLabel:    'OFF',                     // Text content of "off" state
  resizeHandle:      true,                      // Automatically resize the handle to cover either label
  resizeContainer:   true,                      // Automatically resize the widget to contain the labels
  disabledClass:     'iPhoneCheckDisabled',
  containerClass:    'iPhoneCheckContainer',
  labelOnClass:      'iPhoneCheckLabelOn',
  labelOffClass:     'iPhoneCheckLabelOff',
  handleClass:       'iPhoneCheckHandle',
  handleCenterClass: 'iPhoneCheckHandleCenter',
  handleRightClass:  'iPhoneCheckHandleRight',
  dragThreshold:     5                          // Pixels that must be dragged for a click to be ignored
};

})(jQuery, 'iphoneStyle');
