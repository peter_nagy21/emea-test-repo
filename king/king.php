<?php
include ("http://localhost:8888/password_protect.php");
?>
<?php
include '../header.php';
?>
<?php
include '../sideleft.php';
?>

<?php include_once("analyticstracking.php") ?>

<script type="text/javascript" src="king.js"></script>

<div id="maincontent">
	<h1>King.com</h1>
	<h3 class="black plink" id="notestitle">1. Notes <i class="fa fa-arrow-down" id="notesdown"></i><i class="fa fa-arrow-up" id="notesup"></i></h3>
	<div id="notesdiv" class="ndiv">
		We take the "targetAppId" from the URL and use the same URLs we have for the existing campaigns in the dash.
		<br />
		Check the redirect and make sure the link will open a legit page (No server errors etc...).
		<br /><br />
		<h3 class="black">Example URLs:</h3>
		Click:<br />
		http://play.king.com/click?type=ad&idfa_raw=[IDFA]&network=adcolony&os=[PLATFORM]&product_id=[PRODUCT_ID]&raw_advertising_id=[IDFA]&appId=[STORE_ID]&targetAppId=76&countryCode=[COUNTRY_CODE]&publisher=[PUBLISHER_ID]&site=[APP_ID]&campaignName=[RAW_AD_CAMPAIGN_ID]&creativeName=[AD_CREATIVE_NAME]&creativeSize=&noRedirect=TRUE&bypassFingerprint=true
		<br /><br />
		Impression:<br />
		http://play.king.com/impression?type=video&idfa_raw=[IDFA]&network=adcolony&os=[PLATFORM]&product_id=[PRODUCT_ID]&raw_advertising_id=[IDFA]&appId=[STORE_ID]&targetAppId=76&countryCode=[COUNTRY_CODE]&publisher=[PUBLISHER_ID]&site=[APP_ID]&campaignName=[RAW_AD_CAMPAIGN_ID]&creativeName=[AD_CREATIVE_NAME]&creativeSize=&noRedirect=TRUE&bypassFingerprint=true
		<br /><br />
		
	</div>

	<h3 id="valtitle" class="black plink">2. Tracking URL validation <i class="fa fa-arrow-down" id="valdown"></i><i class="fa fa-arrow-up" id="valup"></i></h3>
	<div id="valdiv">
		Paste the click URL below:
		<br />
		<form id="valform">
			<textarea name="king_url" id="king_url"></textarea>
			<br />
			<input type="button" class="btn-class" value="Validate" onclick="Validateking()">
			<br /><br />
		</form>
		<h3 class="black plink" id="urlparamstitle">Check URL parameters <i class="fa fa-arrow-down" id="urlpdown"></i><i class="fa fa-arrow-up" id="urlpup"></i></h3>
		<div id="vnotes">
			<div id="redurldiv"></div>
			<div id="vnotes_comment"></div>
			<div id="vnotes_alert"></div>
		</div>
		<div class="clear"></div>
	</div>
	
	<h3 class="black plink" id="pbtitle">3. Postback - check the 3rd party Dashboard <i class="fa fa-arrow-down" id="pbdown"></i><i class="fa fa-arrow-up" id="pbup"></i></h3>
	<div id="pbdiv" class="ndiv">
		They don't have a dashboard - we don't have to check the postback. (King has set up a global postback.)
		<br /><br />
	</div>

	<h3 class="black plink" id="imptitle">4. Click / Impression URLs <i class="fa fa-arrow-down" id="impdown"></i><i class="fa fa-arrow-up" id="impup"></i></h3>
	<div id="impdiv">
		<form>
			<fieldset id="king_output">
				<textarea name="king_imp" id="king_imp"></textarea>
				<br />
			</fieldset>
		</form>
		<br />
	</div>
	
	<h3 class="black plink" id="vttitle">5. View Through Attribution Window <i class="fa fa-arrow-down" id="vtdown"></i><i class="fa fa-arrow-up" id="vtup"></i></h3>
	<div id="vtdiv">
		<table id="vt_table">
			<tr>
				<th colspan="2">VIEW ATTRIBUTION</th>
				<th colspan="3">LOOKBACK WINDOWS</th>
				<th>CLIENT-SIDE ACTION</th>
			</tr>
			<tr class="tep" id="tpid">
				<th>Dedicated View Tags</th>
				<th>Recommended Implementation</th>
				<th style="min-width: 120px;">Flexible Lookback Window</th>
				<th>Default Click Lookback</th>
				<th>Default View Lookback</th>
				<th></th>
			</tr>
			<tr>
				<td>Yes</td>
				<td class="lefta">View tag goes on complete, click tag goes on HTML5.</td>
				<td>No</td>
				<td>7 days</td>
				<td>12 hours</td>
				<td class="lefta">No action necessary.<br />Can be enabled at app level.</td>
			</tr>
		</table>
		<br />
	</div>
	<h3 class="black plink" id="pietitle">6. PIE <i class="fa fa-arrow-down" id="piedown"></i><i class="fa fa-arrow-up" id="pieup"></i></h3>
	<div id="piediv">
		King will set up the PIE events on their side.<br/><br/>
	</div>
</div>
<div class="clear"></div>

<?php
include '../footer.php';
?>

