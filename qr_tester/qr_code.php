<?php
include ("http://localhost:8888/password_protect.php");
?>

<?php
include '../header.php';
?>

<?php
include '../sideleft.php';
?>

<?php include_once("analyticstracking.php")
?>

<script type="text/javascript" src="qr_tester.js"></script>

<style type="text/css">
	img#chart {
		margin: 0;
		background: #fff;
		border: 1px solid black;
	}
</style>

<div id="maincontent">

		<h1>Attribution tester page</h1>
		
		<h3 class="black plink" id="notestitle1">Notes <i class="fa fa-arrow-down" id="notesdown1"></i><i class="fa fa-arrow-up" id="notesup1"></i></h3>
		<div id="notesdiv1" class="ndiv">
		We can use the Attribution Tester App as an alternative solution to our current testing process.<br />
		<span class="bold">Benefits:</span> much faster testing process, we would not need to change the targeting for testing.<br />
		<span class="bold">Drawbacks:</span> the Attribution Tester App would test the attribution only, not the campaign setup.<br />
		<br />

		The <span class="green_bold">Android</span> <span class="bold">Attribution Tester App</span> apk file can be downloaded from <a class="blue" href="app-debug.apk">here</a>.
		 (<a class="blue" href="https://www.wikihow.tech/Install-APK-Files-on-Android" target="_blank">install apk file on Andoid</a>)
		<br />
		The <span class="black">iOS</span> <span class="bold">Attribution Tester App</span> can be downloaded via Crashlytics. 
		<br />
		<br />
		</div>

		<h3 class="black plink" id="notestitle">Testing process <i class="fa fa-arrow-down" id="notesdown"></i><i class="fa fa-arrow-up" id="notesup"></i></h3>
		<div id="notesdiv" class="ndiv">
			<ol>
				<li class="qr_li">
					Paste the URL below and generate the QR code
					<ul>
						<li>
							The tool will make some changes to the URL
						</li>
						<li>
							In the final link some macros / values are highlighted:
						</li>

						<div id="keydiv">
							<table id="keytable" style="margin: 10px;">
								<tr>
									<td style='color: green; background-color: #DFD;'>Green:</td>
									<td>Values replaced by the TAM tool.</td>
								</tr>
								<tr>
									<td style='color: #D00; background-color: #FFADB1;'>Red:</td>
									<td>The tester app will populate the values for these.</td>
								</tr>
							</table>
						</div>

					</ul>
				</li>
				<li class="qr_li">
					Product ID
					<ul>
						<li>
							This is required (except: Appsflyer)
						</li>
						<li>
							For AppsFlyer the tool populates the product ID form the links automatically.
						</li>
						<li>
							Try to <span class="bold">check the redirect</span> and get the product ID from the store URL if needed.
						</li>
						<!--
						<li>
						This is mandatory when we have the <span class="bold">[PRODUCT_ID]</span> macro in the link (except: Appsflyer)
						</li>
						<li>
						<span class="bold">Not required:</span> <span class="green_bold">TUNE</span>, <span class="green_bold">AppsFlyer</span>, <span class="green_bold">Kochava</span>, etc...
						</li>
						<li>
						<span class="bold">Required:</span> <span class="red_bold">Adjust</span>, <span class="red_bold">Singular</span> (Apsalar), <span class="red_bold">King.com</span>, etc...
						</li>
						-->
					</ul>
				</li>
				<li class="qr_li">
					Scan the QR code from the test device using the Attribution Tester App
					<ul>
						<li>Fire the link from the device, it should redirect the user to the app store.</li>
						<li>S2S links might not redirect to the app store. <span class="bold">If the link does not redirect, please contact the TAM team.</span> </li>
					</ul>
				</li>
				<li class="qr_li">
					Install and open the app
				</li>
				<li class="qr_li">
					Check the postback in the install feed
					<!--
					Check the postback in the <a class="blue" href="http://tamtrackingtool.com/install_feed/install_feed.php" target="_blank">install feed</a>
					-->
					<ul>
						<li>
							The postback can be delayed based on the implementation of the tracking.
						</li>
						<li>
							The main tracking providers should work fine (TUNE, Adjust, AppsFlyer, Kochava, Singular (Apsalar), King.com).
						</li>
						<li>
							If the install doesn't show up in the feed, the attribution might not work -> <span class="bold">contact the TAM team</span>.
						</li>
					</ul>
				</li>
			</ol>
			<br />
		</div>
		
		<div data-role="content">

		<ul data-role="listview">
			<li>

				<select id="size" onchange="setChartImage()" style="display: none;">
					<option value="50x50">50x50</option>
					<option value="75x75">75x75</option>
					<option value="100x100">100x100</option>
					<option value="150x150">150x150</option>
					<option value="300x300" selected="">300x300</option>
				</select>
			</li>
			<li>
				<fieldset data-role="controlgroup" data-type="horizontal" style="display: none;">
					<legend>
						Error correction
					</legend>
					<input type="radio" name="ec" id="ec-L" value="L" onclick="setChartImage()" />
					<label for="ec-L">7%</label>
					<input type="radio" name="ec" id="ec-M" value="M" onclick="setChartImage()" />
					<label for="ec-M">15%</label>
					<input type="radio" name="ec" id="ec-Q" value="Q" onclick="setChartImage()" />
					<label for="ec-Q">25%</label>
					<input type="radio" name="ec" id="ec-H" value="H" onclick="setChartImage()" checked="checked" />
					<label for="ec-H">30%</label>
				</fieldset>
			</li>

			<li>
				Paste the tracking URL here:
				<br />
				<br />
				<textarea id="text" cols="20" rows="5"></textarea>
				<br />
				<br />
				<div style="float: left;">
					Product ID:
					<input type="text" name="prod_id" id="prod_id" style="width: 200px;"/>
					<span id="prod_id_alert" style="margin-right: 10px;"></span>
				</div>

				<button class="btn-class" data-theme="b" onclick="check_red()" style="float: left; margin-right: 20px;">
					Check redirect
				</button>
				<div id="redurldiv" style="height: 20px;"></div>
				<div style="clear: both;"></div>
				<br />
			</li>
			<li>
				<button class="btn-class" data-theme="b" onclick="setChartImage()">
					Generate QR code
				</button>
				<br />
				<br />
			</li>
			<li>
				<div id="vnotes">
					<div id="vnotes_comment"></div>
					<div id="vnotes_alert"></div>
				</div>
			</li>
			<li>
				<div id="linkdiv" style="word-break: break-all; margin: 10px;"></div>

				<br />
			</li>
			<li>
				<h3 class="plink appsfl" id="urlparamstitle" style="margin-left: 10px !important;">Check URL parameters <i class="fa fa-arrow-down" id="urlpdown"></i><i class="fa fa-arrow-up" id="urlpup"></i></h3>
				<div id="urlsplit_div" style="float: none !important;"></div>
				<br />
			</li>
			<li>
				<div>
					<a id="link" href="#"><img id="chart" src="" alt="QR code" style="display: none;" /></a>
					<br />
					<br />
				</div>
			</li>
			<li>
				<div id="feed_link">
					<h3 class="black" id="feddtitle">Install feed </h3>
				 <span class="bold" style="">	Install and open the app, and after check the install feed:</span>
				 <br />
				<span id="if_span" class="blue"></span>
				<br />
				<br />
				</div>
			</li>
		</ul>
	</div>

</div>

<?php
include '../footer.php';
?>