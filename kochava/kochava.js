ValidateKochava = function() {

	$('input[type=checkbox]').attr('checked', false);
	$('#koch_otherparamtable tbody tr').css("color", "#777");

	final_comment = "";
	final_alert = "";

	koch_url = document.getElementById("koch_url").value;

	// remove spaces
	koch_url_nospace = koch_url.replace(/\s+/g, '');

	if (koch_url !== koch_url_nospace) {
		koch_url = koch_url_nospace;
		final_comment += "Spaces have been removed!<br /><br />";
	}

	// validations

	var url_good = validate_url(koch_url);
	if (!url_good) {
		final_alert += "It looks like the URL is not correct.<br /><br />";
	}
	var qs_good = validate_questionmarks(koch_url);
	if (!qs_good) {
		final_alert += "The URL contains more than one questionmarks!<br /><br />"
	}

	var macro_check_text = validate_macros(koch_url);
	var macros_good = (macro_check_text === "");
	final_alert += macro_check_text;

	url_all_good = (url_good && qs_good);

	// END of validation

	// check / add pbr=1

	var and = "[GOOGLE_AD_ID]";
	var is_and = (koch_url.indexOf(and) > -1);

	var pbr = "pbr=1";
	var is_pbr = (koch_url.indexOf(pbr) > -1);

	if (is_and && !is_pbr) {
		koch_url += "&pbr=1";
		final_comment += "'&pbr=1' has been added to the URL!<br /><br />";
	}

	var append = "append_app_conv_trk_params=1";
	var is_append = (koch_url.indexOf(append) > -1);

	if (is_and && !is_append) {
		koch_url += "&append_app_conv_trk_params=1";
		final_comment += "'append_app_conv_trk_params=1' has been added to the URL!<br /><br />";
	}

	if (!is_and) {
		$("#kappend td").css("color", "#000");
		$("#kpbr td").css("color", "#000");
	}

	// view-through

	output_tags(koch_url);

	if (vt_ok && url_all_good) {
		validate_url(koch_url);
		checkKochParams(koch_url);
		redirectURL(koch_url, '#redurldiv');
		splitURL(koch_url, urlsplit_div);
		$("#redurldiv").show();
		$("#urlparamstitle").show();
	} else {
		$("#vnotes_comment").html("");
		$("#vnotes_alert").html(final_alert);
		$("#redurldiv").hide();
	}
};

GetKochIDs = function(string) {
	var koc_url = document.getElementById(string).value;

	var substr1 = koc_url.split("campaign_id=");
	var substr2 = substr1[1].split("&");
	var camp_id = substr2[0];

	var substrn1 = koc_url.split("network_id=");
	var substrn2 = substrn1[1].split("&");
	var net_id = substrn2[0];

	return [camp_id, net_id];
}
koch_imp_checked = false;

output_tags = function(string) {

	var vt0 = document.getElementById("vt0").checked;
	var vt1 = document.getElementById("vt1").checked;
	var vt2 = document.getElementById("vt2").checked;

	vt_ok = false;

	if (vt0 || vt1 || vt2) {
		vt_ok = true;
	} else {
		final_alert += "<br />Select view-through attribution window!<br /><br />";
		$("#vnotes_alert").html(final_alert);
	}

	// koch_imp0 for the output string

	var ids = GetKochIDs("koch_url");
	console.log(ids);
	var camp_id = ids[0];
	console.log(camp_id);
	var net_id = ids[1];
	console.log(net_id);
	var koch_url_arr = koch_url.split(":");
	var koch_base_prefix = koch_url_arr[0];
	console.log(koch_base_prefix);

	var koch_imp = koch_base_prefix + "://imp.control.kochava.com/track/impression?campaign_id=" + camp_id + "&network_id=" + net_id + "&impression_id=[CLICK_ID]&event=completed_view&user_dma=[DMA_CODE]";
	koch_imp += "&country=[COUNTRY_CODE]&ip_address=[IP_ADDRESS]&device_connection=[NETWORK_TYPE]&device_model=[DEVICE_MODEL]&device_os_version=[OS_VERSION]&device_os=[PLATFORM]";
	koch_imp += "&creative_size=[RAW_AD_CREATIVE_ID]&site_id=[APP_ID]&appstore_id=[STORE_ID]&creative_name=[AD_CREATIVE_NAME]&device_ua=[USER_AGENT_MOZILLA]";

	// if Android

	var and = "[GOOGLE_AD_ID]";
	var is_and = (koch_url.indexOf(and) > -1);

	if (is_and) {
		koch_imp += "&device_id=[GOOGLE_AD_ID]&pbr=1";
	} else {
		koch_imp += "&device_id=[IDFA]";
	}

	if (vt0) {
		var koch_imp0 = "Let's use this URL:\n\n";
		koch_imp0 += "HTML5:\n" + string;
		$("#koch_imp").css("height", "180px");
		$("#koch_url_imp, #koch_imp_title, #koch_imp_div, #url_comp_div").hide();
	}

	if (vt1) {
		var koch_imp0 = "Let's use these URLs:\n\n";
		koch_imp0 += "HTML5:\n" + string + "\n\n";
		koch_imp0 += "Video starts:\n" + koch_imp;
		$("#koch_imp").css("height", "300px");
		$("#koch_url_imp, #koch_imp_title, #koch_imp_div, #url_comp_div").hide();
	}

	if (vt2) {
		$("#koch_url_imp, #koch_imp_title, #koch_imp_div, #url_comp_div").show();
		var koch_imp0 = "Let's use these URLs:\n\n";
		koch_imp0 += "HTML5:\n" + string + "\n\n";
		koch_imp0 += "Video starts:\nYou need to check the impression URL first. (Scroll up.)";
		$("#koch_imp").css("height", "300px");
	}
	
	koch_imp0 += "\n\nThank you";

	if (vt_ok) {
		document.getElementById("koch_imp").value = koch_imp0;
	}
}
compare_koch_urls = function() {

	var koch_url = document.getElementById("koch_url").value;
	var koch_url_imp = document.getElementById("koch_url_imp").value;

	var koch_imp0 = "";

	$("#chURLs_span").removeClass("redbold");

	var ids1 = GetKochIDs("koch_url");
	var camp_id1 = ids1[0];
	var net_id1 = ids1[1];

	var ids2 = GetKochIDs("koch_url_imp");
	var camp_id2 = ids2[0];
	var net_id2 = ids2[1];

	if (camp_id1 === camp_id2) {
		$("#url_comp_div").html("<br />The campaing_id-s are the same, we are good.");
		$("#url_comp_div").addClass("greenbold");
		$("#url_comp_div").removeClass("redbold");
		
		koch_imp0 += "Let's use these URLs:\n\n";
		koch_imp0 += "HTML5:\n" + koch_url + "\n";
		koch_imp0 += "Video starts:\n";
		koch_imp0 += koch_url_imp;
		koch_imp0 += "\nThank you";
		$("#koch_imp").css("height", "300px");
		console.log("The campaing_id-s are the same, we are good.");
	} else {
		$("#url_comp_div").html("<br />The campaing_id-s are not the same!");
		$("#url_comp_div").addClass("redbold");
		$("#url_comp_div").removeClass("greenbold");

		koch_imp0 += "The campaing_id-s are not the same in the links! Please ask the client to double check the URLs. Thank you!";
		console.log("The campaing_id-s are not the same!");
		$("#koch_imp").css("height", "50px");
	}

	document.getElementById("koch_imp").value = koch_imp0;
}

FpKoch = function(){
	
	fp_message = "";
	
	var fp0 = document.getElementById("fp0").checked;
	var fp1 = document.getElementById("fp1").checked;

	fp_ok = false;

	if (fp0 || fp1) {
		fp_ok = true;
	} else {
		fp_message += "Select fingerprinting status!<br />";
	}
	
	if (fp0) {
		fp_message += "No need to change the default setup in the dash.<br />";
	}
	
	if (fp1) {
		fp_message += "AdOps will need to disable 'Only target traffic with IDFA/GAID' in the dash.<br />";
		koch_imp0 = document.getElementById("koch_imp").value;
		koch_imp1 = koch_imp0 + "\n\n@adops: please don't forget to disable 'Only target traffic with IDFA/GAID' in the dash. Thanks ";
		document.getElementById("koch_imp").value = koch_imp1;
	}
	
	$("#fp_message").html(fp_message);
	//$("#fp_alert").removeClass("alert");
	
}


checkKochParams = function(string) {
	var campid = "campaign_id";
	var netid = "network_id";
	var idfa = "device_id=[IDFA]";
	var gaid = "adid=[GOOGLE_AD_ID]";
	var append = "append_app_conv_trk_params=1"
	var pbr = "pbr=1";

	var is_campid = (string.indexOf(campid) > -1);
	var is_netid = (string.indexOf(netid) > -1);
	var is_idfa = (string.indexOf(idfa) > -1);
	var is_gaid = (string.indexOf(gaid) > -1);
	var is_append = (string.indexOf(append) > -1);
	var is_pbr = (string.indexOf(pbr) > -1);

	// Check / re-color parameter list
	var kcid_alert = "";
	var knid_alert = "";
	var kdevid_alert = "";
	var tgaid_alert = "";
	var tjson_alert = "";
	var all_good = false;

	if (is_campid) {
		$("#kcid td").css("color", "green");
	} else {
		kcid_alert = "Missing / incorrect 'campaign_id' parameter!<br />";
		$("#kcid td").css("color", "#D00");
	}

	if (is_netid) {
		$("#knid td").css("color", "green");
	} else {
		knid_alert = "Missing / incorrect 'network_id' parameter!<br />";
		$("#knid td").css("color", "#D00");
	}

	if (is_idfa) {
		$("#kdevid td").css("color", "green");
	} else {
		//kdevid_alert = "Missing / incorrect 'ios_ifa' parameter / [IDFA]!<br />"
		//$("#kdevid td").css("color","#D00");
	}

	if (is_gaid) {
		$("#kadid td").css("color", "green");
	} else {
		//tgaid_alert = "Missing / incorrect 'google_aid' parameter / [GOOGLE_AD_ID]!<br />";
		//$("#tgaid td").css("color","#D00");
	}

	if (is_append) {
		$("#kappend td").css("color", "green");
	} else if (is_gaid) {
		tjson_alert = "Missing / incorrect 'append_app_conv_trk_params=1' parameter!<br />";
		$("#kpbr td").css("color", "#D00");
	}

	if (is_pbr) {
		$("#kpbr td").css("color", "green");
	} else if (is_gaid) {
		tjson_alert = "Missing / incorrect 'pbr=1' parameter!<br />";
		$("#kpbr td").css("color", "#D00");
	}

	// iOS or Android

	if (is_idfa && is_gaid) {
		final_alert += "Both [IDFA] and [GOOGLE_AD_ID] are in the URL!<br />";
	}

	if (is_idfa && !is_gaid) {
		final_comment += "It looks like we have an iOS campaign!<br />";
		$("#kadid td").css("color", "#000");
	}

	if (!is_idfa && is_gaid) {
		final_comment += "It looks like we have an Android campaign!<br />";
		$("#kdevid td").css("color", "#000");
	}

	if (!is_idfa && !is_gaid) {
		final_alert += "Missing / incorrect [IDFA] or [GOOGLE_AD_ID]!<br />";
		$("#kdevid td").css("color", "#D00");
		$("#kadid td").css("color", "#D00");
	}

	// End of iOS or Android

	// All good

	all_good = is_campid && is_netid && (is_idfa || is_gaid);

	if (all_good) {
		final_comment += "All the parameters should be fine!<br />";
		$("#vnotes_comment").css("color", "green");
	}

	final_alert += kcid_alert + knid_alert + kdevid_alert + tgaid_alert + tjson_alert + "<br />";

	$("#vnotes_comment").html(final_comment);
	$("#vnotes_alert").html(final_alert);

	// check the other parameters, naming is important!

	check_otherparams = function(param, sub_string) {
		$("#ch_" + param).attr("disabled", false);
		var str = sub_string;
		var is_str = (string.indexOf(str) > -1);
		if (is_str) {
			$("#tr_" + param).css("color", "green");
			$("#ch_" + param).attr("checked", true);
		}
	}
	check_otherparams("sha1", "&device_id=[SHA1_ANDROID_ID]");
	check_otherparams("odin", "&odin=[ODIN1]");
	check_otherparams("imei", "&imei_sha1=[SHA1_IMEI]");
	check_otherparams("mac", "&sub_site=[APP_ID]");
	check_otherparams("openud", "&openudid=[OPEN_UDID]");
	check_otherparams("udid", "&udid=[UDID]");
	check_otherparams("siteid", "&siteid=[APP_ID]");
	check_otherparams("creativeid", "&creative_id=[AD_CREATIVE_NAME]");
	check_otherparams("devmod", "&device_model=[DEVICE_MODEL]");
	check_otherparams("devos", "&device_os=[PLATFORM]");
	check_otherparams("devver", "&device_ver=[OS_VERSION]");
	check_otherparams("devcon", "&device_conection=[NETWORK_TYPE]");
	check_otherparams("ip", "&ip_address=[IP_ADDRESS]");
	check_otherparams("country", "&country=[COUNTRY_CODE]");
	check_otherparams("dma", "&user_dma=[DMA_CODE]");
	check_otherparams("appstoreid", "&appstore_id=[STORE_ID]");
	check_otherparams("impid", "&impression_id=[SERVE_TIME]");

}
add_remove_params = function(elem) {

	var id = $(elem).attr("id");
	var id2 = id.replace("ch_", "");
	var sub_str = "";

	switch(id2) {
	case "sha1":
		sub_str = "&device_id=[SHA1_ANDROID_ID]";
		break;
	case "odin":
		sub_str = "&odin=[ODIN1]";
		break;
	case "imei":
		sub_str = "&imei_sha1=[SHA1_IMEI]";
		break;
	case "mac":
		sub_str = "&sub_site=[APP_ID]";
		break;
	case "openud":
		sub_str = "&openudid=[OPEN_UDID]";
		break;
	case "udid":
		sub_str = "&udid=[UDID]";
		break;
	case "siteid":
		sub_str = "&siteid=[APP_ID]";
		break;
	case "creativeid":
		sub_str = "&creative_id=[AD_CREATIVE_NAME]";
		break;
	case "devmod":
		sub_str = "&device_model=[DEVICE_MODEL]";
		break;
	case "devos":
		sub_str = "&device_os=[PLATFORM]";
		break;
	case "devver":
		sub_str = "&device_ver=[OS_VERSION]";
		break;
	case "devcon":
		sub_str = "&device_conection=[NETWORK_TYPE]";
		break;
	case "impid":
		sub_str = "&impression_id=[SERVE_TIME]";
		break;
	case "dma":
		sub_str = "&user_dma=[DMA_CODE]";
		break;
	case "appstoreid":
		sub_str = "&appstore_id=[STORE_ID]";
		break;
	case "country":
		sub_str = "&country=[COUNTRY_CODE]";
		break;
	case "ip":
		sub_str = "&ip_address=[IP_ADDRESS]";
		break;
	}

	var tr_id = id.replace("ch", "tr");
	tr_id = "#" + tr_id;

	var boxchecked = $(elem).attr("checked");
	if (boxchecked) {
		$(tr_id).css("color", "green");
		koch_url += sub_str;
		output_tags(koch_url);
	} else {
		$(tr_id).css("color", "#777");
		koch_url = koch_url.replace(sub_str, "");
		output_tags(koch_url);
	}
}

$(document).ready(function() {
	$('input[type=checkbox]').attr('disabled', 'true');
	divToggle(false, notestitle, notesdiv, notesdown, notesup);
	divToggle(true, valtitle, valdiv, valdown, valup);
	divToggle(true, urlparamstitle, urlsplit_div, urlpdown, urlpup);
	divToggle(false, koch_otherp_title, koch_otherparams, koch_otherp_down, koch_otherp_up);
	divToggle(true, imptitle, impdiv, impdown, impup);
	divToggle(true, pbtitle, pbdiv, pbdown, pbup);
	divToggle(true, fptitle, fpdiv, fpdown, fpup);
	divToggle(false, pbmtitle, pbmdiv, pbmdown, pbmup);
	divToggle(false, vttitle, vtdiv, vtdown, vtup);
	divToggle(false, pietitle, piediv, piedown, pieup);
});
