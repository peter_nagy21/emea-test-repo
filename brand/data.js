var DATA = { "url": [
    {"brand": "c.medialytics.com", "parameters": [
    	{"find": "{operaid}", "replace": "[ADCOLONY_TIMESTAMP]"},
    	{"find": "{site_id}", "replace": "[APP_ID]"},
    	{"find": "IMPRESSION_ID_MACRO_HERE", "replace": ""},
    	{"find": "{clickid}", "replace": "[CLICK_ID]"}]
    },
    {"brand": "clk.medialytics.com", "parameters": [
        {"find": "{operaid}", "replace": "[ADCOLONY_TIMESTAMP]"},
        {"find": "{site_id}", "replace": "[APP_ID]"},
        {"find": "IMPRESSION_ID_MACRO_HERE", "replace": ""},
        {"find": "{clickid}", "replace": "[CLICK_ID]"}]
    },
    {"brand": "ad.doubleclick.net", "parameters": [
    	{"find": "[timestamp]", "replace": "[ADCOLONY_TIMESTAMP]"},
        {"find": "dc_rdid", "replace": "[DEVICE_ID]"},
        {"find": "[md5_AdID]", "replace": "[MD5_IDFA]"},
    	{"find": "[timestamp]?", "replace": "[ADCOLONY_TIMESTAMP]"}]
    },
    {"brand": "ad.atdmt.com", "parameters": [
    	{"find": "[timestamp]", "replace": "[ADCOLONY_TIMESTAMP]"},
        {"find": "idfa", "replace": "[IDFA]"},
        {"find": "aaid", "replace": "[ANDROID_ID]"},
        {"find": "cache", "replace": "[ADCOLONY_TIMESTAMP]"}]
    },
    {"brand": "servedby.flashtalking.com", "parameters": [
    	{"find": "[CACHEBUSTER]", "replace": "[ADCOLONY_TIMESTAMP]"},
    	{"find": "[%FT_RANDOM%]", "replace": "[ADCOLONY_TIMESTAMP]"}]
    },
    {"brand": "track.adform.net", "parameters": [
    	{"find": "[timestamp]", "replace": "[ADCOLONY_TIMESTAMP]"}]
    },
    {"brand": "rover.ebay.com", "parameters": [
    	{"find": "[CACHEBUSTER]", "replace": "[ADCOLONY_TIMESTAMP]"},
    	{"find": "{ClickId}", "replace": "[CLICK_ID]"},
    	{"find": "{device_id}", "replace": "[DEVICE_ID]"}]
    },
     {"brand": "tags.admetrics.events", "parameters": [
    	{"find": "[CACHEBUSTER]", "replace": "[ADCOLONY_TIMESTAMP]"}]
    },
    {"brand": "fw.adsafeprotected.com", "parameters": [
        {"find": "[timestamp]", "replace": "[ADCOLONY_TIMESTAMP]"}]
    },
    {"brand": "t.mookie1.com", "parameters": [
        {"find": "[timestamp]", "replace": "[ADCOLONY_TIMESTAMP]"}]
    },
    {"brand": "bs.serving-sys.com", "example": "http://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=19&mc=imp&pli=196gfdgdf10003&PluID=0&ord=[ADCOLONY_TIMESTAMP]&rtu=-1", "parameters": [
    	{"find": "[timestamp]", "replace": "[ADCOLONY_TIMESTAMP]"},
    	{"find": "%%REALRAND%%", "replace": "[ADCOLONY_TIMESTAMP]"},
    	{"find": "ebRandom", "replace": "[ADCOLONY_TIMESTAMP]"},
    	{"find": "ord", "replace": "[ADCOLONY_TIMESTAMP]"}]
    }]
};