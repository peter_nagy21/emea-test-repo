ValidateApsalar = function() {

	$('input[type=checkbox]').attr('checked', false);
	$('#apsalar_otherparamtable tbody tr').css("color", "#777");

	final_comment = "";
	final_alert = "";

	apsalar_url = document.getElementById("apsalar_url").value;

	// remove spaces
	apsalar_url_nospace = apsalar_url.replace(/\s+/g, '');

	if (apsalar_url !== apsalar_url_nospace) {
		apsalar_url = apsalar_url_nospace;
		final_comment += "Spaces have been removed!<br /><br />";
	}

	// validations
	// validate_url, validate_questionmarks functions return true or false

	var url_good = validate_url(apsalar_url);
	if (!url_good) {
		final_alert += "It looks like the URL is not correct.<br /><br />";
	}
	var qs_good = validate_questionmarks(apsalar_url);
	if (!qs_good) {
		final_alert += "The URL contains more than one questionmarks!<br /><br />"
	}

	var macro_check_text = validate_macros(apsalar_url);
	var macros_good = (macro_check_text === "");
	final_alert += macro_check_text;

	url_all_good = (url_good && qs_good);

	// END of validation

	// view-through

	output_tags(apsalar_url);

	if (vt_ok && url_all_good) {
		//validate_url(apsalar_url);
		checkapsalarParams(apsalar_url);
		redirectURL(apsalar_url, '#redurldiv');
		splitURL(apsalar_url, urlsplit_div);
		$("#redurldiv").show();
		$("#urlparamstitle").show();
	} else {
		$("#vnotes_comment").html("");
		$("#vnotes_alert").html(final_alert);
		$("#redurldiv").hide();
	}
};

output_tags = function(string) {

	var vt0 = document.getElementById("vt0").checked;
	var vt1 = document.getElementById("vt1").checked;

	vt_ok = false;

	if (vt0 || vt1) {
		vt_ok = true;
	} else {
		final_alert += "<br />Select view-through attribution window!<br /><br />";
		$("#vnotes_alert").html(final_alert);
	}

	// get the impresion URLs from click

	var apsalar_imp = string;
	apsalar_imp += "&op=impression";
	var is_gaid = (apsalar_imp.indexOf('GOOGLE_AD_ID') > -1);

	var click_instr = "HTML5:\n";

	if (is_gaid) {
		click_instr = "Hardcode this:\n";
	}

	// apsalar_imp0 for the output string

	if (vt0) {
		var apsalar_imp0 = "Let's use this URL:\n\n";
		apsalar_imp0 += click_instr + string + "\n\n";
		$("#apsalar_imp").css("height", "180px");
	}

	if (vt1) {
		var apsalar_imp0 = "Let's use these URLs:\n\n";
		apsalar_imp0 += click_instr + string + "\n\n";
		apsalar_imp0 += "Video starts:\n" + apsalar_imp + "\n\n";
		$("#apsalar_imp").css("height", "250px");
	}
	
	apsalar_imp0 += "Thank you";

	if (vt_ok) {
		document.getElementById("apsalar_imp").value = apsalar_imp0;
	}
}
checkapsalarParams = function(string) {

	var aps1 = "&a=";
	var aps2 = "&i=";
	var aps3 = "an=AdColony";
	var aps4 = "ca=";
	var aps5 = "&p=";
	var aps6 = "idfa=[IDFA]";
	var aps7 = "aifa=[GOOGLE_AD_ID]";
	var aps8 = "product_id=[PRODUCT_ID]";
	var aps9 = "api_key";
	var aps10 = "&h=";

	var is_aps1 = (string.indexOf(aps1) > -1);
	var is_aps2 = (string.indexOf(aps2) > -1);
	var is_aps3 = (string.indexOf(aps3) > -1);
	var is_aps4 = (string.indexOf(aps4) > -1);
	var is_aps5 = (string.indexOf(aps5) > -1);
	var is_aps6 = (string.indexOf(aps6) > -1);
	var is_aps7 = (string.indexOf(aps7) > -1);
	var is_aps8 = (string.indexOf(aps8) > -1);
	var is_aps9 = (string.indexOf(aps9) > -1);
	var is_aps10 = (string.indexOf(aps10) > -1);

	// Check / re-color parameter list

	var aps1_alert = "";
	var aps2_alert = "";
	var aps3_alert = "";
	var aps4_alert = "";
	var aps5_alert = "";
	var aps6_alert = "";
	var aps7_alert = "";
	var aps8_alert = "";
	var aps9_alert = "";
	var aps10_alert = "";

	var all_good = false;

	// Apsalar code

	if (is_aps1) {
		$("#aps1 td").css("color", "green");
	} else {
		aps1_alert = "Missing / incorrect 'a' parameter setup!<br />";
		$("#aps1 td").css("color", "#D00");
	}

	if (is_aps2) {
		$("#aps2 td").css("color", "green");
	} else {
		aps2_alert = "Missing / incorrect 'i' parameter setup!<br />";
		$("#aps2 td").css("color", "#D00");
	}

	if (is_aps3) {
		$("#aps3 td").css("color", "green");
	} else {
		aps3_alert = "Missing / incorrect 'an' parameter setup!<br />";
		$("#aps3 td").css("color", "#D00");
	}

	if (is_aps4) {
		$("#aps4 td").css("color", "green");
	} else {
		aps4_alert = "Missing / incorrect 'ca' parameter setup!<br />";
		$("#aps4 td").css("color", "#D00");
	}

	if (is_aps5) {
		$("#aps5 td").css("color", "green");
	} else {
		aps5_alert = "Missing / incorrect 'p' parameter setup!<br />";
		$("#aps5 td").css("color", "#D00");
	}

	if (is_aps6) {
		$("#aps6 td").css("color", "green");
	} else {
		//aps6_alert = "Missing / incorrect 'idfa' parameter setup!<br />";
		$("#aps6 td").css("color", "#D00");
	}

	if (is_aps7) {
		$("#aps7 td").css("color", "green");
	} else {
		//aps7_alert = "Missing / incorrect 'aifa' parameter setup!<br />";
		$("#aps7 td").css("color", "#D00");
	}

	if (is_aps8) {
		$("#aps8 td").css("color", "green");
	} else {
		aps8_alert = "Missing / incorrect 'product_id' parameter setup!<br />";
		$("#aps8 td").css("color", "#D00");
	}

	if (is_aps9) {
		$("#aps9 td").css("color", "green");
	} else {
		aps9_alert = "Missing / incorrect 'api_key' parameter setup!<br />";
		$("#aps9 td").css("color", "#D00");
	}

	if (is_aps10) {
		$("#aps10 td").css("color", "green");
	} else {
		aps10_alert = "Missing / incorrect 'h' parameter setup!<br />";
		$("#aps10 td").css("color", "#D00");
	}

	// iOS or Android

	if (is_aps6 && is_aps7) {
		final_alert += "Both [IDFA] and [GOOGLE_AD_ID] are in the URL!<br />";
	}

	if (is_aps6 && !is_aps7) {
		final_comment += "It looks like we have an iOS camapign!<br />";
		$("#aps7 td").css("color", "#000");
	}

	if (!is_aps6 && is_aps7) {
		final_comment += "It looks like we have an Android camapign!<br />";
		$("#aps6 td").css("color", "#000");
	}

	if (!is_aps6 && !is_aps7) {
		final_alert += "Missing / incorrect [IDFA] or [GOOGLE_AD_ID]!<br />";
		$("#aps6 td").css("color", "#D00");
		$("#aps7 td").css("color", "#D00");
	}

	// End of iOS or Android

	// All good

	all_good = is_aps1 && is_aps2 && is_aps3 && is_aps4 && is_aps5 && (is_aps6 || is_aps7) && is_aps8 && is_aps9 && is_aps10;

	if (all_good) {
		final_comment += "All the parameters should be fine!<br /><br />Link is good!<br />";
		$("#vnotes_comment").css("color", "green");
	}

	final_alert += aps1_alert + aps2_alert + aps3_alert + aps4_alert + aps5_alert + aps6_alert + aps7_alert + aps8_alert + aps9_alert + aps10_alert + "<br />";

	$("#vnotes_comment").html(final_comment);
	$("#vnotes_alert").html(final_alert);

	// check the other parameters, naming is important!

	check_otherparams = function(param, sub_string) {
		$("#ch_" + param).attr("disabled", false);
		var str = sub_string;
		var is_str = (string.indexOf(str) > -1);
		if (is_str) {
			$("#tr_" + param).css("color", "green");
			$("#ch_" + param).attr("checked", true);
		}
	}
	check_otherparams("aps11", "re=");
	check_otherparams("aps12", "andi=");
	check_otherparams("aps13", "and1=");
	check_otherparams("aps14", "udid=");
	check_otherparams("aps15", "odin=");
	check_otherparams("aps16", "udi1=");
	check_otherparams("aps17", "s=[APP_ID]");
	check_otherparams("aps18", "pl=");

}
add_remove_params = function(elem) {

	var id = $(elem).attr("id");
	var id2 = id.replace("ch_", "");
	var sub_str = "";

	switch(id2) {
	case "aps12":
		sub_str = "&andi=[ANDROID_ID]";
		break;
	case "aps13":
		sub_str = "&and1=[SHA1_ANDROID_ID]";
		break;
	case "aps14":
		sub_str = "&udid=[RAW_UDID]";
		break;
	case "aps15":
		sub_str = "&odin=[ODIN1]";
		break;
	case "aps16":
		sub_str = "&udi1=[UDID]";
		break;
	case "aps17":
		sub_str = "&s=[APP_ID]";
		break;
	}

	var tr_id = id.replace("ch", "tr");
	tr_id = "#" + tr_id;

	var boxchecked = $(elem).attr("checked");
	if (boxchecked) {
		$(tr_id).css("color", "green");
		apsalar_url += sub_str;
		output_tags(apsalar_url);
	} else {
		$(tr_id).css("color", "#777");
		apsalar_url = apsalar_url.replace(sub_str, "");
		output_tags(apsalar_url);
	}
}

$(document).ready(function() {
	$('input[type=checkbox]').attr('disabled', 'true');
	divToggle(false, notestitle, notesdiv, notesdown, notesup);
	divToggle(true, valtitle, valdiv, valdown, valup);
	divToggle(true, urlparamstitle, urlsplit_div, urlpdown, urlpup);
	divToggle(false, apsalar_otherp_title, apsalar_otherparams, apsalar_otherp_down, apsalar_otherp_up);
	divToggle(true, imptitle, impdiv, impdown, impup);
	divToggle(true, pbtitle, pbdiv, pbdown, pbup);
	divToggle(false, vttitle, vtdiv, vtdown, vtup);
	divToggle(false, pietitle, piediv, piedown, pieup);
});
