ValidateAppsflyer = function() {

	final_comment = "";

	var appsflyer_url = document.getElementById("appsflyer_url").value;

	// replace spaces

	appsflyer_url_nospace = appsflyer_url.replace(/\s+/g, '');

	if (appsflyer_url !== appsflyer_url_nospace) {
		appsflyer_url = appsflyer_url_nospace;
		final_comment += "Spaces have been removed!<br /><br />";
	}

	/*

	// add &af_c_id=[RAW_AD_CAMPAIGN_ID]

	var is_c_id = (appsflyer_url.indexOf("&af_c_id=") > -1);

	if (!is_c_id){
	appsflyer_url += "&af_c_id=[RAW_AD_CAMPAIGN_ID]";
	final_comment += "&af_c_id=[RAW_AD_CAMPAIGN_ID] has been added to the link!<br /><br />";
	}

	*/

	// add &af_adset=[AD_GROUP_NAME]

	var is_c_id = (appsflyer_url.indexOf("&af_adset=") > -1);

	if (!is_c_id) {
		appsflyer_url += "&af_adset=[AD_GROUP_NAME]";
		final_comment += "&af_adset=[AD_GROUP_NAME] has been added to the link!<br /><br />";
	}

	// add &af_sub5=[STORE_ID]

	/*

	var is_st_id = (appsflyer_url.indexOf("&af_sub5=") > -1);

	if (!is_st_id){
	appsflyer_url += "&af_sub5=[STORE_ID]";
	final_comment += "&af_sub5=[STORE_ID] has been added to the link!<br /><br />";
	}

	*/

	// add &af_adset_id=[RAW_AD_GROUP_ID]

	var is_c_id2 = (appsflyer_url.indexOf("&af_adset_id=") > -1);

	if (!is_c_id2) {
		appsflyer_url += "&af_adset_id=[RAW_AD_GROUP_ID]";
		final_comment += "&af_adset_id=[RAW_AD_GROUP_ID] has been added to the link!<br /><br />";
	}

	// replace [TRANS_ID]

	appsflyer_url_trans = appsflyer_url.replace('[TRANS_ID]', '[CLICK_ID]');

	if (appsflyer_url !== appsflyer_url_trans) {
		appsflyer_url = appsflyer_url_trans;
		final_comment += "[TRANS_ID] has been replaced by [CLICK_ID]!<br /><br />";
	}

	// replace [USER_AGENT]

	appsflyer_url_ua = appsflyer_url.replace('[USER_AGENT]', '[USER_AGENT_MOZILLA]');

	if (appsflyer_url !== appsflyer_url_ua) {
		appsflyer_url = appsflyer_url_ua;
		final_comment += "[USER_AGENT] has been replaced by [USER_AGENT_MOZILLA]!<br /><br />";
	}

	// Add / override "c" parameter with "c=[AD_GROUP_NAME]"

	var cparam = document.getElementById("cparam");
	var cparam_ch = cparam.checked;

	var is_cparam = (appsflyer_url.indexOf("&c=") > -1);

	if (cparam_ch) {

		if (is_cparam) {
			appsflyer_url = remove_param("c=", appsflyer_url);
		}
		appsflyer_url += "&c=[AD_GROUP_NAME]";
		final_comment += "&c=[AD_GROUP_NAME] has been added to the link!<br /><br />";

	}

	// END OF Add / override "c" parameter with "c=[AD_GROUP_NAME]"
	

	// Add cost parameters: &af_cost_value=[BID]&af_cost_model=[BID_TYPE]

	var cost_param = document.getElementById("cost_param");
	var cost_param_ch = cost_param.checked;
	
	//console.log("cost_param_ch: " + cost_param_ch);

	var is_cost_param = (appsflyer_url.indexOf("&af_cost_value=") > -1);
	
	//console.log("is_cost_param: " + is_cost_param);

	if (cost_param_ch) {

		if (is_cost_param) {
			
		} else {
			appsflyer_url += "&af_cost_value=[BID]&af_cost_model=[BID_TYPE]";
			final_comment += "&af_cost_value=[BID]&af_cost_model=[BID_TYPE] has been added to the link!<br /><br />";
		}
	}

	// END OF Add cost parameters: &af_cost_value=[BID]&af_cost_model=[BID_TYPE]
	

	// CREATE LINK TO APP IN APPSFLYER DASHBOARD

	// link to new AppsFlyer dashboard
	var appsflyer_dashboard_url = "https://hq1.appsflyer.com/dashboard/overview/";

	// id for the app in the dashboard

	var appsflyer_base_url = appsflyer_url.split("?")[0];
	var l_base_url = appsflyer_base_url.length;
	var id_for_app = appsflyer_base_url.substring(appsflyer_base_url.lastIndexOf("/") + 1, l_base_url + 1);

	// append the id to the dashboard url
	var complete_appsflyer_dashboard_url = appsflyer_dashboard_url + id_for_app;

	// append href attribute to dashboard url tag
	document.getElementById("appsflyer-dashboard-url").setAttribute("href", complete_appsflyer_dashboard_url);

	// END of CREATE LINK TO APP IN APPSFLYER DASHBOARD

	// af_sub1 parameter logic

	var is_sub1 = (appsflyer_url.indexOf("af_sub1=") > -1);
	var is_sub2 = (appsflyer_url.indexOf("af_sub2=") > -1);
	var is_sub3 = (appsflyer_url.indexOf("af_sub3=") > -1);
	var is_sub4 = (appsflyer_url.indexOf("af_sub4=") > -1);
	var is_sub5 = (appsflyer_url.indexOf("af_sub5=") > -1);

	var add_imp = "";
	imp_ok = true;

	if (!is_sub1) {
		add_imp = "&af_sub1=impression";
	} else {
		if (!is_sub2) {
			add_imp = "&af_sub2=impression";
		} else {
			if (!is_sub3) {
				add_imp = "&af_sub3=impression";
			} else {
				if (!is_sub4) {
					add_imp = "&af_sub4=impression";
				} else {
					if (!is_sub5) {
						add_imp = "&af_sub5=impression";
					} else {
						imp_ok = false;
					}
				}
			}
		}
	}

	// END of af_sub1 parameter logic

	// view-through

	var sevday = document.getElementById("iosvt7");
	var oneday = document.getElementById("iosvt1");
	var noday = document.getElementById("iosvt0");
	var cust1 = document.getElementById("cust1");
	var cust2 = document.getElementById("cust2");

	var sevdaych = sevday.checked;
	var onedaych = oneday.checked;
	var nodaych = noday.checked;
	var cust1ch = cust1.checked;
	var cust2ch = cust2.checked;

	var custvt = document.getElementById("cvt").value;
	var cust_issue = (custvt === "");

	ios_validateVT = function() {

		if (!onedaych && !sevdaych && !nodaych && !cust1ch && !cust2ch) {
			return false;
		} else {
			return true;
		}
	}
	var vt_ok = ios_validateVT();

	// END of view-through

	if (cust1ch && cust_issue) {
		$("#vnotes_alert").html("The view-through value is missing!<br /><br />");
		document.getElementById("appsflyer_imp").value = "The view-through value is missing!";
		$("#cvtdiv").hide();
	} else {

		if (vt_ok) {

			// get the impression URLs from click

			var appsflyer_imp1 = appsflyer_url.replace("app.appsflyer", "impression.appsflyer");
			var appsflyer_imp7 = appsflyer_url.replace("app.appsflyer", "impression.appsflyer") + "&af_viewthrough_lookback​=7d";

			appsflyer_imp1 = remove_param("af_click_lookback", appsflyer_imp1);
			appsflyer_imp7 = remove_param("af_click_lookback", appsflyer_imp7);

			// appsflyer_imp0 for the output string

			cvt2 = "";

			var cct = getcct(appsflyer_url);

			var cct_alert = "";

			if (cct > 7) {
				cct_alert = "\n\n[~adops] please don't forget to set the CT to " + cct + " days in the dashboard. Thanks!";
			}

			if (nodaych) {
				var appsflyer_imp0 = "Let's use this URL:\n\n";
				$("#appsflyer_imp").css("min-height", "150px");
				appsflyer_imp0 += "HTML5:\n" + appsflyer_url;
				appsflyer_imp0 += cct_alert;
				$("#cvtdiv").hide();
			} else {
				var appsflyer_imp0 = "Let's use these URLs:\n\n";
				$("#appsflyer_imp").css("min-height", "260px");
				if (onedaych) {
					appsflyer_imp0 += "HTML5:\n" + appsflyer_url + "\n\n";
					appsflyer_imp0 += "Video starts:\n" + appsflyer_imp1;
					appsflyer_imp0 += cct_alert;
					$("#cvtdiv").hide();
				}
				if (sevdaych) {
					appsflyer_imp0 += "HTML5:\n" + appsflyer_url + "\n\n";
					appsflyer_imp0 += "Video starts:\n" + appsflyer_imp7;
					appsflyer_imp0 += cct_alert;
					$("#cvtdiv").hide();
				}
				if (cust1ch) {
					var vt_alert1 = vt_alert(custvt);
					appsflyer_imp0 += "HTML5:\n" + appsflyer_url + "\n\n";
					appsflyer_imp0 += "Video starts:\n" + appsflyer_imp1 + "&af_viewthrough_lookback​=" + custvt;
					appsflyer_imp0 += cct_alert;
					appsflyer_imp0 += vt_alert1;
					$("#cvtdiv").hide();
				}
				if (cust2ch) {
					$("#cvtdiv").show();
					var str = document.getElementById("appsflyer_url2").value;
					if (str !== "") {
						cvt2 = getcvt(str);
					}

					var vt_alert2 = vt_alert(cvt2);
					appsflyer_imp0 += "HTML5:\n" + appsflyer_url + "\n\n";
					appsflyer_imp0 += "Video starts:\n" + appsflyer_imp1 + cvt2;
					appsflyer_imp0 += cct_alert;
					appsflyer_imp0 += vt_alert2;
				}
			}
			
			checkappsflyerParams(appsflyer_url);
			
			console.log(is_amazon);
			
			if (is_amazon) {
				appsflyer_imp0 += "\n\n[~adops] the Amazon product ID should be this: " + id_for_app;
			}
			
			
			redirectURL(appsflyer_url, '#redurldiv');
			splitURL(appsflyer_url, urlsplit_div);

			document.getElementById("appsflyer_imp").value = appsflyer_imp0 + "\n\nThanks";

			// one line annoying little css fix below
			$("#appsflyerparams").css("margin-top", "-100px");

			$("#urlparamstitle").show();

		} else {
			$("#vnotes_alert").html("Select view-through attribution window!");
		}
	}

};

vt_alert = function(string) {
	// value1: 3d
	// value2: &af_viewthrough_lookback​=2d

	var p1 = "&af_viewthrough_lookback​=";

	if (string.indexOf(p1) > -1) {
		string = string.replace(p1, "");
	}

	var is_hours = string.indexOf("h") > -1;

	var number = string.replace(/[^0-9]/g, '');

	if ((number > 7) && !is_hours) {
		return "\n\n[~adops] please don't forget to set the VT to " + number + " days in the dashboard. Thanks!\n\n";
	}
	return "";
}

remove_param = function(param, string) {
	//var str = "af_click_lookback";
	var str = param;
	var is_ctv = (string.indexOf(str) > -1);

	if (is_ctv) {
		var sub1 = string.split(str);
		console.log("sub1: " + sub1);
		var endsgn = sub1[1].indexOf("&") + 1;
		console.log("endsgn: " + endsgn);
		
		var lsub11 = sub1[1].length;
		console.log("lsub11: " + lsub11);
		var res = sub1[1].substring(endsgn, lsub11);
		console.log("res: " + res);
		
		if (endsgn > 0) {
			var outputstr = sub1[0] + res;
		} else {
			var outputstr = sub1[0].slice(0,-1);
		}
		return outputstr;
	} else {
		return string;
	}
}

getcvt = function(str) {
	var cvt = "";
	var l = str.length;
	var n = str.indexOf("af_viewthrough_lookback");
	var is_cvt = n > -1;
	if (is_cvt) {
		var sub1 = str.substr(n, l);
		var is_and = sub1.indexOf("&");
		if (is_and) {
			var sub2 = sub1.split("&");
			cvt = sub2[0];
		} else {
			cvt = sub1;
		}
		cvt = "&" + cvt;
	}
	return cvt;
}
getcct = function(str) {
	var cct = "";
	var l = str.length;
	var n = str.indexOf("af_click_lookback");
	var is_cct = n > -1;
	if (is_cct) {
		var sub1 = str.substr(n, l);
		var is_and = sub1.indexOf("&");
		if (is_and) {
			var sub2 = sub1.split("&");
			cct = sub2[0];
		} else {
			cct = sub1;
		}
		cct = "&" + cct;
	}
	var cct_number = cct.replace(/[^0-9]/g, '');
	return cct_number;
}
checkappsflyerParams = function(string) {
	var pid = "pid=adcolony_int";
	var clickid1 = "clickid=[MAC_SHA1]";
	var clickid2 = "clickid=[TRANS_ID]";
	var clickid3 = "clickid=[CLICK_ID]";
	var idfa = "idfa=[IDFA]";
	var amazon1 = "Amazon";
	var amazon2 = "amazon";
	var gaid = "advertising_id=[GOOGLE_AD_ID]";
	var red = "redirect=false";
	var appid = "app_id=[PRODUCT_ID]";

	var is_pid = (string.indexOf(pid) > -1);
	var is_clickid = ((string.indexOf(clickid1) > -1) || (string.indexOf(clickid2) > -1) || (string.indexOf(clickid3) > -1));
	var is_idfa = (string.indexOf(idfa) > -1);
	is_amazon = ((string.indexOf(amazon1) > -1) || (string.indexOf(amazon2) > -1));
	var is_gaid = (string.indexOf(gaid) > -1);
	var is_red = (string.indexOf(red) > -1);
	var is_appid = (string.indexOf(appid) > -1);

	if (is_idfa) {
		is_red = true;
	}

	// Check / re-color parameter list
	var tpid_alert = "";
	var tclickid_alert = "";
	var tidfa_alert = "";
	var tgaid_alert = "";
	var tred_alert = "";
	var tappid_alert = "";
	var final_alert = "";
	var all_good = false;

	if (!imp_ok) {
		final_alert += "All the sub1 - sub5 params are in the url! Problem!<br />";
	}

	if (is_pid) {
		$("#pid td").css("color", "green");
	} else {
		tpid_alert = "Missing / incorrect 'pid' parameter!<br />";
		$("#pid td").css("color", "#D00");
	}

	if (is_clickid) {
		$("#clickid td").css("color", "green");
	} else {
		tclickid_alert = "Missing / incorrect 'clickid' parameter!<br />";
		$("#clickid td").css("color", "#D00");
	}

	if (is_idfa) {
		$("#idfa td").css("color", "green");
	} else {
		//tidfa_alert = "Missing / incorrect 'ios_ifa' parameter / [IDFA]!<br />"
		//$("#tidfa td").css("color","#D00");
	}

	if (is_gaid) {
		$("#advid td").css("color", "green");
	} else {
		//tgaid_alert = "Missing / incorrect 'google_aid' parameter / [GOOGLE_AD_ID]!<br />";
		//$("#tgaid td").css("color","#D00");
	}

	if (is_red) {
		$("#red td").css("color", "green");
	} else {
		tred_alert = "Missing / incorrect 'redirect=false'!<br />";
		$("#red td").css("color", "#D00");
	}

	if (is_appid) {
		$("#appid td").css("color", "green");
	} else {
		tappid_alert = "Missing / incorrect 'app_id' parameter!<br />";
		$("#appid td").css("color", "#D00");
	}

	// iOS or Android or Amazon

	if (is_idfa && is_gaid) {
		final_alert += "Both [IDFA] and [GOOGLE_AD_ID] are in the URL!<br />";
	}

	if (is_idfa && !is_gaid) {
		final_comment += "It looks like we have an iOS camapign!<br />";
		$("#advid td").css("color", "#000");
		$("#red td").css("color", "#000");
	}

	if (!is_idfa && is_gaid) {
		if (is_amazon) {
			final_comment += "It looks like we have an Amazon camapign!<br />";
		} else {
			final_comment += "It looks like we have an Android camapign!<br />";
		}
		$("#idfa td").css("color", "#000");
	}

	if (!is_idfa && !is_gaid) {
		final_alert += "Missing / incorrect [IDFA] or [GOOGLE_AD_ID]!<br />";
		$("#idfa td").css("color", "#D00");
		$("#advid td").css("color", "#D00");
	}

	// End of iOS or Android

	// macro check

	var macro_check_text = validate_macros(string);
	var macros_good = (macro_check_text === "");
	final_alert += macro_check_text;

	// All good

	all_good = is_pid && is_clickid && (is_idfa || is_gaid) && is_red && is_appid && imp_ok;

	if (all_good) {
		final_comment += "All the parameters should be fine!<br /><br />";
		$("#vnotes_comment").css("color", "green");
	} else {
		$("#vnotes_comment").css("color", "#777");
	}

	final_alert += tpid_alert + tclickid_alert + tidfa_alert + tgaid_alert + tred_alert + tappid_alert + "<br />";

	$("#vnotes_comment").html(final_comment);
	$("#vnotes_alert").html(final_alert);
}

$(document).ready(function() {
	divToggle(false, notestitle, notesdiv, notesdown, notesup);
	divToggle(true, valtitle, valdiv, valdown, valup);
	divToggle(true, urlparamstitle, urlsplit_div, urlpdown, urlpup);
	divToggle(true, imptitle, impdiv, impdown, impup);
	divToggle(true, pbtitle, pbdiv, pbdown, pbup);
	divToggle(false, vttitle, vtdiv, vtdown, vtup);
	divToggle(false, pietitle, piediv, piedown, pieup);

	var sevday = document.getElementById("iosvt7");
	var oneday = document.getElementById("iosvt1");
	var noday = document.getElementById("iosvt0");
	var cust1 = document.getElementById("cust1");
	var cust2 = document.getElementById("cust2");

});
