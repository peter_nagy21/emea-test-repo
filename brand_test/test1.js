var json_url = 'https://clients-api.adcolony.com/api/v2/advertiser_summary?user_credentials=W0KYONhCiK0usqvGFy1B&date=03102017&format=json&group_by=campaign,ad_group';

var json_resp = {};

function getJSON(json_url) {
	$.ajax({
		url : json_url,
		dataType : 'json',
		success : function(json) {
			//console.log(json);
			json_resp = json;
			//console.log(json_resp);
		}
	});
}

getJSON(json_url);

console.log(json_resp);

var myList = [{
	"name" : "abc",
	"age" : 50
}, {
	"age" : "25",
	"hobby" : "swimming"
}, {
	"name" : "xyz",
	"hobby" : "programming"
}];

setTimeout(function() {
	//myList = json_resp;

	console.log(json_resp);

	// Builds the HTML Table out of myList.
	function buildHtmlTable(selector) {
		var columns = addAllColumnHeaders(json_resp, selector);
		console.log(json_resp.length);

		for (var i = 0; i < json_resp.length; i++) {
			var row$ = $('<tr/>');

			for (var colIndex = 0; colIndex < columns.length; colIndex++) {
				var cellValue = json_resp[i][columns[colIndex]];
				if (cellValue == null)
					cellValue = "";
				row$.append($('<td/>').html(cellValue));
			}
			$(selector).append(row$);
		}
		console.log("1");
	}

	// Adds a header row to the table and returns the set of columns.
	// Need to do union of keys from all records as some records may not contain
	// all records.
	function addAllColumnHeaders(json_resp, selector) {
		var columnSet = [];
		var headerTr$ = $('<tr/>');

		for (var i = 0; i < json_resp.length; i++) {
			var rowHash = myList[i];
			for (var key in rowHash) {
				if ($.inArray(key, columnSet) == -1) {
					columnSet.push(key);
					headerTr$.append($('<th/>').html(key));
				}
			}
		}
		$(selector).append(headerTr$);

		return columnSet;
	}

	buildHtmlTable('#excelDataTable');
	console.log("Finished");
}, 3000); 