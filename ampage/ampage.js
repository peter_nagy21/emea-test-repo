setcolor = function(str, color) {
	// str: pb, vt, pm
	// color: green, yellow, red

	var span_id = "#" + str + "_icon";
	var h3_id = "#" + str + "_h3";

	var yellow_text = "<i class='material-icons icon-yellow'>report_problem</i>";
	var green_text = "<i class='material-icons icon-green'>check_circle</i>";
	var red_text = "<i class='material-icons icon-red'>report_problem</i></span>";

	if (color === "green") {
		$(span_id).html(green_text);
		$(h3_id).css("color", "#396f19");

	}
	if (color === "yellow") {
		$(span_id).html(yellow_text);
		$(h3_id).css("color", "#c99e01");

	}
	if (color === "red") {
		$(span_id).html(red_text);
		$(h3_id).css("color", "#931b1c");

	}
}
show_am_req = function() {

	var tr = $("#tr_select").val();

	if (tr) {
		$("#track_req").show();
		$("#track_req_alert").hide();
	}

	var trh1_text = "";
	var pb_text = "";
	var ai_text = "";
	var vt_text = "";
	var pm_text = "";
	var pie_text = "";
	var hc_text = "";

	switch (tr) {
	case "tune":
		setcolor("pb", "yellow");
		setcolor("ai", "green");
		setcolor("vt", "red");
		setcolor("pm", "yellow");
		setcolor("pie", "green");
		setcolor("hc", "green");
		trh1_text = "Tune";
		pb_text = "No action necessary (yet) - the client should set up the postback in Tune. TAM will check it.";
		pb_text += "<br /><br /><span class='bold'>Postback URL sample:</span><br />http://cpa.adtilt.com/on_user_action?api_key=425ef9e32886669fe4d92efb9cdbddf4&product_id={package_name}&google_ad_id={google_aid}&raw_mac={mac_address}&odin1={odin}&sha1_android_id={android_id_sha1}&adc_conversion={is_publisher_attributed({postback_publisher_id})}";
		ai_text = "Yes. The advertiser must select 'AdColony' as the partner and set the postback to send 'All Installs'.";
		vt_text = "Please ask the client to confirm the view-through. <span class='bold'>Default: 7 days</span><br /> TAM will check it in Tune once the VT has been confirmed.";
		vt_text += "<br />TAM will need to generate impression URLs. Can be enabled at <span class='bold'>Advertiser level</span>."
		pm_text = "The client should let us know if they need any special parameters / values in the URLs.";
		pie_text = "PIE events should be implemented by the client in TUNE dashboard. <br />";
		pie_text += "More info about the client setup can be found here: ";
		pie_text += "<a class='blue' href='http://support.adcolony.com/customer/portal/articles/1914521-postbacks-for-top-tracking-partners#Tune' target='_blank'>Tune PIE setup</a>";
		hc_text = "No - we can use server-to-server (S2S) calls. <i class='em em---1'></i>";
		$(".am_paramdiv").hide();
		$("#am_tune_param").show();
		break;
	case "adjust":
		setcolor("pb", "green");
		setcolor("ai", "green");
		setcolor("vt", "red");
		setcolor("pm", "yellow");
		setcolor("pie", "yellow");
		setcolor("hc", "green");
		trh1_text = "Adjust";
		pb_text = "No action necessary - The postback is appended to the end of the tracking URL.";
		ai_text = "Yes, if the advertiser has enabled the Special Partners module. Otherwise no.";
		vt_text = "Please ask the client to confirm the view-through. <br />Can be enabled at <span class='bold'>App level</span>.<br /><br /><span class='req_red'>1 day</span><br /> Ask the client to enable view-through tracking in their integration with Adjust.<br /><br />";
		vt_text += "<span class='req_green'>7 days</span><br /> No action necessary - we implement it on our end.";
		pm_text = "TAM will need to know what values the client would like. We can only use 3 parameters: campaign, adgroup and creative.<br />";
		pie_text = "We implement PIE in the tracking URLs. Client will need to provide us with a name / token for each event.<br />";
		pie_text += "More info about the client setup can be found here: ";
		pie_text += "<a class='blue' href='http://support.adcolony.com/customer/portal/articles/1914521-postbacks-for-top-tracking-partners#Adjust' target='_blank'>Adjust PIE setup</a>";
		hc_text = "No - we can use server-to-server (S2S) calls. <i class='em em---1'></i>"
		$(".am_paramdiv").hide();
		$("#am_adjust_param").show();
		break;
	case "appsflyer":
		setcolor("pb", "green");
		setcolor("ai", "red");
		setcolor("vt", "red");
		setcolor("pm", "yellow");
		setcolor("pie", "green");
		setcolor("hc", "green");
		trh1_text = "AppsFlyer";
		pb_text = "The postback should be automatically set up in AppsFlyer.";
		pb_text += "<br /><br /><span class='bold'>Postback URL sample:</span><br />http://cpa.adtilt.com/on_user_action?api_key=830b3c7a64b19319e77d8163db7605fe&product_id=%5BPRODUCT_ID%5D&sha1_mac=%5BMAC_SHA1%5D&raw_advertising_id=%5BIDFA%5D";
		ai_text = "No, our integration does not currently have the ability for them to do this.";
		vt_text = "Please ask the client to confirm the view-through. <br />Can be enabled at <span class='bold'>App level</span>.<br /><br /><span class='req_red'>1 day</span><br /> Ask the client to enable view-through tracking in their integration with AppsFlyer.<br />";
		vt_text += "<span class='req_green'>7 days</span><br /> No action necessary - we implement it on our end.";
		pm_text = "The client should let us know if they need any special parameters / values in the URLs.<br />";
		pie_text = "PIE events should be implemented by the client in AppsFlyer dashboard.<br />";
		pie_text += "More info about the client setup can be found here: ";
		pie_text += "<a class='blue' href='http://support.adcolony.com/customer/portal/articles/1914521-postbacks-for-top-tracking-partners#Appsflyer' target='_blank'>AppsFlyer PIE setup</a>";
		hc_text = "No - we can use server-to-server (S2S) calls. <i class='em em---1'></i>"
		$(".am_paramdiv").hide();
		$("#am_apps_param").show();
		break;
	case "kochava":
		setcolor("pb", "yellow");
		setcolor("ai", "green");
		setcolor("vt", "red");
		setcolor("pm", "yellow");
		setcolor("pie", "green");
		setcolor("hc", "green");
		trh1_text = "Kochava";
		pb_text = "Client should set up the postback in Kochava.<br /><br />They will need to set:<br />";
		pb_text += "1. API Key: <span class='bold'>25b8c23e2b6bfe08e75aed432f46149b</span><br />";
		pb_text += "2. Product ID: store / bundle id of the app.<br />";

		pb_text += "&nbsp;&nbsp;&nbsp;&nbsp;For example: ";
		pb_text += "<span class='bold'>Android:</span> com.audible.application; ";
		pb_text += "<span class='bold'>iOS:</span> 946811576<br />";
		pb_text += "TAM can check it.";
		ai_text = "Yes, the advertiser must select “All” as the delivery method.";
		vt_text = "<span class='bold'>Default: 24 hours.</span> (We don't have option for 7 days unfortunately.)<br />Client should enable and/or adjust view-through tracking in their integration with Kochava.<br />";
		vt_text += "Can be enabled at <span class='bold'>App level</span>.";
		pm_text = "The client should let us know if they need any special parameters / values in the URLs.<br />";
		pie_text = "PIE events should be implemented by the client in Kochava dashboard.<br />";
		pie_text += "More info about the client setup can be found here: ";
		pie_text += "<a class='blue' href='http://support.adcolony.com/customer/portal/articles/1914521-postbacks-for-top-tracking-partners#Kochava' target='_blank'>Kochava PIE setup</a>";
		hc_text = "No - we can use server-to-server (S2S) calls. <i class='em em---1'></i>"
		$(".am_paramdiv").hide();
		$("#am_koch_param").show();
		break;
	case "wooga":
		setcolor("pb", "yellow");
		setcolor("vt", "yellow");
		setcolor("pm", "yellow");
		setcolor("pie", "green");
		setcolor("hc", "red");
		trh1_text = "Wooga";
		pb_text = "Client should set up the postback on their side.";
		vt_text = "7 days (mathced to click window). View-through not available for Android.<br />Can be enabled at <span class='bold'>App level</span>.";
		pm_text = "The client should let us know if they need any special parameters / values in the URLs .<br />";
		hc_text = "<span class='req_red'>Yes for Android</span> - we will need to hardcode the URLs to the DEC. <i class='em em-tired_face'></i><br />";
		hc_text += "<span class='req_green'>No for iOS</span> - we can use server-to-server (S2S) calls. <i class='em em---1'></i>"
		pie_text = "No action necessary - PIE not available.";
		$(".am_paramdiv").hide();
		$("#am_woo_param").show();
		break;
	case "gameloft":
		setcolor("pb", "yellow");
		setcolor("vt", "yellow");
		setcolor("pm", "yellow");
		setcolor("pie", "green");
		setcolor("hc", "green");
		trh1_text = "Gameloft";
		pb_text = "Client should set up the postback on their side.";
		vt_text = "3 days - Can be enabled at <span class='bold'>App level</span>.";
		pm_text = "The client should let us know if they need any special parameters / values in the URLs .<br />";
		hc_text = "No - we can use server-to-server (S2S) calls. <i class='em em---1'></i>"
		pie_text = "No action necessary - PIE not available.";
		$(".am_paramdiv").hide();
		$("#am_gloft_param").show();
		break;
	case "apsalar":
		setcolor("pb", "yellow");
		setcolor("ai", "yellow");
		setcolor("vt", "yellow");
		setcolor("pm", "yellow");
		setcolor("pie", "green");
		setcolor("hc", "red");
		trh1_text = "Apsalar";
		pb_text = "No action necessary - Client should set up the postback in apsalar. TAM can check it.";
		pb_text += "<br /><br /><span class='bold'>Postback URL sample:</span><br />http://cpa.adtilt.com/on_user_action?api_key=d4636d565f59e217801b5721af716eac&product_id={product_id?AdColony}&raw_advertising_id={IDFA}";
		ai_text = "Yes, the advertiser must have <span class='bold'>the most recent postback template implemented</span> (updated Feb 2017) and enable sending 'all events'.";
		vt_text = "7 days (mathced to click window). View-through not available for Android.<br />Can be enabled at <span class='bold'>App level</span>.";
		pm_text = "The client should let us know if they need any special parameters / values in the URLs .<br />";
		hc_text = "<span class='req_red'>Yes for Android</span> - we will need to hardcode the URLs to the DEC. <i class='em em-tired_face'></i><br />";
		hc_text += "<span class='req_green'>No for iOS</span> - we can use server-to-server (S2S) calls. <i class='em em---1'></i>"
		pie_text = "PIE events should be implemented by the client in Apsalar dashboard.<br />";
		pie_text += "More info about the client setup can be found here: ";
		pie_text += "<a class='blue' href='http://support.adcolony.com/customer/portal/articles/1914521-postbacks-for-top-tracking-partners#Apsalar' target='_blank'>Apsalar PIE setup</a>.";
		pie_text += " The supported event names can be found <a class='blue' href='http://support.adcolony.com/customer/portal/articles/1917819-supported-post-install-events' target='_blank'>here</a>.";
		$(".am_paramdiv").hide();
		$("#am_aps_param").show();
		break;
	case "king":
		setcolor("pb", "green");
		setcolor("ai", "green");
		setcolor("vt", "green");
		setcolor("pm", "green");
		setcolor("hc", "green");
		setcolor("pie", "green");
		trh1_text = "King.com";
		pb_text = "No action necessary - The postback is set up by King.";
		ai_text = "Yes, we are using mparticle to import segments of users that have installed their apps.";
		vt_text = "12 hours - Can be enabled at <span class='bold'>App level</span>.";
		pm_text = "The client should let us know if they need any special parameters / values in the URLs.<br />";
		hc_text = "No - we can use server-to-server (S2S) calls. <i class='em em---1'></i>"
		pie_text = "No action necessary - King can set up the PIE events on their side.";
		$(".am_paramdiv").hide();
		$("#am_king_param").show();
		break;
	case "fiksu":
		setcolor("pb", "yellow");
		setcolor("vt", "yellow");
		setcolor("pm", "yellow");
		setcolor("pie", "green");
		setcolor("hc", "red");
		trh1_text = "Fiksu";
		pb_text = "FIKSU should send us the full postback logs. If they don't - please ask them to confirm that the postback has been set up.";
		vt_text = "7 days view-through. Not available for Android.<br />Can be enabled at <span class='bold'>App level</span>.";
		pm_text = "The client should let us know if they need any special parameters / values in the URLs .<br />";
		hc_text = "<span class='req_red'>Yes for Android</span> - we will need to hardcode the URLs to the DEC. <i class='em em-tired_face'></i><br />";
		hc_text += "<span class='req_green'>No for iOS</span> - we can use server-to-server (S2S) calls. <i class='em em---1'></i>"
		pie_text = "No action necessary - PIE not available.";
		$(".am_paramdiv").hide();
		$("#am_fik_param").show();
		break;
	case "growmobile":
		setcolor("pb", "yellow");
		setcolor("vt", "yellow");
		setcolor("pm", "yellow");
		setcolor("pie", "green");
		setcolor("hc", "green");
		trh1_text = "GrowMobile";
		pb_text = "Please ask the client to confirm that the postback has been set up.";
		vt_text = "7 days. Can be enabled at <span class='bold'>App level</span>.";
		pm_text = "The client should let us know if they need any special parameters / values in the URLs .<br />";
		hc_text = "No - we can use server-to-server (S2S) calls. <i class='em em---1'></i>"
		pie_text = "No action necessary - PIE not available.";
		$(".am_paramdiv").hide();
		$("#am_grow_param").show();
		break;
	case "notfound":
		$("#track_req").hide();
		$("#track_req_alert").show();
		$("#track_req_alert").html("Please contact the TAM team!");
		$(".am_paramdiv").hide();
		break;
	
	}

	$("#trh1").html(trh1_text);
	$("#tr_req_pb").html(pb_text);
	$("#tr_req_ai").html(ai_text);
	$("#tr_req_vt").html(vt_text);
	$("#tr_req_pm").html(pm_text);
	$("#tr_req_pie").html(pie_text);
	$("#tr_req_hc").html(hc_text);

}
vt_check = function() {

	var html5 = document.getElementById("html5_input").value;
	console.log(html5);
	var start = document.getElementById("vstart_input").value;
	console.log(start);
	var complete = document.getElementById("vcomplete_input").value;
	console.log(complete);

	check_track('html5_input', vt_prov_div);

	switch (tracking_provider) {
	case "Tune":
		if (is_tune(html5) && is_tune(start) && is_tune(complete)) {
			$("#vt_prov_div").append("The view-through is enabled on our side. TAM can check the VT value in TUNE dash if needed.");
		} else {
			if ((start === "") && (complete === "")) {
				$("#vt_prov_div").append("View-through is NOT enabled on our side.");
			} else {
				$("#vt_prov_div").append("There is something wrong with the URLs. For more info please contact the TAM team.");
			}
		}
		break;
	case "Kochava":
		var is_impkoch = (complete.indexOf("imp.control.kochava") > -1);
		if (start !== "") {
			$("#vt_prov_div").append("Something is wrong! The 'Video Starts' field should be empty. Please contact the TAM team.");
		} else {
			if (complete === "") {
				$("#vt_prov_div").append("View-through is NOT enabled on our side.");
			} else {
				if (is_impkoch) {
					$("#vt_prov_div").append("The view-through is enabled on our side. TAM can check the VT value in Kochava dash if needed.");
				} else {
					$("#vt_prov_div").append("There is something wrong with the URLs. For more info please contact the TAM team.");
				}
			}
		}
		break;
	case "AppsFlyer":
		var is_oneday_apps = (complete.indexOf("impression.appsflyer.com") > -1);
		var is_sevenday_apps = (complete.indexOf("=impression") > -1);
		var custom_vt_apps = "";
		
		var cvt = complete.indexOf("viewthrough");

		var is_custom_vt_apps = (cvt > -1);
		
		console.log(cvt);
		
		console.log("custom: ",is_custom_vt_apps);

		if (is_custom_vt_apps) {

			var sub1 = complete.split("af_viewthrough_lookback");
			console.log("sub1: " + sub1);
			var sub2 = sub1[1];
			console.log("sub2: " + sub2);
			var sub3 = sub2.split("&");
			console.log("sub3: " + sub3);
			custom_vt_apps = sub3[0];
			custom_vt_apps = custom_vt_apps.substring(1);
			console.log(custom_vt_apps);
			custom_vt_apps = custom_vt_apps.toString();
			console.log(custom_vt_apps);

			if ((custom_vt_apps.indexOf("h") < 0) && (custom_vt_apps.indexOf("d") < 0)) {
				custom_vt_apps += " days";
			} else {
				if ((custom_vt_apps === "1d") || (custom_vt_apps === "1h")) {
					custom_vt_apps = custom_vt_apps.replace("d", " day");
					custom_vt_apps = custom_vt_apps.replace("h", " hour");
				} else {
					custom_vt_apps = custom_vt_apps.replace("d", " days");
					custom_vt_apps = custom_vt_apps.replace("h", " hours");
				}

			}

		}

		if (start !== "") {
			$("#vt_prov_div").append("Something is wrong! The 'Video Starts' field should be empty. Please contact the TAM team.");
		} else {
			if (complete === "") {
				$("#vt_prov_div").append("View-through is NOT enabled.");
			} else {
				if (is_oneday_apps) {

					if (custom_vt_apps != "") {
						$("#vt_prov_div").append("View-through is enabled on our side. The VT window is " + custom_vt_apps + ".");
					} else {
						$("#vt_prov_div").append("1 day view-through is enabled on our side.");
					}

				} else {
					if (is_sevenday_apps) {
						$("#vt_prov_div").append("7 days view-through is enabled.");
					} else {
						$("#vt_prov_div").append("There is something wrong with the URLs. For more info please contact the TAM team.");
					}
				}
			}
		}
		break;
	case "Adjust":
		var is_oneday_adj = (complete.indexOf("view.adjust.com") > -1);
		if (start !== "") {
			$("#vt_prov_div").append("Something is wrong! The 'Video Starts' field should be empty. Please contact the TAM team.");
		} else {
			if (complete === "") {
				$("#vt_prov_div").append("View-through is NOT enabled.");
			} else {
				if (is_oneday_adj) {
					$("#vt_prov_div").append("1 day view-through is enabled on our side.");
				} else {
					$("#vt_prov_div").append("There is something wrong with the URLs. For more info please contact the TAM team.");
				}
			}
		}
		break;

	// Adjust, Wooga specific rules when HTML5 is empty
	case "":
		var is_adj = (complete.indexOf("adjust.com") > -1);
		var is_wooga = (complete.indexOf("woogatrack") > -1);
		console.log(html5);
		console.log(start);
		console.log(is_adj);
		if ((html5 === "") && (start === "") && is_adj) {
			$("#vt_prov_div").html("This should be <span class='tr_prov'>Adjust</span>.<br /><br />7 days view-through is enabled.");
			console.log("adjust 7days");
		}
		if ((html5 === "") && (start === "") && is_wooga) {
			$("#vt_prov_div").html("This should be <span class='tr_prov'>Wooga</span>.<br /><br />7 days view-through is enabled. (By the way, the click URL should be hardcoded in the DEC.)");
			console.log("wooga 7days");
		}
		break;
	// END OF - Adjust specific rules when HTML5 is empty

	case "Apsalar":
		var is_sevendays_aps = (complete.indexOf("op=impression") > -1);
		if (start !== "") {
			$("#vt_prov_div").append("Something is wrong! The 'Video Starts' field should be empty. Please contact the TAM team.");
		} else {
			if (complete === "") {
				$("#vt_prov_div").append("View-through is NOT enabled.");
			} else {
				if (is_sevendays_aps) {
					$("#vt_prov_div").append("7 days view-through is enabled.");
				} else {
					$("#vt_prov_div").append("There is something wrong with the URLs. For more info please contact the TAM team.");
				}
			}
		}
		break;
	case "Wooga":
		var is_wooga = (complete.indexOf("woogatrack") > -1);
		if (start !== "") {
			$("#vt_prov_div").append("Something is wrong! The 'Video Starts' field should be empty. Please contact the TAM team.");
		} else {
			if (complete === "") {
				$("#vt_prov_div").append("View-through is NOT enabled.");
			} else {
				if (is_wooga) {
					$("#vt_prov_div").append("7 days view-through is enabled.");
				} else {
					$("#vt_prov_div").append("There is something wrong with the URLs. For more info please contact the TAM team.");
				}
			}
		}
		break;
	case "Fiksu":
		var is_fiksu = (complete.indexOf("fiksu.com") > -1);
		if (start !== "") {
			$("#vt_prov_div").append("Something is wrong! The 'Video Starts' field should be empty. Please contact the TAM team.");
		} else {
			if (complete === "") {
				$("#vt_prov_div").append("View-through is NOT enabled.");
			} else {
				if (is_fiksu) {
					$("#vt_prov_div").append("7 days view-through is enabled.");
				} else {
					$("#vt_prov_div").append("There is something wrong with the URLs. For more info please contact the TAM team.");
				}
			}
		}
		break;
	case "GrowMobile":
		var is_growimp = (complete.indexOf("imp.growmobile.com") > -1);
		if (start !== "") {
			$("#vt_prov_div").append("Something is wrong! The 'Video Starts' field should be empty. Please contact the TAM team.");
		} else {
			if (complete === "") {
				$("#vt_prov_div").append("View-through is NOT enabled.");
			} else {
				if (is_growimp) {
					$("#vt_prov_div").append("7 days view-through is enabled.");
				} else {
					$("#vt_prov_div").append("There is something wrong with the URLs. For more info please contact the TAM team.");
				}
			}
		}
		break;
	case "Gameloft":
		var is_gloftimp = (complete.indexOf("extads.gameloft.com/adcolony/impression") > -1);
		if (start !== "") {
			$("#vt_prov_div").append("Something is wrong! The 'Video Starts' field should be empty. Please contact the TAM team.");
		} else {
			if (complete === "") {
				$("#vt_prov_div").append("View-through is NOT enabled.");
			} else {
				if (is_gloftimp) {
					$("#vt_prov_div").append("3 days view-through is enabled.");
				} else {
					$("#vt_prov_div").append("There is something wrong with the URLs. For more info please contact the TAM team.");
				}
			}
		}
		break;
	case "King.com":
		var is_kingimp = (complete.indexOf("king.com/click?type=video") > -1);
		if (start !== "") {
			$("#vt_prov_div").append("Something is wrong! The 'Video Starts' field should be empty. Please contact the TAM team.");
		} else {
			if (complete === "") {
				$("#vt_prov_div").append("View-through is NOT enabled.");
			} else {
				if (is_kingimp) {
					$("#vt_prov_div").append("12 hours view-through is enabled.");
				} else {
					$("#vt_prov_div").append("There is something wrong with the URLs. For more info please contact the TAM team.");
				}
			}
		}
		break;
	case "Adzcore":
		var is_ptrackimp = (complete.indexOf("redirection=0") > -1);
		if (start !== "") {
			$("#vt_prov_div").append("Something is wrong! The 'Video Starts' field should be empty. Please contact the TAM team.");
		} else {
			if (complete === "") {
				$("#vt_prov_div").append("View-through is NOT enabled.");
			} else {
				if (is_ptrackimp) {
					$("#vt_prov_div").append("View-through is enabled, VT is based on AdColony setting.");
				} else {
					$("#vt_prov_div").append("There is something wrong with the URLs. For more info please contact the TAM team.");
				}
			}
		}
		break;
	default:
		$("#vt_prov_div").append("View-through? Good question... For more info please contact the TAM team.");
	}
	/*
	 Tune
	 Kochava
	 AppsFlyer
	 Adjust
	 Apsalar
	 Wooga
	 Fiksu
	 Grow
	 Gameloft
	 King.com
	 PartyTrack
	 */
}

$(document).ready(function() {
	$("#am_tune_param").hide();
	//divToggle(false, reqtabletitle, reqtablediv, reqdown, requp);
	divToggle(false, trchtitle, trchdiv, trchdown, trchup);
	divToggle(true, reqtitle, am_req_div, req_otherp_down, req_otherp_up);
	divToggle(false, vtchecktitle, vtcheck_div, vtcheck_down, vtcheck_up);
	divToggle(false, geturl_title, comp_div, comp_down, comp_up);
	divToggle(false, am_tune_otherp_title, tune_otherparams, tune_otherp_down, tune_otherp_up);
	divToggle(false, am_adj_otherp_title, am_adjparams, adj_otherp_down, adj_otherp_up);
	divToggle(false, am_apps_otherp_title, am_appsparams, apps_otherp_down, apps_otherp_up);
	divToggle(false, am_koch_otherp_title, am_kochparams, koch_otherp_down, koch_otherp_up);
	divToggle(false, am_woo_otherp_title, am_wooparams, woo_otherp_down, woo_otherp_up);
	divToggle(false, am_gloft_otherp_title, am_gloftparams, gloft_otherp_down, gloft_otherp_up);
	divToggle(false, am_aps_otherp_title, am_apsparams, aps_otherp_down, aps_otherp_up);
	divToggle(false, am_fik_otherp_title, am_fikparams, fik_otherp_down, fik_otherp_up);
	divToggle(false, am_grow_otherp_title, am_growparams, grow_otherp_down, grow_otherp_up);
	divToggle(false, am_king_otherp_title, am_kingparams, king_otherp_down, king_otherp_up);
});
