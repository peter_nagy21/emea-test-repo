<?php
include ("http://localhost:8888/password_protect.php");
?>
<?php
include '../header.php';
?>
<?php
include '../brandside.php';
?>

<?php include_once("analyticstracking.php") ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="data.js"></script>
<script src="script.js"></script>
<!-- Bootstrap core CSS -->
<link href="../brand/bootstrap.min.css" rel="stylesheet">
<!-- CSS -->
<link href="../brand/css.css" rel="stylesheet">


<div id="maincontent">
	<div class="container">
		<div class="form-group">
			<h1>Nielsen tracking converter - AdMarvel</h1>
			<h5>This is a tool to create Nielsen tracking pixels for AdMarvel.</h5>
			<hr>
			<p>
				Paste your URL to be converted below:
			</p>
			<textarea rows="4" type="text" class="form-control" name="textbox" id="textbox"></textarea>
		</div>
		<p>
			Select whether your URL is for iOS or Android.
		</p>
		<div class="row">
			<div class="col-sm-6">
				<input class="btn btn-lg btn-primary btn-block" type="submit" name="button" id="button1" onclick="hardCodedIos()" value="iOS"/>
			</div>
			<div class="col-sm-6">
				<input class="btn btn-lg btn-primary btn-block" type="submit" name="button" id="button1" onclick="hardCodedDrd()" value="Android"/>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-12">
				<p id="parameters-text">
					Values that have been changed are highlighted in <span class="text-success">green</span>.
				</p>
				<div id="parameters"></div>
				<div id="error-message"></div>
				<div id="protocol-message"></div>
				<p id="complete-url-text">
					Tracking URL to be used:
				</p>
				<div class="well" id="complete-url"></div>
			</div>
		</div>
	</div>

<script type="text/javascript">
	$('#complete-url').hide();
	$('#complete-impression-url').hide();
	$('#complete-url-text').hide();
	$('#parameters-text').hide(); 
</script>

</div>

<?php
include '../footer.php';
?>