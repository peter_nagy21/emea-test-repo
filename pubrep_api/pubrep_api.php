<?php
include ("http://localhost:8888/password_protect.php");
?>

<?php
include '../header.php';
?>

<?php
include '../sideleft.php';
?>

<?php include_once("analyticstracking.php") ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
	$(function() {
		$("#start_date").datepicker();
	}); 
</script>
<script>
	$(function() {
		$("#end_date").datepicker();
	}); 
</script>

<script type="text/javascript" src="pubrep_api.js"></script>

<div id="maincontent" class="comp_content">

	<h1>Publisher Reporting API info page</h1>
	
	<h3 class="black plink" id="notestitle">1. Notes <i class="fa fa-arrow-down" id="notesdown"></i><i class="fa fa-arrow-up" id="notesup"></i></h3>
	<div id="notesdiv" class="ndiv">
		The use of the reporting API is intended for publishers running on AdColony Video Ad Network to retrieve ad performance <br /> reporting data via an API. 
		The API uses HTTP GET to make reporting requests and retrieve results from the server.
		<br />
		<br />
		The full reporting API documentation can be downloaded from <a href="http://support.adcolony.com/customer/portal/articles/1838228-adcolony-publisher-api-2-1-update" target="_blank" class="blue">this</a> AdColony support page.
		<br />
		<br />
	</div>

	<h3 id="bapititle" class="black plink">2. Build the query URL <i class="fa fa-arrow-down" id="bapidown"></i><i class="fa fa-arrow-up" id="bapiup"></i></h3>
	<div id="bapidiv">
		<form id="bapiform">

			<table class="pietable">
				<tr>
					<td>Base URL:</td>
					<td colspan="3">https://clients-api.adcolony.com/api/v2/publisher_summary</td>
				</tr>
				<tr>
					<td>API key (user_credentials):</td>
					<td>
					<input type="text" name="api_key" id="api_key">
					</td>
					<td class="red_td">*required</td>
					<td><a href="https://ops.adcolony.com/admin/master_account_list" target="_blank" class="blue">Search for the API key in AdColony dashboard.</a></td>
				</tr>
				<tr>
					<td>Start date / Date:</td>
					<td>
					<input type="text" name="start_date" id="start_date">
					</td>
					<td colspan="2" class="red_td">*required, unless interval is specified</td>
				</tr>
				<tr>
					<td>End date:</td>
					<td>
					<input type="text" name="end_date" id="end_date">
					</td>
					<td colspan="2">*optional</td>
				</tr>
				<tr>
					<td>Format:</td>
					<td>
					<input type="radio" name="format" id="json" value="json">
					json
					<input type="radio" name="format" id="csv" value="csv" checked="checked">
					csv
					<input type="radio" name="format" id="xml" value="xml">
					xml</td>
					<td colspan="2">*optional, default: json, however it's set to csv here...</td>
				</tr>
				<tr>
					<td>Date group:</td>
					<td>
					<input type="radio" name="date_group" id="aggregate" value="aggregate">
					aggregate
					<input type="radio" name="date_group" id="day" value="day">
					day
					<input type="radio" name="date_group" id="hour" value="hour">
					hour</td>
					<td colspan="2">*optional, default: aggregate</td>
				</tr>
				<tr>
					<td>Group by:</td>
					<td>
					
					<input type="checkbox" name="group_by" id="gr_app" value="app">
					app
					<br />
					<input type="checkbox" name="group_by" id="gr_zone" value="zone">
					zone
					<br />
					<input type="checkbox" name="group_by" id="gr_country" value="country">
					country
					<br />
					
					</td>
					<td colspan="2">*optional</td>
				</tr>
				<tr>
					<td>App ID:</td>
					<td>
					<input type="text" name="app_id" id="app_id">
					</td>
					<td colspan="2">*optional</td>
				</tr>
				<tr>
					<td>Interval</td>
					<td>
					<select name="interval" id="interval">
						<option value=""></option>
						<option value="today">today</option>
						<option value="yesterday">yesterday</option>
						<option value="last24">last24</option>
						<option value="this_week">this_week</option>
						<option value="last_week">last_week</option>
						<option value="this_month">this_month</option>
						<option value="last_month">last_month</option>
						<option value="last_30_days">last_30_days</option>
						<option value="this_year">this_year</option>
						<option value="last_year">last_year</option>
						<option value="all_time">all_time</option>
					</select></td>
					<td colspan="2">*optional, it will override the "date"</td>
				</tr>
			</table>
			<br />
			<input type="button" class="btn-class" name="buildURL" id="buildURL" value="Build the query URL!" onclick="build_url()">
		</form>

		<br />
		<div id="alertdiv"></div>
		<div id="urldiv"></div>
		<br />

	</div>
	
	
	<h3 class="black plink" id="hiddentitle"> &nbsp;<i class="fa fa-arrow-down" id="hiddendown" style="display: none !important;"></i><i class="fa fa-arrow-up" id="hiddenup" style="display: none !important;"></i></h3>
	<div id="hiddendiv">
		Peter test campaign API key: W0KYONhCiK0usqvGFy1B
	</div>

</div>

<?php
include '../footer.php';
?>

