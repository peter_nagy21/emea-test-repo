check_red = function() {
	var testlink = document.getElementById("text").value;
	redirectURL(testlink, '#redurldiv');
	
}

setChartImage = function() {

	var testlink = document.getElementById("text").value;
	var input_prod_id = document.getElementById("prod_id").value;
	
	var final_comment = "";

	// product ID validation

	var is_prod_id = (testlink.indexOf("[PRODUCT_ID]") > -1);
	var is_appsflyer = (testlink.indexOf("appsflyer") > -1);

	get_af_prod = function() {
		var appsflyer_base_url = testlink.split("?")[0];
		var l_base_url = appsflyer_base_url.length;
		var id_for_app = appsflyer_base_url.substring(appsflyer_base_url.lastIndexOf("/") + 1, l_base_url + 1);

		// delete "id" from ios product_ids
		var x = id_for_app.substr(0, 2);
		if (x === "id") {
			id_for_app = id_for_app.replace("id", "");
		}
		return id_for_app;
	}
	if (is_appsflyer) {
		get_af_prod();
	}

	var prod_id_ok = true;

	$(prod_id_alert).html("");

	// product ID alert

	//if ((input_prod_id === "") && (is_prod_id) && !is_appsflyer) {  -- old logic, now the product ID required for the install feed
	if ((input_prod_id === "") && !is_appsflyer) {

		var prod_id_alert_html = "<span>Please enter the product ID.</span>";
		$(prod_id_alert).html(prod_id_alert_html);
		prod_id_ok = false;
	}

	// remove S2S parameters

	// Tune
	var sub_str1 = "&response_format=json";
	// Adjust
	var sub_str2 = "s2s=1&";
	// AppsFlyer
	var sub_str3 = "&redirect=false";
	// Kochava
	var sub_str4 = "&pbr=1";
	// King.com
	var sub_str5 = "&noRedirect=TRUE";
	
	// notification about removing s2s parameters
	
	
	
	var is_sub_str1 = (testlink.indexOf(sub_str1) > -1);
	var is_sub_str2 = (testlink.indexOf(sub_str2) > -1);
	var is_sub_str3 = (testlink.indexOf(sub_str3) > -1);
	var is_sub_str4 = (testlink.indexOf(sub_str4) > -1);
	var is_sub_str5 = (testlink.indexOf(sub_str5) > -1);
	
	if (is_sub_str1){
		final_comment += "'" + sub_str1 + "'" + " has been removed from the link.<br />";
	}
	if (is_sub_str2){
		final_comment += "'" + sub_str2 + "'" + " has been removed from the link.<br />";
	}
	if (is_sub_str3){
		final_comment += "'" + sub_str3 + "'" + " has been removed from the link.<br />";
	}
	if (is_sub_str4){
		final_comment += "'" + sub_str4 + "'" + " has been removed from the link.<br />";
	}
	if (is_sub_str5){
		final_comment += "'" + sub_str5 + "'" + " has been removed from the link.<br />";
	}

	var redURL = testlink.replace(sub_str1, "");
	redURL = redURL.replace(sub_str2, "");
	redURL = redURL.replace(sub_str3, "");
	redURL = redURL.replace(sub_str4, "");
	redURL = redURL.replace(sub_str5, "");
	
	// click ID generator

	function makeid() {
		var text = "";
		var possible = "abcdef0123456789";

		for (var i = 0; i < 40; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));

		return text;
	}

	var click_id = makeid();
	var html_click_id = "<span class='bold green'>" + click_id + "</span>";
	
	// for Adjust replace "cost_id=[CLICK_ID]"
	
	var cost_ci = "cost_id=" + click_id;
	var html_cost_ci = "cost_id=" + "<span class='bold green'>" + click_id + "</span>";

	// redURL1 to display in HTML, redURL for the QR generator
	
	redURL1 = redURL;
	
	redURL1 = redURL1.replace("[IDFA]", "<span class='bold red'>[IDFA]</span>");
	redURL1 = redURL1.replace("%7Bidfa%7D", "<span class='bold red'>[IDFA]</span>");
	redURL1 = redURL1.replace("raw_advertising_id=[IDFA]", "raw_advertising_id=<span class='bold red'>[IDFA]</span>");
	
	redURL1 = redURL1.replace("[GOOGLE_AD_ID]", "<span class='bold red'>[GOOGLE_AD_ID]</span>");
	redURL1 = redURL1.replace("%7Bgps_adid%7D", "<span class='bold red'>[GOOGLE_AD_ID]</span>");
	redURL1 = redURL1.replace("google_ad_id=[GOOGLE_AD_ID]", "google_ad_id=<span class='bold red'>[GOOGLE_AD_ID]</span>");
	
	redURL1 = redURL1.replace("[CLICK_ID]", html_click_id);
	redURL1 = redURL1.replace("cost_id=[CLICK_ID]", html_cost_ci);
	
	// redURL for the QR generator
	
	redURL = redURL.replace("%7Bidfa%7D", "[IDFA]");
	redURL = redURL.replace("%7Bgps_adid%7D", "[GOOGLE_AD_ID]");
	redURL = redURL.replace("[CLICK_ID]", click_id);
	redURL = redURL.replace("cost_id=[CLICK_ID]", cost_ci);

	// product ID swap

	if (is_appsflyer) {
		var html_prod_id = "<span class='bold green'>" + get_af_prod() + "</span>";
		redURL = redURL.replace("[PRODUCT_ID]", get_af_prod());
		redURL1 = redURL1.replace("[PRODUCT_ID]", html_prod_id);
		$(prod_id).val(get_af_prod());

	} else {
		if (input_prod_id !== "") {
			var html_prod_id = "<span class='bold green'>" + input_prod_id + "</span>";
			redURL = redURL.replace("[PRODUCT_ID]", input_prod_id);
			redURL1 = redURL1.replace("[PRODUCT_ID]", html_prod_id);
		}
	}
	
	var feed_prod_id = "";
	
	if (is_appsflyer) {
		feed_prod_id = get_af_prod();
	} else {
		feed_prod_id = input_prod_id;
	}
	
	//console.log("feed prod id: " + feed_prod_id);
	
	var feed_url = "http://cpa.adcolony.com/most_recent_actions?api_key=bb2cf0647ba654d7228dd3f9405bbc6a&product_id=" + feed_prod_id;
	feed_html = "<a class='blue' href='" + feed_url  + "' target='_blank'>" + feed_url + "</a>";

	// display the modified link

	console.log(prod_id_ok);

	if (prod_id_ok) {

		$(linkdiv).html(redURL1);
		//$("#keydiv").show();
		splitURL(redURL, urlsplit_div);
		$("#urlparamstitle").show();
		//redirectURL(redURL, '#redurldiv');
		
		// feed URL
		$(if_span).html(feed_html);
		$("#feed_link").show();

		// generate QR code

		$("#chart").show();
		var query = {
			cht : "qr",
			choe : "UTF-8",
			chs : $("#size").val(),
			chld : $(":radio[name='ec']:checked").val(),
			chl : redURL
		};
		var url = "http://chart.apis.google.com/chart?" + $.param(query);

		$("#chart").attr('src', url);
		$("#url").val(url);
		$("#link").attr('href', url);
		
		// notification about link changes
		
		$("#vnotes_comment").css("color", "green");
		$("#vnotes_comment").html(final_comment);

	}

}

$(document).ready(function() {
	//$("#keydiv").hide();
	$("#feed_link").hide();
	divToggle(false, notestitle, notesdiv, notesdown, notesup);	
	divToggle(false, notestitle1, notesdiv1, notesdown1, notesup1);
	divToggle(false, urlparamstitle, urlsplit_div, urlpdown, urlpup);
});

