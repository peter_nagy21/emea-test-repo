// global variable changed depending on whether the url needs to be hard coded for ios or android
var hardCoded = "regular";

// hard coded drd button pressed
function hardCodedDrd() {
	window.hardCoded = "android";
	fixUrl();
}

// hard coded ios button pressed
function hardCodedIos() {
	window.hardCoded = "ios";
	fixUrl();
}

// find the protocol of the url and return the type
function findProtocol(url) {
	var protocol = "";
	if (url.includes("safari")) {
		protocol = "ios";
	} else if (url.includes("browser")) {
		protocol = "android";
	} else {
		protocol = "regular";
	}
	return protocol;
}

//main function
function fixUrl() {

	$('#button').hide();
	$('#parameters').empty();
	$('#complete-url').hide();
	$('#parameters-text').hide();
	$('#complete-url-text').hide();
	$('#error-message').empty();
	$('#protocol-message').empty();

	// error messages
	var brandNotRecognisedMessage = "<p class='text-danger'>Brand is not recognised - <a href='mailto:max.elston@adcolony.com?Subject=URL%20convertor%20error:%20Brand%20is%20not%20recognised' target='_top'>please contact support.</a></p>";
	var urlAlreadyConverted = "<p class='text-danger'>This looks like a URL that has already been converted. Are you sure you should be inputting this URL? <a href='mailto:max.elston@adcolony.com?Subject=URL%20convertor%20error:%20Hard%20coded%20URL%20is%20trying%20to%20be%20passed' target='_top'>Please contact support.</a></p>";
	var noChangesNeeded = "<p>This URL looks fine - no changes are needed</p>";
	var changedToAndroid = "<p>Protocol has been changed to <span class='text-success'>browser://</span></p>";
	var changedToSafari = "<p>Protocol has been changed to <span class='text-success'>safari://</span></p>";
	var errorInProtocol = "<p class='text-danger'>This URL has an error at the start (incorrect http/https) - <a href='mailto:max.elston@adcolony.com?Subject=URL%20convertor%20error:%20Protocol%20is%20incorrect' target='_top'>please contact support.</a></p>";
	var nothingHasChanged = "<p class='text-warning'>Nothing has in the URL has changed - please check this is as expected</p>";

	//store the protocol message div in a variable for easier access later
	var protocolMessage = document.getElementById("protocol-message");

	// grab url from textbox
	var url = document.getElementById("textbox").value;

	// remove whitespace
	url = url.replace(/\s/g, '');

	// redirect boolean to test for when a url contains a redirect
	var isRedirect = false;

	// check if the tracking provider exists in our database, if not throw an error
	if (getUrlBrand(url) == "not recognised") {
		document.getElementById("error-message").innerHTML = brandNotRecognisedMessage;
		window.hardCoded = "regular";
		return;
		// check if a hard coded url is being passed through, if not throw an error
	} else if (findProtocol(url) == "android" || findProtocol(url) == "ios") {
		document.getElementById("error-message").innerHTML = urlAlreadyConverted;
		window.hardCoded = "regular";
		return;
		// check if the url contains a redirect parameter
	} else {
		for (var i = 0; i < getUrl(url).parameters.length; i++) {
			try {
				if (getUrl(url).parameters[i].value.includes("http")) {
					// url contains redirect
					var redirect = "http" + url.split("http")[2];
					if (getUrlBrand(redirect) == "not recognised") {
						document.getElementById("error-message").innerHTML = brandNotRecognisedMessage;
						window.hardCoded = "regular";
						return;
					}
					isRedirect = true;
				}
				// catch for doubleclick click tracker URL with no macros (double ; type)
			} catch(err) {
				if (window.hardCoded == "regular") {
					document.getElementById("error-message").innerHTML = noChangesNeeded;
					window.hardCoded = "regular";
					return;
				} else {
					var protocol = "";
					if (window.hardCoded == "ios") {
						protocol = "safari:"
					};
					if (window.hardCoded == "android") {
						protocol = "browser:"
					};
					var baseUrl = url.split(":")[1];
					protocolMessage.innerHTML = "<p>Protocol has been changed to <span class='text-success'>" + protocol + "//</span></p>";
					document.getElementById("complete-url").innerHTML = protocol + baseUrl;
					$('#complete-url').show();
					$('#complete-url-text').show();
					window.hardCoded = "regular";
					return
				}
			}
		}
	}

	// before we start manipulating the url, we save it in a variable to compare with the new url later
	var oldUrl = getUrl(url);

	// this variable will be contain the url after manipulation
	var completeUrl = "";

	// replaces the url with the correct macros
	// returns a url string of the query end of the complete URL
	// retusn array of objects containing parameter names and values (macros)
	// returns a boolean declaring whether any changes have been made

	completeUrl = correctParameters(url, completeUrl);

	if (isRedirect) {
		completeRedirectUrl = generateCorrectRedirectUrl(redirect)[0];
		completeRedirect = generateCorrectRedirectUrl(redirect)[1];
	} else {
		parameterReplaced = completeUrl[2];
	}

	// object containing the array of parameter names and values
	newUrl = completeUrl[1];

	// assign the parameter end of the converted url
	completeUrl = completeUrl[0];
	completeUrl = completeUrl.slice(0, -1);

	if (isRedirect) {
		completeUrl = addRedirectValue(url, completeUrl, completeRedirectUrl)
	};
	if (isRedirect) {
		completeUrl = completeUrl.slice(0, -1)
	};

	var separator = "";

	if (determineIfParameterSplitSemiColon(url)) {
		separator = ";";
	} else {
		separator = "?";
	}

	// seperate protocol from base url
	var protocol = newUrl.baseUrl.split(":")[0];
	newUrl.baseUrl = newUrl.baseUrl.split(":")[1];

	// replace the protocols depending on which button was pressed
	if (window.hardCoded == "android") {
		protocol = "browser:";
		protocolMessage.innerHTML = changedToAndroid;
	} else if (window.hardCoded == "ios") {
		protocol = "safari:";
		protocolMessage.innerHTML = changedToSafari;
	} else if (protocol == "https" || protocol == "http") {
		protocol = protocol + ":";
	} else {
		protocolMessage.innerHTML = errorInProtocol;
		window.hardCoded = "regular";
		return;
	}

	if (parameterReplaced) {

		if (isRedirect) {
			oldRedirectParameters = getUrl(redirect).parameters;
			for (var i = 0; i < completeRedirect[1].parameters.length; i++) {
				newUrl.parameters.push(completeRedirect[1].parameters[i])
				oldUrl.parameters.push(oldRedirectParameters[i])
			}
		}

		createParameterTable(newUrl, oldUrl);
		$('#parameters-text').show();
	}

	// compile the url
	completeUrl = protocol + newUrl.baseUrl + separator + completeUrl;

	// check for any changes to the url
	if (!parameterReplaced && findProtocol(url) == window.hardCoded) {
		document.getElementById("error-message").innerHTML = nothingHasChanged;
		window.hardCoded = "regular";
		return;
	}

	$('#complete-url').show();

	// remove special characters

	completeUrl = completeUrl.replace(/\^/g, "%5E");
	completeUrl = completeUrl.replace(/,/g, "%2C");

	document.getElementById("complete-url").innerHTML = completeUrl;

	$('#complete-url-text').show();

	window.hardCoded = "regular";
}

function getUrl(url) {
	// this splits the URL into the base URL and an array of the paramters names and values
	// check if url has a semicolon seperater
	if (determineIfParameterSplitSemiColon(url)) {
		// is ; url

		var parameters = url.split(';');
		var baseUrl = parameters[0];
		parameters.shift();
		return loopThroughParameters(parameters, baseUrl);
	} else {
		// is & url
		var parameters = url.split('?');
		var baseUrl = parameters[0];
		parameters = parameters[1].split('&');
		return loopThroughParameters(parameters, baseUrl);
	}
}

function determineIfParameterSplitSemiColon(url) {
	brand = getUrlBrand(url);
	if (brand == "ad.doubleclick.net" || brand == "ad.atdmt.com") {
		return true
	} else {
		return false
	}
}

function loopThroughParameters(parameters, baseUrl) {
	var parametersLength = parameters.length;

	// create a new array to populate with the split parameter values
	var splitParameters = [];

	// loop through the array splitting apart the variables and the values
	for (var i = 0; i < parametersLength; i++) {
		var variableSplit = parameters[i].split('=');
		splitParameters.push({
			name : variableSplit[0],
			value : variableSplit[1]
		});
	}

	// create an object to return the base url and paramters of a link
	var newUrl = {
		baseUrl : baseUrl,
		parameters : splitParameters
	}

	return newUrl;
}

function getUrlBrand(url) {
	var urlParts = url.split('/', 3);
	var urlBrand = urlParts[2];
	for (var i = 0; i < DATA.url.length; i++) {
		if (DATA.url[i].brand == urlBrand) {
			return DATA.url[i].brand;
		}
	}
	return "not recognised";
}

function getSeparator(url) {
	var urlParts = url.split('/', 3);
	var urlBrand = urlParts[2];
	for (var i = 0; i < DATA.url.length; i++) {
		if (DATA.url[i].brand == urlBrand) {
			return DATA.url[i].separator;
		}
	}
	return "no separator";
}

// returns a list of the parameters found in the DATA object
function getCorrectParameters(url) {

	var urlParts = url.split('/', 3);
	urlBrand = urlParts[2];
	for (var i = 0; i < DATA.url.length; i++) {
		if (DATA.url[i].brand == urlBrand) {
			return DATA.url[i].parameters;
		}
	}
	return "no parameters";
}

// returns an example url found in the DATA object - currently not in use
function getUrlExample(url) {

	var urlParts = url.split('/', 3);
	urlBrand = urlParts[2];
	for (var i = 0; i < DATA.url.length; i++) {
		if (DATA.url[i].brand == urlBrand) {
			return DATA.url[i].example;
		}
	}

	var brandNotRecognised = true;
	return brandNotRecognised;
}

// corrects any of the parameters found in the url
function correctParameters(url, completeUrl) {

	var newUrl = getUrl(url);
	var parameters = getCorrectParameters(url);
	var parameterReplaced = false;
	for (var i = 0; i < newUrl.parameters.length; i++) {
		for (var j = 0; j < parameters.length; j++) {

			if (newUrl.parameters[i].value == parameters[j].find || newUrl.parameters[i].name == parameters[j].find) {
				if (newUrl.parameters[i].value != parameters[j].replace) {
					newUrl.parameters[i].value = parameters[j].replace;
					parameterReplaced = true;
				}
			}
		}
		if (determineIfParameterSplitSemiColon(url)) {
			completeUrl = completeUrl + newUrl.parameters[i].name + "=" + newUrl.parameters[i].value + ";";
		} else {
			completeUrl = completeUrl + newUrl.parameters[i].name + "=" + newUrl.parameters[i].value + "&";
		}
	}

	return [completeUrl, newUrl, parameterReplaced];
}

function generateCorrectRedirectUrl(redirect) {
	var completeRedirect = "";
	completeRedirect = correctParameters(redirect, completeRedirect);

	if (determineIfParameterSplitSemiColon(redirect)) {
		redirectSeparator = ";";
	} else {
		redirectSeparator = "?";
	}
	if (completeRedirect[2] == true || completeUrl[2] == true) {
		// assign the boolean on whether changes have been made
		parameterReplaced = true;

	}
	completeRedirectUrl = completeRedirect[1].baseUrl + redirectSeparator + completeRedirect[0];
	completeRedirectUrl = completeRedirectUrl.slice(0, -1);

	return [completeRedirectUrl, completeRedirect];
}

function addRedirectValue(url, completeUrl, completeRedirectUrl) {

	var completeUrlObject = [];

	if (determineIfParameterSplitSemiColon(url)) {
		completeUrl = completeUrl.split(";");
	} else {
		completeUrl = completeUrl.split("&");
	}

	// console.log(completeUrl);

	for (var i = 0; i < completeUrl.length; i++) {
		split = completeUrl[i].split('=');
		completeUrlObject.push({
			name : split[0],
			value : split[1]
		});
	}

	for (var i = 0; i < completeUrlObject.length; i++) {
		if (completeUrlObject[i].value.includes("http")) {
			completeUrlObject[i].value = completeRedirectUrl;
		}
	}

	completeUrl = "";

	if (determineIfParameterSplitSemiColon(url)) {
		for (var i = 0; i < completeUrlObject.length; i++) {

			completeUrl = completeUrl + completeUrlObject[i].name + "=" + completeUrlObject[i].value + ";";
		}
	} else {
		for (var i = 0; i < completeUrlObject.length; i++) {

			completeUrl = completeUrl + completeUrlObject[i].name + "=" + completeUrlObject[i].value + "&";
		}
	}

	return completeUrl;
}

function createParameterTable(newUrl, oldUrl) {

	var $table = $("<table class='table table-hover table-bordered'></table>");
	var $head = $("<thead></thead>");
	var $headLine = $("<tr></tr>");

	$headLine.append($("<th>Parameter</th>"));
	$headLine.append($("<th>New value</th>"));
	$headLine.append($("<th>Old value</th>"));

	$head.append($headLine);

	$table.append($head);
	var $body = $("<tbody></tbody>")

	for (var i = 0; i < newUrl.parameters.length; i++) {
		var parameters = newUrl.parameters[i];
		var oldParameters = oldUrl.parameters[i];
		if (parameters.value.includes("http") == true) {
			var $line = $("<tr></tr>");
			$line.append($("<td></td>").html(parameters.name));
			$line.append($("<td></td>").html("<span>CONTAINS REDIRECT URL<span>"));
			$line.append($("<td></td>").html("<span>CONTAINS REDIRECT URL<span>"));
		} else if (parameters.value == oldParameters.value) {
			var $line = $("<tr></tr>");
			$line.append($("<td></td>").html(parameters.name));
			$line.append($("<td></td>").html(parameters.value));
			$line.append($("<td></td>").html(oldParameters.value));
		} else {
			var $line = $("<tr class='success'></tr>");
			$line.append($("<td class='text-success'></td>").html(parameters.name));
			$line.append($("<td class='text-success'></td>").html(parameters.value));
			$line.append($("<td class='text-success'></td>").html(oldParameters.value));
		}
		$body.append($line);
	}

	$table.append($body);

	$table.appendTo(document.body);

	$table.appendTo($("#parameters"));
}