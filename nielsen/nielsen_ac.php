<?php
include ("http://localhost:8888/password_protect.php");
?>
<?php
include '../header.php';
?>
<?php
include '../brandside.php';
?>

<?php include_once("analyticstracking.php")
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="script_ac.js"></script>
<!-- Bootstrap core CSS -->
<link href="../brand/bootstrap.min.css" rel="stylesheet">
<!-- CSS -->
<link href="../brand/css.css" rel="stylesheet">

<div id="maincontent">
	<div class="container">
		<div class="form-group">
			<h1>Nielsen tracking converter - AdColony</h1>
			<p style="word-break: break-all; max-width: 720px;">
				This is a tool will replace the <span style="font-weight: bold;">"[timestamp]"</span> with the following substring in the URL:
				<br />
				<br />
				<span style="font-weight: bold;">"[ADCOLONY_TIMESTAMP_MILLIS]&c8=devgrp%2C[DEVICE_GROUP]&c9=devid%2C[IDFA]&c10=plt%2CMBL&c13=asid%2CPF8FEA3C9-CC42-40C4-E040-070AAD3115EC"</span>
				<br />
				<br />
				Example URL:<br /> 
				https://secure-us.imrworldwide.com/cgi-bin/m?ci=nlsnci39&am=3&at=view&rt=banner&st=image&ca=nlsn18910&cr=crtve&pc=adcolony_plc0031&ce=adcolony&r=[timestamp]<br /></p>
			<hr>
			<p>
				Paste your URL below to be converted:
			</p>
			<textarea rows="4" type="text" class="form-control" name="textbox" id="textbox"></textarea>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<input class="btn btn-lg btn-primary btn-block" type="submit" name="button" id="button1" onclick="fix_ac_nielsen()" value="CONVERT URL"/>
			</div>
		</div>

		<br />

		<div class="form-group" id="outputdiv" style="display: none;">
			<p>
				Here is the new URL:
			</p>
			<textarea rows="4" type="text" class="form-control" name="textbox" id="output_textarea"></textarea>
		</div>

	</div>

</div>

<?php
include '../footer.php';
?>