<?php
include ("http://localhost:8888/password_protect.php");
?>
<?php
include '../header.php';
?>
<?php
include '../brandside.php';
?>

<?php include_once("analyticstracking.php") ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="data_am.js"></script>
<script src="max.js?version=1"></script>
<!-- Bootstrap core CSS -->
<link href="bootstrap.min.css" rel="stylesheet">
<!-- CSS -->
<link href="css.css" rel="stylesheet">

<div id="maincontent">
	<div class="container">
		<div class="form-group">
			<h1>Brand URL converter - AdMarvel</h1>
			<h5>This is a tool to replace third party tracking URL macros with the correct AdMarvel ones.</h5>
			<br />The list of implemented tracking link types can be found <a href="https://docs.google.com/spreadsheets/d/1Vz8nlYEFwUR5-n3wbR1x_q9CKOEhVHoA9-A8pHQ7z0Y/edit#gid=1951326556" target="_blank">here</a>.
			<hr>
			<p>
				Paste your URL to be converted below:
			</p>
			<textarea rows="2" type="text" class="form-control" name="textbox" id="textbox"></textarea>
		</div>
		<!--
		<p>
			Select whether your URL should be hard coded (usually for click tracking) and if so whether it is for iOS only or generic (Android and iOS).
		</p>
		-->
		<div class="row">
			<!--
			<div class="col-sm-4">
				<input class="btn btn-lg btn-default btn-block" type="submit" name="button" id="button1" onclick="hardCodedIos()" value="CLICK URL FOR DEC (iOS only)"/>
			</div>
			<div class="col-sm-4">
				<input class="btn btn-lg btn-primary btn-block" type="submit" name="button" id="button2" onclick="hardCodedDrd()" value="CLICK URL FOR DEC"/>
			</div>
			-->
			<div class="col-sm-4">
				<input class="btn btn-lg btn-primary btn-block" type="submit" name="button" id="button3" onclick="fixUrl()" value="CONVERT URL"/>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-12">
				<p id="parameters-text">
					Values that have been changed are highlighted in <span class="text-success">green</span>.
				</p>
				<div id="parameters"></div>
				<div id="error-message"></div>
				<div id="protocol-message"></div>
				<p id="complete-url-text">
					Converted URL to be used:
				</p>
				<div class="well" id="complete-url"></div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$('#complete-url').hide();
		$('#complete-impression-url').hide();
		$('#complete-url-text').hide();
		$('#parameters-text').hide();
	</script>

</div>

<?php
include '../footer.php';
?>