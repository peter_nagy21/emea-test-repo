<?php
include ("http://localhost:8888/password_protect.php");
?>

<?php
include '../header.php';
?>

<?php
include '../sideleft.php';
?>

<?php include_once("analyticstracking.php") ?>

<style>
	p {
		width: 840px;
		word-break: break-all;
	}
</style>

<script type="text/javascript" src="adjust.js"></script>
<link rel="stylesheet" type="text/css" href="adjust.css" />

<div id="maincontent">

	<h1>Adjust</h1>
	
	<h3 class="bold"><a href="https://docs.google.com/document/d/1qhkU3fhmvUJO8GqJ5CZq5Yc_AGxklvwo8udxSePR8ps/" target="_blank" style='color: rgb(147, 27, 28) !important; font-weight: bold !important;'>Adjust Validation instructions</a></h3>

	<h3 class="black plink" id="notestitle">1. Notes <i class="fa fa-arrow-down" id="notesdown"></i><i class="fa fa-arrow-up" id="notesup"></i></h3>
	<div id="notesdiv" class="ndiv">
		<h3>Tag validation</h3>
		<ul>
			<li>
				Click on the tag and make sure that it redirects to the correct app.
			</li>
			<li>
				For Adjust - we just need to build out the url and if an account manager has attempted to create it that they built it correctly.
			</li>
			<li>
				<a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ad-ops/campaign-set-up---by-integration/custom-integrations/adjust-integration-new/adeven-integration" target="_blank">Adjust Integration (old)</a>
			</li>
			<br />
			<li>
				EXAMPLE URL1: https://app.adjust.com/eez5w4
			</li>
			<br />
			<li>
				EXAMPLE URL2: https://app.adjust.com/1tfsk7?s2s=1&idfa=[IDFA]&install_callback=https%3A%2F%2Fcpa.adcolony.com%2Fon_user_action%3Fapi_key%3D7d442b42abea4549de732321531487fe%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D&campaign=[AD_GROUP_NAME]&adgroup=[APP_ID]&creative=[AD_CREATIVE_NAME]&label=sub_publisher%3D[PUBLISHER_ID]%26Sub_site%3D[APP_ID]%26MobileApp%3D[STORE_ID]%26CreativeID%3D[RAW_AD_CREATIVE_ID]%26Partner_id%3D8&tracker_limit=100000
			</li>
		</ul>
		<br />
	</div>

	<h3 id="valtitle" class="black plink">2. Tracking URL generator <i class="fa fa-arrow-down" id="valdown"></i><i class="fa fa-arrow-up" id="valup"></i></h3>
	<div id="valdiv">
		<div id="adjparams">
			<table id="adj_paramtable">
				<tr>
					<th class="firstcol">Parameters</th>
					<th class="secondcol">Macro/Value</th>
					<th class="thirdcol">Notes</th>
				</tr>
				<tr>
					<td>idfa</td>
					<td>[IDFA]</td>
					<td>for iOS</td>
				</tr>
				<tr>
					<td>gps_adid</td>
					<td>[GOOGLE_AD_ID]</td>
					<td>for Android</td>
				</tr>
				<tr>
					<td>android_id_lower_sha1</td>
					<td>[SHA1_ANDROID_ID]</td>
					<td>for Android</td>
				</tr>
				<tr>
					<td>s2s</td>
					<td>1</td>
					<td></td>
				</tr>
				<tr>
					<td>campaign</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>adgroup</td>
					<td>[APP_ID]</td>
					<td></td>
				</tr>
				<tr>
					<td>creative</td>
					<td>[AD_CREATIVE_NAME]</td>
					<td></td>
				</tr>
				<tr>
					<td>label</td>
					<td></td>
					<td>rarely used - requires config in Adjust on client's end</td>
				</tr>
				<tr>
					<td>tracker limit</td>
					<td>#</td>
					<td>only allows the amount of values we pass to be up to # specified</td>
				</tr>
				<tr>
					<td>ip_address</td>
					<td>[IP_ADDRESS]</td>
					<td>added automatically for fingerprinting</td>
				</tr>
				<tr>
					<td>adcolony_click_id</td>
					<td>[CLICK_ID]</td>
					<td>added automatically for fingerprinting</td>
				</tr>
				<tr>
					<td>cost_id</td>
					<td>[CLICK_ID]</td>
					<td>added automatically for cost</td>
				</tr>
			</table>
		</div>

		<div id="adjust_os_switch">
			<h3>Please choose platform first:</h3>
			<div id="adjust_ios_logodiv">
				<img id="adjust_ios_logo" src="IOS-Logo.png" onclick="chooseios()"/>
			</div>
			<div id="adjust_android_logodiv">
				<img id="adjust_android_logo" src="android_logo2.png" onclick="chooseandroid()"/>
			</div>
			<div id="adjust_clear1"></div>
		</div>

		<div id='adjust_ios_gen'>

			<h3><span style="color: #000000"><span style="text-transform: lowercase;">i</span>OS</span> Adjust Click Tag Generator
			<input type="button" id="adjust_ios_back" class="btn-class" value="Back" onclick="iosback()">
			</h3>
			<br />
			<form>
				Adjust URL:
				<input type="text" name="adjust_url" id="adjust_url" size="67">
				<br />
				<br />
				Add Optional Macros:
				<br />
				<br />

				<table class="macrotable">
					<tr>
						<td>
						<input type="checkbox" name="adj_campaign" id="adj_campaign">
						&amp;campaign=</td>
						<td>
						<input type="text" name="adj_campaign_token" id="adj_campaign_token">
						</td>
						<td>
						<input type="button" id="adjust_ios_campname" class="btn-class" value="[AD_GROUP_NAME]" onclick="ioscampname()">
						</td>
					</tr>
					<tr>
						<td>
						<input type="checkbox" name="adj_ad_group" id="adj_ad_group">
						&amp;adgroup=</td>
						<td>
						<input type="text" name="adj_ad_group_token" id="adj_ad_group_token">
						</td>
						<td>
						<input type="button" id="adjust_ios_appid" class="btn-class" value="[APP_ID]" onclick="iosappid()">
						</td>
					</tr>
					<tr>
						<td>
						<input type="checkbox" name="adj_creative" id="adj_creative">
						&amp;creative=</td>
						<td>
						<input type="text" name="adj_creative_token" id="adj_creative_token">
						</td>
						<td>
						<input type="button" id="adjust_ios_adcreativename" class="btn-class" value="[AD_CREATIVE_NAME]" onclick="iosadcreativename()">
						</td>
					</tr>
					<tr>
						<td>
						<input type="checkbox" name="adj_other" id="adj_other">
						other parameters: </td>
						<td colspan="2">
						<input type="text" name="adj_other_token" id="adj_other_token">
						</td>
					</tr>
				</table>
				<br />
				<br />
				<input type="radio" name="iosvt" id="iosvt0" value="No view-through">
				No view-through
				<br>
				<input type="radio" name="iosvt" id="iosvt1" value="1 day">
				1 day view-through
				<br />
				<input type="radio" name="iosvt" id="iosvt7" value="7 days">
				7 days view-through
				<input type="button" class="btn-class" id="ios_pie_toggle" value="Show / Hide PIE parameter list">
				<br />
				<br />

				<div id="adjust_ios_pie">

					<h3>PIE Postbacks</h3>
					<table class="pietable">
						<tr>
							<td>
							<input type="checkbox" name="pie_open" id="pie_open">
							</td>
							<td>Open</td>
							<td>
							<input type="text" name="pie_open_token" id="pie_open_token">
							</td>
							<td>
							<input type="checkbox" name="pie_session" id="pie_session">
							</td>
							<td>Session</td>
							<td>
							<input type="text" name="pie_session_token" id="pie_session_token">
							</td>
							<td>
							<input type="checkbox" name="pie_payment_info_added" id="pie_payment_info_added">
							</td>
							<td>Payment Info Added</td>
							<td>
							<input type="text" name="pie_payment_info_added_token" id="pie_payment_info_added_token">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_transaction" id="pie_transaction">
							</td>
							<td>Transaction</td>
							<td>
							<input type="text" name="pie_transaction_token" id="pie_transaction_token">
							</td>
							<td>
							<input type="checkbox" name="pie_add_to_cart" id="pie_add_to_cart">
							</td>
							<td>Add to Cart</td>
							<td>
							<input type="text" name="pie_add_to_cart_token" id="pie_add_to_cart_token">
							</td>
							<td>
							<input type="checkbox" name="pie_checkout_initiated" id="pie_checkout_initiated">
							</td>
							<td>Checkout Initiated</td>
							<td>
							<input type="text" name="pie_checkout_initiated_token" id="pie_checkout_initiated_token">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_credits_spent" id="pie_credits_spent">
							</td>
							<td>Credits Spent</td>
							<td>
							<input type="text" name="pie_credits_spent_token" id="pie_credits_spent_token">
							</td>
							<td>
							<input type="checkbox" name="pie_add_to_wishlist" id="pie_add_to_wishlist">
							</td>
							<td>Add to Wishlist</td>
							<td>
							<input type="text" name="pie_add_to_wishlist_token" id="pie_add_to_wishlist_token">
							</td>
							<td>
							<input type="checkbox" name="pie_activated" id="pie_activated">
							</td>
							<td>Activated</td>
							<td>
							<input type="text" name="pie_activated_token" id="pie_activated_token">
							</td>
						</tr>

						<tr>
							<td>
							<input type="checkbox" name="pie_achievement_unlocked" id="pie_achievement_unlocked">
							</td>
							<td>Achievement Unlocked</td>
							<td>
							<input type="text" name="pie_achievement_unlocked_token" id="pie_achievement_unlocked_token">
							</td>
							<td>
							<input type="checkbox" name="pie_content_view" id="pie_content_view">
							</td>
							<td>Content View</td>
							<td>
							<input type="text" name="pie_content_view_token" id="pie_content_view_token">
							</td>
							<td>
							<input type="checkbox" name="pie_level_achieved" id="pie_level_achieved">
							</td>
							<td>Level Achieved</td>
							<td>
							<input type="text" name="pie_level_achieved_token" id="pie_level_achieved_token">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_install" id="pie_install">
							</td>
							<td>Install</td>
							<td>
							<input type="text" name="pie_install_token" id="pie_install_token">
							</td>
							<td>
							<input type="checkbox" name="pie_app_rated" id="pie_app_rated">
							</td>
							<td>App Rated</td>
							<td>
							<input type="text" name="pie_app_rated_token" id="pie_app_rated_token">
							</td>
							<td>
							<input type="checkbox" name="pie_invite" id="pie_invite">
							</td>
							<td>Invite</td>
							<td>
							<input type="text" name="pie_invite_token" id="pie_invite_token">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_login" id="pie_login">
							</td>
							<td>Login</td>
							<td>
							<input type="text" name="pie_login_token" id="pie_login_token">
							</td>
							<td>
							<input type="checkbox" name="pie_tutorial_completed" id="pie_tutorial_completed">
							</td>
							<td>Tutorial Completed</td>
							<td>
							<input type="text" name="pie_tutorial_completed_token" id="pie_tutorial_completed_token">
							</td>
							<td>
							<input type="checkbox" name="pie_reservation" id="pie_reservation">
							</td>
							<td>Reservation</td>
							<td>
							<input type="text" name="pie_reservation_token" id="pie_reservation_token">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_social_sharing_event" id="pie_social_sharing_event">
							</td>
							<td>Social Sharing Event</td>
							<td>
							<input type="text" name="pie_social_sharing_event_token" id="pie_social_sharing_event_token">
							</td>
							<td>
							<input type="checkbox" name="pie_search" id="pie_search">
							</td>
							<td>Search</td>
							<td>
							<input type="text" name="pie_search_token" id="pie_search_token">
							</td>
							<td>
							<input type="checkbox" name="pie_registration_completed" id="pie_registration_completed">
							</td>
							<td>Registration Completed</td>
							<td>
							<input type="text" name="pie_registration_completed_token" id="pie_registration_completed_token">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_update" id="pie_update">
							</td>
							<td>Update</td>
							<td>
							<input type="text" name="pie_update_token" id="pie_update_token">
							</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>

					</table>
					<br />
					Custom Events: Currently, publishers are allowed up to 5 custom events and are required to keep track of what each ADCT_CUSTOM_EVENT corresponds to on their end.
					<br />
					<br />
					<table class="pietable">
						<tr>
							<td>
							<input type="checkbox" name="pie_custom1" id="pie_custom1">
							</td>
							<td>Custom Event 1</td>
							<td>
							<input type="text" name="pie_custom1_token" id="pie_custom1_token">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_custom2" id="pie_custom2">
							</td>
							<td>Custom Event 2</td>
							<td>
							<input type="text" name="pie_custom2_token" id="pie_custom2_token">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_custom3" id="pie_custom3">
							</td>
							<td>Custom Event 3</td>
							<td>
							<input type="text" name="pie_custom3_token" id="pie_custom3_token">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_custom4" id="pie_custom4">
							</td>
							<td>Custom Event 4</td>
							<td>
							<input type="text" name="pie_custom4_token" id="pie_custom4_token">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_custom5" id="pie_custom5">
							</td>
							<td>Custom Event 5</td>
							<td>
							<input type="text" name="pie_custom5_token" id="pie_custom5_token">
							</td>
						</tr>
					</table>
					<br />
				</div>

				<!-- Original iOS PIE div

				<div id="adjust_ios_pie">

				PIE Postbacks
				<br />
				<input type="checkbox" name="pie_open" id="pie_open">
				Open
				<input type="text" name="pie_open_token" id="pie_open_token">
				<br />
				<input type="checkbox" name="pie_transaction" id="pie_transaction">
				Transaction
				<input type="text" name="pie_transaction_token" id="pie_transaction_token">
				<br />
				<input type="checkbox" name="pie_credits_spent" id="pie_credits_spent">
				Credits Spent
				<input type="text" name="pie_credits_spent_token" id="pie_credits_spent_token">
				<br />
				<input type="checkbox" name="pie_payment_info_added" id="pie_payment_info_added">
				Payment Info Added
				<input type="text" name="pie_payment_info_added_token" id="pie_payment_info_added_token">
				<br />
				<input type="checkbox" name="pie_achievement_unlocked" id="pie_achievement_unlocked">
				Achievement Unlocked
				<input type="text" name="pie_achievement_unlocked_token" id="pie_achievement_unlocked_token">
				<br />
				<br />

				<input type="checkbox" name="pie_level_achieved" id="pie_level_achieved">
				Level Achieved
				<input type="text" name="pie_level_achieved_token" id="pie_level_achieved_token">
				<br />
				<input type="checkbox" name="pie_app_rated" id="pie_app_rated">
				App Rated
				<input type="text" name="pie_app_rated_token" id="pie_app_rated_token">
				<br />
				<input type="checkbox" name="pie_activated" id="pie_activated">
				Activated
				<input type="text" name="pie_activated_token" id="pie_activated_token">
				<br />
				<input type="checkbox" name="pie_tutorial_completed" id="pie_tutorial_completed">
				Tutorial Completed
				<input type="text" name="pie_tutorial_completed_token" id="pie_tutorial_completed_token">
				<br />
				<input type="checkbox" name="pie_social_sharing_event" id="pie_social_sharing_event">
				Social Sharing Event
				<input type="text" name="pie_social_sharing_event_token" id="pie_social_sharing_event_token">
				<br />
				<br />

				<input type="checkbox" name="pie_registration_completed" id="pie_registration_completed">
				Registration Completed
				<input type="text" name="pie_registration_completed_token" id="pie_registration_completed_token">
				<br />
				<input type="checkbox" name="pie_session" id="pie_session">
				Session
				<input type="text" name="pie_session_token" id="pie_session_token">
				<br />
				<input type="checkbox" name="pie_add_to_cart" id="pie_add_to_cart">
				Add to Cart
				<input type="text" name="pie_add_to_cart_token" id="pie_add_to_cart_token">
				<br />
				<input type="checkbox" name="pie_add_to_wishlist" id="pie_add_to_wishlist">
				Add to Wishlist
				<input type="text" name="pie_add_to_wishlist_token" id="pie_add_to_wishlist_token">
				<br />
				<input type="checkbox" name="pie_checkout_initiated" id="pie_checkout_initiated">
				Checkout Initiated
				<input type="text" name="pie_checkout_initiated_token" id="pie_checkout_initiated_token">
				<br />
				<br />

				<input type="checkbox" name="pie_content_view" id="pie_content_view">
				Content View
				<input type="text" name="pie_content_view_token" id="pie_content_view_token">
				<br />
				<input type="checkbox" name="pie_install" id="pie_install">
				Install
				<input type="text" name="pie_install_token" id="pie_install_token">
				<br />
				<input type="checkbox" name="pie_invite" id="pie_invite">
				Invite
				<input type="text" name="pie_invite_token" id="pie_invite_token">
				<br />
				<input type="checkbox" name="pie_login" id="pie_login">
				Login
				<input type="text" name="pie_login_token" id="pie_login_token">
				<br />
				<input type="checkbox" name="pie_reservation" id="pie_reservation">
				Reservation
				<input type="text" name="pie_reservation_token" id="pie_reservation_token">
				<br />
				<input type="checkbox" name="pie_search" id="pie_search">
				Search
				<input type="text" name="pie_search_token" id="pie_search_token">
				<br />
				<br />
				<input type="checkbox" name="pie_update" id="pie_update">
				Update
				<input type="text" name="pie_update_token" id="pie_update_token">
				<br />
				<br />
				<br />
				Custom Events: Currently, publishers are allowed up to 5 custom events and are required to keep track of what each ADCT_CUSTOM_EVENT corresponds to on their end.
				<br />
				<br />
				<input type="checkbox" name="pie_custom1" id="pie_custom1">
				Custom Event 1
				<input type="text" name="pie_custom1_token" id="pie_custom1_token">
				<br />
				<input type="checkbox" name="pie_custom2" id="pie_custom2">
				Custom Event 2
				<input type="text" name="pie_custom2_token" id="pie_custom2_token">
				<br />
				<input type="checkbox" name="pie_custom3" id="pie_custom3">
				Custom Event 3
				<input type="text" name="pie_custom3_token" id="pie_custom3_token">
				<br />
				<input type="checkbox" name="pie_custom4" id="pie_custom4">
				Custom Event 4
				<input type="text" name="pie_custom4_token" id="pie_custom4_token">
				<br />
				<input type="checkbox" name="pie_custom5" id="pie_custom5">
				Custom Event 5
				<input type="text" name="pie_custom5_token" id="pie_custom5_token">
				<br />
				<br />
				<br />
				</div>
				-->

				<input type="button" class="btn-class" value="Generate" onclick="PostbackGen()">
				<br />
				<br />

			</form>

			<div id="adj_alert_ios"></div>
			<div id="redurldiv2"></div>
			<div id="adj_out_ios">
				Link is good.
				<br />
				<br />
				<div id="adj_html5_ios">
					HTML5:
					<br />
					<p id="adj_out1_ios"></p>
				</div>
				<div id="adj_complete_ios">
					Complete:
					<br />
					<p id="adj_out2_ios"></p>
				</div>
			</div>
		</div>

		<div id="adjust_android_gen">

			<h3><span style="color: #A4C639">Android</span> Adjust Click Tag Generator
			<input type="button" class="btn-class" id="adjust_android_back" value="Back" onclick="andback()">
			</h3>
			<br />
			<form>
				Adjust URL:
				<input type="text" name="adjust_url_a" id="adjust_url_a" size="67">
				<br />
				<br />
				Add Optional Macros:
				<br />
				<br />
				<table class="macrotable">
					<tr>
						<td>
						<input type="checkbox" name="adj_campaign_a" id="adj_campaign_a">
						&amp;campaign=</td>
						<td>
						<input type="text" name="adj_campaign_token_a" id="adj_campaign_token_a">
						</td>
						<td>
						<input type="button" id="adjust_and_campname" class="btn-class" value="[AD_GROUP_NAME]" onclick="andcampname()">
						</td>
					</tr>
					<tr>
						<td>
						<input type="checkbox" name="adj_ad_group_a" id="adj_ad_group_a">
						&amp;adgroup=</td>
						<td>
						<input type="text" name="adj_ad_group_token_a" id="adj_ad_group_token_a">
						</td>
						<td>
						<input type="button" id="adjust_and_appid" class="btn-class" value="[APP_ID]" onclick="andappid()">
						</td>
					</tr>
					<tr>
						<td>
						<input type="checkbox" name="adj_creative_a" id="adj_creative_a">
						&amp;creative=</td>
						<td>
						<input type="text" name="adj_creative_token_a" id="adj_creative_token_a">
						</td>
						<td>
						<input type="button" id="adjust_and_adcreativename" class="btn-class" value="[AD_CREATIVE_NAME]" onclick="andadcreativename()">
						</td>
					</tr>
					<tr>
						<td>
						<input type="checkbox" name="adj_other_a" id="adj_other_a">
						other parameters: </td>
						<td colspan="2">
						<input type="text" name="adj_other_token_a" id="adj_other_token_a">
						</td>
					</tr>
				</table>
				<br />
				<br />
				<input type="radio" name="andvt" id="andvt0" value="No view-through">
				No view-through
				<br>
				<input type="radio" name="andvt" id="andvt1" value="1 day">
				1 day view-through
				<br />
				<input type="radio" name="andvt" id="andvt7" value="7 days">
				7 days view-through
				<input type="button" class="btn-class" id="and_pie_toggle" value="Show / Hide PIE parameter list">
				<br />
				<br />

				<div id="adjust_android_pie">

					<h3>PIE Postbacks</h3>
					<table class="pietable">
						<tr>
							<td>
							<input type="checkbox" name="pie_open_a" id="pie_open_a">
							</td>
							<td>Open</td>
							<td>
							<input type="text" name="pie_open_token_a" id="pie_open_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_session" id="pie_session_a">
							</td>
							<td>Session</td>
							<td>
							<input type="text" name="pie_session_token_a" id="pie_session_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_payment_info_added_a" id="pie_payment_info_added_a">
							</td>
							<td>Payment Info Added</td>
							<td>
							<input type="text" name="pie_payment_info_added_token_a" id="pie_payment_info_added_token_a">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_transaction_a" id="pie_transaction_a">
							</td>
							<td>Transaction</td>
							<td>
							<input type="text" name="pie_transaction_token_a" id="pie_transaction_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_add_to_cart_a" id="pie_add_to_cart_a">
							</td>
							<td>Add to Cart</td>
							<td>
							<input type="text" name="pie_add_to_cart_token_a" id="pie_add_to_cart_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_checkout_initiated_a" id="pie_checkout_initiated_a">
							</td>
							<td>Checkout Initiated</td>
							<td>
							<input type="text" name="pie_checkout_initiated_token_a" id="pie_checkout_initiated_token_a">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_credits_spent_a" id="pie_credits_spent_a">
							</td>
							<td>Credits Spent</td>
							<td>
							<input type="text" name="pie_credits_spent_token_a" id="pie_credits_spent_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_add_to_wishlist_a" id="pie_add_to_wishlist_a">
							</td>
							<td>Add to Wishlist</td>
							<td>
							<input type="text" name="pie_add_to_wishlist_token_a" id="pie_add_to_wishlist_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_activated_a" id="pie_activated_a">
							</td>
							<td>Activated</td>
							<td>
							<input type="text" name="pie_activated_token_a" id="pie_activated_token_a">
							</td>
						</tr>

						<tr>
							<td>
							<input type="checkbox" name="pie_achievement_unlocked_a" id="pie_achievement_unlocked_a">
							</td>
							<td>Achievement Unlocked</td>
							<td>
							<input type="text" name="pie_achievement_unlocked_token_a" id="pie_achievement_unlocked_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_content_view_a" id="pie_content_view_a">
							</td>
							<td>Content View</td>
							<td>
							<input type="text" name="pie_content_view_token_a" id="pie_content_view_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_level_achieved_a" id="pie_level_achieved_a">
							</td>
							<td>Level Achieved</td>
							<td>
							<input type="text" name="pie_level_achieved_token_a" id="pie_level_achieved_token_a">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_install_a" id="pie_install_a">
							</td>
							<td>Install</td>
							<td>
							<input type="text" name="pie_install_token_a" id="pie_install_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_app_rated_a" id="pie_app_rated_a">
							</td>
							<td>App Rated</td>
							<td>
							<input type="text" name="pie_app_rated_token_a" id="pie_app_rated_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_invite_a" id="pie_invite_a">
							</td>
							<td>Invite</td>
							<td>
							<input type="text" name="pie_invite_token_a" id="pie_invite_token_a">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_login_a" id="pie_login_a">
							</td>
							<td>Login</td>
							<td>
							<input type="text" name="pie_login_token_a" id="pie_login_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_tutorial_completed_a" id="pie_tutorial_completed_a">
							</td>
							<td>Tutorial Completed</td>
							<td>
							<input type="text" name="pie_tutorial_completed_token_a" id="pie_tutorial_completed_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_reservation_a" id="pie_reservation_a">
							</td>
							<td>Reservation</td>
							<td>
							<input type="text" name="pie_reservation_token_a" id="pie_reservation_token_a">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_social_sharing_event_a" id="pie_social_sharing_event_a">
							</td>
							<td>Social Sharing Event</td>
							<td>
							<input type="text" name="pie_social_sharing_event_token_a" id="pie_social_sharing_event_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_search_a" id="pie_search_a">
							</td>
							<td>Search</td>
							<td>
							<input type="text" name="pie_search_token_a" id="pie_search_token_a">
							</td>
							<td>
							<input type="checkbox" name="pie_registration_completed_a" id="pie_registration_completed_a">
							</td>
							<td>Registration Completed</td>
							<td>
							<input type="text" name="pie_registration_completed_token_a" id="pie_registration_completed_token_a">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_update_a" id="pie_update_a">
							</td>
							<td>Update</td>
							<td>
							<input type="text" name="pie_update_token_a" id="pie_update_token_a">
							</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>

					</table>
					<br />
					Custom Events: Currently, publishers are allowed up to 5 custom events and are required to keep track of what each ADCT_CUSTOM_EVENT corresponds to on their end.
					<br />
					<br />
					<table class="pietable">
						<tr>
							<td>
							<input type="checkbox" name="pie_custom1_a" id="pie_custom1_a">
							</td>
							<td>Custom Event 1</td>
							<td>
							<input type="text" name="pie_custom1_token_a" id="pie_custom1_token_a">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_custom2_a" id="pie_custom2_a">
							</td>
							<td>Custom Event 2</td>
							<td>
							<input type="text" name="pie_custom2_token_a" id="pie_custom2_token_a">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_custom3_a" id="pie_custom3_a">
							</td>
							<td>Custom Event 3</td>
							<td>
							<input type="text" name="pie_custom3_token_a" id="pie_custom3_token_a">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_custom4_a" id="pie_custom4_a">
							</td>
							<td>Custom Event 4</td>
							<td>
							<input type="text" name="pie_custom4_token_a" id="pie_custom4_token_a">
							</td>
						</tr>
						<tr>
							<td>
							<input type="checkbox" name="pie_custom5_a" id="pie_custom5_a">
							</td>
							<td>Custom Event 5</td>
							<td>
							<input type="text" name="pie_custom5_token_a" id="pie_custom5_token_a">
							</td>
						</tr>
					</table>
					<br />
				</div>

				<input type="button" class="btn-class" value="Generate" onclick="PostbackGenAnd()">
				<br />
				<br />

			</form>
			</form>
			<div id="adj_alert"></div>
			<div id="redurldiv1"></div>
			<div id="adj_out">
				Link is good.
				<br />
				<br />
				<div id="adj_html5">
					HTML5:
					<br />
					<p id="adj_out1"></p>
				</div>
				<div id="adj_complete">
					Complete:
					<br />
					<p id="adj_out2"></p>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<br />
	</div>
	
	<h3 class="black plink" id="vtltitle">3. Generate View Through links <i class="fa fa-arrow-down" id="vtldown"></i><i class="fa fa-arrow-up" id="vtlup"></i></h3>
	<div id="vtldiv" class="ndiv">

		<textarea id="adjvt_1" style="width: 880px; height: 80px;"></textarea>
		<br />
			<input type="radio" name="adjvtr" id="adjvtr1" value="1 day" checked="checked">
			1 day view-through
			<br />
			<input type="radio" name="adjvtr" id="adjvtr7" value="7 days">
			7 days view-through
			<br />
			<br />
			<input type="button" class="btn-class" value="Generate links" onclick="AdjVT()">
			<br />
			<br />
			<div id="adjvt_mess"></div>
		<div id="adjvt_2">
			
		</div>
	</div>
	
	<h3 class="black plink" id="pie_session_title">4. Session PIE callback parameter <i class="fa fa-arrow-down" id="pie_session_down"></i><i class="fa fa-arrow-up" id="pie_session_up"></i></h3>
	<div id="pie_session_div" class="ndiv">
		iOS session PIE:
		<br />
		<textarea id="pie_session_ta1" style="width: 880px; height: 90px;">&session_callback=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fsession%3Fapi_key%3D7d442b42abea4549de732321531487fe%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D%26session_length%3D%7Btime_spent%7D</textarea>
		<br />
		<br />
		Android session PIE:
		<br />
		<textarea id="pie_session_ta2" style="width: 880px; height: 90px;">&session_callback=https%3A%2F%2Fpie.adcolony.com%2Fapi%2Fv1%2Fsession%3Fapi_key%3D7d442b42abea4549de732321531487fe%26product_id%3D[PRODUCT_ID]%26google_ad_id%3D%7Bgps_adid%7D%26app_version%3D%7Bapp_version%7D%26device_ip%3D%7Bip_address%7D%26device_model%3D%7Bdevice_name%7D%26language%3D%7Blanguage%7D%26os_name%3D%7Bos_name%7D%26os_version%3D%7Bsdk_version%7D%26action_time%3D%7Bcreated_at%7D%26user_agent%3D%7Buser_agent%7D%26session_id%3D%7Brandom_user_id%7D%26session_length%3D%7Btime_spent%7D</textarea>
		<br />
		<br />
	</div>
	
	<h3 class="black plink" id="pbtitle">5. Postback - check the 3rd party Dashboard <i class="fa fa-arrow-down" id="pbdown"></i><i class="fa fa-arrow-up" id="pbup"></i></h3>
	<div id="pbdiv" class="ndiv">

		The postback URL is included in the tracking link. (We don't have access to the Adjust dashboard.)
		<br />
		<br />
	</div>
	<h3 class="black plink" id="vttitle">6. View Through Attribution Window <i class="fa fa-arrow-down" id="vtdown"></i><i class="fa fa-arrow-up" id="vtup"></i></h3>
	<div id="vtdiv">
		<table id="vt_table">
			<tr>
				<th colspan="2">VIEW ATTRIBUTION</th>
				<th colspan="3">LOOKBACK WINDOWS</th>
				<th>CLIENT-SIDE ACTION</th>
			</tr>
			<tr class="tep" id="tpid">
				<th>Dedicated View Tags</th>
				<th>Recommended Implementation</th>
				<th style="min-width: 120px;">Flexible Lookback Window</th>
				<th>Default Click Lookback</th>
				<th>Default View Lookback</th>
				<th></th>
			</tr>
			<tr>
				<td>No</td>
				<td class="lefta">Implement click tag on the Complete only (nothing in HTML5).</td>
				<td>UNCHANGEABLE</td>
				<td>7 days</td>
				<td>7 days (matched to click window)</td>
				<td class="lefta">No action necessary - we implement on our end.</td>
			</tr>
			<tr>
				<td>Yes</td>
				<td class="lefta">Implement impression tag on complete.</td>
				<td>UNCHANGEABLE</td>
				<td>7 days</td>
				<td>1 day</td>
				<td class="lefta">Enable view through tracking in their integration with Adjust.<br />Can be enabled at App level.</td>
			</tr>
		</table>
		<br />
	</div>
	<h3 class="black plink" id="pietitle">7. PIE <i class="fa fa-arrow-down" id="piedown"></i><i class="fa fa-arrow-up" id="pieup"></i></h3>
	<div id="piediv">
		We implement PIE in the tracking URLs. The client should send AdColony a list of event tokens and let us know which token maps to which event.
		More info about the client setup can be found here:
		<a class="blue" href="http://support.adcolony.com/customer/portal/articles/1914521-postbacks-for-top-tracking-partners#Adjust" target="_blank">Adjust PIE setup</a>
	</div>

	<div class="clear"></div>

</div>

<?php
include '../footer.php';
?>

