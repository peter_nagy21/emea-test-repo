// URL comparison tool

url_to_array = function(input_url) {
	// example: url = http://www.example.com?a=1&b=2&c=3
	// var url = document.getElementById(input_url).value;
	// var url = input_url;
	var sub1 = input_url.split("?");
	var base_url = sub1[0];
	var params_str = sub1[1];
	var params1 = params_str.split("&");
	var pl = params1.length;
	var params2 = [];
	params2[0] = [base_url, ""];
	for (var i = 1; i <= pl; i++) {
		params2[i] = params1[i - 1].split("=");
	}
	var output_array = params2;
	return output_array;
}
url_to_array_sort = function(input_url) {
	// example: url = http://www.example.com?a=1&b=2&c=3
	// var url = document.getElementById(input_url).value;
	// var url = input_url;
	var sub1 = input_url.split("?");
	var base_url = sub1[0];
	var params_str = sub1[1];
	var params1 = params_str.split("&");
	params1.sort();
	var pl = params1.length;
	var params2 = [];
	params2[0] = [base_url, ""];
	for (var i = 1; i <= pl; i++) {
		params2[i] = params1[i - 1].split("=");
	}
	var output_array = params2;
	return output_array;
}
first_elements = function(arr) {
	var l = arr.length;
	var out_arr = [];
	for (var i = 0; i < l; i++) {
		out_arr[i] = arr [i][0];
	}
	return out_arr;
}
// this will return true if everything is fine (no duplications)
check_duplications = function(arr) {
	if (arr) {
		var elem = "";
		var l = arr.length;
		var dup = false;
		for (var i = 0; i < l; i++) {
			elem = arr[i];
			for (var j = i + 1; j <= l; j++) {
				if (elem === arr[j]) {
					console.log("Duplication!");
					dup = true;
				}
			}
		}
		if (dup === false) {
			//console.log("All values are unique!");
		}
	}
	var nodup = !dup;
	return nodup;
}

compare_arrays = function(arr1, arr2) {

	var l1 = arr1.length;
	var l2 = arr2.length;

	for (var i = 0; i < l1; i++) {
		arr1[i].push(1);
	}

	for (var j = 0; j < l2; j++) {
		arr2[j].push(1);
	}

	for (var k = 0; k < l1; k++) {
		for (var l = 0; l < l2; l++) {
			if (arr1[k][0] === arr2[l][0]) {
				if (arr1[k][1] !== arr2[l][1]) {
					arr1[k][2] = 2;
					arr2[l][2] = 2;
				} else {
					arr1[k][2] = 0;
					arr2[l][2] = 0;
				}
			}
		}
	}
	
	var new_arrays = [];
	new_arrays[0] = arr1;
	new_arrays[1] = arr2;	
	return new_arrays;
}
display_arrays = function(new_arrays, output_div, output_div2) {
	var arr1 = new_arrays[0];
	var arr2 = new_arrays[1];

	var output_text = "";
	var output_text2 = "";
	
	//check if the arrays are the same (all 3.values should be 0)
	
	var sal = arr1.length;
	var sal2 = arr2.length;
	
	var same = true;
	
	for (var s = 0; s < sal; s++) {
		if (arr1[s][2] !== 0) {
			same = false;
		}
	}
	
	for (var sz = 0; sz < sal2; sz++) {
		if (arr2[sz][2] !== 0) {
			same = false;
		}
	}
	
	console.log(same);
	
	if (same) {
		$("#samealert").html("The URLs are the same! <i class='em em-blush'></i>");
		$("#urlsplitdiv, #urlsplitdiv2").css("color","green");
		$("#urlsplitdiv td, #urlsplitdiv2 td").css("background-color","#DFD");
		$("#samediv").show();
	} else {
		$("#samealert").html("");
		$("#urlsplitdiv, #urlsplitdiv2").css("color","#777");
		$("#urlsplitdiv td, #urlsplitdiv2 td").css("background-color","#FFF");
	}

	//div1
	output_text += "<div id='urlsplitdiv_new1' class='geturl1_visible'><span style='";
	if (arr1[0][2] === 1) {
		output_text += "color: #D00; font-weight: bold;";
	}
	output_text += "'><span class='base_url' style='font-weight: bold;'>Base URL: </span>";
	output_text += arr1[0][0];
	output_text += "</span><br /><table style='margin-top: 5px;'><tbody>";
	output_text += "<tr><td class='tabletitle'>Parameters</td><td class='tabletitle'>Macros/values</td>";

	var l1 = arr1.length;

	for (var i = 1; i < l1; i++) {
		var st = "";
		if (arr1[i][2] === 0) {
			st = "";
		}
		if (arr1[i][2] === 1) {
			st = " style='color: #D00; background-color: #FFADB1;'";
		}
		if (arr1[i][2] === 2) {
			st = " style='color: #C90; background-color: #FFE9AD;'";
		}
		if (same) {
			st = " style='color: green; background-color: #DFD;'";
		}
		output_text += "<tr><td" + st + ">" + arr1[i][0] + "</td><td" + st + ">" + arr1[i][1] + "</td>";
	}

	output_text += "</tbody></table><br /></div>";

	//div2
	output_text2 += "<div id='urlsplitdiv_new2' class='geturl2_visible'><span style='";
	if (arr2[0][2] === 1) {
		output_text2 += "color: #D00; font-weight: bold;";
	}
	output_text2 += "'><span class='base_url' style='font-weight: bold;'>Base URL: </span>";
	output_text2 += arr2[0][0];
	output_text2 += "</span><br /><table style='margin-top: 5px;'><tbody>";
	output_text2 += "<tr><td class='tabletitle'>Parameters</td><td class='tabletitle'>Macros/values</td>";

	var l2 = arr2.length;

	for (var j = 1; j < l2; j++) {
		var st = "";
		if (arr2[j][2] === 0) {
			st = "";
		}
		if (arr2[j][2] === 1) {
			st = " style='color: #D00; background-color: #FFADB1;'";
		}
		if (arr2[j][2] === 2) {
			st = " style='color: #C90; background-color: #FFE9AD;'";
		}
		if (same) {
			st = " style='color: green; background-color: #DFD;'";
		}
		output_text2 += "<tr><td" + st + ">" + arr2[j][0] + "</td><td" + st + ">" + arr2[j][1] + "</td>";
	}

	output_text2 += "</tbody></table><br /></div>";
	output_text2 += "<div style='clear: both;'></div>";

	//console.log(output_text);

	$(output_div).html(output_text);
	$(output_div2).html(output_text2);
}
/* some tests
 var testarr1 = url_to_array("http://www.example.com?a=1&b=2&c=3");
 var testarrfirst1 = first_elements(testarr1);
 check_duplications(testarrfirst1);
 var testarr2 = url_to_array("http://www.example.com?c=11&d=22&e=33&a=1");
 var testarrfirst2 = first_elements(testarr2);
 check_duplications(testarrfirst2);
 var newarr = compare_arrays(testarr1,testarr2);
 console.log(newarr);
 display_arrays(newarr,output_tables);
 */

compareURLs = function() {
	var url1 = document.getElementById("comp_url1").value;
	url1 = url1.replace(/\s+/g, '');
	var url2 = document.getElementById("comp_url2").value;
	url2 = url2.replace(/\s+/g, '');
	var arr1 = url_to_array(url1);
	var arr2 = url_to_array(url2);
	var arrfirst1 = first_elements(arr1);
	check_duplications(arrfirst1);
	var arrfirst2 = first_elements(arr2);
	check_duplications(arrfirst2);
	var newarr = compare_arrays(arr1, arr2);
	display_arrays(newarr, output_tables1, output_tables2);
	$("#keydiv").show();
}
compareURLs1 = function() {
	var url1 = document.getElementById("input_url").value;
	url1 = url1.replace(/\s+/g, '');
	var url2 = document.getElementById("input_url2").value;
	url2 = url2.replace(/\s+/g, '');
	var arr1 = url_to_array(url1);
	var arr2 = url_to_array(url2);
	var arrfirst1 = first_elements(arr1);
	check_duplications(arrfirst1);
	var arrfirst2 = first_elements(arr2);
	check_duplications(arrfirst2);
	var newarr = compare_arrays(arr1, arr2);
	display_arrays(newarr, urlsplitdiv, urlsplitdiv2);
	$("#keydiv").show();
}
compareURLs2 = function() {
	var url1 = document.getElementById("input_url").value;
	url1 = url1.replace(/\s+/g, '');
	var url2 = document.getElementById("input_url2").value;
	url2 = url2.replace(/\s+/g, '');
	var arr1 = url_to_array_sort(url1);
	var arr2 = url_to_array_sort(url2);
	var arrfirst1 = first_elements(arr1);
	check_duplications(arrfirst1);
	var arrfirst2 = first_elements(arr2);
	check_duplications(arrfirst2);
	var newarr = compare_arrays(arr1, arr2);
	display_arrays(newarr, urlsplitdiv, urlsplitdiv2);
	$("#keydiv").show();
}