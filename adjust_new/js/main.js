$(document).ready(function() {

	divToggle(false, notestitle, notesdiv, notesdown, notesup);

});

function validate(os) {
	// clear the screen
	if (document.getElementById("pie-table")) {
		document.getElementById('pie').innerHTML = ""
	}
	document.getElementById('complete-url').innerHTML = "";
	// grab the url from the text box
	var url = document.getElementById('url').value
	var parameters = [];

	if (!url.split('?')[1]) {
		// no parameters
		if (url.split('/')[3].indexOf('?') > -1) {
			url = url.split('?')[0].split('/')[3];
		} else {
			url = url.split('/')[3];
		}
		parameters.push({
			name : 'campaign',
			value : ''
		});
		parameters.push({
			name : 'adgroup',
			value : ''
		});
		parameters.push({
			name : 'creative',
			value : ''
		});
	} else {
		// parameters exist
		parameters = getParameters(url);
		checkForPie(url);
		parameters = parameters.filter(function(index) {
			return index !== undefined
		});
		url = url.split('?')[0].split('/')[3];
		parameters = checkParameters(parameters);
	}

	// push through standard parameters
	parameters.push({
		name : 'ip_address',
		value : '[IP_ADDRESS]'
	});
	parameters.push({
		name : 'adcolony_click_id',
		value : '[CLICK_ID]'
	});
	parameters.push({
		name : 'cost_id',
		value : '[CLICK_ID]'
	});
	parameters.push({
		name : 's2s',
		value : '1'
	});

	// os specific parameters
	if (os.innerText == 'Android') {
		parameters.push({
			name : 'android_id_lower_sha1',
			value : '[SHA1_ANDROID_ID]'
		});
		parameters.push({
			name : 'gps_adid',
			value : '[GOOGLE_AD_ID]'
		});
		parameters.push({
			name : 'install_callback',
			value : 'https%3A%2F%2Fcpa.adcolony.com%2Fon_user_action%3Fapi_key%3D7d442b42abea4549de732321531487fe%26product_id%3D[PRODUCT_ID]%26raw_android_id%3D%7Bandroid_id%7D%26google_ad_id%3D%7Bgps_adid%7D'
		});
	} else {
		parameters.push({
			name : 'idfa',
			value : '[IDFA]'
		});
		parameters.push({
			name : 'install_callback',
			value : 'https%3A%2F%2Fcpa.adcolony.com%2Fon_user_action%3Fapi_key%3D7d442b42abea4549de732321531487fe%26product_id%3D[PRODUCT_ID]%26raw_advertising_id%3D%7Bidfa%7D'
		});
	}

	// create table from the url
	createTable(parameters, url, os);
}

var input_ca1 = "";
var input_a1 = "";
var input_cr1 = "";

function createTable(parameters, url, os) {
	// create table for the base url
	var html = '<table class="table table-bordered">';
	html += '<tr>';
	html += '<td>https://app.adjust.com/</td>';
	html += '<td><input type="text" class="form-control" oninput="checkRedirect()" id="base-url-value" value=' + url + '></td>';
	html += '<td><a id="redirect" target="_blank" href="https://app.adjust.com/' + url + '">Click here to check the redirect!</a></td>';
	html += '</tr>';
	html += '</table>';
	$('#base-url').html(html);

	// create table containing the parameters
	html = '<table class="table table-bordered">';
	for (var i = 0; i < parameters.length; i++) {
		html += '<tr>';
		html += '<td class="parameter-name">' + parameters[i].name + '</td>';
		// if campaign, adgroup or creative make them editable

		if (parameters[i].name == 'campaign') {

			input_ca1 = parameters[i].value;

			html += '<td><input list="datalist1" type="text" id="input_ca" class="form-control parameter-value" value=' + parameters[i].value + '></td><td><input type="button" id="button_ca" value ="[AD_GROUP_NAME]" '
			if (input_ca1 === "[AD_GROUP_NAME]") {
				html += 'class="pressed_button"';
			} else {
				html += 'onclick="cm_ca()"; ';
			}
			html += '/></td>';

		} else if (parameters[i].name == 'adgroup') {

			input_a1 = parameters[i].value;

			html += '<td><input list="datalist1" type="text" id="input_a" class="form-control parameter-value" value=' + parameters[i].value + '></td><td><input type="button" id="button_a" value ="[APP_ID]" ';
			if (input_a1 === "[APP_ID]") {
				html += 'class="pressed_button"';
			} else {
				html += 'onclick="cm_a()"; ';
			}
			html += '/></td>';

		} else if (parameters[i].name == 'creative') {

			input_cr1 = parameters[i].value;

			html += '<td><input list="datalist1" type="text"id="input_cr" class="form-control parameter-value" value=' + parameters[i].value + '></td><td><input type="button" id="button_cr" value ="[AD_CREATIVE_NAME]" ';
			if (input_cr1 === "[AD_CREATIVE_NAME]") {
				html += 'class="pressed_button"';
			} else {
				html += 'onclick="cm_cr()"; ';
			}
			html += '/></td>';

		} else if (parameters[i].name == 'label') {
			html += '<td colspan="2"><input list="datalist1" type="text" class="form-control parameter-value" value=' + parameters[i].value + '></td>';
		} else {
			html += '<td colspan="2"><input readonly type="text" class="form-control parameter-value" value=' + parameters[i].value + ' list="datalist1" ></td>';
		}
		html += '</tr>';
	}
	html += '</table>';
	$('#table').html(html);

	// add pie button
	html = '<button type="button" onclick="addPie()" class="btn btn-default">Add PIE</button>';
	$('#add-pie').html(html);

	// add generate url buttons with VT
	html = '<button type="button" onclick="generateUrl(&quot;' + os.innerText + '&quot;, this)" class="btn btn-default">No VT</button><button type="button" onclick="generateUrl(&quot;' + os.innerText + '&quot;, this)" class="btn btn-default">1 day VT</button><button type="button" onclick="generateUrl(&quot;' + os.innerText + '&quot;, this)" class="btn btn-default">7 day VT</button>';
	$('#os').html(html);
}

// add remove  default macros

var pressed_ca = false;
var pressed_a = false;
var pressed_cr = false;

function cm_ca() {
	change_macro("button_ca", pressed_ca, "input_ca", "[AD_GROUP_NAME]");
}
function cm_a() {
	change_macro("button_a", pressed_a, "input_a", "[APP_ID]");
}
function cm_cr() {
	change_macro("button_cr", pressed_cr, "input_cr", "[AD_CREATIVE_NAME]");
}

function change_macro(button_id, button_pressed, input_id, value) {

	// grab input_value
	var input_id_t = "#" + input_id;
	var button_id_t = "#" + button_id;

	var pressed_var;

	var btn_pr = button_pressed;
	console.log(btn_pr);

	switch (input_id) {
	case "input_ca":
		input_old = input_ca1;
		break;
	case "input_a":
		input_old = input_a1;
		break;
	case "input_cr":
		input_old = input_cr1;
		break;
	}

	if (!btn_pr) {
		// change color
		$(button_id_t).addClass("pressed_button");
		// put new value
		$(input_id_t).val(value);

		pressed_var = true;
	} else {
		// change color back
		$(button_id_t).removeClass("pressed_button");
		// put input value back
		$(input_id_t).val(input_old);

		console.log(input_ca1);

		pressed_var = false;
	}

	switch (input_id) {
	case "input_ca":
		pressed_ca = pressed_var;
		break;
	case "input_a":
		pressed_a = pressed_var;
		break;
	case "input_cr":
		pressed_cr = pressed_var;
		break;
	}
}

// END OF add remove  default macros

function checkRedirect() {
	document.getElementById('redirect').href = 'https://app.adjust.com/' + document.getElementById('base-url-value').value;
}

function getParameters(url) {
	// we are only looking to take campaign, adgroup, creative and tracker_limit parameter names and values here as the rest do not change
	var parameters = url.split('?')[1].split('&');
	// custom parameters
	customParameters = ['campaign', 'adgroup', 'creative', 'tracker_limit', 'label', 'device_type', 'deeplink', 'cost_type', 'cost_amount', 'cost_currency']
	return parameters.map(function(index) {
		// taking campaign, adgroup, creative and tracker_limit macros
		if (customParameters.indexOf(index.split('=')[0]) > -1) {
			return {
				name : index.split('=')[0],
				value : index.split('=')[1]
			};
		} else {
			return;
		}
	});
}

function checkParameters(parameters) {
	// if campaign, adgroup or creative do not exist, then push them into the parameter array with empty values
	var campaign = false;
	var adgroup = false;
	var creative = false;
	parameters.forEach(function(index) {
		if (index.name == 'campaign')
			campaign = true;
		if (index.name == 'adgroup')
			adgroup = true;
		if (index.name == 'creative')
			creative = true;
	})
	if (!campaign)
		parameters.push({
			name : 'campaign',
			value : ''
		});
	if (!adgroup)
		parameters.push({
			name : 'adgroup',
			value : ''
		});
	if (!creative)
		parameters.push({
			name : 'creative',
			value : ''
		});
	return parameters;
}

function checkForPie(url) {
	// loop through and check if pie parameter is there
	var parameters = url.split('?')[1].split('&');
	pieFromUrl = parameters.map(function(index) {
		if (index.split('=')[0].indexOf('event_callback') !== -1) {
			if (index.split('ADCT_CUSTOM_EVENT_')[1]) {
				pieValue = "Custom Event " + index.split('ADCT_CUSTOM_EVENT_')[1].split('%')[0];
			} else {
				pieValue = decodeURIComponent(index.split('=')[1]).split('/')[5].split('?')[0];
				pieValue = pieValue.replace("_", " ");
				pieValue = pieValue.replace(/\w\S*/g, function(txt) {
					return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
				});
			}
			return {
				name : index.split('=')[0],
				value : pieValue
			};
		} else {
			return;
		}
	});
	pieFromUrl = pieFromUrl.filter(function(index) {
		return index !== undefined
	});

	if (pieFromUrl[0]) {
		createPieTable(pieFromUrl);
	} else {
		return;
	}
}

function createPieTable(pieFromUrl) {
	// create table headings
	var html = '<table id="pie-table" class="table table-bordered">';
	html += '<tr>';
	html += '<th>PIE name</th>';
	html += '<th>PIE value</th>';
	html += '<th>Delete PIE</th>';
	html += '</tr>';
	// create a pie table from url containing pie
	if (pieFromUrl[0]) {
		for (var i = 0; i < pieFromUrl.length; i++) {
			html += '<tr>';
			html += '<td><select id="pie-event-name" class="form-control pie-event-name"><option>Choose PIE</option></select></td>';
			html += '<td><input type="text" class="form-control pie-value" value="' + pieFromUrl[i].name.split('_')[2] + '"></td>';
			html += '<td><button type="button" onclick="deleteRow(this)" class="btn btn-danger btn-block"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>';
			html += '</tr>';
		}
		// no pie in the url so create a new table
	} else {
		html += '<tr>';
		html += '<td><select id="pie-event-name" class="form-control pie-event-name"><option>Choose PIE</option></select></td>';
		html += '<td><input type="text" class="form-control pie-value" placeholder="PIE value"></td>';
		html += '<td><button type="button" onclick="deleteRow(this)" class="btn btn-danger btn-block"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>';
		html += '</tr>';
	}

	html += '</table>';
	$('#pie').html(html);

	// options list
	var options = ['Open', 'Session', 'Payment Info Added', 'Transaction', 'Add to Cart', 'Checkout Initiated', 'Credits Spent', 'Add to Wishlist', 'Activated', 'Achievement Unlocked', 'Content View', 'Level Achieved', 'Install', 'App Rated', 'Invite', 'Login', 'Tutorial Completed', 'Reservation', 'Social Sharing Event', 'Search', 'Custom Event 1', 'Custom Event 2', 'Custom Event 3', 'Custom Event 4', 'Custom Event 5', 'Registration Completed', 'Update'];

	options.sort();

	// if a url contains pie generate options list and then select the relevant pie name
	if (pieFromUrl[0]) {
		var select = document.getElementsByClassName('pie-event-name');
		for (var j = 0; j < pieFromUrl.length; j++) {
			for (var i = 0; i < options.length; i++) {
				var opt = options[i];
				var el = document.createElement('option');
				el.textContent = opt;
				if (opt == pieFromUrl[j].value) {
					el.selected = true;
				}
				el.value = opt;
				select[j].appendChild(el);
			}
		}
	} else {
		var select = document.getElementById('pie-event-name');
		for (var i = 0; i < options.length; i++) {
			var opt = options[i];
			var el = document.createElement('option');
			el.textContent = opt;
			el.value = opt;
			select.appendChild(el);
		}
	}
}

function addPie() {
	if (document.getElementById('pie-table')) {
		var x = document.getElementById('pie-table');
		var len = x.rows.length;
		var new_row = x.rows[len - 1].cloneNode(true);
		console.log(new_row.getElementsByTagName('td')[1].firstChild.value)
		new_row.getElementsByTagName('td')[1].firstChild.value = '';
		console.log(new_row)
		x.firstElementChild.appendChild(new_row)

	} else {
		pieFromUrl = '';
		createPieTable(pieFromUrl);
	}
}

function deleteRow(btn) {
	if (document.getElementById('pie-table').rows.length == 2) {
		document.getElementById('pie').innerHTML = "";
	} else {
		var row = btn.parentNode.parentNode;
		row.parentNode.removeChild(row);
	}
}

function generateUrl(os, viewThrough) {
	// grab pie event names and values
	var x = document.getElementsByClassName('pie-event-name');
	var y = document.getElementsByClassName('pie-value');
	var pieToFetch = [];
	var pieValues = [];

	for ( i = 0; i < x.length; i++) {
		pieToFetch.push({
			name : x[i].value.toLowerCase().split(' ').join('_'),
			value : y[i].value
		});
		pieValues.push(y[i].value);
	}

	var pieForUrl = [];
	if (os == 'Android') {
		var standardParameters = pie.androidParameters;
	} else {
		var standardParameters = pie.iosParameters;
	}

	pieToFetch.forEach(function(pieEvent) {
		pie.eventType.forEach(function(index) {
			console.log(pieEvent.value);
			if (pieEvent.value !== '') {
				if (pieEvent.name == index.name) {
					if (index.extraParameters) {
						if (index.name.includes('custom_event_')) {
							pieForUrl.push({
								url : pie.baseUrl + 'custom_event' + standardParameters + index.extraParameters,
								value : pieEvent.value
							});
						} else {
							pieForUrl.push({
								url : pie.baseUrl + pieEvent.name + standardParameters + index.extraParameters,
								value : pieEvent.value
							});
						}
					} else {
						pieForUrl.push({
							url : pie.baseUrl + pieEvent.name + standardParameters,
							value : pieEvent.value
						});
					}
				}
			}
		});
	});

	pieForUrl = pieForUrl.map(function(index) {
		console.log(index);
		var url = index.url;
		url = url.split('[PRODUCT_ID]');
		console.log(index);
		return index = {
			name : 'event_callback_' + index.value,
			value : encodeURIComponent(url[0]) + '[PRODUCT_ID]' + encodeURIComponent(url[1])
		};
	});

	completeUrl = 'https://app.adjust.com/' + document.getElementById('base-url-value').value + '?';
	impressionUrl = 'https://view.adjust.com/impression/' + document.getElementById('base-url-value').value + '?';
	parameterValues = document.getElementsByClassName('parameter-value');
	parameterNames = document.getElementsByClassName('parameter-name');
	parameters = [];

	for ( i = 0; i < parameterValues.length; i++) {
		if (parameterNames[i].innerText == 'campaign' && parameterValues[i].value == '' || parameterNames[i].innerText == 'adgroup' && parameterValues[i].value == '' || parameterNames[i].innerText == 'creative' && parameterValues[i].value == '') {

		} else {
			parameters.push({
				name : parameterNames[i].innerText,
				value : parameterValues[i].value
			});
		}
	}

	// impression parameters are the same apart from s2s so remove the s2s parameter for the impression url
	impressionParameters = parameters.filter(function(index) {
		return index.name !== "s2s";
	});

	pieForUrl.forEach(function(index) {
		parameters.push(index)
	});
	pieForUrl.forEach(function(index) {
		impressionParameters.push(index)
	});
	parameters.forEach(function(index) {
		completeUrl = completeUrl + index.name + '=' + index.value + '&';
	});

	impressionParameters.forEach(function(index) {
		impressionUrl = impressionUrl + index.name + '=' + index.value + '&';
	});

	completeUrl = completeUrl.slice(0, -1);
	impressionUrl = impressionUrl.slice(0, -1);

	var textout = "Let's use this link:\n\n";
	var hta = "240px";
	if (viewThrough.innerText == 'No VT') {
		textout += 'HTML5:\n' + completeUrl + '\n\nThanks';
		hta = "240px";
	} else if (viewThrough.innerText == '1 day VT') {
		textout = "Let's use these links:\n\nHTML5:\n" + completeUrl + "\n\nVideo starts:\n" + impressionUrl + "\n\nThanks";
		hta = "400px";
	} else {
		textout += 'Video starts:\n' + completeUrl + '\n\nThanks';
		hta = "240px";

	}
	$('#complete-url').html('<textarea id="complete-url-txta" class="form-control"></textarea>');
	document.getElementById("complete-url-txta").value = textout;
	$("#complete-url-txta").css("height", hta);
}