function fix_ac_nielsen() {
	
	var output = "";
	var url = $("#textbox").val();
	
	
	// replace spaces
	url = url.replace(/\s+/g, '');
	
	
	var has_ts = url.indexOf("[timestamp]") >= 0;
	
	var new_string = "[ADCOLONY_TIMESTAMP_MILLIS]&c8=devgrp%2C[DEVICE_GROUP]&c9=devid%2C[IDFA]&c10=plt%2CMBL&c13=asid%2CPF8FEA3C9-CC42-40C4-E040-070AAD3115EC";
	
	
	if (has_ts) {
		url = url.replace("[timestamp]",new_string);
		output = url;
	}
	
	else {
		output = "It looks like the '[timestamp]' is not included in the URL.";
	}
	
	$("#output_textarea").val(output);
	$("#outputdiv").css("display","block");
}
