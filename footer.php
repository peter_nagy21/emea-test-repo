<!-- footer -->

<div id="footer" class="container">
	<h2></h2>
	<div class="floatClear">
		<div class="box">

			<span class="line"> </span>
		</div>
		<div class="boxGIT">
			<div class="info floatClear">
				Content: Jessi Kizer, Junggu Kwon, Max Elston, Armand Edwards and Peter Nagy
				<br /><br />
				Please contact Peter (<a href="mailto:peter.nagy@adcolony.com" target="_top">peter.nagy@adcolony.com</a>) if you have any questions regarding this website.
			</div>
		</div>
	</div>
</div>

<div id="copyright" class="container">
	Peter Nagy 2016 - 2018
</div>
<!-- /footer -->

</div>

</body>
</html>
