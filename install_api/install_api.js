build_url = function() {

	var api_key = $("#api_key").val();

	// we need to conver the date values
	var start_date = $("#start_date").val();
	var start_date1 = date_convert(start_date);

	var end_date = $("#end_date").val();
	var end_date1 = date_convert(end_date);
	// end of date values

	var format = $("input[name='format']:checked").val();
	var date_group = $("input[name='date_group']:checked").val();
	//var group_by = get_group_by();
	var campaign_id = $("#campaign_id").val();
	var store_id = $("#store_id").val();
	//var interval = $("#interval").val();

	// API key and start date validation

	var cool = true;

	var is_api = api_key !== "";
	var is_start_date = start_date !== "";

	cool = is_api && is_start_date;

	var alert = "";

	if (!is_api)
		alert += "The API key is missing!<br /><br />";
	if (!is_start_date)
		alert += "Please specify the 'date'!<br / ><br / >";

	$("#alertdiv").html(alert);

	// end of API key and start date validation

	var output_url = "https://clients.adcolony.com/api/v2/install?user_credentials=" + api_key;

	if (start_date1 !== "")
		output_url += "&date=" + start_date1;
	if (end_date1 !== "")
		output_url += "&end_date=" + end_date1;

	if (format !== undefined)
		output_url += "&format=" + format;
	//if (interval !== "")
	//	output_url += "&interval=" + interval;
	if (campaign_id !== "")
		output_url += "&campaign_id=" + campaign_id;
	if (store_id !== "")
		output_url += "&store_id=" + store_id;
	//if (group_by !== "")
	//	output_url += "&group_by=" + group_by;
	if (date_group !== undefined)
		output_url += "&date_group=" + date_group;
	

	var html_text = "<span class='bold'>The URL:</span><br /><br />";
	html_text += "<div id='querydiv'>" + output_url + "</div><br />";
	html_text += "<a class='blue' href='" + output_url + "' target='_blank'>Click here to see the result!</a><br /><br />";

	$("#urldiv").html(html_text);

	if (cool) {
		$("#urldiv").show();
		$("#alertdiv").hide();
	} else {
		$("#urldiv").hide();
		$("#alertdiv").show();
	}
}
date_convert = function(string) {
	/* old logic
	var year = string.substr(0, 4);
	var month = string.substr(5, 2);
	var day = string.substr(8, 2);
	var new_date = month + day + year;

	return new_date;
	*/
	var new_date = string.replace(/\//g,'');
	return new_date;
	
}

/*
get_group_by = function() {

	var group_str = "";

	var gr_campaign1 = get_chb_value("gr_campaign");
	var gr_adgroup1 = get_chb_value("gr_adgroup");
	var gr_creative1 = get_chb_value("gr_creative");
	var gr_country1 = get_chb_value("gr_country");
	var gr_app1 = get_chb_value("gr_app");
	var gr_zone1 = get_chb_value("gr_zone");
	var gr_platform1 = get_chb_value("gr_platform");

	group_str = gr_campaign1 + gr_adgroup1 + gr_creative1 + gr_country1 + gr_app1 + gr_zone1 + gr_platform1;

	var l = group_str.length;

	var output_str = group_str;

	if (l > 0) {
		output_str = group_str.substring(1);
	}

	return output_str;

}
*/

get_chb_value = function(str) {
	var id = "#" + str;
	var checked = $(id).prop("checked");
	var param1 = $(id).val();
	if (checked) {
		return "," + param1;
	}
	return "";
}

example = function(input){
	
	var api_id = "#" + input + "_api";
	var link_id = "#" + input + "_link";
	//var api_link_id = "#" + input + "_api_link";
	var output_id = "#" + input + "_output";
	
	var api = $(api_id).val();
	var link = $(link_id).html();
	link = link.replace(/\&amp;/g,'&');
	link = link.replace("<span class=\"bold\">","");
	link = link.replace("</span>","");
	
	var api_key_from_link = get_api_key(link);
	var empty_case = "user_credentials=" + api;
	var outputlink = "";
	
	if (api_key_from_link === "") {
		outputlink = link.replace("user_credentials=",empty_case);
		
	} else {
		outputlink = link.replace(api_key_from_link,api);
	}
	
	$(link_id).html(outputlink);
	
	var csv_link = outputlink + "&format=csv";
	var json_link = outputlink + "&format=json";
	var xml_link = outputlink + "&format=xml";
	
	var output_text = "Click here for the result: <a href='" + csv_link + "' target='_blank' class='blue'>csv</a>, ";
	output_text += "<a href='" + json_link + "' target='_blank' class='blue'>json</a>, ";
	output_text += "<a href='" + xml_link + "' target='_blank' class='blue'>xml</a>";
	
	$(output_id).html(output_text);
	$(output_id).show();
	
	console.log(outputlink);
}

get_api_key = function(link) {
	
	var substr1arr = link.split("user_credentials=");
	var substr1 = substr1arr[1];
	console.log(substr1);
	var substr2arr = substr1.split("&");
	var substr2 = substr2arr[0];
	return substr2;
	
}

$(document).ready(function() {

	$("#urldiv").hide();
	$("#alertdiv").hide();
	divToggle(true, bapititle, bapidiv, bapidown, bapiup);
	divToggle(true, notestitle, notesdiv, notesdown, notesup);
	divToggle(false, hiddentitle, hiddendiv, hiddendown, hiddenup);
	$("#hiddendown, #hiddenup").addClass("importantHide");

});




