<?php
include ("http://localhost:8888/password_protect.php");
?>

<?php
include '../../header.php';
?>

<?php
include '../../maxside.php';
?>

<?php include_once("analyticstracking.php")
?>

<link href="../style_max.css" rel="stylesheet">

<div id="maincontent">

	<!-- Top -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="script.js"></script>
	<!-- Bootstrap core CSS -->
	<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

	<div class="container">
		<div class="form-group">
			<h1>URL editor</h1>
			<br />
			<p>
				Paste your URL to edit below:
			</p>
			<textarea rows="4" type="text" class="form-control" name="textbox" id="textbox"></textarea>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<input class="btn-class" onchange="myFunction()" type="submit" name="button" id="edit-url-button" onclick="fixUrl()" value="EDIT URL"/>
			</div>
		</div>
		<br />
		<p id="demo"></p>
		<div id="text_output"></div>
		<br>
		<div id="table1"></div>
		<datalist id="datalist1">
			<option value="[DEVICE_ID]"><option value="[IDFA]"><option value="[MD5_IDFA]"><option value="[SHA1_UC_ADVERTISER_ID]"><option value="[SHA1_ADVERTISER_ID]"><option value="[ODIN1]"><option value="[MAC_SHA1]"><option value="[GOOGLE_AD_ID]"><option value="[SHA1_ANDROID_ID]"><option value="[SHA1_IMEI]"><option value="[PRODUCT_ID]"><option value="[APP_ID]"><option value="[RAW_APP_ID]"><option value="[APP_NAME]"><option value="[RAW_AD_CAMPAIGN_ID]"><option value="[AD_CAMPAIGN_NAME]"><option value="[RAW_AD_GROUP_ID]"><option value="[AD_GROUP_NAME]"><option value="[RAW_AD_CREATIVE_ID]"><option value="[AD_CREATIVE_NAME]"><option value="[ADC_VERSION]"><option value="[OS_VERSION]"><option value="[PLATFORM]"><option value="[DEVICE_MODEL]"><option value="[DEVICE_GROUP]"><option value="[NETWORK_TYPE]"><option value="[LANGUAGE]"><option value="[SERVE_TIME]"><option value="[DMA_CODE]"><option value="[COUNTRY_CODE]"><option value="[COUNTRY_CODE_UK]"><option value="[ZONE_TYPE]"><option value="[ZONE_UUID]"><option value="[PUBLISHER_ID]"><option value="[PUBLISHER_NAME]"><option value="[ADCOLONY_TIMESTAMP]"><option value="[ADCOLONY_TIMESTAMP_MILLIS]"><option value="[IP_ADDRESS]"><option value="[STORE_ID]"><option value="[BID]"><option value="[BID_TYPE]"><option value="[TRANS_ID]"><option value="[CLICK_ID]"><option value="[USER_AGENT_MOZILLA]"><option value="[USER_AGENT]">
		</datalist>

		<div class="row">
			<div class="col-sm-12"></div>
		</div>
	</div>

</div>
<?php
include '../../footer.php';
?>