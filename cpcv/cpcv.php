<?php
include ("http://localhost:8888/password_protect.php");
?>

<?php
include '../header.php';
?>
<?php
include '../brandside.php';
?>

<?php include_once("analyticstracking.php")
?>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="cpcv.js"></script>


<div id="maincontent">
	<div class="container" style="padding-top: 10px !important;">

		<h1>CPCV calculator</h1>

		<h3 class="black plink" id="notestitle">1. Notes <i class="fa fa-arrow-down" id="notesdown"></i><i class="fa fa-arrow-up" id="notesup"></i></h3>
		<div id="notesdiv" class="ndiv">
			This page can help you to calculate the Traffic CPCV from the "Gross" and "Unit booked" values.
			<br />
			The "AdColony target margin" value can be modified as well.
			<br />
			<br />
			

			<h3 class="plink" id="formulastitle">Formulas <i class="fa fa-arrow-down" id="formulasdown"></i><i class="fa fa-arrow-up" id="formulasup"></i></h3>
			<div id="formulasdiv">
				<ul>
					<li>
						Agency discount = Gross * (Agency discount (%) / 100)
					</li>
					<li>
						Net of agency = Gross - Agency discount
					</li>
					<li>
						Agency trading = Net of agency * (Agency trading (%) / 100)
					</li>
					<li>
						AdColony target margin = Net of agency * (AdColony target margin (%) / 100)
					</li>
					<li>
						Publisher payout = Net of agency - Agency trading - AdColony target margin
					</li>
					<li>
						Traffic CPCV (£) = Publisher payout / Unit booked
					</li>
					<li>
						Traffic CPCV ($) = Traffic CPCV (£) * Exchange rate (GBP/USD)
					</li>
					<li>
						Traffic CPM (£) = Traffic CPCV (£) * 1000
					</li>
					<li>
						Traffic CPM ($) = Traffic CPM (£) * Exchange rate (GBP/USD)
					</li>
					<br />
					<li>
						Total amount needed = Unit booked / (1 - Discrepancy / 100)
					</li>
					<li>
						Overdelivery = (Discrepancy / 100) / (1 - Discrepancy / 100)
					</li>
				</ul>
				<br />
			</div>

			<h3 class="plink" id="overdtitle">Overdelivery calculator <i class="fa fa-arrow-down" id="overddown"></i><i class="fa fa-arrow-up" id="overdup"></i></h3>
			<div id="overddiv">
				<table class="cpcvtable">
					<tr>
						<td>Unit booked </td>
						<td>
						<input type="text" name="unit_odc" id="unit_odc" class="orange" oninput="discr_calc()">
						</td>
						<td></td>
						<td>Total amount needed </td>
						<td>
						<input type="text" name="total_odc" id="total_odc" class="green" readonly>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>Discrepancy </td>
						<td>
						<input type="text" name="discr_odc" id="discr_odc" class="orange" oninput="discr_calc()" value="0">
						</td>
						<td>%</td>
						<td>Overdelivery</td>
						<td>
						<input type="text" name="overd_odc" id="overd_odc" class="green" readonly>
						</td>
						<td>%</td>
					</tr>
				</table>
				<br />
			</div>

			<h3 class="black">Exchange rate</h3>
			The GBP/USD rate is: <span id="exch_disp" class="bold"></span>
			<br />
			Source: <a href="https://finance.yahoo.com/currency-converter/#from=GBP;to=USD;amt=1" target="_blank" class="bold">Yahoo Finance</a>

			<br />
			<br />
			Pre-set values:
			<br />
			<ul>
				<li>
					Non-barter: Agency discount: 15%, Agency trading: 30%
				</li>
				<li>
					Barter: Agency discount: 27.75%, Agency trading: 30%
				</li>
			</ul>
			<br />
			<br />

		</div>

		<h3 id="cpcvtitle" class="black plink">2. CPCV calculator <i class="fa fa-arrow-down" id="cpcvdown"></i><i class="fa fa-arrow-up" id="cpcvup"></i></h3>
		<div id="cpcvdiv">
			Pre-set values:
			<!--
			Non-barter: ag_disc: 15%, ag_trad: 30%
			Barter: ag_disc: 27.75%, ag_trad: 30%
			-->
			<input type="radio" name="preset" value="non-barter" checked="checked" id="non-barter" onclick="barter()"/>
			Non-barter
			<input type="radio" name="preset" value="barter" id="barter" onclick="barter()"/>
			Barter
			<br />
			<br />

			<table class="cpcvtable">
				<tr>
					<td></td>
					<td class="bold center">Input fields</td>
					<td></td>
					<td></td>
					<td class="bold center">Calculated fields</td>
					<td></td>
				</tr>
				<tr>
					<td class="bold">Gross (£)</td>
					<td>
					<input type="text" name="gross" id="gross" class="bold orange" oninput="calculate()">
					</td>
					<td></td>
					<td>Agency discount (£)</td>
					<td>
					<input type="text" name="ag_disc_val" id="ag_disc_val" readonly>
					</td>
					<td></td>
				</tr>
				<tr>
					<td>Agency discount</td>
					<td>
					<input type="text" name="ag_disc" id="ag_disc" value="15" readonly oninput="calculate()">
					</td>
					<td>%</td>
					<td class="bold">Net of agency (£)</td>
					<td>
					<input type="text" name="net_of_ag" id="net_of_ag" readonly class="bold">
					</td>
					<td></td>
				</tr>
				<tr>
					<td>Agency trading</td>
					<td>
					<input type="text" name="ag_trad" id="ag_trad" value="30" readonly oninput="calculate()">
					</td>
					<td>%</td>
					<td>Agency trading (£)</td>
					<td>
					<input type="text" name="ag_trad_val" id="ag_trad_val" readonly>
					</td>
					<td></td>
				</tr>
				<tr>
					<td class="bold">AdColony target margin</td>
					<td>
					<input type="text" name="ac_margin" id="ac_margin" class="bold orange" value="30" oninput="calculate()">
					</td>
					<td>%</td>
					<td>AdColony target margin (£)</td>
					<td>
					<input type="text" name="margin_val" id="margin_val" readonly>
					</td>
					<td></td>
				</tr>
				<tr>
					<td class="bold">Unit booked</td>
					<td>
					<input type="text" name="unit_am" id="unit_am" class="bold orange" oninput="calculate()">
					</td>
					<td></td>
					<td class="bold">Publisher payout (£)</td>
					<td>
					<input type="text" name="cogs" id="cogs" readonly class="bold">
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td>Traffic CPCV (£)</td>
					<td>
					<input type="text" name="cpcv_pound" id="cpcv_pound" readonly>
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td class="bold">Traffic CPCV ($)</td>
					<td><input type="text" name="cpcv_dol" id="cpcv_dol" readonly" class="bold green"></td>
					<td></td>
					</tr>
					<tr>
					<td>
					<!--
					<input type="button" class="btn-class" name="buildURL" id="buildURL" value="Calculate!" onclick="calculate()">
					-->
					</td>
					<td></td>
					<td></td>
					<td>Traffic CPM (£)</td>
					<td><input type="text" name="cpm_pound" id="cpm_pound" readonly></td>
					<td></td>
					</tr>
					<tr>
					<td></td> <td></td>
					<td></td>
					<td class="bold">Traffic CPM ($)</td>
					<td>
					<input type="text" name="cpm_dol" id="cpm_dol" readonly class="bold green">
					</td>
					<td></td>
				</tr>
			</table>
			<br />

		</div>

	</div>

</div>

<?php
include '../footer.php';
?>

