str1 = "http://extads.gameloft.com/adcolony/click.php?gcampaign_id=2786&gproduct_id=1694&gos=ios&idfa=[IDFA]&country_code=[COUNTRY_CODE]&ip_address=[IP_ADDRESS]&app_id=[APP_ID]";
str2 = "http://extads.gameloft.com/adcolony/impression.php?gcampaign_id=2786&gproduct_id=1694&gos=ios&idfa=[IDFA]&country_code=[COUNTRY_CODE]&ip_address=[IP_ADDRESS]&app_id=[APP_ID]";
str3 = "http://extads.gameloft.com/adcolony/click.php?gcampaign_id=2766&gproduct_id=1694&gos=android&google_ad_id=[GOOGLE_AD_ID]&country_code=[COUNTRY_CODE]&ip_address=[IP_ADDRESS]&app_id=[APP_ID]";
str4 = "http://extads.gameloft.com/adcolony/impression.php?gcampaign_id=2766&gproduct_id=1694&gos=android&google_ad_id=[GOOGLE_AD_ID]&country_code=[COUNTRY_CODE]&ip_address=[IP_ADDRESS]&app_id=[APP_ID]";

select_link = function() {
	if ($("#comp_r1")[0].checked)
		return "comp_texta1";
	if ($("#comp_r2")[0].checked)
		return "comp_texta2";
	if ($("#comp_r3")[0].checked)
		return "comp_texta3";
	if ($("#comp_r4")[0].checked)
		return "comp_texta4";
}
comp_links = function(string) {

	$("#comp_ios_cl_cont").hide();
	$("#comp_ios_imp_cont").hide();
	$("#comp_and_cl_cont").hide();
	$("#comp_and_imp_cont").hide();

	$("#hc_ios_cl").hide();
	$("#hc_ios_imp").hide();
	$("#hc_and_cl").hide();
	$("#hc_and_imp").hide();
	$("#redirectdiv").hide();
	$("#redirectdiv2").hide();

	$("#comp_notes_cont").hide();

	$("input:radio").attr("checked", false);

	$("#urlsplitdiv_new1, #keydiv, #samediv").hide();
	$("#geturl2").removeClass("geturl2_visible");
	$("#input_url").val("");

	switch (string) {
	case 'gameloft':
		$("#comp_track_title").html("Gameloft");

		str1 = "http://extads.gameloft.com/adcolony/click.php?gcampaign_id=2786&gproduct_id=1694&gos=ios&idfa=[IDFA]&country_code=[COUNTRY_CODE]&ip_address=[IP_ADDRESS]&app_id=[APP_ID]";
		str2 = "http://extads.gameloft.com/adcolony/impression.php?gcampaign_id=2786&gproduct_id=1694&gos=ios&idfa=[IDFA]&country_code=[COUNTRY_CODE]&ip_address=[IP_ADDRESS]&app_id=[APP_ID]";
		str3 = "http://extads.gameloft.com/adcolony/click.php?gcampaign_id=2766&gproduct_id=1694&gos=android&google_ad_id=[GOOGLE_AD_ID]&country_code=[COUNTRY_CODE]&ip_address=[IP_ADDRESS]&app_id=[APP_ID]";
		str4 = "http://extads.gameloft.com/adcolony/impression.php?gcampaign_id=2766&gproduct_id=1694&gos=android&google_ad_id=[GOOGLE_AD_ID]&country_code=[COUNTRY_CODE]&ip_address=[IP_ADDRESS]&app_id=[APP_ID]";
		$("#comp_texta1").val(str1);
		$("#comp_texta2").val(str2);
		$("#comp_texta3").val(str3);
		$("#comp_texta4").val(str4);

		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();
		$("#comp_and_cl_cont").show();
		$("#comp_and_imp_cont").show();

		var gl_notes = "1. Confirm with the account manager that the client has setup the postback.<br />";
		gl_notes += "2. Are the macros correct in the URL? Does it include IDFA or GOOGLE_AD_ID macro?<br />";

		$("#comp_notes_div").html(gl_notes);
		$("#comp_notes_cont").show();

		break;
		
	case 'glispa2':
		$("#comp_track_title").html("Gameloft");

		str1 = "http://trk.glispa.com/pc/dHkK9xgXH9Blx2y_REuUwLaf2pbEMqvbMp-7BtITVBc/CF?m.idfa=[IDFA]&subid1=[CLICK_ID]&placement=[APP_ID]";
		str2 = "http://trk.glispa.com/pc/dHkK9xgXH9Blx2y_REuUwLaf2pbEMqvbMp-7BtITVBc/CF?m.idfa=[IDFA]&subid1=[CLICK_ID]&placement=[APP_ID]&op=impression";
		str3 = "http://trk.glispa.com/pc/dDoKfhhsHwhllGwgRESUUbak2t7EGqsUMom7Q9JcVBY/CF?m.gaid=[GOOGLE_AD_ID]&subid1=[CLICK_ID]&placement=[APP_ID]";
		str4 = "http://trk.glispa.com/pc/dDoKfhhsHwhllGwgRESUUbak2t7EGqsUMom7Q9JcVBY/CF?m.gaid=[GOOGLE_AD_ID]&subid1=[CLICK_ID]&placement=[APP_ID]&op=impression";
		$("#comp_texta1").val(str1);
		$("#comp_texta2").val(str2);
		$("#comp_texta3").val(str3);
		$("#comp_texta4").val(str4);

		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();
		$("#comp_and_cl_cont").show();
		$("#comp_and_imp_cont").show();

		break;
		
	case 'singular':
		$("#comp_track_title").html("Singular");

		str1 = "https://c.singular.net/api/v1/ad?st=4940583110&udid=[RAW_UDID]&and1=[SHA1_ANDROID_ID]&aifa=[GOOGLE_AD_ID]&s=[APP_ID]&idfa=[IDFA]&odin=[ODIN1]&udi1=[UDID]&api_key=d4636d565f59e217801b5721af716eac&product_id=[PRODUCT_ID]&ip=[IP_ADDRESS]&ve=[OS_VERSION]&cl=[CLICK_ID]&pc=[RAW_AD_CAMPAIGN_ID]&pcn=[AD_CAMPAIGN_NAME]&pcid=[RAW_AD_CAMPAIGN_ID]&pscid=[RAW_AD_GROUP_ID]&pscn=[AD_GROUP_NAME]&cr=[RAW_AD_CREATIVE_ID]&pcrn=[AD_CREATIVE_NAME]&pcrid=[RAW_AD_CREATIVE_ID]&ps=[APP_ID]&psn=[APP_NAME]&redirect=false&h=550cf5c44917f60b7f2c101f4331ad8b67444bff&p=ios&pssid=[ZONE_UUID]&psid=[STORE_ID]";
		str2 = "https://i.singular.net/api/v1/imp?st=4940583110&udid=[RAW_UDID]&and1=[SHA1_ANDROID_ID]&aifa=[GOOGLE_AD_ID]&s=[APP_ID]&idfa=[IDFA]&odin=[ODIN1]&udi1=[UDID]&api_key=d4636d565f59e217801b5721af716eac&product_id=[PRODUCT_ID]&ip=[IP_ADDRESS]&ve=[OS_VERSION]&cl=[CLICK_ID]&pc=[RAW_AD_CAMPAIGN_ID]&pcn=[AD_CAMPAIGN_NAME]&pcid=[RAW_AD_CAMPAIGN_ID]&pscid=[RAW_AD_GROUP_ID]&pscn=[AD_GROUP_NAME]&cr=[RAW_AD_CREATIVE_ID]&pcrn=[AD_CREATIVE_NAME]&pcrid=[RAW_AD_CREATIVE_ID]&ps=[APP_ID]&psn=[APP_NAME]&redirect=false&h=550cf5c44917f60b7f2c101f4331ad8b67444bff&p=ios&pssid=[ZONE_UUID]&psid=[STORE_ID]";
		str3 = "https://c.singular.net/api/v1/ad?st=5814041873&udid=[RAW_UDID]&and1=[SHA1_ANDROID_ID]&aifa=[GOOGLE_AD_ID]&s=[APP_ID]&idfa=[IDFA]&odin=[ODIN1]&udi1=[UDID]&api_key=d4636d565f59e217801b5721af716eac&product_id=[PRODUCT_ID]&ip=[IP_ADDRESS]&ve=[OS_VERSION]&cl=[CLICK_ID]&pc=[RAW_AD_CAMPAIGN_ID]&pcn=[AD_CAMPAIGN_NAME]&pcid=[RAW_AD_CAMPAIGN_ID]&pscid=[RAW_AD_GROUP_ID]&pscn=[AD_GROUP_NAME]&cr=[RAW_AD_CREATIVE_ID]&pcrn=[AD_CREATIVE_NAME]&pcrid=[RAW_AD_CREATIVE_ID]&ps=[APP_ID]&psn=[APP_NAME]&redirect=false&h=739b6ba13504691a3647550e96178a8200a36559&p=android&pssid=[ZONE_UUID]&psid=[STORE_ID]";
		str4 = "https://i.singular.net/api/v1/imp?st=5814041873&udid=[RAW_UDID]&and1=[SHA1_ANDROID_ID]&aifa=[GOOGLE_AD_ID]&s=[APP_ID]&idfa=[IDFA]&odin=[ODIN1]&udi1=[UDID]&api_key=d4636d565f59e217801b5721af716eac&product_id=[PRODUCT_ID]&ip=[IP_ADDRESS]&ve=[OS_VERSION]&cl=[CLICK_ID]&pc=[RAW_AD_CAMPAIGN_ID]&pcn=[AD_CAMPAIGN_NAME]&pcid=[RAW_AD_CAMPAIGN_ID]&pscid=[RAW_AD_GROUP_ID]&pscn=[AD_GROUP_NAME]&cr=[RAW_AD_CREATIVE_ID]&pcrn=[AD_CREATIVE_NAME]&pcrid=[RAW_AD_CREATIVE_ID]&ps=[APP_ID]&psn=[APP_NAME]&redirect=false&h=739b6ba13504691a3647550e96178a8200a36559&p=android&pssid=[ZONE_UUID]&psid=[STORE_ID]";
		$("#comp_texta1").val(str1);
		$("#comp_texta2").val(str2);
		$("#comp_texta3").val(str3);
		$("#comp_texta4").val(str4);

		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();
		$("#comp_and_cl_cont").show();
		$("#comp_and_imp_cont").show();

		break;

	case 'plarium':
		$("#comp_track_title").html("Plarium");

		str1 = "https://mt.x-plarium.com/?type=click&campaignId=55858&deviceId=[IDFA]&appId=blXCA&publisher=[AD_CREATIVE_NAME]&placement=[PUBLISHER_ID]&attributionId=[CLICK_ID]&partner=adcolony&ip=[IP_ADDRESS]&useragent=[USER_AGENT_MOZILLA]&noredirect=1";
		str2 = "https://mt.x-plarium.com/?type=impression&campaignId=55858&deviceId=[IDFA]&appId=blXCA&publisher=[AD_CREATIVE_NAME]&placement=[PUBLISHER_ID]&attributionId=[CLICK_ID]&partner=adcolony&ip=[IP_ADDRESS]&useragent=[USER_AGENT_MOZILLA]&noredirect=1";
		str3 = "https://mt.x-plarium.com/?type=click&campaignId=61972&deviceId=[GOOGLE_AD_ID]&appId=blXCA&publisher=[AD_CREATIVE_NAME]&placement=[PUBLISHER_ID]&attributionId=[CLICK_ID]&partner=adcolony&ip=[IP_ADDRESS]&useragent=[USER_AGENT_MOZILLA]&noredirect=1";
		str4 = "https://mt.x-plarium.com/?type=impression&campaignId=61972&deviceId=[GOOGLE_AD_ID]&appId=blXCA&publisher=[AD_CREATIVE_NAME]&placement=[PUBLISHER_ID]&attributionId=[CLICK_ID]&partner=adcolony&ip=[IP_ADDRESS]&useragent=[USER_AGENT_MOZILLA]&noredirect=1";
		$("#comp_texta1").val(str1);
		$("#comp_texta2").val(str2);
		$("#comp_texta3").val(str3);
		$("#comp_texta4").val(str4);

		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();
		$("#comp_and_cl_cont").show();
		$("#comp_and_imp_cont").show();

		break;

	case 'apsalar':
		$("#comp_track_title").html("apsalar");

		str1 = "http://ad.apsalar.com/api/v1/ad?re=0&a=naturalmotion&i=com.naturalmotion.dawnoftitans&ca=AdColony_DOT_IOS&an=AdColony&p=iOS&pl=[AD_GROUP_NAME]&udid=[RAW_UDID]&idfa=[IDFA]&odin=[ODIN1]&udi1=[UDID]&api_key=d4636d565f59e217801b5721af716eac&product_id=[PRODUCT_ID]&s=[APP_ID]&h=7fbaad31c18baa3dd25d27b85fd1c84acd11f083";
		str2 = "http://ad.apsalar.com/api/v1/ad?re=0&a=naturalmotion&i=com.naturalmotion.dawnoftitans&ca=AdColony_DOT_IOS&an=AdColony&p=iOS&pl=[AD_GROUP_NAME]&udid=[RAW_UDID]&idfa=[IDFA]&odin=[ODIN1]&udi1=[UDID]&api_key=d4636d565f59e217801b5721af716eac&product_id=[PRODUCT_ID]&s=[APP_ID]&h=7fbaad31c18baa3dd25d27b85fd1c84acd11f083&op=impression";
		str3 = "http://ad.apsalar.com/api/v1/ad?re=0&a=naturalmotion&i=com.naturalmotion.dawnoftitans&ca=AdColony_DOT_GP&an=AdColony&p=Android&pl=[AD_GROUP_NAME]&andi=[ANDROID_ID]&and1=[SHA1_ANDROID_ID]&aifa=[GOOGLE_AD_ID]&api_key=d4636d565f59e217801b5721af716eac&product_id=[PRODUCT_ID]&s=[APP_ID]&h=7fbaad31c18baa3dd25d27b85fd1c84acd11f083";
		str4 = "http://ad.apsalar.com/api/v1/ad?re=0&a=naturalmotion&i=com.naturalmotion.dawnoftitans&ca=AdColony_DOT_GP&an=AdColony&p=Android&pl=[AD_GROUP_NAME]&andi=[ANDROID_ID]&and1=[SHA1_ANDROID_ID]&aifa=[GOOGLE_AD_ID]&api_key=d4636d565f59e217801b5721af716eac&product_id=[PRODUCT_ID]&s=[APP_ID]&h=7fbaad31c18baa3dd25d27b85fd1c84acd11f083&op=impression";
		$("#comp_texta1").val(str1);
		$("#comp_texta2").val(str2);
		$("#comp_texta3").val(str3);
		$("#comp_texta4").val(str4);

		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();
		$("#comp_and_cl_cont").show();
		$("#comp_and_imp_cont").show();

		$("#hc_and_cl").show();

		var aps_notes = "See: <a href='http://tamtrackingtool.com/apsalar/apsalar.php' target='_blank' class='blue'>Apsalar page</a>.";

		$("#comp_notes_div").html(aps_notes);
		$("#comp_notes_cont").show();

		break;

	case 'localytics':
		$("#comp_track_title").html("Localytics");

		str1 = "http://a.localytics.com/redirect/fsy80yvatb05dsd0q1to?partner=ad_colony&idfa=[IDFA]&bundle=376510438";
		str2 = "http://a.localytics.com/android?id=com.hulu.plus&referrer=utm_source%3Dad_colony%26utm_campaign%3DAdcolony-Android-%253A30s2016Q1-N%252FA-A113_A146_A000943";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();
		break;

	case 'appcoach':
		$("#comp_track_title").html("Appcoach");

		str1 = "http://fc.appcoachs.com/tl?a=25&o=1566&s5=[PRODUCT_ID]&s3=[GOOGLE_AD_ID]&s4=[GOOGLE_AD_ID]";
		str2 = "http://fc.appcoachs.com/tl?a=25&o=1577&s5=[PRODUCT_ID]&s4=[GOOGLE_AD_ID]&s1=impression";
		str3 = "http://fc.appcoachs.com/tl?a=25&o=1474&s5=1087708023&s4=[IDFA]";
		str4 = "http://fc.appcoachs.com/tl?a=25&o=1474&s5=1087708023&s4=[IDFA]&s1=impression";
		$("#comp_texta3").val(str1);
		$("#comp_texta4").val(str2);
		$("#comp_texta1").val(str3);
		$("#comp_texta2").val(str4);

		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();
		$("#comp_and_cl_cont").show();
		$("#comp_and_imp_cont").show();

		$("#hc_and_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("***if the Google store URL has a referrer on the end, the link needs to be hardcoded; s5=product_id");
		break;

	case 'wooga':
		$("#comp_track_title").html("Wooga");

		str1 = "http://woogatrack.com/track?network=adcolony&campaign=[AD_GROUP_NAME]&adgroup=_[APP_ID]_[COUNTRY_CODE]&idfa=[IDFA]&app_id=pli&ad=_[DEVICE_MODEL]_[AD_CREATIVE_NAME]";
		str2 = "http://woogatrack.com/track?network=adcolony&campaign=[AD_GROUP_NAME]&adgroup=_[APP_ID]_[COUNTRY_CODE]&idfa=[IDFA]&app_id=pli&ad=_[DEVICE_MODEL]_[AD_CREATIVE_NAME]";
		str3 = "http://woogatrack.com/track?network=adcolony&campaign=g9a_se_mobile&adgroup=[APP_ID]&partner_data=[GOOGLE_AD_ID]&app_id=g9a&adid=[GOOGLE_AD_ID]&utm_source=adcolony&utm_medium=g9a_se_mobile&utm_term=[PUBLISHER_ID]&utm_content=video&utm_campaign=test&ad=[AD_CREATIVE_NAME]";
		str4 = "http://woogatrack.com/track?network=adcolony&campaign=g9a_se_mobile&adgroup=[APP_ID]&partner_data=[GOOGLE_AD_ID]&app_id=g9a&adid=[GOOGLE_AD_ID]&utm_source=adcolony&utm_medium=g9a_se_mobile&utm_term=[PUBLISHER_ID]&utm_content=video&utm_campaign=test&ad=[AD_CREATIVE_NAME]";

		$("#comp_texta1").val(str1);
		$("#comp_texta2").val(str2);
		$("#comp_texta3").val(str3);
		$("#comp_texta4").val(str4);

		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();
		$("#comp_and_cl_cont").show();
		$("#comp_and_imp_cont").show();

		$("#hc_and_cl").show();

		var wooga_notes = "1. Are the macros correct? idfa=[IDFA] or partner_data=[GOOGLE_AD_ID], adgroup=[APP_ID]<br />";
		wooga_notes += "2. Click on the tag and make sure that it redirects to the correct app.<br />";
		wooga_notes += "<span class='black'>VT</span>: Implement same click tag on HTML5 + complete.";

		$("#comp_notes_div").html(wooga_notes);
		$("#comp_notes_cont").show();

		break;

	case 'pmad':
		$("#comp_track_title").html("Pmad");
		str1 = "http://pmad-mbad.com/api/click?creative_id=419&partner_clickid=[PRODUCT_ID]&publisher_id=[PUBLISHER_ID]&idfa=[IDFA]";
		str2 = "http://pmad-mbad.com/api/click?creative_id=413&partner_clickid=[PRODUCT_ID]&publisher_id=[PUBLISHER_ID]&ga_id=[GOOGLE_AD_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();

		break;
		
	case 'headway':
		$("#comp_track_title").html("Headway Digital");
		str1 = "https://go4.mobrain.xyz/8e9b063?p=[APP_ID]&sid=[CLICK_ID]&android_a_id=[GOOGLE_AD_ID]&idfa=[IDFA]&app_id=[PRODUCT_ID]&param1=[IDFA]&param2=[GOOGLE_AD_ID]&param3=[PRODUCT_ID]";
		str2 = "https://go4.mobrain.xyz/ce3f02b?p=[APP_ID]&sid=[CLICK_ID]&android_a_id=[GOOGLE_AD_ID]&idfa=[IDFA]&app_id=[PRODUCT_ID]&param1=[IDFA]&param2=[GOOGLE_AD_ID]&param3=[PRODUCT_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();
		
		var headway_notes = "By the way - we use the universal links on both iOS and Android.";

		$("#comp_notes_div").html(headway_notes);
		$("#comp_notes_cont").show();

		break;

	case 'thrive':
		$("#comp_track_title").html("Thrive");

		str1 = "http://loadinghere.com/path/lp.php?trvid=10591&trvx=8fc872b4&idfa=[IDFA]&prodid=[PRODUCT_ID]";
		str2 = "http://loadinghere.com/path/lp.php?trvid=10794&trvx=08d478ff&advertising_id=[GOOGLE_AD_ID]&sid=ac&prodid=[PRODUCT_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("Not sure if this is working...");

		break;

	case 'addict':
		$("#comp_track_title").html("Addict Mobile");

		str1 = "https://track.addict-mobile.net/2/track/1108414210?device_ip=[IP_ADDRESS]&idfa=[IDFA]&sub_publisher=[PUBLISHER_ID]&sub_site=[APP_ID]&creative=[RAW_AD_CREATIVE_ID]&country=[COUNTRY_CODE]";
		str2 = "https://track.addict-mobile.net/2/track/1392192272?vta=1&device_ip=[IP_ADDRESS]&idfa=[IDFA]&sub_publisher=[PUBLISHER_ID]&sub_site=[APP_ID]&creative=[RAW_AD_CREATIVE_ID]&country=[COUNTRY_CODE]";
		str3 = "http://track.addict-mobile.net/2/track/705373453?device_ip=[IP_ADDRESS]&gaid=[GOOGLE_AD_ID]&sub_publisher=[PUBLISHER_ID]&sub_site=[APP_ID]&android_id=[SHA1_ANDROID_ID]&creative=video15&country=[COUNTRY_CODE]";

		$("#comp_texta1").val(str1);
		$("#comp_texta2").val(str2);
		$("#comp_texta3").val(str3);

		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();
		$("#comp_and_cl_cont").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("they use different ID in the impression link and an extra vta=1 parameter");

		break;

	case 'inmobi':
		$("#comp_track_title").html("Inmobi");

		str1 = "http://wadogo.go2cloud.org/aff_c?offer_id=8399&aff_id=3333&aff_sub3=[PRODUCT_ID]&google_aid=[GOOGLE_AD_ID]&ios_ifa=[IDFA]";
		str2 = "http://wadogo.go2cloud.org/aff_c?offer_id=5655&aff_id=3333&aff_sub3=[PRODUCT_ID]&google_aid=[GOOGLE_AD_ID]&ios_ifa=[IDFA]";
		str3 = "http://wadogo.go2cloud.org/aff_c?offer_id=19540&aff_id=3333&aff_sub3=[PRODUCT_ID]&ios_ifa=[IDFA]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		break;

	case 'domob':
		$("#comp_track_title").html("Domob");

		str1 = "http://c.dsp.domob.cn/chn/clk?chnid=10003&cid=12622&idfa=[IDFA]&extinfo=997636530&u=https%3A%2F%2Fitunes.apple.com%2Fcn%2Fapp%2Fheng-sao-qian-jun-tang-guo%2Fid997636530%3Fl%3Dzh";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("*** extinfo=product_id; Another old iOS click URL:<br /> http://r.ow.domob.cn/ow/chn/clk?mid=1005515&cid=1017114&jump=1&ifa=[IDFA]&cb_offid=1060506717");

		break;

	case 'adways':
		$("#comp_track_title").html("Adways");

		str1 = "https://tc.pgss.jp/aff_c?offer_id=1197&aff_id=1047&aff_sub=[PRODUCT_ID]&aff_sub2=[APP_ID]&source=[PUBLISHER_ID]&ios_ifa=[IDFA]";
		str2 = "https://tc.pgss.jp/aff_c?offer_id=1715&aff_id=1047&aff_sub=[PRODUCT_ID]&aff_sub2=[APP_ID]&source=[PUBLISHER_ID]&android_id=[GOOGLE_AD_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("***Android links must use https://");

		break;

	case 'adbrix':
		$("#comp_track_title").html("AdBrix");

		str1 = "https://ref.ad-brix.com/v1/referrallink?ak=710530923&ck=5697997&agreement_key=[IDFA]&cb_param1=[PRODUCT_ID]&cb_param2=[IDFA]";
		str2 = "https://ref.ad-brix.com/v1/impression?ak=710530923&ck=5706941&agreement_key=[IDFA]&cb_param1=[PRODUCT_ID]&cb_param2=[IDFA] ";
		str3 = "https://ref.ad-brix.com/v1/referrallink?ak=637832603&ck=8224528&agreement_key=[GOOGLE_AD_ID]&cb_param1=[PRODUCT_ID]&cb_param2=[GOOGLE_AD_ID] ";
		str4 = "https://ref.ad-brix.com/v1/impression?ak=637832603&ck=2649406&agreement_key=[GOOGLE_AD_ID]&cb_param1=[PRODUCT_ID]&cb_param2=[GOOGLE_AD_ID] ";

		$("#comp_texta1").val(str1);
		$("#comp_texta2").val(str2);
		$("#comp_texta3").val(str3);
		$("#comp_texta4").val(str4);

		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();
		$("#comp_and_cl_cont").show();
		$("#comp_and_imp_cont").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("agreement_key=device_id and cb_param1=product_id and cb_param2=device_id");

		break;

	case 'adzcore':
		$("#comp_track_title").html("Adzcore");

		str1 = "https://click.adzcore.com/1.0.6d41602d2d33138b978cf8373fcc5f58c?pt_gid=904260&idfa=[IDFA]&sha1_mac=[MAC_SHA1]&prod_id=[PRODUCT_ID]&direct=1";
		str2 = "https://click.adzcore.com/1.0.118e425b313b141c9664844e680a6d3dc?sha1_imei=[SHA1_IMEI]&sha1_androidid=[SHA1_ANDROID_ID]&google_ad_id=[GOOGLE_AD_ID]&prod_id=[PRODUCT_ID]&direct=1";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		var adz_notes = "<span class='black'>VT</span>: Click tag (with &redirection=0) goes on complete, regular click tag goes on HTML5.";

		$("#comp_notes_div").html(adz_notes);
		$("#comp_notes_cont").show();

		break;

	case 'oneway':
		$("#comp_track_title").html("Oneway");

		str1 = "https://tracking.oneway.mobi/tl?a=9&o=721&s1=[IDFA]&s4=1166856103&s3=[CLICK_ID]";
		str2 = "https://tracking.oneway.mobi/tl?a=9&o=561&s1=[GOOGLE_AD_ID]&s4=[PRODUCT_ID]&s3=[CLICK_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();
		break;

	case 'metaps':
		$("#comp_track_title").html("Metaps");

		str1 = "https://t.metaps.biz/v1/cpi/click?campaign_id=kojp-co-gu3-alchemist-f6-android559a81435013cbf3fc7f0d0b8a&network_id=223&adid=[GOOGLE_AD_ID]&appstore_id=[STORE_ID]&creative_name=[AD_CREATIVE_NAME]&creative_size=[RAW_AD_CREATIVE_ID]&device_hash_method=sha1&device_id=[SHA1_ANDROID_ID]&device_id_is_hashed=true&device_id_type=android_id&imei_sha1=[SHA1_IMEI]&mac_sha1=[MAC_SHA1]&odin=[ODIN1]&pbr=1&site_id=[APP_ID]";
		str2 = "https://t.metaps.biz/v1/cpi/click?campaign_id=kojp-co-fenris-kira-00-ios559a816786d1daa8e0a6bb99e6&network_id=18&appstore_id=[STORE_ID]&creative_name=[AD_CREATIVE_NAME]&creative_size=[RAW_AD_CREATIVE_ID]&device_id=[IDFA]&device_id_type=idfa&mac_sha1=[MAC_SHA1]&odin=[ODIN1]&openudid=[OPEN_UDID]&site_id=[APP_ID]&udid=[UDID]";

		$("#comp_texta1").val(str2);
		$("#comp_texta3").val(str1);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		var metaps_notes = "1. Confirm with the account manager that the client has setup the postback.<br />";
		metaps_notes += "2. Are the macros correct in the URL? Does it include IDFA or GOOGLE_AD_ID macro?<br />";

		$("#comp_notes_div").html(metaps_notes);
		$("#comp_notes_cont").show();

		break;

	case 'minimob':
		$("#comp_track_title").html("Minimob");

		str1 = "http://clicks.minimob.com/tracking/click?clickid=1&trafficsource=1373690092&offerid=24442908107855287&product_id=[PRODUCT_ID]&idfa=[IDFA]&gaid=[GOOGLE_AD_ID]&pub_subid=[APP_ID]";
		str2 = "http://clicks.minimob.com/tracking/click?clickid=1&trafficsource=1373690092&offerid=24625609996489966&product_id=[PRODUCT_ID]&idfa=[IDFA]&gaid=[GOOGLE_AD_ID]&pub_subid=[APP_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("VPN needed for both OS");

		break;

	case 'lenzmx':
		$("#comp_track_title").html("Lenzmx");

		str1 = "http://tracking.lenzmx.com/async_click?mb_pl=ios&mb_nt=cb460&mb_campid=wy_yys_cn_ios&product_id=[PRODUCT_ID]&mb_idfa=[IDFA]&mb_auth=QJ8jRShfltJW5nU0&mb_ip=[IP_ADDRESS]&mb_subid=[PUBLISHER_ID]&mb_os=[OS_VERSION]&mb_device=[DEVICE_MODEL]&mb_language=[LANGUAGE]";
		str2 = "http://tracking.lenzmx.com/async_click?mb_pl=android&mb_nt=cb460&mb_campid=bst_er_tier2&product_id=[PRODUCT_ID]&mb_gaid=[GOOGLE_AD_ID]&mb_auth=QJ8jRShfltJW5nU0&mb_ip=[IP_ADDRESS]&mb_subid=[PUBLISHER_ID]&mb_os=[OS_VERSION]&mb_device=[DEVICE_MODEL]&mb_language=[LANGUAGE]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("Need to be in the target country for testing, product_id=product_id mb_gaid or mb_idfa=device_id Mobvista will put 'action=click' if it requires");

		break;

	case 'talking':
		$("#comp_track_title").html("Talking data");

		str1 = "https://lnk0.com/ckIJNh?idfa=[IDFA]&product_id=[PRODUCT_ID]";
		str2 = "https://ctl.talkingdata.com/v1/cpi/click?campaign_id=kosoco-veggie-wedgie-android570b23f60abb8cb59b88d99158&network_id=223&adid=[GOOGLE_AD_ID]&appstore_id=[STORE_ID]&device_hash_method=sha1&device_id=[SHA1_ANDROID_ID]&device_id_is_hashed=true&device_id_type=android_id&imei_sha1=[SHA1_IMEI]&mac_sha1=[MAC_SHA1]&odin=[ODIN1]&pbr=1&site_id=[APP_ID]&append_app_conv_trk_params=1";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("The Android URL is a Talking Data - Kochava hybrid URL.");

		break;

	case '42trck':
		$("#comp_track_title").html("42trck");

		str1 = "https://42trck.com/click?action=54306&pubid=ad_colony&click_id=[SERVE_TIME]&mac_address_sha1=[MAC_SHA1]&apple_ifa=[IDFA]&device_os_version=[OS_VERSION]&device_os=[PLATFORM]&device_model=[DEVICE_MODEL]&wifi_status=[NETWORK_TYPE]&device_countrycode=[COUNTRY_CODE]&sub_adgroup=[ZONE_TYPE]&source_app_id=[PRODUCT_ID]&google_advertising_id=[GOOGLE_AD_ID]&sub_site=[APP_ID]&source_site_id=[APP_ID]";
		str2 = "https://42trck.com/click?pub=247&campaign=6225&action=52797&pubid=ad_colony&click_id=[SERVE_TIME]&mac_address_sha1=[MAC_SHA1]&apple_ifa=[IDFA]&device_os_version=[OS_VERSION]&device_os=[PLATFORM]&device_model=[DEVICE_MODEL]&wifi_status=[NETWORK_TYPE]&device_countrycode=[COUNTRY_CODE]&sub_adgroup=[ZONE_TYPE]&source_app_id=[PRODUCT_ID]&google_advertising_id=[GOOGLE_AD_ID]&sub_site=[APP_ID]&source_site_id=[APP_ID]&bannerid=911936";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();

		break;

	case 'alibaba':
		$("#comp_track_title").html("Alibaba");

		str1 = "http://click.union.ucweb.com/index.php?service=RedirectService&pub=limj3@adcolony&offer_id=aliexpress.ios&uc_trans_1=436672029&uc_trans_2=[IDFA]&uc_trans_3=[IDFA]&subpub=[APP_ID]";
		str2 = "http://click.union.ucweb.com/index.php?service=RedirectService&pub=luohy1@adcolonycn&offer_id=aliexpress.iosgp&uc_trans_1=[PRODUCT_ID]&uc_trans_2=[GOOGLE_AD_ID]&uc_trans_3=[GOOGLE_AD_ID]&uc_trans_4=[CLICK_ID] &subpub=[APP_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("they use different ID in the impression link and an extra vta=1 parameter");

		break;

	case 'gaea':
		$("#comp_track_title").html("Gaea");

		str1 = "https://ad.gaeamobile.net/?r=adcolony&auth=34kAiHWz&idfa=[IDFA]&prod_id=[PRODUCT_ID]&gc=[APP_ID]&click_id=[CLICK_ID]&device_ip=[IP_ADDRESS]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		break;

	case 'idreamsky':
		$("#comp_track_title").html("iDreamSky");

		str1 = "http://spm.mobgi.com/track/common?acid=2433&idfa=[IDFA]&af_siteid=[APP_ID]&af_sub1=[COUNTRY_CODE]";
		str2 = "http://spm.mobgi.com/track/common?acid=2433&idfa=[IDFA]&af_siteid=[APP_ID]&af_sub1=[COUNTRY_CODE]";

		$("#comp_texta1").val(str1);
		$("#comp_texta2").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_ios_imp").show();

		break;

	case 'adcrops':
		$("#comp_track_title").html("adcrops");

		str1 = "https://t.adcrops.net/ad/p/r?_site=3845&_article=16668&_image=7527&ssub1=biz.fields.tawapri&ssub2=[GOOGLE_AD_ID]";
		str2 = "https://t.adcrops.net/ad/p/r?_site=416&_article=23306&_image=9022&ssub1=[PRODUCT_ID]&ssub2=[IDFA]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		break;

	case 'babeltime':
		$("#comp_track_title").html("Babeltime");

		str1 = "http://mapieoh.gamepanda.us/adstat/appclick/appclickrurl?s2s&transid=com.babeltime.fknsg.us.gp&source=Adcolony&os=android&idfa=[GOOGLE_AD_ID]";
		str2 = "http://mapieoh.gamepanda.us/adstat/appclick/appclickrurl?s2s&transid=1021924301&source=Adcolony&os=ios&idfa=[IDFA]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		break;

	case 'motive':
		$("#comp_track_title").html("Motive");

		str1 = "http://traktum.com/?a=65066&c=689307&s3=[PRODUCT_ID]&s4=[SHA1_ANDROID_ID]&s5=[GOOGLE_AD_ID]";
		str2 = "http://traktum.com/?a=65066&c=699863&s3=[PRODUCT_ID]&s5=[IDFA]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("s3=product_id and s5=device_id");

		break;

	case 'aquto':
		$("#comp_track_title").html("Aquto");

		str1 = "https://app.kickbit.com/api/campaign/datarewards/identify?r=https%3A%2F%2Fapp.appsflyer.com%2Fcom.zeptolab.thieves.google%3Faf_prt%3DAquto%26pid%3Dadcolony_int%26c%3DATT_MoVE_Zepto_Android_Control%26advertising_id%3D[GOOGLE_AD_ID]%26app_id%3Dcom.zeptolab.thieves.google%26clickid%3D[MAC_SHA1]&debugId=ATT_MoVE_Zepto_Android_Control";

		$("#comp_texta3").val(str1);

		$("#comp_and_cl_cont").show();

		$("#hc_and_cl").show();

		break;

	case 'blind':
		$("#comp_track_title").html("Blind Ferret");

		str1 = "http://tracking.blindferretmedia.com/aff_c?offer_id=16146&aff_id=3844&google_aid=[GOOGLE_AD_ID]&aff_sub5=com.bethsoft.falloutshelter&aff_sub4=com.bethsoft.falloutshelter&aff_sub2=[APP_ID]&source=[APP_ID]";
		str2 = "http://tracking.blindferretmedia.com/aff_c?offer_id=16150&aff_id=3844&ios_ifa=[IDFA]&aff_sub5=991153141&aff_sub4=991153141&aff_sub2=[APP_ID]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		break;

	case 'adwaysnonin':
		$("#comp_track_title").html("Adways Non-incent");

		str1 = "https://tc.pgss.jp/aff_c?offer_id=1995&aff_id=1047&aff_sub=[PRODUCT_ID]&aff_sub2=[APP_ID]&source=[PUBLISHER_ID]&android_id=[GOOGLE_AD_ID]";
		str2 = "https://tc.pgss.jp/aff_c?offer_id=1987&aff_id=1047&aff_sub=[PRODUCT_ID]&aff_sub2=[APP_ID]&source=[PUBLISHER_ID]&ios_ifa=[IDFA]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		break;

	case 'seads':
		$("#comp_track_title").html("Seads");

		str1 = "http://tc.pgss.jp/aff_c?offer_id=1995&aff_id=1047&aff_sub=[PRODUCT_ID]&aff_sub2=[APP_ID]&source=[PUBLISHER_ID]&android_id=[GOOGLE_AD_ID]";
		str2 = "http://tc.pgss.jp/aff_c?offer_id=1987&aff_id=1047&aff_sub=[PRODUCT_ID]&aff_sub2=[APP_ID]&source=[PUBLISHER_ID]&ios_ifa=[IDFA]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("VPN needed for both OS");

		break;

	case 'wink':
		$("#comp_track_title").html("The Wink Mobile");

		str1 = "http://track.twkmobile.com/aff_c?offer_id=880&aff_id=1480&aff_sub=sg.com.theedgeproperty.app&aff_sub2=[GOOGLE_AD_ID]";
		str2 = "http://track.twkmobile.com/aff_c?offer_id=870&aff_id=1480&aff_sub=[PRODUCT_ID]&aff_sub2=[IDFA]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();

		break;

	case 'adinnovation':
		$("#comp_track_title").html("Adinnovation");

		str1 = "https://api.adstapi.com/app/click?k=yqxove3T&gaid=[GOOGLE_AD_ID]&pubid=[APP_ID]&prod_id=[PRODUCT_ID]";
		str2 = "https://api.adstapi.com/app/click?k=DPnZppfI&idfa=[IDFA]&prod_id=[PRODUCT_ID]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		break;

	case 'appcounter':
		$("#comp_track_title").html("AppCounter");

		str1 = "http://ad.appcount.net/cl/?b_id=GxF26Lea&t_id=1&afad_param_1=[GOOGLE_AD_ID]&afad_param_2=[PRODUCT_ID]&afad_param_3=[APP_ID]";
		str2 = "http://ad.appcount.net/cl/?b_id=lId36M8T&t_id=1&afad_param_1=[IDFA]&afad_param_2=[PRODUCT_ID]&afad_param_3=[APP_ID]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("afad_param_1=device_id and afad_param_2=product_id");

		break;

	case 'jampp':
		$("#comp_track_title").html("Jampp");

		str1 = "http://tracking.jampp.com/click?pub=357&campaign=4743&action=33253&pubid=ad_colony&click_id=[SERVE_TIME]&apple_ifa=[IDFA]&device_os_version=[OS_VERSION]&device_os=[PLATFORM]&device_model=[DEVICE_MODEL]&wifi_status=[NETWORK_TYPE]&device_countrycode=[COUNTRY_CODE]&sub_adgroup=[ZONE_TYPE]&source_app_id=com.contextlogic.wish&google_advertising_id=[GOOGLE_AD_ID]&sub_site=[APP_ID]";
		str2 = "http://tracking.jampp.com/click?pub=280&campaign=3429&action=32892&pubid=ad_colony&click_id=[SERVE_TIME]&device_os_version=[OS_VERSION]&device_os=[PLATFORM]&device_model=[DEVICE_MODEL]&wifi_status=[NETWORK_TYPE]&device_countrycode=[COUNTRY_CODE]&sub_adgroup=[ZONE_TYPE]&source_app_id=com.namshi.android&google_advertising_id=[GOOGLE_AD_ID]&sub_site=[APP_ID]&bannerid=132065";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("source_app_id=product_id");

		break;

	case 'fiksu':
		$("#comp_track_title").html("Fiksu");

		str1 = "https://handler.fiksu.com/click?adid=22c3d0f0-9aee-0134-51c3-22000b1a8fe9&advertising_id=[GOOGLE_AD_ID]&click_id=[CLICK_ID]&site_id=[APP_ID]";
		str2 = "https://handler.fiksu.com/click?adid=03e4f5c0-9aeb-0134-45f0-22000b27a828&advertising_id=[IDFA]&click_id=[CLICK_ID]&site_id=[APP_ID]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();n
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();

		$("#comp_notes_cont").show();

		var fiksu_notes = "1. Fiksu sends us the full postback logs.<br />";
		fiksu_notes += "2. Check the most recent postbacks to ensure we’re receiving them based on <span class='black'>productid_fiksu</span>, for example:<br />";
		fiksu_notes += "<span class='bold'>http://cpa.adtilt.com/most_recent_actions?api_key=bb2cf0647ba654d7228dd3f9405bbc6a&product_id=</span><br />";
		fiksu_notes += "3. If there are no postbacks, confirm with the account manager that the postback has been setup.<br />";
		fiksu_notes += "4. If the postbacks look okay, post the goal ID in Basecamp for adops: <span class='black'>123456789_fiksu</span><br />";
		fiksu_notes += "5. Add the <span class='black'>af_siteid</span> parameter and set to APP_ID";

		$("#comp_notes_div").html(fiksu_notes);

		break;

	case 'fox':
		$("#comp_track_title").html("CyberZ/FOX");

		str1 = "https://app-adforce.jp/ad/p/r?_site=40734&_article=250523&_link=6313842&_image=5688561&suid=[PRODUCT_ID]&_spl=[APP_ID]&adid=[GOOGLE_AD_ID]";
		str2 = "https://app-adforce.jp/ad/p/r?_site=40735&_article=250525&_link=6313854&_image=5688562&suid=[PRODUCT_ID]&_spl=[APP_ID]&adid=[IDFA]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("suid=product_id;<br /><span class='black'>VT</span>: Click tag (with &_nored=1) goes on complete, regular click tag goes on HTML5.");

		break;

	case 'fun':
		$("#comp_track_title").html("Fun games for free");

		str1 = "https://a.topaz-analytics.com/api/v2/ads/click/battletanksbeta?source=adcolony&campaign=[RAW_AD_CAMPAIGN_ID]&medium=video&adid=[GOOGLE_AD_ID]&source_app=[APP_ID]&country=[COUNTRY_CODE]&group_id=[RAW_AD_GROUP_ID]&creative_id=[RAW_AD_CREATIVE_ID]&platform=play";
		str2 = "https://a.topaz-analytics.com/api/v2/ads/click/bikecompetition?source=adcolony&campaign=[RAW_AD_CAMPAIGN_ID]&medium=video&adid=[IDFA]&source_app=[APP_ID]&country=[COUNTRY_CODE]&group_id=[RAW_AD_GROUP_ID]&creative_id=[RAW_AD_CREATIVE_ID]&platform=ios";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		break;

	case 'smartc':
		$("#comp_track_title").html("Smart-C");

		str1 = "https://smart-c.jp/c?i=4Dk0N50npej4016ry&guid=ON&u=&gaid=[GOOGLE_AD_ID]&product_id=[PRODUCT_ID]";

		$("#comp_texta3").val(str1);

		$("#comp_and_cl_cont").show();

		$("#hc_and_cl").show();

		break;

	case 'urad':
		$("#comp_track_title").html("Urad.com");

		str1 = "http://adtracker.urad.com.tw/appsflyer?campaign=720&redirect=aHR0cDovL2FwcC5hcHBzZmx5ZXIuY29tL2NvbS5haWNvbWJvLm9sOVlpbj9waWQ9dXJhZF9pbnQmYz1waWMwMSZjbGlja2lkPXtjbGlja2lkfSZhZl9zaXRlaWQ9e2FuaWR9JmFmX3N1YjE9e2FjaWR9&ad_network=5a623a15d7aad95453fad9854da503d5&adcolony_appid=[PRODUCT_ID]&advertising_id=[GOOGLE_AD_ID]";
		str2 = "http://adtracker.urad.com.tw/appsflyer?campaign=718&redirect=aHR0cHM6Ly9hcHAuYXBwc2ZseWVyLmNvbS9pZDEwMzI3MTQxMDc/cGlkPXVyYWRfaW50JmNsaWNraWQ9e2NsaWNraWR9JmFmX3NpdGVpZD17YW5pZH0mYWZfc3ViMT17YWNpZH0=&ad_network=5a623a15d7aad95453fad9854da503d5&adcolony_appid=1032714107&idfa=[IDFA]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("adcolony_appid=product_id and advertising_id=device_id");

		break;

	case 'deepforest':
		$("#comp_track_title").html("Deepforest Media");

		str1 = "https://ad.dpclk.com/dfad/click?promotion_unit_id=aV2tNlPN&network_id=adcolony&mac_sha1=[MAC_SHA1]&open_udid=[OPEN_UDID]&odin=[ODIN1]&android_id_sha1=[SHA1_ANDROID_ID]&device_id_sha1=[SHA1_IMEI]&platform=[PLATFORM]&adc_version=[ADC_VERSION]&os_version=[OS_VERSION]&device_model=[DEVICE_MODEL]&network_type=[NETWORK_TYPE]&language=[LANGUAGE]&timestamp=[SERVE_TIME]&dma_code=[DMA_CODE]&country_code=[COUNTRY_CODE]&zone_type=[ZONE_TYPE]&pub_id=[PUBLISHER_ID]&buy_id=XE3cxI12&google_aid=[GOOGLE_AD_ID]";
		str2 = "https://ad.dpclk.com/dfad/click?promotion_unit_id=TqQW2Usv&network_id=adcolony&ios_ifa=[IDFA]&mac_sha1=[MAC_SHA1]&open_udid=[OPEN_UDID]&odin=[ODIN1]&platform=[PLATFORM]&adc_version=[ADC_VERSION]&os_version=[OS_VERSION]&device_model=[DEVICE_MODEL]&network_type=[NETWORK_TYPE]&language=[LANGUAGE]&timestamp=[SERVE_TIME]&dma_code=[DMA_CODE]&country_code=[COUNTRY_CODE]&zone_type=[ZONE_TYPE]&pub_id=[PUBLISHER_ID]&buy_id=mpaJWscc";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		break;

	case 'apptizer':
		$("#comp_track_title").html("Apptizer");

		str1 = "http://tr.apptizer.jp/ad/p/r?_site=1145&_article=3913&_link=1411649&_image=1411649&gaid=[GOOGLE_AD_ID]";
		str2 = "http://tr.apptizer.jp/ad/p/r?_site=1144&_article=3912&_link=1411648&_image=1411648&idfa=[IDFA]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		break;

	case 'aarki':
		$("#comp_track_title").html("Aarki");

		str1 = "http://ar.aarki.net/rd?src=26E44EEB0368E09AAA&ofr=828B74DC0CF4671BOU&advertising_id=[GOOGLE_AD_ID]&site_id=[APP_ID]";
		str2 = "http://ar.aarki.net/rd?src=00A9CCB6EA26C939AA&ofr=9E0448AB7D78CB25OU&advertising_id=[IDFA]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("goal ID should have the format 'id<appid>_aarki'");

		break;

	case 'glispa':
		$("#comp_track_title").html("Glispa");

		str1 = "http://ads.glispa.com/sw/229620/CD43268/[SERVE_TIME]&placement=[APP_ID]&subid5=[GOOGLE_AD_ID]";
		str2 = "http://ads.glispa.com/sw/229914/CD43268/[SERVE_TIME]&placement=[APP_ID]&subid5=[IDFA]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		break;

	case '.games':
		$("#comp_track_title").html(".Games");

		str1 = "https://dotgames.info/spots/PdhKJ7LZoWNoAB2e?partner=adcolony&product_id=[PRODUCT_ID]&google_ad_id=[GOOGLE_AD_ID]";
		str2 = "https://dotgames.info/spots/PdhKJ7LZoWNoAB2e?partner=adcolony&product_id=[PRODUCT_ID]&raw_advertising_id=[IDFA]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();

		break;

	case 'plunware':
		$("#comp_track_title").html("Plunware");

		str1 = "http://tapit.go2cloud.org/aff_c?offer_id=8705&aff_id=1105&aff_sub=[APP_ID]&aff_sub4=[GOOGLE_AD_ID]&aff_sub2=com.ubercab";
		str2 = "http://tapit.go2cloud.org/aff_c?offer_id=8703&aff_id=1105&aff_sub=[APP_ID]&aff_sub4=[IDFA]&aff_sub2=368677368";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("aff_sub4=device_id and aff_sub2=product_id");

		break;

	case 'smaad':
		$("#comp_track_title").html("Smaad");

		str1 = "https://tr.smaad.net/redirect?zo=634715417&ad=292831523&d=711cf36b7c5de7037dcf8dbab526823f56543c6a329753a917bf36a37b199796&api_key=KRPEEMsgwupXeKt54yYZz3MBAMKwQqH5&product_id=info.fine_talk&idfa=[IDFA]&advertising_id=[GOOGLE_AD_ID]";
		str2 = "https://tr.smaad.net/redirect?zo=634715417&ad=515569182&d=21ac9ad3824854a976a332c76121c53d61799eca1af8f9e19d69b9054f504b10&product_id=995910356&idfa=[IDFA]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		break;

	case 'dauup':
		$("#comp_track_title").html("DauUp");

		str1 = "https://app.appsflyer.com/com.netmarble.gholdem?af_prt=dauup&pid=dauupnw_int&c=DAUUP_T1&clickid=[MAC_SHA1]&af_siteid=1070&advertising_id=[GOOGLE_AD_ID]&af_sub1=[GOOGLE_AD_ID]&af_sub2=4646&af_sub3=[APP_ID]&app_id=[PRODUCT_ID]";
		str2 = "https://app.appsflyer.com/com.netmarble.gholdem?af_prt=dauup&pid=dauupnw_int&c=DAUUP_T1&clickid=[MAC_SHA1]&af_siteid=1070&advertising_id=[GOOGLE_AD_ID]&af_sub1=[GOOGLE_AD_ID]&af_sub2=4646&af_sub3=[APP_ID]&app_id=[PRODUCT_ID]&af_sub4=impression";
		str3 = "https://app.appsflyer.com/id979504819?af_prt=dauup&pid=dauupnw_int&c=DAUUP_T1&clickid=0&af_siteid=1070&idfa=[IDFA]&af_sub1=[IDFA]&af_sub2=4644&af_sub3=[APP_ID]&app_id=[PRODUCT_ID]";
		str4 = "https://app.appsflyer.com/id979504819?af_prt=dauup&pid=dauupnw_int&c=DAUUP_T1&clickid=0&af_siteid=1070&idfa=[IDFA]&af_sub1=[IDFA]&af_sub2=4644&af_sub3=[APP_ID]&app_id=[PRODUCT_ID]&af_sub4=impression";

		$("#comp_texta3").val(str1);
		$("#comp_texta4").val(str2);
		$("#comp_texta1").val(str3);
		$("#comp_texta2").val(str4);

		$("#comp_and_cl_cont").show();
		$("#comp_and_imp_cont").show();
		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		//$("#comp_notes_cont").show();
		//$("#comp_notes_div").html("ios_ifa=[IDFA] (iOS) unid=[GOOGLE_AD_ID] (Android)");

		break;

	case 'lionmobi':
		$("#comp_track_title").html("Lionmobi");

		str1 = "https://play.google.com/store/apps/details?id=com.lionmobi.powerclean&referrer=channel%3Dadcolony%26post_back_id%3D[GOOGLE_AD_ID]";

		$("#comp_texta3").val(str1);

		$("#comp_and_cl_cont").show();

		$("#hc_and_cl").show();

		break;

	case 'nswitch':
		$("#comp_track_title").html("Nswitch");

		str1 = "http://inter-nswitch.nasmob.com/click?nsw_camp_id=503&nsw_media_id=50&product_id=[PRODUCT_ID]&google_ad_id=[GOOGLE_AD_ID]";

		$("#comp_texta3").val(str1);

		$("#comp_and_cl_cont").show();

		$("#hc_and_cl").show();

		break;

	case 'adaction':
		$("#comp_track_title").html("AdAction");

		str1 = "http://tracking.adactioninteractive.com/aff_c?offer_id=4114&aff_id=2118&google_aid=[GOOGLE_AD_ID]&aff_sub5=[AD_CREATIVE_NAME]&aff_sub=[PRODUCT_ID]&source=[APP_ID]";
		str2 = "http://tracking.adactioninteractive.com/aff_c?offer_id=9559&aff_id=2118&ios_ifa=[IDFA]&aff_sub5=[AD_CREATIVE_NAME]&aff_sub=284910350&source=[APP_ID]";

		$("#comp_texta3").val(str1);
		$("#comp_texta1").val(str2);

		$("#comp_and_cl_cont").show();
		$("#comp_ios_cl_cont").show();

		$("#hc_and_cl").show();
		$("#hc_ios_cl").show();

		break;

	case 'clickky':
		$("#comp_track_title").html("Clickky");

		str1 = "https://cpactions.com/api/v1.0/clk/track/proxy?ad_id=2193255&site_id=12931";

		$("#comp_texta3").val(str1);

		$("#comp_and_cl_cont").show();

		break;

	case 'mediamob':
		$("#comp_track_title").html("Mediamob");

		str1 = "https://trace.mediamob.cn/tracking/t?sourceCode=45&rep=62&stId=41&idfa=[IDFA]&canalId=[APP_ID]&appId=[PRODUCT_ID]&location=[COUNTRY_CODE]&ip=[IP_ADDRESS]&clickid=[CLICK_ID]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		break;

	case 'affle':
		$("#comp_track_title").html("Affle");

		str1 = "http://click.affle.co/global/click.php?af_cid=1462868247&af_pid=23343&af_pageid=22950&product_id=[PRODUCT_ID]&af_advertising_id=[IDFA]&app_id=[APP_ID]&raw_advertising_id=[IDFA]&api_key=kWY5NexbS7U4aKWMFq7PbPxpwy26ykSj";
		str2 = "http://click.affle.co/global/click.php?af_cid=1459762924&af_pid=16025&af_pageid=22288&product_id=[PRODUCT_ID]&af_advertising_id=[GOOGLE_AD_ID]&app_id=[APP_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta2").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();

		break;

	case 'adstore':
		$("#comp_track_title").html("Adstore");

		str1 = "http://a1.adstore.jp/app/click?k=feqTwH&idfa=[IDFA]&pubid=[APP_ID]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#hc_ios_cl").show();

		break;

	case 'flurry':
		$("#comp_track_title").html("Flurry");

		str1 = "https://ad.apps.fm/p40PAWVODYNiKrDDfYB4p_E7og6fuV2oOMeOQdRqrE1ojLKTQI29FN4Vbd2LmB3OfUIfpf9ZrDKSrawWTWLvcNr40zlxO_eaNuvNMdsBXhIcJ00KE2Pk4h0K0aJ1EXWX?raw_advertising_id=[IDFA]&product_id=932493382";
		str2 = "https://play.google.com/store/apps/details?id=com.revolut.revolut&referrer=utm_source%3Dadcolony%26utm_medium%3Dmobilevideocpi%26utm_campaign%3Dmobilevideo2016%26anid%3Dadcolony";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("****do not pass competitive data");

		break;

	case 'altrooz':
		$("#comp_track_title").html("Altrooz");

		str1 = "http://tracking.altrooz.com/mobile/click?c=3736&s=69&cp=video&p=[APP_ID]&idfa=[IDFA]";
		str2 = "http://tracking.altrooz.com/mobile/click?c=3166&s=69&cp=video&p=[APP_ID]&googleaid=[GOOGLE_AD_ID]&androidid=[SHA1_ANDROID_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		break;

	case 'webmedia':
		$("#comp_track_title").html("Webmedia");

		str1 = "http://wmadv.go2cloud.org/aff_c?offer_id=2494&aff_id=1716&aff_sub={google_ad_id}&source=[PUBLISHER_ID]&android_id=[GOOGLE_AD_ID]&ios_ifa=[IDFA]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#hc_ios_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("aff_sub=device_id");

		break;

	case 'tenjin':
		$("#comp_track_title").html("Tenjin");

		str1 = "https://track.tenjin.io/v0/click/fJzZRKqx9IS5fFQ1uUIC9O?advertising_id=[IDFA]&site_id=[APP_ID]&remote_click_id=[CLICK_ID]&ip_address=[IP_ADDRESS]&user_agent=[USER_AGENT_MOZILLA]";
		str2 = "https://track.tenjin.io/v0/impression/fJzZRKqx9IS5fFQ1uUIC9O?advertising_id=[IDFA]&site_id=[APP_ID]&remote_click_id=[CLICK_ID]&ip_address=[IP_ADDRESS]&user_agent=[USER_AGENT_MOZILLA]";
		str3 = "https://track.tenjin.io/v0/click/hE1iqN3mQey0K9Ze1F94iF?advertising_id=[GOOGLE_AD_ID]&site_id=[APP_ID]&remote_click_id=[CLICK_ID]&ip_address=[IP_ADDRESS]&user_agent=[USER_AGENT_MOZILLA]";
		str4 = "https://track.tenjin.io/v0/impression/hE1iqN3mQey0K9Ze1F94iF?advertising_id=[GOOGLE_AD_ID]&site_id=[APP_ID]&remote_click_id=[CLICK_ID]&ip_address=[IP_ADDRESS]&user_agent=[USER_AGENT_MOZILLA]";

		$("#comp_texta1").val(str1);
		$("#comp_texta2").val(str2);
		$("#comp_texta3").val(str3);
		$("#comp_texta4").val(str4);

		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();
		$("#comp_and_cl_cont").show();
		$("#comp_and_imp_cont").show();
		
		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("<a class='blue' href='tenjin_test.pdf' target='_blank'>Tenjin testing instructions</a> (from Tenjin)");

		break;

	case 'happy':
		$("#comp_track_title").html("Happy Elements");

		str1 = "http://adcall.happyelements.com/call?ukey=langyabang_ioscn_prod&adver=adcolony&srcid=359&product_id=939493542&idfa=[IDFA]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#hc_ios_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("product_id needs hardcoding");

		break;

	case 'firebase':
		$("#comp_track_title").html("Firebase");

		str1 = "https://app-measurement.com/redirect?gmpaid=1:1051489540849:ios:40a019052404dd58&anid=adcolony&idfa=[IDFA]&cm=video&cn=[AD_CAMPAIGN_NAME]&cs=AdColony&url=https%3A%2F%2Fitunes.apple.com%2Fus%2Fapp%2Fid1105834924";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#hc_ios_cl").show();

		break;

	case 'spoint':
		$("#comp_track_title").html("Social Point");

		str1 = "http://socialpoint15.go2cloud.org/aff_c?offer_id=109&aff_id=1009&aff_sub=[PRODUCT_ID]&aff_sub2=6dBaBBwSSzJNRDZ8EN5CCPrmnWKTHYFu&aff_sub3=[APP_ID]&aff_sub4=[IDFA]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("aff_sub=product_id and aff_sub2=api_key and aff_sub4=device_id");

		break;

	case 'hasoffers':
		$("#comp_track_title").html("Hasoffers");

		str1 = "https://launch1.co/serve?action=click&publisher_id=184123&site_id=108642&agency_id=184&my_ad=test&ios_ifa=[IDFA]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#hc_ios_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("cid=product_id and ifa=device_id");

		break;

	case 'bluetrack':
		$("#comp_track_title").html("Bluetrack");

		str1 = "http://clicks.bluetrackmedia.com/cclick.php?creative=248545&campaign=31369&affiliate=7605&sid=[IDFA]&sid3=[PRODUCT_ID]";
		str2 = "http://clicks.bluetrackmedia.com/cclick.php?affiliate=7605&campaign=24972&sid=[GOOGLE_AD_ID]&sid2=8UDvtBydfyEuB6ETqAjb5XcbnnXzp2Kz&sid3=[PRODUCT_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();

		break;

	case 'hypergrowth':
		$("#comp_track_title").html("Hypergrowth");

		str1 = "https://app.appsflyer.com/id1108305861?pid=hypergrowthhq_int&af_click_lookback=7d&c=hg-ios-au-adc&af_siteid=57a2f6c75b3776.36847671&clickid=[MAC_SHA1]&idfa=[IDFA]&app_id=[PRODUCT_ID]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#hc_ios_cl").show();

		break;

	case 'microfan':
		$("#comp_track_title").html("Microfan");

		str1 = "http://da.microfunplus.cn/mfp_bl/adColony/click?idfa=[IDFA]&platform=iOS&prodid=897447840&siteid=[APP_ID]&ip=[IP_ADDRESS]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		break;

	case 'wisetracker':
		$("#comp_track_title").html("Wisetracker");

		str1 = "http://cdn.wisetracker.co.kr/wa/wiseAdt.do?_wtno=704&_wtpkg=_N_&_wtufn=ALL&_wtus=N&_wthst=trk.hnsmall.wisetracker.co.kr&_wts=P1472096140379&_wtc=C1478521418236&_wtm=AT0003&_wtw=DA&_wtdl=https%3A%2F%2Fitunes.apple.com%2Fkr%2Fapp%2Fid687619460&_wtp=1&_wtckp=1440&_wtland=https%3A%2F%2Fitunes.apple.com%2Fkr%2Fapp%2Fid687619460&_wtcntr=&_wtadpr=&_wtlngt=&_wtal=hnsmallapp%3A%2F%2Fm.hnsmall.com%2Fevent%2Fview%2F201501%2Fa1%3Fchannel_code%3D20552%26utm_source%3Dadcolony%26utm_medium%3Dbanner%26utm_content%3DTVC%26utm_campaign%3Dvideo_1610&_wtcid=[CLICK_ID]&_wtgpid=[GOOGLE_AD_ID]&_wtidfa=[IDFA]&_wtpst=adcolony";
		str2 = "http://cdn.wisetracker.co.kr/wa/wiseAdt.do?_wtno=703&_wtpkg=com.hnsmall&_wtufn=ALL&_wtus=N&_wthst=trk.hnsmall.wisetracker.co.kr&_wts=P1472096140379&_wtc=C1472445899667&_wtdl=market%3A%2F%2Fdetails%3Fid%3Dcom.hnsmall&_wtp=1&_wtckp=1440&_wtal=hnsmallapp%3A%2F%2Fm.hnsmall.com%2Fgoods%2Fview%2F12365874%3Fchannel_code%3D20533&_wtcid=[CLICK_ID]&_wtgpid=[GOOGLE_AD_ID]&_wtidfa=[IDFA]&_wtpst=adcolony";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();

		break;

	case 'master':
		$("#comp_track_title").html("Master of Times");

		str1 = "https://trace.mediamob.cn/tracking/t?sourceCode=45&rep=63&stId=42&idfa=[IDFA]&canalId=[APP_ID]&appId=[PRODUCT_ID]&location=[COUNTRY_CODE]&ip=[IP_ADDRESS]&clickid=[CLICK_ID]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		break;

	case 'youmi2':
		$("#comp_track_title").html("Youmi2");

		str1 = "http://af.api.youmi.net/ios/v1/recv?s=571997dc5b4wwwUm222mA9LdZjyytJqxqxi&ifa=[IDFA]&ip=[IP_ADDRESS]&clickid=[CLICK_ID] ";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		break;

	case 'longtu':
		$("#comp_track_title").html("Longtu");

		str1 = "http://p.longtugame.com/apimob/colonyAd?idfa=[IDFA]&product_id=[PRODUCT_ID]&domain=sks&click_id=[CLICK_ID]&ad_id=[APP_ID]&union_id=11034";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		break;

	case 'oupeng':
		$("#comp_track_title").html("Oupeng");

		str1 = "http://t.adbxb.cn/clk/adcolony?product_id=[PRODUCT_ID]&idfa=[IDFA]&gaid=[GOOGLE_AD_ID]&clk_id=[CLICK_ID]&app_id=[APP_ID]&publisher_id=[PUBLISHER_ID]&campaign_id=[RAW_AD_CAMPAIGN_ID]&campaign_name=[AD_CAMPAIGN_NAME]&ad_group_id=[RAW_AD_GROUP_ID]&ad_group_name=[AD_GROUP_NAME]&creative_id=[RAW_AD_CREATIVE_ID]&creative_name=[AD_CREATIVE_NAME]&os=[PLATFORM]&os_version=[OS_VERSION]&model=[DEVICE_MODEL]&country=[COUNTRY_CODE]&ip=[IP_ADDRESS]&language=[LANGUAGE]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		break;

	case 'chance':
		$("#comp_track_title").html("Chance");

		str1 = "http://www.adsensor.org/track/537062/3B959E123A81326E2E4DF20DBC52BE20?campaignId=znhMLWJIX63g&os=iOS&s2s=1&product_id=997636530&idfa=[IDFA]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		break;

	case 'art':
		$("#comp_track_title").html("ART");

		str1 = "http://art.d2c.ne.jp/ad/p/r?_site=433&_app=406&_article=3214&_link=29130&_image=29301&sad=653542050&_deviceid=[IDFA] ";
		str2 = "http://art.d2c.ne.jp/ad/p/r?_site=433&_app=397&_article=3180&_link=29088&_image=29259&sad=biz.fields.pachinkomonster&_deviceid=[GOOGLE_AD_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("sad=product_id and _device_id=device_id");

		break;

	case 'simpysam':
		$("#comp_track_title").html("Simpysam");

		str1 = "http://app.simpysam.com/?id=1065845844&pid=adcolony&c=WA_Global&clickid=[MAC_SHA1]&idfa=[IDFA]&app_id=1065845844";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#hc_ios_cl").show();

		break;

	case 'adsage':
		$("#comp_track_title").html("Adsage");

		str1 = "http://track.adsage.com/trc/ptrac/x.gif?ver=T01&slpg=9CRX71&trctype=1&orderid=191&ordertype=outside&channel=206&appid=1059976078&idfa=[IDFA]&clientip=[IP_ADDRESS]&from=client ";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#hc_ios_cl").show();

		break;

	case 'accesstrade':
		$("#comp_track_title").html("Accesstrade");

		str1 = "http://h.accesstrade.net/sp/cc?rk=0100jizf00f31d&option=949352161&add=[IDFA] ";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#hc_ios_cl").show();

		break;

	case 'adstrack':
		$("#comp_track_title").html("Adstrack");

		str1 = "http://mt.adstrck.com/aff_c?offer_id=566&aff_id=1076&aff_sub=[IDFA]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#hc_ios_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("aff_sub=device_id");

		break;

	case 'massive':
		$("#comp_track_title").html("Massiveimpact");

		str1 = "http://tracking.affilimob.com/aff_c?ms=205&offer_id=6856&aff_id=1&ad=460096&prod_id=1004862413&idfa=[IDFA]&pubid=[APP_ID]";
		str2 = "http://tracking.affilimob.com/aff_c?ms=205&offer_id=6802&aff_id=1&ad=460088&prod_id=[PRODUCT_ID]&idfa=[IDFA]&gaid=[GOOGLE_AD_ID]&pubid=[PUBLISHER_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();

		break;

	case 'youappi':
		$("#comp_track_title").html("Youappi");

		str1 = "http://track.56txs4.com/app/directlink?accesstoken=9d8c2ede-aae6-4d8d-94bf-58d75fd86d17&appid=39691&campaignpartnerid=97220&subid=[PRODUCT_ID]&publishertoken=[APP_ID]&publishername=&usertoken=&deviceAndroidId=[GOOGLE_AD_ID]&deviceIfa=[IDFA]";
		str2 = "http://track.56txs4.com/app/directlink?accesstoken=9d8c2ede-aae6-4d8d-94bf-58d75fd86d17&appid=64285&campaignpartnerid=97215&subid=[PRODUCT_ID]&subid2=[GOOGLE_AD_ID]&publishertoken=[APP_ID]&deviceAndroidId=[GOOGLE_AD_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta3").val(str2);

		$("#comp_ios_cl_cont").show();
		$("#comp_and_cl_cont").show();

		$("#hc_ios_cl").show();
		$("#hc_and_cl").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("subid=product_id");

		break;

	case 'zadsmobile':
		$("#comp_track_title").html("Zadsmobile");

		str1 = "http://zadsmobilellc.go2cloud.org/aff_c?offer_id=13&aff_id=5&aff_sub=993999961&aff_sub2=[IDFA]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#hc_ios_cl").show();

		break;

	case 'opera':
		$("#comp_track_title").html("Opera");

		str1 = "http://www.mediamob.in/?a=21k82&b=ktiv6tm";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("s2=device_id");

		break;

	case 'applift':
		$("#comp_track_title").html("Applift");

		str1 = "http://tracking.applift.com/aff_c?offer_id=13893&aff_id=6382&aff_sub=xxxtransidxxx&aff_sub2=xxxpubidxxx";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("ios_ifa=device_id");

		break;

	case 'hunt':
		$("#comp_track_title").html("Hunt Mobile");

		str1 = "http://interactions.huntmad.com/interactions/?hm_campaign=34688&hm_creative=46628&hm_advertiser_id=2179&hm_token=669151455&hm_cid=xxxtransidxxx&hm_custom5=xxxgoogleadidxxx&hm_custom4=xxxidfaxxx&hm_sid=xxxpubidxxx&hm_datetime=xxxtimestampxxx&hm_format=REDIRECT&hm_type=click&hm_fid=81&hm_sourcename=Offer+Manager+OR";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		break;

	case 'icon':
		$("#comp_track_title").html("Icon Peak");

		str1 = "http://ads.iconpeak.com/aff_c?offer_id=10780&aff_id=3101&aff_sub=xxxtransidxxx&aff_sub2=xxxpubidxxx";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		break;

	case 'ironsource':
		$("#comp_track_title").html("Ironsource");

		str1 = "http://ironsource.go2cloud.org/aff_c?offer_id=10116&aff_id=1461&aff_sub=xxxtransidxxx&aff_sub4=xxxpubidxxx_xxxxsubidxxx&device_id=xxxgoogleadidxxx";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		$("#comp_notes_cont").show();
		$("#comp_notes_div").html("&deviceIds%5BAID%5D=[GOOGLE_AD_ID] for Android &deviceIds%5BIFA%5D=[IDFA] for iOS");

		break;

	case 'tapsense':
		$("#comp_track_title").html("TapSense");

		str1 = "http://c.tapsense.com/c/track/adcolony/521c0e41e4b0661979161a7c?cid=ae677a394a&creative_name=TestAds_320x480_1&idfa=[IDFA]&odin=[ODIN1]&sha1_imei=[SHA1_IMEI]&sha1_android_id=[SHA1_ANDROID_ID]&ext_click_id={ext_click_id}&app_id=[PUBLISHER_ID]";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		break;

	case 'mdotm':
		$("#comp_track_title").html("MdotM");

		str1 = "http://ads.mdotm.com/ads/c.php?uc=63348&c=21b9dc42&utm_source=adcolony&odin=[ODIN1]&aid=[IDFA]&aid_type=raw";

		$("#comp_texta1").val(str1);

		$("#comp_ios_cl_cont").show();

		break;

	case 'mail.ru':
		$("#comp_track_title").html("Mail.ru");

		str1 = "http://1l-s2s.mail.ru/s2s/adid/2197207_1/sipn/source/pid/101320/pof/1/f/3/?_1lpb_id=1642&idfa=[IDFA]&_1larg_sub=[APP_ID]";
		str2 = "http://1l-s2s.mail.ru/s2s/adid/2197207_1/sipn/source/pid/101320/pof/1/f/3/?_1lpb_id=1642&idfa=[IDFA]&_1larg_sub=[APP_ID] ";
		str3 = "http://1l-s2s.mail.ru/s2s/adid/2213215_1/sipn/source/pid/101627/pof/1/f/3/?_1lpb_id=1480&gaid=[GOOGLE_AD_ID]&_1larg_sub=[APP_ID]";
		str4 = "http://1l-s2s.mail.ru/v/adid/2213215_1/sipn/source/pid/101627/pof/1/f/3/?_1lpb_id=1480&gaid=[GOOGLE_AD_ID]&_1larg_sub=[APP_ID]";

		$("#comp_texta1").val(str1);
		$("#comp_texta2").val(str2);
		$("#comp_texta3").val(str3);
		$("#comp_texta4").val(str4);

		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();
		$("#comp_and_cl_cont").show();
		$("#comp_and_imp_cont").show();

		break;

	case 'growmobile':
		$("#comp_track_title").html("Growmobile");

		str1 = "http://click.growmobile.com/b/?a=414&c=4799&ts=40&as=7256&GF=0&tsd_idfa=[IDFA]&tsd_cr=[AD_CREATIVE_NAME]&tsd_pubid=[APP_ID]&tsd_tsm=[SERVE_TIME]";
		str2 = "http://imp.growmobile.com/i/imp_pixel.png?a=414&c=4799&ts=40&as=7256&GF=0&tsd_idfa=[IDFA]&tsd_cr=[AD_CREATIVE_NAME]&tsd_pubid=[APP_ID]&tsd_tsm=[SERVE_TIME]";
		str3 = "http://click.growmobile.com/a/?id=com.orbitz&a=421&c=4936&ts=40&as=7459&GF=0&tsd_advid=[GOOGLE_AD_ID]&tsd_cr=[AD_CREATIVE_NAME]&tsd_pubid=[APP_ID]&tsd_tsm=[SERVE_TIME]";
		str4 = "http://imp.growmobile.com/i/imp_pixel.png?a=421&c=4936&ts=40&as=7459&GF=0&tsd_advid=[GOOGLE_AD_ID]&tsd_cr=[AD_CREATIVE_NAME]&tsd_pubid=[APP_ID]&tsd_tsm=[SERVE_TIME]";

		$("#comp_texta1").val(str1);
		$("#comp_texta2").val(str2);
		$("#comp_texta3").val(str3);
		$("#comp_texta4").val(str4);

		$("#comp_ios_cl_cont").show();
		$("#comp_ios_imp_cont").show();
		$("#comp_and_cl_cont").show();
		$("#comp_and_imp_cont").show();

		var grow_notes = "1. Confirm with the account manager that the client has setup the postback.<br />";
		grow_notes += "2. Are the macros correct in the URL? Does it include IDFA or GOOGLE_AD_ID macro?<br />";
		grow_notes += "3. Set pub_id=[APP_ID] for more granularity.<br />";
		grow_notes += "4. Click on the tag and make sure that it redirects to the correct app<br />";

		$("#comp_notes_div").html(grow_notes);
		$("#comp_notes_cont").show();

		break;

	}
}
compareURLs1_val = function() {

	if (select_link()) {
		var comp_texta = "#" + select_link();
		var comp_str = $(comp_texta).val();
	} else {
		$("#input_url2").val("Please select one URL from the examples above.");
	}

	$("#input_url2").val(comp_str);
	$("#geturl2").addClass("geturl2_visible");
	compareURLs1();

	
		$("#redirectdiv").show();
		$("#redirectdiv2").show();
		var url = document.getElementById("input_url").value;
		redirectURL(url, redirectdiv);
		var url2 = document.getElementById("input_url2").value;
		redirectURL(url2, redirectdiv2);
}
compareURLs2_val = function() {
	if (select_link()) {
		var comp_texta = "#" + select_link();
		var comp_str = $(comp_texta).val();
	} else {
		$("#input_url2").val("Please select one URL from the examples above.");
	}

	$("#input_url2").val(comp_str);
	$("#geturl2").addClass("geturl2_visible");
	compareURLs2();
}

$(document).ready(function() {

	$("#comp_texta1").val(str1);
	$("#comp_texta2").val(str2);
	$("#comp_texta3").val(str3);
	$("#comp_texta4").val(str4);

	var gl_notes1 = "1. Confirm with the account manager that the client has setup the postback.<br />";
	gl_notes1 += "2. Are the macros correct in the URL? Does it include IDFA or GOOGLE_AD_ID macro?<br />";

	$("#comp_notes_div").html(gl_notes1);
	$("#comp_notes_cont").show();

	divToggle(true, trchtitle, trchdiv, trchdown, trchup);
	divToggle(false, trchtitle2, trchdiv2, trchdown2, trchup2);
});
