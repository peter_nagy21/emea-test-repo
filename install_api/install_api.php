<?php
include ("http://localhost:8888/password_protect.php");
?>

<?php
include '../header.php';
?>

<?php
include '../sideleft.php';
?>

<?php include_once("analyticstracking.php") ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
	$(function() {
		$("#start_date").datepicker();
	}); 
</script>
<script>
	$(function() {
		$("#end_date").datepicker();
	}); 
</script>

<script type="text/javascript" src="install_api.js"></script>

<div id="maincontent" class="comp_content">

	<h1>Install API page</h1>
	
	<h3 id="notestitle" class="black plink">1. Notes <i class="fa fa-arrow-down" id="notesdown"></i><i class="fa fa-arrow-up" id="notesup"></i></h3>
	<div id="notesdiv">
		The	use	of the Attributed Installs API is intended for advertisers running campaigns on AdColony Video Ad Network to retrieve 
		device level information of	attributed installs	via	an API. The API uses HTTP GET to make reporting requests to	the	server and retrieve results. 
		<br />
		<br />
		The Install API documentation can be downloaded form <a class="blue" href="install_api.pdf" target="_blank">here</a>.
		<br />
		<br />
	</div>

	<h3 id="bapititle" class="black plink">2. Build the URL <i class="fa fa-arrow-down" id="bapidown"></i><i class="fa fa-arrow-up" id="bapiup"></i></h3>
	<div id="bapidiv">
		<form id="bapiform">

			<table class="pietable">
				<tr>
					<td>Base URL:</td>
					<td colspan="3">https://clients.adcolony.com/api/v2/install</td>
				</tr>
				<tr>
					<td>API key (user_credentials):</td>
					<td>
					<input type="text" name="api_key" id="api_key">
					</td>
					<td class="red_td">*required</td>
					<td><a href="https://ops.adcolony.com/admin/master_account_list" target="_blank" class="blue">Search for the API key in AdColony dashboard.</a></td>
				</tr>
				<tr>
					<td>Start date / Date:</td>
					<td>
					<input type="text" name="start_date" id="start_date">
					</td>
					<td colspan="2" class="red_td">*required</td>
				</tr>
				<tr>
					<td>End date:</td>
					<td>
					<input type="text" name="end_date" id="end_date">
					</td>
					<td colspan="2">*optional</td>
				</tr>
				<tr>
					<td>Campaign ID:</td>
					<td>
					<input type="text" name="campaign_id" id="campaign_id">
					</td>
					<td colspan="2">*optional, can support multiple comma separated values</td>
				</tr>
				<tr>
					<td>Store ID:</td>
					<td>
					<input type="text" name="store_id" id="store_id">
					</td>
					<td colspan="2">*optional, can support multiple comma separated values</td>
				</tr>
				<tr>
					<td>Format:</td>
					<td>
					<input type="radio" name="format" id="json" value="json">
					json
					<input type="radio" name="format" id="csv" value="csv" checked="checked">
					csv
					<input type="radio" name="format" id="xml" value="xml">
					xml</td>
					<td colspan="2">*optional, default: json, however it's set to csv here...</td>
				</tr>
			</table>
			<br />
			<input type="button" class="btn-class" name="buildURL" id="buildURL" value="Build the URL!" onclick="build_url()">
		</form>
		

		<br />
		<div id="alertdiv"></div>
		<div id="urldiv"></div>
		<br />
		<br />
		<br />

	</div>
	
	<h3 class="black plink" id="hiddentitle"> &nbsp;<i class="fa fa-arrow-down" id="hiddendown" style="display: none !important;"></i><i class="fa fa-arrow-up" id="hiddenup" style="display: none !important;"></i></h3>
	<div id="hiddendiv">
		Peter test campaign API key: W0KYONhCiK0usqvGFy1B
	</div>

</div>

<?php
include '../footer.php';
?>

