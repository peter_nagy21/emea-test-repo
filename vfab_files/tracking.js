divToggle = function(init, tlink, tdiv, ard, aru) {
	var isOpen = init;
	if (isOpen) {
		$(tdiv).show();
		$(aru).show();
		$(ard).hide();
	} else {
		$(tdiv).hide();
		$(aru).hide();
		$(ard).show();
	}
	$(tlink).click(function() {
		if (isOpen) {
			$(ard).show();
			$(aru).hide();
			isOpen = false;
		} else {
			$(ard).hide();
			$(aru).show();
			isOpen = true;
		}
		$(tdiv).slideToggle({
			duration : 400,
		});
	});
}
splitURL = function(string, div_id) {
	var res1 = string.split("?");
	var base_url = res1[0];
	var params_string = res1[1];
	var params = params_string.split("&");
	var params2 = [];
	var tablestr = "<span style='font-weight: bold;'>Base URL: </span>";
	tablestr += base_url;
	tablestr += "<br />";
	tablestr += "<table style='margin-top: 5px;'><tr><td class='tabletitle'>Parameters</td><td class='tabletitle'>Macros/values</td></tr>";

	for (var i = 0; i < params.length; i++) {
		var psub = params[i].split("=");
		params2[i, 0] = psub[0];
		params2[i, 1] = psub[1];
		tablestr += "<tr><td>";
		tablestr += params2[i, 0];
		tablestr += "</td><td>";
		tablestr += params2[i, 1];
		tablestr += "</td></tr>";
	}

	tablestr += "</table><br />";
	$(div_id).html(tablestr);
}
splitURLsort = function(string, div_id) {
	var res1 = string.split("?");
	var base_url = res1[0];
	var params_string = res1[1];
	var params = params_string.split("&");
	params.sort();
	var params2 = [];
	var tablestr = "<span style='font-weight: bold;'>Base URL: </span>";
	tablestr += base_url;
	tablestr += "<br />";
	tablestr += "<table style='margin-top: 5px;'><tr><td class='tabletitle'>Parameters</td><td class='tabletitle'>Macros/values</td></tr>";

	for (var i = 0; i < params.length; i++) {
		var psub = params[i].split("=");
		params2[i, 0] = psub[0];
		params2[i, 1] = psub[1];
		tablestr += "<tr><td>";
		tablestr += params2[i, 0];
		tablestr += "</td><td>";
		tablestr += params2[i, 1];
		tablestr += "</td></tr>";
	}

	tablestr += "</table><br />";
	$(div_id).html(tablestr);
}
getURLparams = function(input_id, output_id) {
	var url = document.getElementById(input_id).value;
	splitURL(url, output_id);
	$("#keydiv, #samediv").hide();
	$("#urlsplitdiv, #urlsplitdiv2").css("color","#777");
}
getURLparamsSort = function(input_id, output_id) {
	var url = document.getElementById(input_id).value;
	splitURLsort(url, output_id);
	$("#keydiv, #samediv").hide();
	$("#urlsplitdiv, #urlsplitdiv2").css("color","#777");
}

decodeURL = function(input_id,output_id) {
	var url = document.getElementById(input_id).value;
	var dec = decodeURI(url);
	$(output_id).html(dec);
}

encodeURL = function(input_id,output_id) {
	var url = document.getElementById(input_id).value;
	var dec = encodeURI(url);
	$(output_id).html(dec);
}

showURL2 = function() {
	$("#geturl2").show();
}

$(document).ready(function() {
	$("#geturl2").hide();
	$("#comp_url_toggle").click(function() {
		$("#geturl1").toggleClass("geturl1_visible");
		$("#geturl2").toggleClass("geturl2_visible");
	});
});

// validations

validate_url = function(url) {
	var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	var pattern2 = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;

	if (pattern2.test(url)) {
		return true;
	}
	return false;
}
validate_questionmarks = function(url) {
	var parts = url.split("?");
	if (parts.length > 2) {
		return false;
	}
	return true;
}
validate_macros = function(url) {

	var output = "";

	// macro array
	var macro_string = "";
	macro_string += "[DEVICE_ID],";
	macro_string += "[IDFA],";
	macro_string += "[MD5_IDFA],";
	macro_string += "[MAC_SHA1],";
	macro_string += "[ODIN1],";
	macro_string += "[OPEN_UDID],";
	macro_string += "[GOOGLE_AD_ID],";
	macro_string += "[SHA1_ANDROID_ID],";
	macro_string += "[SHA1_IMEI],";
	macro_string += "[ANDROID_ID],";
	macro_string += "[IMEI],";
	macro_string += "[RAW_UDID],";
	macro_string += "[UDID],";
	macro_string += "[PRODUCT_ID],";
	macro_string += "[APP_ID],";
	macro_string += "[RAW_AD_CAMPAIGN_ID],";
	macro_string += "[AD_CAMPAIGN_NAME],";
	macro_string += "[RAW_AD_GROUP_ID],";
	macro_string += "[AD_GROUP_NAME],";
	macro_string += "[RAW_AD_CREATIVE_ID],";
	macro_string += "[AD_CREATIVE_NAME],";
	macro_string += "[RAW_APP_ID],";
	macro_string += "[ADC_VERSION],";
	macro_string += "[OS_VERSION],";
	macro_string += "[PLATFORM],";
	macro_string += "[DEVICE_MODEL],";
	macro_string += "[DEVICE_GROUP],";
	macro_string += "[NETWORK_TYPE],";
	macro_string += "[LANGUAGE],";
	macro_string += "[SERVE_TIME],";
	macro_string += "[DMA_CODE],";
	macro_string += "[COUNTRY_CODE],";
	macro_string += "[COUNTRY_CODE_UK],";
	macro_string += "[ZONE_TYPE],";
	macro_string += "[ZONE_UUID],";
	macro_string += "[PUBLISHER_ID],";
	macro_string += "[ADCOLONY_TIMESTAMP],";
	macro_string += "[ADCOLONY_TIMESTAMP_MILLIS],";
	macro_string += "[IP_ADDRESS],";
	macro_string += "[STORE_ID],";
	macro_string += "[BID],";
	macro_string += "[BID_TYPE],";
	macro_string += "[TRANS_ID],";
	macro_string += "[USER_AGENT_MOZILLA],";
	macro_string += "[USER_AGENT],";
	macro_string += "[CLICK_ID]";

	var macro_array = macro_string.split(",");
	// console.log(macro_array);

	// url_values array

	var arr1 = url.split("?");
	var base_url = arr1[0];
	var params_string = arr1[1];
	var params = params_string.split("&");
	var url_values = [];

	for (var i = 1; i <= params.length; i++) {
		var psub = params[i - 1].split("=");
		url_values[i - 1] = psub[1];
	}
	//console.log(url_values);
	//console.log(url_values.length);

	// check values

	for (var j = 1; j <= url_values.length; j++) {
		var elem = url_values[j - 1];
		//console.log(elem + ": " + shouldbe_macro(elem) + " " + multi_macro(elem) + " not just macro: " + not_just_macro(elem));
		if (shouldbe_macro(elem)) {
			if ((multi_macro(elem)) || (not_just_macro(elem))) {
				output += (elem + " is not a simple macro, please double check it.<br />");
			} else if (!($.inArray(elem, macro_array) > -1)) {
				output += (elem + " is NOT on our supported macro list.<br />");
			}
		}
	}
	return output;
}
// if the string contains "[" or "]" we consider it as a macro (even if it's incorrect)

shouldbe_macro = function(stringm) {
	var br1 = (stringm.indexOf("[") > -1);
	var br2 = (stringm.indexOf("]") > -1);

	if (br1 || br2) {
		return true;
	}
	return false;
}
// complex value - multiply macros / incorrect macro

multi_macro = function(stringm) {
	var br = stringm.split("[");
	if (br.length > 2) {
		return true;
	}
	return false;
}
not_just_macro = function(stringm) {
	var br1 = stringm.indexOf("[");
	var br2 = stringm.indexOf("]");
	hossz = stringm.length - 1;

	if (shouldbe_macro(stringm)) {
		if ((br1 === 0) && (br2 === hossz)) {
			return false;
		}
		return true;
	}
	return false;

}

// tracking identifier

is_tune = function(string) {
	var is_tune = ((string.indexOf("hastrk3.com") > -1) || (string.indexOf("api-03.com") > -1) || (string.indexOf("measurementapi.com") > -1) || (string.indexOf("api-01.com") > -1) || (string.indexOf("api-02.com") > -1) || (string.indexOf("tlnk.io") > -1));
	return is_tune;
}

check_track = function(input, output_div) {

	var string = document.getElementById(input).value;

	tracking_provider = "";
	var tracking_provider_output = "This should be <span class='tr_prov'>";

	// important ones

	var is_tune = ((string.indexOf("hastrk3.com") > -1) || (string.indexOf("api-03.com") > -1) || (string.indexOf("measurementapi.com") > -1) || (string.indexOf("api-01.com") > -1) || (string.indexOf("api-02.com") > -1) || (string.indexOf("tlnk.io") > -1));
	var is_gameloft = (string.indexOf("gameloft.com") > -1);
	var is_king = ((string.indexOf("play.king.com") > -1) && (string.indexOf("adtrack.king.com") > -1));
	var is_wooga = (string.indexOf("woogatrack.com") > -1);
	var is_adjust = (string.indexOf("adjust.") > -1);
	var is_appsflyer = (string.indexOf("appsflyer.com") > -1);
	var is_fiksu = (string.indexOf("fiksu.com") > -1);
	var is_kochava = (string.indexOf("kochava.com") > -1);

	// others
	
	var is_addict = (string.indexOf("addict-mobile") > -1);

	var is_betterday = (string.indexOf("betterdaywireless.com") > -1);
	var is_big_blue = (string.indexOf("bbbgame.net") > -1);
	var is_check_me = (string.indexOf("pageoncetrk.com") > -1);
	var is_efun = (string.indexOf("efun.com") > -1);
	var is_fun_games = (string.indexOf("fungames-forfree.com") > -1);
	var is_pocket_gems = (string.indexOf("pocketgems") > -1);
	var is_uken = (string.indexOf("uken.com") > -1);
	var is_adcrops = (string.indexOf("adcrops.net") > -1);
	var is_adzcore = (string.indexOf("adzcore.com") > -1);
	var is_amoad = (string.indexOf("amoad.net") > -1);
	var is_adwo = (string.indexOf("adwo.com") > -1);
	var is_appflood = (string.indexOf("appflood.com") > -1);
	var is_appsperse = (string.indexOf("appsperse.com") > -1);
	var is_apptimiser = (string.indexOf("appsdk.net") > -1);
	var is_apsalar = (string.indexOf("apsalar.com") > -1);
	var is_babel = (string.indexOf("gamepanda.us") > -1);
	var is_chance = (string.indexOf("adsensor.org") > -1);
	var is_facebook = (string.indexOf("facebook.com") > -1);
	var is_flurry = (string.indexOf("apps.fm") > -1);
	var is_google = (string.indexOf("google.analytics.com") > -1);
	var is_inmobi = (string.indexOf("wadogo.go2cloud.org") > -1);
	var is_localytics = (string.indexOf("localytics") > -1);
	var is_tapstream = (string.indexOf("taps.io") > -1);
	var is_tenjin = (string.indexOf("tenjin.io") > -1);
	var is_thrive = (string.indexOf("loadinghere.com") > -1);
	var is_hasoffers = (string.indexOf("launch1.co") > -1);
	var is_yahoo_jp = (string.indexOf("yahoo.com.jp") > -1);
	var is_yozio = (string.indexOf("yozio") > -1);
	//var is_8crops = (string.indexOf("adcrops") > -1);
	var is_aarki = (string.indexOf("aarki.net") > -1);
	var is_ad4screen = (string.indexOf("apptrk.a4") > -1);
	var is_adaction = (string.indexOf("adactioninteractive.com") > -1);
	var is_adbrix = (string.indexOf("ad-brix.com") > -1);
	var is_adknowledge = (string.indexOf("adk-mobile.com") > -1);
	var is_adstore = (string.indexOf("adstore.jp") > -1);
	var is_adperio = (string.indexOf("pxlvlt2.com") > -1);
	var is_adways = (string.indexOf("pgss.jp") > -1);
	var is_adsage = (string.indexOf("adsage.com") > -1);
	var is_adstrack = (string.indexOf("adstrck.com") > -1);
	var is_alibaba = (string.indexOf("union.ucweb") > -1);
	var is_altrooz = (string.indexOf("altrooz.com") > -1);
	var is_appaba = (string.indexOf("appaba.go2cloud.org") > -1);
	var is_appcoachs = (string.indexOf("appcoachs") > -1);
	var is_appia = (string.indexOf("appia.com") > -1);
	var is_applift = (string.indexOf("applift.com") > -1);
	var is_apprupt = (string.indexOf("adfarm1.adition.com") > -1);
	var is_apptizer = (string.indexOf("apptizer.jp") > -1);
	var is_art = (string.indexOf("atr.d2c.ne.jp") > -1);
	var is_avazu = (string.indexOf("avazutracking.net") > -1);
	var is_beintoo = (string.indexOf("beintoo.net") > -1);
	var is_bluetrack = (string.indexOf("bluetrackmedia.com") > -1);
	var is_claymotion = (string.indexOf("gammo.me") > -1);
	var is_clearmob = (string.indexOf("clearmob.com") > -1);
	var is_click_rocket = (string.indexOf("track.cpatool.net") > -1);
	var is_clickky = (string.indexOf("cpactions.com") > -1);
	var is_adforce = (string.indexOf("app-adforce") > -1);
	var is_dauup = (string.indexOf("tr.sensorclick.com") > -1);
	var is_dedicated_media = (string.indexOf("smash.athinkingape") > -1);
	var is_deepforestmedia = (string.indexOf("deepforestmedia") > -1);
	var is_domob = (string.indexOf("domob.cn") > -1);
	var is_dsnr = (string.indexOf("stage.traffiliate.com") > -1);
	var is_eclipse = (string.indexOf("eclipseio") > -1);
	var is_entermate = (string.indexOf("ilovegame.co.kr") > -1);
	var is_fl_mobile = (string.indexOf("feeliu.com") > -1);
	var is_dotgames = (string.indexOf("dotgames.info") > -1);
	var is_glispa = (string.indexOf("glispa.com") > -1);
	var is_growmobile = (string.indexOf("growmobile.com") > -1);
	var is_happy_elements = (string.indexOf("happyelements.com") > -1);
	var is_hubtrack = ((string.indexOf("hubtrack.pw") > -1) || (string.indexOf("taptice.com") > -1));
	var is_hunt = (string.indexOf("huntmad.com") > -1);
	var is_icon_peak = (string.indexOf("iconpeak.com") > -1);
	var is_ironsource = (string.indexOf("ironsource.go2cloud") > -1);
	var is_jampp = (string.indexOf("jampp.") > -1);
	var is_komli = (string.indexOf("komlimobile") > -1);
	var is_lovoo = (string.indexOf("lovoo.com") > -1);
	var is_mail_ru = (string.indexOf("mail.ru") > -1);
	var is_mdotm = (string.indexOf("mdotm") > -1);
	var is_metaps = (string.indexOf("metaps") > -1);
	var is_mlt = (string.indexOf("mlttracker") > -1);
	var is_massiveimpact = (string.indexOf("tracking.affilimob") > -1);
	var is_mobils = (string.indexOf("mobils.go2cloud") > -1);
	var is_mobithink = (string.indexOf("mtafftracking") > -1);
	var is_moblin = (string.indexOf("moblin.com") > -1);
	var is_mobvista = (string.indexOf("lenzmx") > -1);
	var is_moko = (string.indexOf("tm.trackmobi.com") > -1);
	var is_motive = (string.indexOf("traktum") > -1);
	var is_mpire = (string.indexOf("mpire.nxus") > -1);
	var is_nswitch = (string.indexOf("nswitch.nasmob") > -1);
	var is_opera = (string.indexOf("offermob.me") > -1);
	var is_phunware = (string.indexOf("tapit.go2cloud") > -1);
	var is_pixonic = (string.indexOf("appmetr.com") > -1);
	var is_pocketwhale = (string.indexOf("pocket-group.com") > -1);
	var is_portable_marketing = (string.indexOf("pmad-test.com") > -1);
	var is_raftika = (string.indexOf("raftika.com") > -1);
	var is_simpysam = (string.indexOf("simpysam") > -1);
	var is_smaad = (string.indexOf("smaad") > -1);
	var is_smart_c = (string.indexOf("smart-c") > -1);
	var is_snail = (string.indexOf("woniu.com") > -1);
	var is_snakk = (string.indexOf("snkk") > -1);
	var is_social_point = (string.indexOf("socialpoint") > -1);
	var is_stratoshear = (string.indexOf("stratoshear") > -1);
	var is_sublinet = (string.indexOf("sublinet") > -1);
	var is_tapsense = (string.indexOf("tapsense") > -1);
	var is_talking_data = (string.indexOf("lnk8.cn") > -1);
	var is_tukmob2 = (string.indexOf("tukmob2") > -1);
	var is_trackingbird = (string.indexOf("trackingbird") > -1);
	var is_trademob = (string.indexOf("trademob") > -1);
	var is_urad = (string.indexOf("urad.com") > -1);
	var is_webmedia = (string.indexOf("wmadv.go2cloud.org") > -1);
	var is_yandex = (string.indexOf("yandex") > -1);
	var is_ydigital = (string.indexOf("ydigitalmedia") > -1);
	var is_youmi = (string.indexOf("adxmi.com") > -1);
	var is_youappi = ((string.indexOf("youappi.com") > -1) || (string.indexOf("tracking.adactioninteractive") > -1) || (string.indexOf("track.56txs4.com") > -1));
	var is_zennapps = (string.indexOf("zennapps.com") > -1);
	var is_zen_studios = (string.indexOf("zenstudios") > -1);
	var is_valuepotion = (string.indexOf("valuepotion") > -1);
	var is_s4m = (string.indexOf("sam4m") > -1);
	var is_seads = (string.indexOf("tc.pgss.jp") > -1);
	var is_affle = (string.indexOf("affle.co") > -1);
	var is_admaster = (string.indexOf("fw4.me") > -1);
	var is_eqsview = (string.indexOf("eqs-manage") > -1);
	var is_accesstrade = (string.indexOf("accesstrade") > -1);
	var is_42trck = (string.indexOf("42trck") > -1);

	if (is_tune) {
		tracking_provider += "Tune"
	}
	if (is_gameloft) {
		tracking_provider += "Gameloft"
	}
	if (is_king) {
		tracking_provider += "King.com"
	}
	if (is_wooga) {
		tracking_provider += "Wooga"
	}
	if (is_adjust) {
		tracking_provider += "Adjust"
	}
	if (is_addict) {
		tracking_provider += "Addict mobile"
	}
	if (is_appsflyer) {
		tracking_provider += "AppsFlyer"
	}
	if (is_fiksu) {
		tracking_provider += "Fiksu"
	}
	if (is_kochava) {
		tracking_provider += "Kochava"
	}
	if (is_betterday) {
		tracking_provider += "Betterday Wireless"
	}
	if (is_big_blue) {
		tracking_provider += "Big Blue Bubble"
	}
	if (is_check_me) {
		tracking_provider += " Check Me"
	}
	if (is_efun) {
		tracking_provider += "eFun"
	}
	if (is_fun_games) {
		tracking_provider += "Fun Games for Free"
	}
	if (is_pocket_gems) {
		tracking_provider += "Pocket Gems"
	}
	if (is_uken) {
		tracking_provider += "Uken Games"
	}
	if (is_adcrops) {
		tracking_provider += "Adcrops"
	}
	if (is_adzcore) {
		tracking_provider += "Adzcore"
	}
	if (is_amoad) {
		tracking_provider += "AmoAd"
	}
	if (is_adwo) {
		tracking_provider += "Adwo"
	}
	if (is_appflood) {
		tracking_provider += "Appflood"
	}
	if (is_appsperse) {
		tracking_provider += "Appsperse"
	}
	if (is_apptimiser) {
		tracking_provider += "Apptimiser"
	}
	if (is_apsalar) {
		tracking_provider += "Apsalar"
	}
	if (is_babel) {
		tracking_provider += "BabelTime"
	}
	if (is_chance) {
		tracking_provider += "Chance"
	}
	if (is_facebook) {
		tracking_provider += "Facebook"
	}
	if (is_flurry) {
		tracking_provider += "Flurry"
	}
	if (is_google) {
		tracking_provider += "Google Analytics"
	}
	if (is_inmobi) {
		tracking_provider += "Inmobi"
	}
	if (is_localytics) {
		tracking_provider += "Localytics"
	}
	if (is_tapstream) {
		tracking_provider += "TapStream"
	}
	if (is_tenjin) {
		tracking_provider += "Tenjin"
	}
	if (is_thrive) {
		tracking_provider += "Thrive"
	}
	if (is_hasoffers) {
		tracking_provider += "HasOffers (non-MAT)"
	}
	if (is_yahoo_jp) {
		tracking_provider += "Yahoo JP"
	}
	if (is_yozio) {
		tracking_provider += "Yozio"
	}
	if (is_aarki) {
		tracking_provider += "Aarki"
	}
	if (is_ad4screen) {
		tracking_provider += "Ad4Screen"
	}
	if (is_adaction) {
		tracking_provider += "AdAction Media"
	}
	if (is_adbrix) {
		tracking_provider += "Adbrix"
	}
	if (is_adknowledge) {
		tracking_provider += "AdKnowledge"
	}
	if (is_adstore) {
		tracking_provider += "Adstore"
	}
	if (is_adperio) {
		tracking_provider += "Adperio"
	}
	if (is_adways) {
		tracking_provider += "Adways NON-Incent"
	}
	if (is_adsage) {
		tracking_provider += "Adsage"
	}
	if (is_adstrack) {
		tracking_provider += "Adstrack"
	}
	if (is_alibaba) {
		tracking_provider += "Alibaba"
	}
	if (is_altrooz) {
		tracking_provider += "Altrooz"
	}
	if (is_appaba) {
		tracking_provider += "Appaba"
	}
	if (is_appcoachs) {
		tracking_provider += "Appcoachs"
	}
	if (is_appia) {
		tracking_provider += "Appia"
	}
	if (is_applift) {
		tracking_provider += "AdLift"
	}
	if (is_apprupt) {
		tracking_provider += "Apprupt"
	}
	if (is_apptizer) {
		tracking_provider += "Apptizer"
	}
	if (is_art) {
		tracking_provider += "ART"
	}
	if (is_avazu) {
		tracking_provider += "Avazu"
	}
	if (is_beintoo) {
		tracking_provider += "Beintoo"
	}
	if (is_bluetrack) {
		tracking_provider += "Bluetrack"
	}
	if (is_claymotion) {
		tracking_provider += "Claymotion"
	}
	if (is_clearmob) {
		tracking_provider += "Clearmob"
	}
	if (is_click_rocket) {
		tracking_provider += "Click Rocket"
	}
	if (is_clickky) {
		tracking_provider += "Clickky"
	}
	if (is_adforce) {
		tracking_provider += "Cyber-Z/FOX/AdForce"
	}
	if (is_dauup) {
		tracking_provider += "DauUp"
	}
	if (is_dedicated_media) {
		tracking_provider += "Dedicated Media"
	}
	if (is_deepforestmedia) {
		tracking_provider += "Deep Forest Media"
	}
	if (is_domob) {
		tracking_provider += "Domob"
	}
	if (is_dsnr) {
		tracking_provider += "DSNR Media Group (DMG)"
	}
	if (is_eclipse) {
		tracking_provider += "Eclipse.io"
	}
	if (is_entermate) {
		tracking_provider += "Entermate"
	}
	if (is_fl_mobile) {
		tracking_provider += "FL Mobile"
	}
	if (is_dotgames) {
		tracking_provider += ".Games"
	}
	if (is_glispa) {
		tracking_provider += "Glispa Media"
	}
	if (is_growmobile) {
		tracking_provider += "GrowMobile"
	}
	if (is_happy_elements) {
		tracking_provider += "Happy Elements"
	}
	if (is_hubtrack) {
		tracking_provider += "Hubtrack"
	}
	if (is_hunt) {
		tracking_provider += "Hunt Mobile"
	}
	if (is_icon_peak) {
		tracking_provider += "Icon Peak"
	}
	if (is_ironsource) {
		tracking_provider += "IronSource"
	}
	if (is_jampp) {
		tracking_provider += "Jampp"
	}
	if (is_komli) {
		tracking_provider += "Komli"
	}
	if (is_lovoo) {
		tracking_provider += "Lovoo"
	}
	if (is_mail_ru) {
		tracking_provider += "Mail.ru"
	}
	if (is_mdotm) {
		tracking_provider += "MdotM"
	}
	if (is_metaps) {
		tracking_provider += "Metaps"
	}
	if (is_mlt) {
		tracking_provider += "MLT Media"
	}
	if (is_massiveimpact) {
		tracking_provider += "Massiveimpact"
	}
	if (is_mobils) {
		tracking_provider += "Mobils"
	}
	if (is_mobithink) {
		tracking_provider += "Mobithink"
	}
	if (is_moblin) {
		tracking_provider += "Moblin"
	}
	if (is_mobvista) {
		tracking_provider += "MobVista"
	}
	if (is_moko) {
		tracking_provider += "MOKO Performance Network"
	}
	if (is_motive) {
		tracking_provider += "Motive"
	}
	if (is_mpire) {
		tracking_provider += "mPire Network"
	}
	if (is_nswitch) {
		tracking_provider += "Nswitch"
	}
	if (is_opera) {
		tracking_provider += "Opera"
	}
	if (is_phunware) {
		tracking_provider += "Phunware"
	}
	if (is_pixonic) {
		tracking_provider += "Pixonic"
	}
	if (is_pocketwhale) {
		tracking_provider += "Pocketwhale"
	}
	if (is_portable_marketing) {
		tracking_provider += "Portable MArketing Co."
	}
	if (is_raftika) {
		tracking_provider += "Raftika"
	}
	if (is_simpysam) {
		tracking_provider += "Simpysam"
	}
	if (is_smaad) {
		tracking_provider += "Smaad SDK"
	}
	if (is_smart_c) {
		tracking_provider += "Smart-C"
	}
	if (is_snail) {
		tracking_provider += "Snail Games"
	}
	if (is_snakk) {
		tracking_provider += "Snakk Media"
	}
	if (is_social_point) {
		tracking_provider += "Social Point"
	}
	if (is_stratoshear) {
		tracking_provider += "StratosHear"
	}
	if (is_sublinet) {
		tracking_provider += "Sublinet"
	}
	if (is_tapsense) {
		tracking_provider += "TapSense"
	}
	if (is_talking_data) {
		tracking_provider += "Talking Data"
	}
	if (is_tukmob2) {
		tracking_provider += "Tukmob2"
	}
	if (is_trackingbird) {
		tracking_provider += "TrackingBird"
	}
	if (is_trademob) {
		tracking_provider += "Trademob"
	}
	if (is_urad) {
		tracking_provider += "UrAd.com"
	}
	if (is_webmedia) {
		tracking_provider += "WebMedia"
	}
	if (is_yandex) {
		tracking_provider += "Yandex"
	}
	if (is_ydigital) {
		tracking_provider += "YDigital"
	}
	if (is_youmi) {
		tracking_provider += "Youmi"
	}
	if (is_youappi) {
		tracking_provider += "Youappi"
	}
	if (is_zennapps) {
		tracking_provider += "Zennapps"
	}
	if (is_zen_studios) {
		tracking_provider += "Zen Studios"
	}
	if (is_valuepotion) {
		tracking_provider += "Valuepotion"
	}
	if (is_s4m) {
		tracking_provider += "S4M"
	}
	if (is_seads) {
		tracking_provider += "Seads"
	}
	if (is_affle) {
		tracking_provider += "Affle"
	}
	if (is_admaster) {
		tracking_provider += "AdMaster"
	}
	if (is_eqsview) {
		tracking_provider += "EQSView"
	}
	if (is_accesstrade) {
		tracking_provider += "AccessTrade"
	}
	if (is_42trck) {
		tracking_provider += "42trck (Jampp)"
	}
	
	tracking_provider_output += tracking_provider;
	tracking_provider_output += "</span>.<br /><br />";

	if (tracking_provider === "") {
		tracking_provider_output = "Tracking provider not found. Please contact the TAM team!<br /><br />"
	}

	$(output_div).html(tracking_provider_output);
	console.log(tracking_provider);
}
// END of tracking identifier

redirectURL = function(string, outputdiv) {
	// Tune
	var sub_str1 = "&response_format=json";
	// Adjust
	var sub_str2 = "s2s=1&";
	// AppsFlyer
	var sub_str3 = "&redirect=false";
	// Kochava
	var sub_str4 = "&pbr=1";
	// King.com
	var sub_str5 = "&noRedirect=TRUE";

	var redURL = string.replace(sub_str1, "");
	redURL = redURL.replace(sub_str2, "");
	redURL = redURL.replace(sub_str3, "");
	redURL = redURL.replace(sub_str4, "");
	redURL = redURL.replace(sub_str5, "");
	$(outputdiv).html("<a class='blue' href='" + redURL + "' target='_blank'>Click here to be redirected to the app!</a><br /><br />");
}
checkURLgen = function(string) {
	var url = document.getElementById(string).value;
	redirectURL(url, redurldiv);

	// remove spaces
	url_nospace = url.replace(/\s+/g, '');

	if (url !== url_nospace) {
		url = url_nospace;
		$("#vnotes_comment").append("Spaces have been removed!<br /><br />");
	}

	// validations

	var final_alert = "";

	var url_good = validate_url(url);
	if (!url_good) {
		final_alert += "It looks like the URL is not correct.<br /><br />";
	}
	var qs_good = validate_questionmarks(url);
	if (!qs_good) {
		final_alert += "The URL contains more than one questionmarks!<br /><br />"
	}

	check_track(string, vnotes_comment);

	var macro_check_text = validate_macros(url);
	var macros_good = (macro_check_text === "");
	final_alert += macro_check_text + "<br />";

	$("#vnotes_alert").html(final_alert);

	// END of validation
}